<?php if (!defined('BASEPATH')) exit('No permitir el acceso directo al script'); 

class Usuariolib {

	function valida_fecha($post_array)
	{
		$fecha = $post_array['Fecha_Visado'];
		$fecha_actual = strftime("%Y-%m-%d-%H-%M-%S", time());
		if ($fecha >= $fecha_actual)
		  {
			return TRUE;
		  }
		  else
		  {
			//$this->form_validation->set_message('Fecha_Visado', "La fecha de debe ser posterior a la fecha actual.");
			$crud->set_lang_string('form_validation_error_message_not_set','La fecha de debe ser posterior a la fecha actual.');
			return FALSE;
		  }
	}
	
	
	
//Funciones para trabajar con password
	
	function hmac($key, $data, $hash = 'md5', $blocksize = 64) {

		if (strlen($key)>$blocksize) {
			$key = pack('H*', $hash($key));
		}
		$key  = str_pad($key, $blocksize, chr(0));
		$ipad = str_repeat(chr(0x36), $blocksize);
		$opad = str_repeat(chr(0x5c), $blocksize);
		return $hash(($key^$opad) . pack('H*', $hash(($key^$ipad) . $data)));
	}

	function codificar_pw($password) {
		mt_srand(microtime()*1000000);
		$semilla = substr('00' . dechex(mt_rand()), -3) .
		substr('00' . dechex(mt_rand()), -3) .
		substr('0' . dechex(mt_rand()), -2);
		return $this->hmac($semilla, $password, 'md5', 64) . $semilla;
	}



	function verificar_pw($password, $stored_value) {
		$semilla = substr($stored_value, 32, 8);
		if($this->hmac($semilla, $password, 'md5', 64) . $semilla==$stored_value) return true;
		else return false;
//        return hmac($semilla, $password, 'md5', 64) . $semilla==$stored_value;
	}



/*

	function microtime_float(){
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	
	//Funcion para convertir fecha
	function convertir_fecha($fecha){
		if($fecha!=null)
		{
			$fecha = str_replace('/', '-', $fecha);
			$time = strtotime($fecha);
			$fecha_nueva = date("Y-m-d",$time);
			return $fecha_nueva;
		}
		else{
			return null;
		}
	}
		*/	

}
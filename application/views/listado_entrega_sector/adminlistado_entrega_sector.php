<script>
	
	$( document ).ready(function() {
		
	 /*Al hacer click en el boton cancelar me redirecciona a la pagina principal*/
	$( "#btn_Cancelar" ).click(function() {
  		//window.location.href = "<?=base_url('index.php/pdf/generar_pdf')?>";
  		window.location = "<?=base_url('index.php/principal/inicio')?>";
  		//$(location).attr('href', 'pdf/generar_pdf');
		});
	
	
	/*Al hacer click en Generar PDF*/
	//$( "#Generar_PDF" ).click(function() {
		$( "#btn_Generar_PDF" ).on('click', function() {
  				
		/*Muestra los valores de las variables*/
		var NumOT = $(".search_s6c98dd8b").val();
		var Sector = $(".search_s65b71b08").val();
		var FechaEntregaSector = $(".search_FechaEntregaSector").val();
		var Vers_Protocolo = $(".search_Vers_Protocolo").val();
		var Observacion_Entrega = $(".search_Observacion_Entrega").val();
		
				
		var ajax_data = {
						  "NumOT"      			: $(".search_s6c98dd8b").val(),
						  "Sector"				: $(".search_s65b71b08").val(),
						  "FechaEntregaSector"	: $(".search_FechaEntregaSector").val(),
						  "Vers_Protocolo"   	: $(".search_Vers_Protocolo").val(),
						  "Observacion_Entrega" : $(".search_Observacion_Entrega").val()
						  //"Parcial"   		: $("#field-Parcial").val()
						 
						};
				
					$.ajax({
				         method: "POST",
				         url: base_url + "index.php/listado_entrega_sector/Buscar_info_entrega_sector", 
				          data: {ajax_data: ajax_data},
				         //data: {IdProdElab: IdProdElab},
				         dataType: "json",
				         success: function( json ) {
				         		/*Utilizo el for para mostrar los datos por la consola como para poner en blanco los valores que tienen el valor null y que no lo muestre en el listado*/
					            				            
					            //console.log(json);
					            for(var i=0;i<json.length;i++)
					            {
					            	if(json[i].NumOT == null)
						            		json[i].NumOT = ''; 
									console.log(json[i].NumOT);
									 
					            	if(json[i].DescSector == null)
						                    json[i].DescSector = ''; 
									console.log(json[i].DescSector);
											    
					            	if(json[i].FechaEntregaSector == null)
						                	json[i].FechaEntregaSector = ''; 
									console.log(json[i].FechaEntregaSector);
											   
					                if(json[i].Vers_Protocolo == null)
						                 	json[i].Vers_Protocolo = ''; 
									console.log(json[i].Vers_Protocolo);
											  
						            if(json[i].Observacion_Entrega == null) 
						                 	json[i].Observacion_Entrega = '';
						            console.log(json[i].Observacion_Entrega);
						            
					            }
								
								var html;
								
								for(var i=0;i<json.length;i++)
								{
								 html += "<tr> ";
								 html += "<td> " + (i+1) + "</td>";
						         html += "<td> " + json[i].NumOT + "</td>";
						         html += "<td> " + json[i].DescSector + "</td>";
						         
						         /*Convertir fecha*/
					              /*Ordeno la fecha dia-mes-anio hora:minuto:segundo */
			                      var anio=String(json[i].FechaEntregaSector).substring(0,4);
			                      var mes= String(json[i].FechaEntregaSector).substring(5,7);
			                      var dia= String(json[i].FechaEntregaSector).substring(8,10);
			                      var hora= String(json[i].FechaEntregaSector).substring(11,13);
			                      var minuto= String(json[i].FechaEntregaSector).substring(14,16);
			                      var segundo= String(json[i].FechaEntregaSector).substring(17,19);
					              var FechaEntregaSector_ord = dia +'-'+ mes +'-'+ anio + ' ' + hora + ':'+ minuto + ':' + segundo;
						         html += "<td> " + FechaEntregaSector_ord + "</td>";
						         
						         html += "<td> " + json[i].Vers_Protocolo + "</td>";
						         html += "<td> " + json[i].Observacion_Entrega + "</td>";
						         
		                    	 html += "</tr>";
								 	
								}

								var pagina = base_url + "index.php/generar_pdf/pdf_datos_entrega/generar_pdf_datos_entrega"
							         	
							    window.open(pagina + "?texto_html=" + html,'_blank');
								
				         		   
									}
																
							});
				
		
		});
	
	
	
	});

</script>




<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />

<?php
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
font-family: Arial;
font-size: 14px;
}
a {
color: blue;
text-decoration: none;
font-size: 14px;
}
a:hover
{
text-decoration: underline;
}
</style>
</head>
<body>
<!-- Beginning header -->
    <!--<center>
    <div>
        <a href='#'>Productos</a> | 
        <a href='#'>Maquinas</a> |
        <a href='#'>Punz&oacute;n</a> |
        
 
    </div>
    </center>-->
<!-- End of header-->

<h3>Listado de Entregas a Sectores</h3>

<div class="container-fluid">
  <div class="col-xs-10"></div>
  <div class="col-xs-2">
  
  <button id="btn_Generar_PDF" class="datatables-add-button">Generar PDF</button>
  <button id="btn_Cancelar" class="datatables-add-button">Cancelar</button>
  
  </div>
</div>

<div>
<?php echo $output; ?>
</div>

<!--<div>
<br/>
<button type="submit" id="Cargar_OT">&nbsp;&nbsp;Cargar OT</button>
</div>-->
</body>
</html>
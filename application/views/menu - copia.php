<!DOCTYPE html>
<html lang="en">
  <head>
       
     <meta charset="utf-8">
        <title>Sistema de Producci&oacute;n</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?= base_url('css/bootstrap.css'); ?>" media="screen">
        <link rel="stylesheet" href="<?= base_url('css/bootswatch.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/bootstrap-toggle.min.css'); ?>">

         <link rel="stylesheet" href="<?= base_url('css/datepicker/bootstrap-datepicker3.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/datepicker/bootstrap-datepicker3.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/datepicker/bootstrap-datepicker3.standalone.css'); ?>">
        
        <link rel="stylesheet" href="<?= base_url('css/sticky-footer.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/signin.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/bootstrap-multiselect.css'); ?>">
        
        
        <link rel="stylesheet" href="<?= base_url('css/select2.min.css'); ?>">
        
        <!--<link rel="stylesheet" href="<?= base_url('font-awesome/css/font-awesome.min.css'); ?>">-->
                
        <link rel="stylesheet" href="<?= base_url('css/general.css'); ?>">
         
         <!--<link rel="stylesheet" href="<?= base_url('jQuery-autoComplete-master/jquery.auto-complete.css'); ?>"> -->                  
        
  		<!--<link rel="shortcut icon" href="<?= base_url('img/favicon.ico'); ?>">-->
			<link rel="shortcut icon" href="<?= base_url('img/Captura(Logo Raffo).png'); ?>">
      
        <script src="<?= base_url(); ?>js/jquery-1.10.2.min.js"></script>
        <script src="<?= base_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>js/bootswatch.js"></script>
        <script src="<?= base_url(); ?>js/bootstrap-toggle.min.js"></script>
        <script src="<?= base_url(); ?>js/datepicker/bootstrap-datepicker.js"></script>
        <script src="<?= base_url(); ?>js/datepicker/bootstrap-datepicker.es.min.js"></script>
        <script src="<?= base_url(); ?>js/bootstrap-multiselect.js"></script>
        <script src="<?= base_url(); ?>js/general.js"></script>
        
        <script src="<?= base_url(); ?>js/select2.min.js"></script>
                
        <!--<script src="<?= base_url(); ?>jQuery-autoComplete-master/jquery.auto-complete.min.js"></script>-->
               
        <script>
            $(document).ready(function() {
            	$("form").keypress(function(e) {
                    if (e.which == 13) {
                        return false;
                    }
                });
                
            });
            
            /******** Inicio del Script para hacer el men� desplegable al pasar el mouse********/           
           $(function(){
			    $(".dropdown").hover(            
			            function() {
			                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
			                $(this).toggleClass('open');         
			            },
			            function() {
			                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
			                $(this).toggleClass('open');          
			            });
			});
           /********Fin del Script para hacer el men� desplegable al pasar el mouse********/
						
			                                  
       </script>
  
  </head>
  <body>
   
   <div class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img id="logoraffo" src="<?=base_url('img/Captura(Logo Raffo).png')?>" width='90' class="pull-left" />
            <a class="navbar-brand" href="<?= base_url('index.php/principal/inicio'); ?>"><p style="font-size: 30px">Sistema de Producci&oacute;n</p></a> 
        </div>
        <div class="acordeon">
        <div class="navbar-collapse collapse navbar-responsive-collapse">
        
        <!--------------------------------------------------------------------->
        <!--Hago el switch en php para mostrar el menu dependiendo del nivel-->
        <!--------------------------------------------------------------------->
        <?
        
        switch($this->session->userdata('Nivel')) 
        {//inicio del switch
         case 0: //Inicio del nivel 0
         		{?>
				
				 <ul class="nav navbar-nav">
                <!--<li><a href="<?= base_url('usuario/login/principal'); ?>">INICIO</a></li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;ABM<b class="caret"></b></a>
                    
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/producto/adminproducto'); ?>"><span class=""></span>&nbsp;&nbsp;Producto</a></li>
                        <li><a href="<?= base_url('index.php/maquina/adminmaquina'); ?>"><span class=""></span>&nbsp;&nbsp;Maquina</a></li>
                        <li><a href="<?= base_url('index.php/punzon/adminpunzon'); ?>"><span class=""></span>&nbsp;&nbsp;Punz&oacute;n</a></li>
                        <li><a href="<?= base_url('index.php/subetapa/adminsubetapa'); ?>"><span class=""></span>&nbsp;&nbsp;SubEtapa</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/usuario/adminusuario'); ?>"><span class=""></span>&nbsp;&nbsp;Usuario</a></li>
                        <!--<li><a href="<?= base_url('index.php/cambiar_password/admincambiar_password/add'); ?>"><span class=""></span>&nbsp;&nbsp;Cambiar Password</a></li>-->
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/punzonxmaquina/adminpunzonxmaquina'); ?>"><span class=""></span>&nbsp;&nbsp;Punz&oacute;n por Maquina</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/pun_arch_buscar/adminpun_arch_buscar/add'); ?>"><span class=""></span>&nbsp;&nbsp;Archivos por Punz&oacute;n</a></li>
                        <li role="separator" class="divider"></li>
                                                                  
                   </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Orden<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/cargar_editarot/admincargar_editarot'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar/Editar OT</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/cargardatosot/admincargardatosot'); ?>"><span class=""></span>&nbsp;&nbsp;Carga Datos OT</a></li>
                                               
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/mostrar_pdf_datosot/admin_mostrar_pdf_datosot'); ?>"><span class=""></span>&nbsp;&nbsp;Listado de OT y Generar PDF</a></li>
                        <li><a href="<?= base_url('index.php/datosot_pdf/admin_datosot_pdf/add'); ?>"><span class=""></span>&nbsp;&nbsp;Consultar OT en PDF</a></li>                        
                        <li role="separator" class="divider"></li>

                    </ul>
                </li>
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Entrega<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    	<li><a href="<?= base_url('index.php/entrega_sector/adminentrega_sector'); ?>"><span class=""></span>&nbsp;&nbsp;Carga Entrega</a></li>
                    	<li><a href="<?= base_url('index.php/cargar_fechas/admincargar_fechas'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar/Cambiar Fechas</a></li>
                    	 <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/listado_entrega_sector/adminlistado_entrega_sector'); ?>"><span class=""></span>&nbsp;&nbsp;Consultar Entregas en PDF</a></li>
                        <!--<li><a href="<?= base_url('index.php/mensaje/comentario'); ?>"><span class=""></span>&nbsp;&nbsp;Listado de Entregas en PDF</a></li>-->
                        <li role="separator" class="divider"></li>                        
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Veh&iacute;culo</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Conciliaci&oacute;n<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/conciliacion_buscar/adminconciliacion_buscar/add'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar Conciliaci&oacute;n</a></li> 
                        <!--<li><a href="<?= base_url('index.php/conciliacion/adminconciliacion'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar Conc. 2</a></li>-->
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Veh&iacute;culo</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;Otros Informes<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    <li><a href="<?=base_url('index.php/consulta_totales/adminconsulta_totales/add')?>"><span class=""></span>&nbsp;&nbsp;Consulta Totales</a></li>
                        <li><a href="<?=base_url('index.php/listar_productos_ws/adminlistar_productos_ws')?>"><span class=""></span>&nbsp;&nbsp;Listado ProductosWS</a></li> 
                        <li role="separator" class="divider"></li>
                        <li><a href="<?=base_url('index.php/listar_log/visualiza_log')?>"><span class=""></span>&nbsp;&nbsp;Consultar Log</a></li>
                        <!--<li><a href="<?=base_url('index.php/mensaje/comentario')?>"><span class=""></span>&nbsp;&nbsp;Listado de Log en PDF</a></li>--> 
                        <li role="separator" class="divider"></li> 
                        <!--<li><a href="<?=base_url('index.php/pdf/generar_pdf')?>" target="_blank"><span class=""></span>&nbsp;&nbsp;PDF</a></li>--> 
                        
                    </ul>
                </li>
            </ul>

				
				<?break;
				}//Fin del nivel 0

	    case 1:
		    	{//Inicio del nivel 1
		    	?>
				 <ul class="nav navbar-nav">
                <!--<li><a href="<?= base_url('usuario/login/principal'); ?>">INICIO</a></li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;ABM<b class="caret"></b></a>
                    
                    <ul class="dropdown-menu">
                        <!--<li><a href="<?= base_url('index.php/producto/adminproducto'); ?>"><span class=""></span>&nbsp;&nbsp;Producto</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/maquina/adminmaquina'); ?>"><span class=""></span>&nbsp;&nbsp;Maquina</a></li>-->
                        <li><a href="<?= base_url('index.php/punzon/adminpunzon'); ?>"><span class=""></span>&nbsp;&nbsp;Punz&oacute;n</a></li>
                        <li><a href="<?= base_url('index.php/subetapa/adminsubetapa'); ?>"><span class=""></span>&nbsp;&nbsp;SubEtapa</a></li>
                        <li role="separator" class="divider"></li>
                        <!--<li><a href="<?= base_url('index.php/usuario/adminusuario'); ?>"><span class=""></span>&nbsp;&nbsp;Usuario</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/cambiar_password/admincambiar_password/add'); ?>"><span class=""></span>&nbsp;&nbsp;Cambiar Password</a></li>-->
                        <!--<li role="separator" class="divider"></li>-->
                        <li><a href="<?= base_url('index.php/punzonxmaquina/adminpunzonxmaquina'); ?>"><span class=""></span>&nbsp;&nbsp;Punz&oacute;n por Maquina</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/pun_arch_buscar/adminpun_arch_buscar/add'); ?>"><span class=""></span>&nbsp;&nbsp;Archivos por Punz&oacute;n</a></li>
                        <li role="separator" class="divider"></li>
                                                                  
                   </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Orden<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/cargar_editarot/admincargar_editarot'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar/Editar OT</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/cargardatosot/admincargardatosot'); ?>"><span class=""></span>&nbsp;&nbsp;Carga Datos OT</a></li>
                                               
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/mostrar_pdf_datosot/admin_mostrar_pdf_datosot'); ?>"><span class=""></span>&nbsp;&nbsp;Listado de OT y Generar PDF</a></li>
                        <li><a href="<?= base_url('index.php/datosot_pdf/admin_datosot_pdf/add'); ?>"><span class=""></span>&nbsp;&nbsp;Consultar OT en PDF</a></li>                        
                        <li role="separator" class="divider"></li>

                    </ul>
                </li>
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Entrega<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    	<li><a href="<?= base_url('index.php/entrega_sector/adminentrega_sector'); ?>"><span class=""></span>&nbsp;&nbsp;Carga Entrega</a></li>
                    	<li><a href="<?= base_url('index.php/cargar_fechas/admincargar_fechas'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar/Cambiar Fechas</a></li>
                    	 <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/listado_entrega_sector/adminlistado_entrega_sector'); ?>"><span class=""></span>&nbsp;&nbsp;Consultar Entregas en PDF</a></li>
                        <!--<li><a href="<?= base_url('index.php/mensaje/comentario'); ?>"><span class=""></span>&nbsp;&nbsp;Listado de Entregas en PDF</a></li>-->
                        <li role="separator" class="divider"></li>                        
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Veh&iacute;culo</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Conciliaci&oacute;n<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/conciliacion_buscar/adminconciliacion_buscar/add'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar Conciliaci&oacute;n</a></li> 
                        <!--<li><a href="<?= base_url('index.php/conciliacion/adminconciliacion'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar Conc. 2</a></li>-->
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Veh&iacute;culo</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;Otros Informes<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    <li><a href="<?=base_url('index.php/consulta_totales/adminconsulta_totales/add')?>"><span class=""></span>&nbsp;&nbsp;Consulta Totales</a></li>
                        <li><a href="<?=base_url('index.php/listar_productos_ws/adminlistar_productos_ws')?>"><span class=""></span>&nbsp;&nbsp;Listado ProductosWS</a></li> 
                        <li role="separator" class="divider"></li>
                        <!--<li><a href="<?=base_url('index.php/listar_log/visualiza_log')?>"><span class=""></span>&nbsp;&nbsp;Consultar Log</a></li>-->
                        <!--<li><a href="<?=base_url('index.php/mensaje/comentario')?>"><span class=""></span>&nbsp;&nbsp;Listado de Log en PDF</a></li>--> 
                        <!--<li role="separator" class="divider"></li> -->
                        <!--<li><a href="<?=base_url('index.php/pdf/generar_pdf')?>" target="_blank"><span class=""></span>&nbsp;&nbsp;PDF</a></li>--> 
                        
                    </ul>
                </li>
            </ul>	
				
				
				
				 <?break;
				}//Fin del nivel 1	 

		case 2:
		    	{//Inicio nivel 2
				 ?>
				 
				 <ul class="nav navbar-nav">
                <!--<li><a href="<?= base_url('usuario/login/principal'); ?>">INICIO</a></li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;ABM<b class="caret"></b></a>
                    
                    <ul class="dropdown-menu">
                        <!--<li><a href="<?= base_url('index.php/producto/adminproducto'); ?>"><span class=""></span>&nbsp;&nbsp;Producto</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/maquina/adminmaquina'); ?>"><span class=""></span>&nbsp;&nbsp;Maquina</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/punzon/adminpunzon'); ?>"><span class=""></span>&nbsp;&nbsp;Punz&oacute;n</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/subetapa/adminsubetapa'); ?>"><span class=""></span>&nbsp;&nbsp;SubEtapa</a></li>-->
                        <!--<li role="separator" class="divider"></li>-->
                        <!--<li><a href="<?= base_url('index.php/usuario/adminusuario'); ?>"><span class=""></span>&nbsp;&nbsp;Usuario</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/cambiar_password/admincambiar_password/add'); ?>"><span class=""></span>&nbsp;&nbsp;Cambiar Password</a></li>-->
                        <!--<li role="separator" class="divider"></li>-->
                        <li><a href="<?= base_url('index.php/punzonxmaquina/adminpunzonxmaquina'); ?>"><span class=""></span>&nbsp;&nbsp;Punz&oacute;n por Maquina</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/pun_arch_buscar/adminpun_arch_buscar/add'); ?>"><span class=""></span>&nbsp;&nbsp;Archivos por Punz&oacute;n</a></li>
                        <li role="separator" class="divider"></li>
                                                                  
                   </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Orden<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/cargar_editarot/admincargar_editarot'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar/Editar OT</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/cargardatosot/admincargardatosot'); ?>"><span class=""></span>&nbsp;&nbsp;Carga Datos OT</a></li>
                                               
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/mostrar_pdf_datosot/admin_mostrar_pdf_datosot'); ?>"><span class=""></span>&nbsp;&nbsp;Listado de OT y Generar PDF</a></li>
                        <li><a href="<?= base_url('index.php/datosot_pdf/admin_datosot_pdf/add'); ?>"><span class=""></span>&nbsp;&nbsp;Consultar OT en PDF</a></li>                        
                        <li role="separator" class="divider"></li>

                    </ul>
                </li>
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Entrega<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    	<li><a href="<?= base_url('index.php/entrega_sector/adminentrega_sector'); ?>"><span class=""></span>&nbsp;&nbsp;Carga Entrega</a></li>
                    	<li><a href="<?= base_url('index.php/cargar_fechas/admincargar_fechas'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar/Cambiar Fechas</a></li>
                    	 <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/listado_entrega_sector/adminlistado_entrega_sector'); ?>"><span class=""></span>&nbsp;&nbsp;Consultar Entregas en PDF</a></li>
                        <!--<li><a href="<?= base_url('index.php/mensaje/comentario'); ?>"><span class=""></span>&nbsp;&nbsp;Listado de Entregas en PDF</a></li>-->
                        <li role="separator" class="divider"></li>                        
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Veh&iacute;culo</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Conciliaci&oacute;n<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/conciliacion_buscar/adminconciliacion_buscar/add'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar Conciliaci&oacute;n</a></li> 
                        <!--<li><a href="<?= base_url('index.php/conciliacion/adminconciliacion'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar Conc. 2</a></li>-->
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Veh&iacute;culo</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;Otros Informes<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    <li><a href="<?=base_url('index.php/consulta_totales/adminconsulta_totales/add')?>"><span class=""></span>&nbsp;&nbsp;Consulta Totales</a></li>
                        <li><a href="<?=base_url('index.php/listar_productos_ws/adminlistar_productos_ws')?>"><span class=""></span>&nbsp;&nbsp;Listado ProductosWS</a></li> 
                        <li role="separator" class="divider"></li>
                        <!--<li><a href="<?=base_url('index.php/listar_log/visualiza_log')?>"><span class=""></span>&nbsp;&nbsp;Consultar Log</a></li>-->
                        <!--<li><a href="<?=base_url('index.php/mensaje/comentario')?>"><span class=""></span>&nbsp;&nbsp;Listado de Log en PDF</a></li>--> 
                        <!--<li role="separator" class="divider"></li> -->
                        <!--<li><a href="<?=base_url('index.php/pdf/generar_pdf')?>" target="_blank"><span class=""></span>&nbsp;&nbsp;PDF</a></li>--> 
                        
                    </ul>
                </li>
            </ul>
				 
				 
				 
				 
				 <?break;	
				}//Fin nivel 2
					  	
	    case 3:
		    	{//Inicion nivel 3
				?>	
				
				<ul class="nav navbar-nav">
                <!--<li><a href="<?= base_url('usuario/login/principal'); ?>">INICIO</a></li>-->
                <li class="dropdown">
                    <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;ABM<b class="caret"></b></a>
                    
                    <ul class="dropdown-menu">
                        <!--<li><a href="<?= base_url('index.php/producto/adminproducto'); ?>"><span class=""></span>&nbsp;&nbsp;Producto</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/maquina/adminmaquina'); ?>"><span class=""></span>&nbsp;&nbsp;Maquina</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/punzon/adminpunzon'); ?>"><span class=""></span>&nbsp;&nbsp;Punz&oacute;n</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/subetapa/adminsubetapa'); ?>"><span class=""></span>&nbsp;&nbsp;SubEtapa</a></li>-->
                        <!--<li role="separator" class="divider"></li>-->
                        <!--<li><a href="<?= base_url('index.php/usuario/adminusuario'); ?>"><span class=""></span>&nbsp;&nbsp;Usuario</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/cambiar_password/admincambiar_password/add'); ?>"><span class=""></span>&nbsp;&nbsp;Cambiar Password</a></li>-->
                        <!--<li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/punzonxmaquina/adminpunzonxmaquina'); ?>"><span class=""></span>&nbsp;&nbsp;Punz&oacute;n por Maquina</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/pun_arch_buscar/adminpun_arch_buscar/add'); ?>"><span class=""></span>&nbsp;&nbsp;Archivos por Punz&oacute;n</a></li>
                        <li role="separator" class="divider"></li>
                                                                  
                   </ul>-->
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Orden<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <!--<li><a href="<?= base_url('index.php/cargar_editarot/admincargar_editarot'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar/Editar OT</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('index.php/cargardatosot/admincargardatosot'); ?>"><span class=""></span>&nbsp;&nbsp;Carga Datos OT</a></li>
                                               
                        <li role="separator" class="divider"></li>-->
                        <li><a href="<?= base_url('index.php/mostrar_pdf_datosot/admin_mostrar_pdf_datosot'); ?>"><span class=""></span>&nbsp;&nbsp;Listado de OT y Generar PDF</a></li>
                        <li><a href="<?= base_url('index.php/datosot_pdf/admin_datosot_pdf/add'); ?>"><span class=""></span>&nbsp;&nbsp;Consultar OT en PDF</a></li>                        
                        <li role="separator" class="divider"></li>

                    </ul>
                </li>
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Entrega<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    	<!--<li><a href="<?= base_url('index.php/entrega_sector/adminentrega_sector'); ?>"><span class=""></span>&nbsp;&nbsp;Carga Entrega</a></li>
                    	<li><a href="<?= base_url('index.php/cargar_fechas/admincargar_fechas'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar/Cambiar Fechas</a></li>
                    	 <li role="separator" class="divider"></li>-->
                        <li><a href="<?= base_url('index.php/listado_entrega_sector/adminlistado_entrega_sector'); ?>"><span class=""></span>&nbsp;&nbsp;Consultar Entregas en PDF</a></li>
                        <!--<li><a href="<?= base_url('index.php/mensaje/comentario'); ?>"><span class=""></span>&nbsp;&nbsp;Listado de Entregas en PDF</a></li>-->
                        <!--<li role="separator" class="divider"></li>-->                        
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Veh&iacute;culo</a></li>-->
                    </ul>
                </li>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Conciliaci&oacute;n<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/conciliacion_buscar/adminconciliacion_buscar/add'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar Conciliaci&oacute;n</a></li>--> 
                        <!--<li><a href="<?= base_url('index.php/conciliacion/adminconciliacion'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar Conc. 2</a></li>-->
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Veh&iacute;culo</a></li>
                    </ul>
                </li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;Otros Informes<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    <li><a href="<?=base_url('index.php/consulta_totales/adminconsulta_totales/add')?>"><span class=""></span>&nbsp;&nbsp;Consulta Totales</a></li>
                        <li><a href="<?=base_url('index.php/listar_productos_ws/adminlistar_productos_ws')?>"><span class=""></span>&nbsp;&nbsp;Listado ProductosWS</a></li> 
                        <li role="separator" class="divider"></li>
                        <!--<li><a href="<?=base_url('index.php/listar_log/visualiza_log')?>"><span class=""></span>&nbsp;&nbsp;Consultar Log</a></li>-->
                        <!--<li><a href="<?=base_url('index.php/mensaje/comentario')?>"><span class=""></span>&nbsp;&nbsp;Listado de Log en PDF</a></li>--> 
                        <!--<li role="separator" class="divider"></li> -->
                        <!--<li><a href="<?=base_url('index.php/pdf/generar_pdf')?>" target="_blank"><span class=""></span>&nbsp;&nbsp;PDF</a></li>--> 
                        
                    </ul>
                </li>
            </ul>
				
				
				
				 <?break;
				}//Fin nivel 3	    	 
					   	 
				
		 		  	 	
        }
        
        ?>
        
        
            
            <ul class="nav navbar-nav pull-right">
                <li><a href="<?= base_url('index.php/login/salir'); ?>"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Salir</a></li>
                <!--<li><a href="<?= base_url('usuario/login/salir'); ?>"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Salir</a></li>-->
            </ul>
            <ul class="nav navbar-nav pull-right">
                <li><a><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?=$this->session->userdata('Usuario');?></a></li>
            </ul>
            
        </div>
       </div> 
    </div>

    
  </body>
</html>
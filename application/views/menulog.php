<!DOCTYPE html>
<html lang="en">
  <head>
       
     <meta charset="utf-8">
        <title>Sistema de Producci&oacute;n</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?= base_url('css/bootstrap.css'); ?>" media="screen">
        <link rel="stylesheet" href="<?= base_url('css/bootswatch.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/bootstrap-toggle.min.css'); ?>">

         <link rel="stylesheet" href="<?= base_url('css/datepicker/bootstrap-datepicker3.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/datepicker/bootstrap-datepicker3.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/datepicker/bootstrap-datepicker3.standalone.css'); ?>">
        
        <link rel="stylesheet" href="<?= base_url('css/sticky-footer.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/signin.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/bootstrap-multiselect.css'); ?>">
        
        
        <link rel="stylesheet" href="<?= base_url('css/select2.min.css'); ?>">
        
        <!--<link rel="stylesheet" href="<?= base_url('font-awesome/css/font-awesome.min.css'); ?>">-->
                
        <link rel="stylesheet" href="<?= base_url('css/general.css'); ?>">
         
         <!--<link rel="stylesheet" href="<?= base_url('jQuery-autoComplete-master/jquery.auto-complete.css'); ?>"> -->                  
        
  		<!--<link rel="shortcut icon" href="<?= base_url('img/favicon.ico'); ?>">-->
			<link rel="shortcut icon" href="<?= base_url('img/Captura(Logo Raffo).png'); ?>">
      
        <script src="<?= base_url(); ?>js/jquery-1.10.2.min.js"></script>
        <script src="<?= base_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>js/bootswatch.js"></script>
        <script src="<?= base_url(); ?>js/bootstrap-toggle.min.js"></script>
        <script src="<?= base_url(); ?>js/datepicker/bootstrap-datepicker.js"></script>
        <script src="<?= base_url(); ?>js/datepicker/bootstrap-datepicker.es.min.js"></script>
        <script src="<?= base_url(); ?>js/bootstrap-multiselect.js"></script>
        <script src="<?= base_url(); ?>js/general.js"></script>
        
        <script src="<?= base_url(); ?>js/select2.min.js"></script>
                
        <!--<script src="<?= base_url(); ?>jQuery-autoComplete-master/jquery.auto-complete.min.js"></script>-->
               
        <script>
            $(document).ready(function() {
            	$("form").keypress(function(e) {
                    if (e.which == 13) {
                        return false;
                    }
                });
                
            });
            
            /******** Inicio del Script para hacer el menú desplegable al pasar el mouse********/           
           $(function(){
			    $(".dropdown").hover(            
			            function() {
			                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
			                $(this).toggleClass('open');         
			            },
			            function() {
			                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
			                $(this).toggleClass('open');          
			            });
			});
           /********Fin del Script para hacer el menú desplegable al pasar el mouse********/
						
			                                  
       </script>
  
  </head>
  <body>
   
   <div class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img id="logoraffo" src="<?=base_url('img/Captura(Logo Raffo).png')?>" width='90' class="pull-left" />
            <!--<a class="navbar-brand" href="<?= base_url('index.php/login/inicio'); ?>"><p style="font-size: 30px">Sistema de Producci&oacute;n</p></a>-->
            <a class="navbar-brand"><p style="font-size: 30px">Sistema de Producci&oacute;n</p></a> 
        </div>
        
    </div>
	
	
    
  </body>
</html>


<!DOCTYPE html>
<html lang="en">
  <head>
       
     <meta charset="utf-8">
        <title>Sistema de Producci&oacute;n</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?= base_url('css/bootstrap.css'); ?>" media="screen">
        <link rel="stylesheet" href="<?= base_url('css/bootswatch.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/bootstrap-toggle.min.css'); ?>">

         <link rel="stylesheet" href="<?= base_url('css/datepicker/bootstrap-datepicker3.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/datepicker/bootstrap-datepicker3.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/datepicker/bootstrap-datepicker3.standalone.css'); ?>">
        
        <link rel="stylesheet" href="<?= base_url('css/sticky-footer.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/signin.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('css/bootstrap-multiselect.css'); ?>">
        
        <link rel="stylesheet" href="<?= base_url('css/general.css'); ?>">
         
         <!--<link rel="stylesheet" href="<?= base_url('jQuery-autoComplete-master/jquery.auto-complete.css'); ?>"> -->                  
        
  		<!--<link rel="shortcut icon" href="<?= base_url('img/favicon.ico'); ?>">-->
			<link rel="shortcut icon" href="<?= base_url('img/Captura(Logo Raffo).png'); ?>">
      
        <script src="<?= base_url(); ?>js/jquery-1.10.2.min.js"></script>
        <script src="<?= base_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>js/bootswatch.js"></script>
        <script src="<?= base_url(); ?>js/bootstrap-toggle.min.js"></script>
        <script src="<?= base_url(); ?>js/datepicker/bootstrap-datepicker.js"></script>
        <script src="<?= base_url(); ?>js/datepicker/bootstrap-datepicker.es.min.js"></script>
        <script src="<?= base_url(); ?>js/bootstrap-multiselect.js"></script>
        <script src="<?= base_url(); ?>js/general.js"></script>
        
        <!--<script src="<?= base_url(); ?>jQuery-autoComplete-master/jquery.auto-complete.min.js"></script>-->
               
        <script>
            $(document).ready(function() {
                $("form").keypress(function(e) {
                    if (e.which == 13) {
                        return false;
                    }
                });
            });
            
            /******** Inicio del Script para hacer el munu desplegable al pasar el mouse********/           
           $(function(){
			    $(".dropdown").hover(            
			            function() {
			                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
			                $(this).toggleClass('open');         
			            },
			            function() {
			                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
			                $(this).toggleClass('open');          
			            });
			});
           /********Fin del Script para hacer el munu desplegable al pasar el mouse********/
			                                  
       </script>
  
  </head>
  <body>
   
   <div class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img id="logoraffo" src="<?=base_url('img/Captura(Logo Raffo).png')?>" width='90' class="pull-left" />
            <!--<a class="navbar-brand" href="<?= base_url('usuario/login/principal'); ?>"><p style="font-size: 30px">Sistema de Producci&oacute;n</p></a> -->
            <a class="navbar-brand" href="<?= base_url('index.php/principal/principal'); ?>"><p style="font-size: 30px">Sistema de Producci&oacute;n</p></a> 
        </div>
        
        <div class="acordeon">
        <div class="navbar-collapse collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav">
                <!--<li><a href="<?= base_url('usuario/login/principal'); ?>">INICIO</a></li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;ABM<b class="caret"></b></a>
                    
                    <ul class="dropdown-menu">
                        <!--MENU ABM PRODCUTO-->
                        <li><a href="<?= base_url('index.php/personas/administracion'); ?>"><span class=""></span>&nbsp;&nbsp;Producto</a></li>
                        <li><a href="<?= base_url('index.php/maquina/datos_maquina/cargar_maquina'); ?>"><span class=""></span>&nbsp;&nbsp;Maquina</a></li>
                        <li><a href="<?= base_url('index.php/punzon/datos_punzon/cargar_punzon'); ?>"><span class=""></span>&nbsp;&nbsp;Punz&oacute;n</a></li>
                        <li><a href="<?= base_url('index.php/punzon/datos_punzon/cargar_punzon'); ?>"><span class=""></span>&nbsp;&nbsp;SubEtapa</a></li>
                        
                        <!--<li class="dropdown"><a href="<?= base_url('index.php/producto/datos_producto/cargar_producto'); ?>" class="dropdown-toggle" data-toggle="dropdown"><span></span>&nbsp;&nbsp;Producto</a>-->
	                        <!--<ul class="dropdown-menu">
	                        	<li><a href="<?= base_url('index.php/producto/datos_producto/cargar_producto'); ?>" ></span>&nbsp;&nbsp;Cargar Producto</a></li>
	                        	<li><a href="<?= base_url('index.php/producto/datos_producto/modificar_producto'); ?>" ></span>&nbsp;&nbsp;Modificar Producto</a></li>
	                        	<li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>" ></span>&nbsp;&nbsp;Eliminar Producto</a></li>
	                        </ul>-->
                       <!-- </li>-->
                        
                        <!--MENU ABM MAQUINA-->
                        <!--<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span></span>&nbsp;&nbsp;Maquina</a>
	                        <!--<ul class="dropdown-menu ">
	                        	<li><a href="<?= base_url('index.php/maquina/datos_maquina/cargar_maquina'); ?>" ></span>&nbsp;&nbsp;Cargar Maquina</a></li>
	                        	<li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>" ></span>&nbsp;&nbsp;Modificar Maquina</a></li>
	                        	<li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>" ></span>&nbsp;&nbsp;Eliminar Maquina</a></li>
	                        </ul>-->
                       <!-- </li>-->
                        
                        <!--MENU ABM PUNZ�N-->
                       <!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span></span>&nbsp;&nbsp;Punz&oacute;n</a>
	                        <!--<ul class="dropdown-menu ">
	                        	<li><a href="<?= base_url('index.php/punzon/datos_punzon/cargar_punzon'); ?>" ></span>&nbsp;&nbsp;Cargar Punz&oacute;n</a></li>
	                        	<li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>" ></span>&nbsp;&nbsp;Modificar Punz&oacute;n</a></li>
	                        	<li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>" ></span>&nbsp;&nbsp;Eliminar Punz&oacute;n</a></li>
	                        </ul>-->
                       <!-- </li>-->
                         
                         <!--MENU ABM SUBETAPA-->
                       <!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span></span>&nbsp;&nbsp;SubEtapa</a>-->
	                        <!--<ul class="dropdown-menu ">
	                        	<li><a href="<?= base_url('index.php/subetapa/datos_subetapa/cargar_subetapa'); ?>" ></span>&nbsp;&nbsp;Cargar SubEtapa</a></li>
	                        	<li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>" ></span>&nbsp;&nbsp;Modificar SubEtapa</a></li>
	                        	<li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>" ></span>&nbsp;&nbsp;Eliminar SubEtapa</a></li>
	                        </ul>-->
                        <!--</li>-->
                         
                        <!--<li><a href="<?= base_url('index.php/maquina/datos_maquina/cargar_maquina'); ?>"><span class=""></span>&nbsp;&nbsp;Maquina</a></li>-->
                        <!--<li><a href="<?= base_url('index.php/punzon/datos_punzon/cargar_punzon'); ?>"><span class=""></span>&nbsp;&nbsp;Punz�n</a></li>
                        <li><a href="<?= base_url('index.php/subetapa/datos_subetapa/cargar_subetapa'); ?>"><span class=""></span>&nbsp;&nbsp;SubEtapa</a></li>-->
                   
                   </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Orden<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar OT</a></li>
                        <li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>"><span class=""></span>&nbsp;&nbsp;Editar OT</a></li>
                        <li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar OT Parcial</a></li>
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Chofer</a></li>-->

                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp;&nbsp;Conciliaci&oacute;n<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('vehiculo/datos_vehiculo/cargarvehiculo'); ?>"><span class=""></span>&nbsp;&nbsp;Cargar Conc.</a></li>                        
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Eliminar Veh&iacute;culo</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;Consultas<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>"><span class=""></span>&nbsp;&nbsp;Consultas OT</a></li>
                        <li><a href="<?= base_url('index.php/mensajes/controlador_mensajes/en_construccion'); ?>"><span class=""></span>&nbsp;&nbsp;Entregas</a></li>
                        <!--<li><a href="# "><span class=""></span>&nbsp;&nbsp;Verificaci&oacute;n Vehicular</a></li>
                        <li><a href="# "><span class=""></span>&nbsp;&nbsp;Autorizaci&oacute;n Transportista</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;Otros Informes<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=base_url('index.php/mensajes/controlador_mensajes/en_construccion')?>"><span class=""></span>&nbsp;&nbsp;Lista</a></li>   
                        
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav pull-right">
                <li><a href="<?= base_url('usuario/login/salir'); ?>"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Salir</a></li>
            </ul>
            
        </div>
       </div> 
    </div>

    
  </body>
</html>
<?
/* Heredamos de la clase CI_Controller */
class Punzon_archivo extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
          
    /* Cargamos la base de datos */
    $this->load->database();
    
    $this->load->library('session');
    
    $this->load->model('Model_gestion_punzon_archivo');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  	
  	//De esta forma aumentamos el tama�o de los archivos que queremos hacer un upload
  	//Para que tome la modificacion hay que modificar el upload_max_filesize en el php.ini
  	//ini_set("upload_max_filesize","10M");
  	
  	
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('punzon_archivo/adminpunzon_archivo');
  }
 
  
  function adminpunzon_archivo()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if($this->session->userdata('Nivel') == 0)
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 	
 		
 	$state = $crud->getState();
 	
 	if($state == 'list')
		{
		$IdPunzon = $_GET['IdPunzon'];
		$crud->where('punzon_archivo.IdPun',$IdPunzon);
		
		//Almaceno en una variable de session el valor que envio para poder trabajarlo en todas las funciones
		$this->session->set_userdata('IdPunzon',$IdPunzon);
		
		}
 		
 	
 	
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('punzon_archivo');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Punz&oacute;n y Archivos');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    /*-------------C�digo para registrar el log cuando elimino el archivo-------------*/
    
    $state_info; 
    $primary_key;
    
   	/*if($state == 'edit')
		{
		 $state_info = $crud->getStateInfo();
		 $primary_key = $state_info->primary_key;
		 
		 $this->session->set_flashdata('IdArchPun',$primary_key);
		 //$this->session->set_userdata('IdArchPun',$primary_key);
		 
		 $result = $this->Model_gestion_punzon_archivo->busca_desc_arch($primary_key);
	 
	 	 $this->session->set_flashdata('Archivo',$result);
	 	 //$this->session->set_userdata('Archivo',$result);		 
		
		
		}*/
    
    /*if($state == 'delete_file')
    {
	 $this->Model_gestion_punzon_archivo->log_elim_arch($this->session->userdata('IdArchPun'), $this->session->userdata('Archivo'));
	 
	}*/
    
    /*-------------Fin del codigo para registrar el log cuando elimino el archivo-------------*/
    
    //$crud->callback_after_upload(array($this,'redirect_desp_upload'));
    
    
    
    /*if($state == 'upload_file')
    {
	 redirect('punzon_archivo/adminpunzon_archivo'); 
	 
	}*/
  
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
          
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    
    /*$crud->display_as('DescPunzon','Descripci&oacute;n de Punz&oacute;n');
    $crud->display_as('Fecha_Ingreso','Fecha de Ingreso');
    $crud->display_as('Fecha_Egreso','Fecha de Egreso');
    $crud->display_as('EstadoPunzon','Estado');   */ 
       
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'IdPun'
      
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    /*$crud->columns(
      'IdPunzon',
      'DescPunzon',
      'Fecha_Ingreso',
      'Fecha_Egreso',
      'EstadoPunzon'
    );
    $crud->where('EstadoPunzon', 'Habilitado');*/
    
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('IdPun','Archivo');
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    //$crud->edit_fields('IdPun','Archivo');
 
 
 	$crud->set_field_upload('Archivo','assets/uploads/files');
 	
 	//De esta forma definimos las extensiones de los archivos que vamos a permitir agregar
 	//$this->config->set_item('grocery_crud_file_upload_allow_file_types','gif|jpeg|jpg|png');
 	
 
 	$crud->set_relation('IdPun','punzon','DescPunzon');
 	
 	$crud->display_as('Fecha_Hora_Reg','Fecha y Hora de Carga');
 	
 	//De esta forma redirecciona cuando me muestra el mensaje que se inserto correctamente (ADD)
 	//Tambien le concateno la parte final de la url con el id que estaba enviando
 	//Le saque el mensaje del comienzo para que no me muestre nada
 	$crud->set_lang_string('insert_success_message',
		 '<script type="text/javascript">
		  window.location = "'.site_url(strtolower('punzon_archivo').'/'.strtolower('adminpunzon_archivo')).'?IdPunzon='.$this->session->userdata('IdPunzon').'";
		 </script>
		 '
   		);
 	
 	
 	/*//De esta forma redirecciona cuando me muestra el mensaje que se actualizo correctamente (EDIT)
 	//Tambien le concateno la parte final de la url con el id que estaba enviando
 	//Le saque el mensaje del comienzo para que no me muestre nada
 	$crud->set_lang_string('update_success_message',
		 'Los datos se Actualizaron correctamente.
		 <script type="text/javascript">
		  window.location = "'.site_url(strtolower('punzon_archivo').'/'.strtolower('adminpunzon_archivo')).'?IdPunzon='.$this->session->userdata('IdPunzon').'";
		 </script>
		 <div style="display:none">
		 '
   		);*/
 	
 	
 	//De esta forma no me muestra la opcion para eliminar el archivo
 	$crud->set_lang_string('form_upload_delete','');
 	
 	//Cambio los nombre de los botones para no confundir
 	//$crud->set_lang_string('form_save_and_go_back','Cancelar');
 	$crud->set_lang_string('form_save','Volver a la Lista');
 	$crud->set_lang_string('form_insert_loading','');
 	
 	
 	$crud->set_lang_string('update_error','El registro ya se encuentra cargado');
 	
 	
 	//Llamo a esta funcion para que no me inserte los valores, ya que los inserto cuando subo el archivo
 	$crud->callback_insert(array($this,'no_insertar'));
 	
 	
 	//Llama a la funcion para armar el campo del punz�n cuando hago un add
 	$crud->callback_add_field('IdPun',array($this,'armar_IdPun_add'));	
 	
 	//Llama a la funcion para armar el campo del punz�n cuando hago un edit
 	$crud->callback_edit_field('IdPun',array($this,'armar_IdPun_edit'));
 	
 	//Llama a la funcion para validar los campos antes de hacer un insertar
 	//$crud->callback_before_insert(array($this,'valida_campos_add'));
 	
 	//Llama a la funcion para validar los campos antes de hacer un edit
 	//$crud->callback_before_update(array($this,'valida_campos_edit'));
 	
 	//Cambiar el nombre del archivo para que no me inserte el codigo delante del nombre
 	//$crud->callback_before_upload(array($this,'cambia_nom_arch'));
 	
 	$crud->callback_after_upload(array($this,'cargar_upload'));

	//Delete
	$crud->callback_before_delete(array($this,'Vbles_antes_delete'));
 	$crud->callback_after_delete(array($this,'Vbles_despues_delete'));	
 		
 	//Funciones para registrar el log
 	$crud->callback_after_insert(array($this, 'log_desp_insertar'));
	$crud->callback_after_update(array($this, 'log_desp_editar'));
 	
 	
 	$crud->unset_edit();		//Oculto el boton editar
 	
 	$crud->unset_read();		//Oculto el boton ver
 	$crud->unset_export();		//Ocultar el boton exportar
 	$crud->unset_print();		//Ocultar el boton imprimir
 
 	//$crud->unset_back_to_list();	//Oculta el boton bolver a la lista y el boton cancelar
 	
 	//$crud->set_lang_string('form_save_and_go_back','Cancelar');
    //$crud->set_lang_string('form_save','Guardar y volver a la lista');
    
    //$crud->set_lang_string('form_update_and_go_back','Cancelar');
    //$crud->set_lang_string('form_update_changes','Actualizar y volver a la lista');
    //$crud->set_lang_string('form_button_clear','Prueba');
    
 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('punzon_archivo/adminpunzon_archivo', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }
 
 
 //Funcion que llamo para armar el campo IdPun y hacerlo solo lectura, adem�s que no me de inconvenientes al querer cargar los registros en la base de datos 
function armar_IdPun_add()
{ 
 //Trabajo con el valor que tengo en la variable de session y hago una consulta para obtener el nro de ot de la base de datos
 $value_pun = $this->session->userdata('IdPunzon');
 $desc_pun = $this->Model_gestion_punzon_archivo->obtiene_descpun($value_pun);
 return '<input type="text" value="'.$desc_pun.'" name="IdPun" id="IdPun" readonly>';	

}


//Funcion que llamo para armar el campo IdPun y hacerlo solo lectura, adem�s que no me de inconvenientes al querer modificar los registros en la base de datos 
function armar_IdPun_edit()
{ 
 //Trabajo con el valor que tengo en la variable de session y hago una consulta para obtener el nro de ot de la base de datos
 $value_pun = $this->session->userdata('IdPunzon');
 $desc_pun = $this->Model_gestion_punzon_archivo->obtiene_descpun($value_pun);
 return '<input type="text" value="'.$desc_pun.'" name="IdPun" id="IdPun" readonly>';	

}



//Funcion que valida los campos antes de insertar
function valida_campos_add($post_array)
{ 
 $IdPun = $this->session->userdata('IdPunzon');
 $DescArch = $post_array['Archivo'];
 
 /*C�digo para verificar si el nombre del archivo ya se encuentra registrado*/
 $i = 0;
 while($i<strlen($DescArch) &&($DescArch[$i] != '-') )
 {$i++; 
  		
 }
 $cadena = substr($DescArch, $i);
 
 $result = $this->Model_gestion_punzon_archivo->consulta_archivo_add($IdPun, $cadena);
 
 /*----------------------------------------------------------------------------*/
 
 if($result == TRUE)
 		 return FALSE;
 	else {
 		  $post_array['IdPun'] = $IdPun; 
		  $post_array['Archivo'] = $DescArch; 
		  return $post_array;
		 }	 

}

//Funcion que valida los campos antes de editar
function valida_campos_edit($post_array, $primary_key)
{ 
 $IdPun = $this->session->userdata('IdPunzon');
 $DescArch = $post_array['Archivo'];
 
 /*------C�digo para verificar si el nombre del archivo ya se encuentra registrado-----*/
 $i = 0;
 while($i<strlen($DescArch) &&($DescArch[$i] != '-') )
 {$i++; 
  		
 }
 $cadena = substr($DescArch, $i);
 
 $result = $this->Model_gestion_punzon_archivo->consulta_archivo_edit($primary_key, $IdPun, $cadena);
 
 /*--------------------------------------------------------------------------------*/
 
 if($result == TRUE)
 		 return FALSE;
 	else {
 		  $post_array['IdPun'] = $IdPun; 
		  $post_array['Archivo'] = $DescArch; 
		  return $post_array;
		 }	 

}


//Funcion que inserta el log cuando hago un insertar Archivo a un Punzon
function log_desp_insertar($post_array, $primary_key)
{
 
 $result = $this->Model_gestion_punzon_archivo->log_pun_arch_add($post_array, $primary_key);
 
 if($result == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
}



//Funcion que inserta el log cuando hago un editar Archivo a un Punzon
function log_desp_editar($post_array, $primary_key)
{
 
 $result = $this->Model_gestion_punzon_archivo->log_pun_arch_edit($post_array, $primary_key);
 
 if($result == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
}
 


//Funcion que inserta los valores cuando cuando subiomos un archivo 
function cargar_upload($uploader_response, $field_info, $files_to_upload)
{
 $IdPun = $this->session->userdata('IdPunzon');
 $DescArch = $uploader_response[0]->name;
 
 	 
 $consulta = $this->Model_gestion_punzon_archivo->graba_datos($IdPun, $DescArch);		

 
 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
 //redirect('punzon_archivo/adminpunzon_archivo');	
 //return TRUE;
} 



//Funcion que uso para que no inserte los registros 
function no_insertar($post_array)
{
 //No pongo nada de contenido para que no inserte
 return TRUE;
 
} 
 
 
//Funcion que almacena los valores del registro que voy a eliminar para despues almacenarlos en el log 
function Vbles_antes_delete($primary_key)
{
 $consulta = $this->Model_gestion_punzon_archivo->obtiene_datos_registro($primary_key);		
 
 
 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else {
				  //$this->session->flashdata('IdPun', $consulta->IdPun);
				  	//$data['valor'] = $consulta; 
				  	//$this->session->set_flashdata('IdPun', $row['IdPun']);	
				  	//$this->session->set_userdata('Archivo', 'Hola');
				  	$this->session->set_flashdata('IdPun', $consulta[0]->IdPun);
				  	$this->session->set_flashdata('Archivo', $consulta[0]->Archivo);
				  	$this->session->set_flashdata('Fecha_Hora_Reg', $consulta[0]->Fecha_Hora_Reg);
				  	//$this->session->set_flashdata('Fecha_Hora_Reg', $row['Fecha_Hora_Reg']);		
				  
				  //$this->session->set_flashdata('Archivo', $consulta->Archivo);
				  //$this->session->set_flashdata('Archivo','Hola');
				  //$this->session->flashdata('Archivo', $consulta['Archivo']);
				  //$this->session->flashdata('Fecha_Hora_Reg', $consulta->Fecha_Hora_Reg);
				  return TRUE;
				 }
 
} 




//Funcion que almacena los valores del registro que elimino en el log 
function Vbles_despues_delete($primary_key)
{
 $consulta = $this->Model_gestion_punzon_archivo->Log_Delete_Punz_Arch($primary_key);		
 
 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
 
}  
    
}
?>
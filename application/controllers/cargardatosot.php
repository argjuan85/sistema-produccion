<?
/* Heredamos de la clase CI_Controller */
class CargardatosOT extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
  
 	$this->load->model('Model_gestion_datos_ot');
 	 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
    //$this->load->library('gc_dependent_select');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('cargardatosot/admincargardatosot');
  }
 
  
  function admincargardatosot()
  {
  	//Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
  	
  	//Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2))
 		{//Inicio del if de nivel de usuario
  	
    try{
 
    /* Creamos el objeto */
    //$crud = new grocery_CRUD();
    $crud = new grocery_CRUD();
    
    //C�digo que se utiliz� para una prueba
     	
 	//$res_NumOT = $this->Model_gestion_datos_ot->obtiene_NumOT($IdOT);
 	//$crud->where('IdDatosOT',$NumOT);
    
    /*
    $state = $crud->getState();
	 
	if($state == 'list')
		{
		$IdOT = $_GET['IdOT'];
		$crud->where('datosot.IdOT',$IdOT);
		//redirect('cargardatosot/admincargardatosot?IdOT='.$IdOT);	  
		}
	*/
		      
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
   
    $crud->set_table('datosot');
    //$crud->set_table('datosot','maquina','subetapa','etapa');
     
    /* Le asignamos un nombre */ 
    $crud->set_subject('Datos de OT'); 		//Este nombre es el que va a aparecer en el formulario, en el boton a�adir, editar, etc
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/     
     
    /*Relacion con la Tabla OT*/
    $crud->set_relation('IdOT','ot','NumOT');
    
       
     /*Relacion con la Tabla SubEtapa*/
    //$crud->set_relation('IdSubEtapa','subetapa','DescSubEtapa');
    
    /*Relacion con la Tabla Producto Elaboracion*/
    
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC', 'IdProdElab' => 27));
    //$crud->field_type('IdProdElab','dropdown',array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));
    //$crud->order_by('CodProdElab','desc');
    
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}');
    //$crud->order_by('CodProdElab','desc');
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC'));
    //$crud->where('EstadoProdElab','AC');
    //$crud->or_where('IdProdElab',1);
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC'));
    //$crud->or_where('IdProdElab',1);
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('IdProdElab' => 27));
    
    /*Saca los botones para editar el campo de texto*/ 
    $crud->unset_texteditor('Observacion_DatosOT','full_text');
    $crud->unset_texteditor('Observacion_Reproceso','full_text');
    
    /*Obtiene el estado en el que se encuentra la aplicacion*/
    $state = $crud->getState();
    $state_info = $crud->getStateInfo();
    
    if($state == 'add')
    //Para agregar un registro muestro solamente los que estan en AC o habilitados
		    {
			 $array_result_prod = $this->Model_gestion_datos_ot->obtiene_datos_prod_add(); 
			 
			 //$crud->field_type('IdProdElab','dropdown',$array_result_prod);
			 
			 $crud->set_relation('IdPunzon','Punzon','DescPunzon',array('EstadoPunzon' => 'Habilitado'));   
			 
			 $array_result = $this->Model_gestion_datos_ot->obtiene_datos_maq_add();    
    		 $crud->field_type('IdMaquina','multiselect',$array_result);
			
			/*Relacion con la Tabla SubEtapa*/
    		$crud->set_relation('IdSubEtapa','subetapa','DescSubEtapa',array('EstadoSub' => 'Habilitada'));
			
			}
    	else {
    		  if($state == 'edit')
    		 //Para editar o las demas acciones no evalua los estados para poder observar todos los registros	
					{					 
					 //De esta forma armo los select personalizados para que incluyan los producto en  estado AC y el producto seleccionado en cualquier estado que este  
					 //Obtengo la clave primaria de la fila que estoy modificando, con esto hago una consulta y obtengo el id del producto que quiero buscar				 
					 $primary_key = $state_info->primary_key;
					 $idprod = $this->Model_gestion_datos_ot->obtiene_idprod($primary_key);
					 //llamo al modelo para armar el arreglo para el select dropdown
					 //$array_result_prod = $this->Model_gestion_datos_ot->obtiene_datos_prod_edit($idprod);
					 
					 //$crud->field_type('IdProdElab','dropdown',$array_result_prod);
					 
					 //$crud->set_relation('IdMaquina','Maquina','{DescMaquina} -- {CodMaq}');
				     $crud->set_relation('IdPunzon','Punzon','DescPunzon');   
					
					 $idmaq = $this->Model_gestion_datos_ot->obtiene_idmaq($primary_key);
					 
					/*C�digo para obtener los id de las maquinas registradas*/
					/*Esto se utiliza para armar el multiselect*/
					 $array_maq = array();
					 $cadena = '';
					 $i = 0;
					 $j = 0;
					 while($i<strlen($idmaq))
					 {
					  if($idmaq[$i] != ',')
						  		{
						  		 $cadena .= $idmaq[$i]; 	
						  		}
					  	   else {
						   	     $array_maq[$j] = $cadena; 
						   	     $j++;
						   	     $cadena = '';
						        }
					  
					  $i++;
					  $array_maq[$j] = $cadena; 		
					 }
					 
					/*----------------------------------------------------*/
									 	
					 
					 //$array_result_maq = $this->Model_gestion_datos_ot->obtiene_datos_maq_edit($idmaq);
					 $array_result_maq = $this->Model_gestion_datos_ot->obtiene_datos_maq_edit($array_maq);    
    				 $crud->field_type('IdMaquina','multiselect',$array_result_maq);					
    				 
					 /*Relacion con la Tabla SubEtapa*/
    				 $idsub = $this->Model_gestion_datos_ot->obtiene_idsub($primary_key);
    				 
    				 $array_result_sub = $this->Model_gestion_datos_ot->obtiene_datos_sub_edit($idsub);    
    				 $crud->field_type('IdSubEtapa','dropdown',$array_result_sub);
		             	
					}
			else  	{
				     //Armo el select de producto para que me muestre el C�digo, Descripci�n y Clase
				     				     
				     //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}');	
			 		 $array_result_maq = $this->Model_gestion_datos_ot->obtiene_datos_maq_gral();    
    				 $crud->field_type('IdMaquina','multiselect',$array_result_maq);
			 		 
			 		 //$crud->set_relation('IdMaquina','Maquina','{DescMaquina} -- {CodMaq}');
			 		 //$crud->set_relation('IdMaquina','Maquina','{DescMaquina}');
					 //$crud->field_type('IdPunzon','dropdown', array('0' => ''));
					 $crud->set_relation('IdPunzon','Punzon','DescPunzon');   
					
					 /*Relacion con la Tabla SubEtapa*/
    			  	 $crud->set_relation('IdSubEtapa','subetapa','DescSubEtapa');
					}	
    
    		}
    /*Hace la relaci�n para mostrar la maquina perteneciente a una la subetapa seleccionada*/
    //$crud->set_relation('IdEtapa','Etapa');
    
    /*Relacion con la Tabla Maquina, permite seleccionar solo las maquinas Habilitadas*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina',array('EstadoMaquina' => 'Habilitada'));
	/*$crud->where('datosot.IdMaquina','maquina.IdMaquina');
	$crud->where('datosot.IdSubEtapa','subetapa.IdSubEtapa');
    $crud->where('maquina.IdEtapa','etapa.IdEtapa');
    $crud->where('subetapa.IdEtapa','etapa.IdEtapa');*/
        
    
    //$array_result = $this->Model_gestion_datos_ot->obtiene_datos_maq();    
    //$crud->field_type('IdMaquina','multiselect',$array_result);      
       
    /*Relacion con la Tabla Punzon, permite seleccionar solo los punzones habilitados*/
    //$crud->set_relation('IdPunzon','Punzon','DescPunzon',array('EstadoPunzon' => 'Habilitado'));   
    
    $crud->field_type('Parcial','enum',array('Si','No'));
    
    $crud->field_type('HParcial','enum',array('Si','No'));
    
    $crud->field_type('Reproceso','enum',array('Si','No'));
          
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    
    /*Relacion con la Tabla Punzon*/
    //$crud->set_relation('IdPunzon','Punzon','DescPunzon');
    
    /*Relacion con la Tabla Maquina*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
      
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'IdOT',
      //'IdProdElab',
      'IdSubEtapa',
      'Cant_Teorica',
      'Cant_Real',
      //'IdMaquina',
      'Parcial',
      'HParcial',
      'Reproceso'
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
  $crud->columns(
      //'IdDatosOT', 
      'IdOT',
      //'IdProdElab',
      'Lote',
      'Cod_Prod',
      'Desc_Prod',
      'IdSubEtapa',
      'Fecha_Carga',
      'Cant_Teorica',
      'Cant_Real',
      'Tiempo_Preparacion',
      'Tiempo_Ejecucion',
      'Muestra_ControlCalidad',      
      'IdMaquina',
      'IdPunzon',
      'Cantidad_Rotulos',
      'Peso_Promedio',
      //Columnas relativas al Rendimiento
      'Rendimiento',
      //'Evalua_Rto'
      'AP',
      'Codigo',
      'Version'
      
    );
    
    /*Ocultamos los campos y botones que no queremos que el usuario vea o utilice*/
 	//$crud->unset_read();			//Deshabilita la funcion view
 	//$crud->unset_edit();			//Deshabilita la funcion edit
 	$crud->unset_delete();			//Deshabilita la funcion delete
 	//$crud->unset_export();			//Deshabilita la funcion export
 	$crud->unset_print();			//Deshabilita la funcion print
     
    //$crud->unset_columns('IdOT','NumOT','Lote','FechaVto','IdVaP','IdProdWS');
     
 	/*Cambiar el Label de los campos*/
    $crud->display_as('IdOT','Nro de OT');
    //$crud->display_as('IdProdElab','Prod. Elaboraci&oacute;n');
    $crud->display_as('IdSubEtapa','SubEtapa');
    $crud->display_as('Fecha_Carga','Fecha Carga');
    $crud->display_as('Tiempo_Preparacion','Tiempo Preparaci&oacute;n');
    $crud->display_as('Tiempo_Ejecucion','Tiempo Ejecuci&oacute;n');
    $crud->display_as('Cant_Teorica','Cant. Te&oacute;rica');
    $crud->display_as('Observacion_DatosOT','Observaci&oacute;n Datos OT');
    $crud->display_as('IdMaquina','Maquina');
    $crud->display_as('IdPunzon','Punz&oacute;n');
    $crud->display_as('Muestra_ControlCalidad','Muestra CC');
    $crud->display_as('Muestra_ValidacionEstabilidad','Muestra V y E');
    $crud->display_as('Observacion_Reproceso','Observaci&oacute;n Reproceso');
    $crud->display_as('Cantidad_Rotulos','Cant. R&oacute;tulos');
    $crud->display_as('Peso_Promedio','Peso Prom.');
    
    $crud->display_as('Codigo','C&oacute;digo');
    $crud->display_as('Version','Versi&oacute;n');
    
    //Cambia el nombre de la columna Evalua Rendimiento
    //$crud->display_as('Evalua_Rto','Evalua Rto');
    
    //Cambia el nombre al boton para exportar en excel
    $crud->set_lang_string('list_export','Exportar Excel');
    
    
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('IdOT','IdSubEtapa','Cant_Teorica','Cant_Real','Muestra_ControlCalidad','Muestra_ValidacionEstabilidad','Tiempo_Preparacion','Tiempo_Ejecucion','Observacion_DatosOT','IdMaquina','IdPunzon','Parcial','HParcial','Reproceso','Lote_Anterior','Observacion_Reproceso','Cantidad_Rotulos','Peso_Promedio');
      
       
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('IdOT','IdSubEtapa','Cant_Teorica','Cant_Real','Muestra_ControlCalidad','Muestra_ValidacionEstabilidad','Tiempo_Preparacion','Tiempo_Ejecucion','Observacion_DatosOT','IdMaquina','IdPunzon','Parcial','HParcial','Reproceso','Lote_Anterior','Observacion_Reproceso','Cantidad_Rotulos','Peso_Promedio');
 	
	//Validaciones de los campos
	/*--------------------------------------------------------------------------------------*/
	$crud->set_rules('Cant_Teorica','Cantidad Te&oacute;rica','trim|required|callback_decimal_numeric');	 	
 	$crud->set_rules('Cant_Real','Cantidad Real','trim|required|callback_decimal_numeric');
 	//$crud->set_rules('Cant_Muest_Productiva','Cantidad Muestra Productiva','trim|callback_decimal_numeric');
 	//$crud->set_rules('Cant_Muest_No_Productiva','Cantidad Muestra No Productiva','trim|callback_decimal_numeric');
 	$crud->set_rules('Tiempo_Preparacion','Tiempo de Preparacion','trim|callback_decimal_numeric');
 	$crud->set_rules('Tiempo_Ejecucion','Tiempo de Ejecucion','trim|callback_decimal_numeric');
 	$crud->set_rules('Lote_Anterior','Lote Anterior','trim|min_length[3]|alpha_numeric');
 	$crud->set_rules('Cantidad_Rotulos','Cantidad de Rotulos','trim|integer');
 	//$crud->set_rules('RotuloCP','Rotulo CP','trim|integer');
 	$crud->set_rules('Peso_Promedio','Peso Promedio','trim|callback_decimal_numeric');
 	$crud->set_rules('Muestra_ControlCalidad','Muestra Control de Calidad','trim|callback_decimal_numeric');
 	$crud->set_rules('Muestra_ValidacionEstabilidad','Muestra Validacion y Estabilidad','trim|callback_decimal_numeric');
 	
 	/*--------------------------------------------------------------------------------------*/
 	
 	/*Llamo a una funcion para que despues de insertar me limpie los campos menos el campor del numero de orden*/
 	//$crud->callback_after_insert(array($this, 'limpiar_campos'));
 	
 	$crud->add_action('Aprobar', '', 'cambiaestado_ot_ap_add/admincambiaestado_ot_ap_add/edit','ui-icon-plus'); 
 	
 	//$crud->add_action('Quitar AP', '', 'cambiaestadoot/Quitar_AP','ui-icon-minus'); 
 	$crud->add_action('Quitar AP', '', 'cambiaestado_ot_ap_del/admincambiaestado_ot_ap_del/edit','ui-icon-minus'); 
 	
 	
 	//Funciones para Armar las columnas relativas al Rendimiento
 	//Llamo a la funcion para armar la columna del Rendimiento
 	$crud->callback_column('Rendimiento',array($this,'calcula_rendimiento'));
 	
 	$crud->callback_column('Lote',array($this,'columna_lote'));
 	$crud->callback_column('Cod_Prod',array($this,'columna_codprodws'));
 	$crud->callback_column('Desc_Prod',array($this,'columna_descprodws'));
 	$crud->callback_column('Tpo_Preparacion',array($this,'columna_tpo_preparacion'));
 	$crud->callback_column('Tpo_Ejecucion',array($this,'columna_tpo_ejecucion'));
 	$crud->callback_column('Muestras_CC',array($this,'columna_muestra_cc'));
 	
 	
 	//Llamo a la funcion para armar la columna del aviso por rendimiento
 	//$crud->callback_column('Evalua_Rto',array($this,'evalua_rendimiento'));
 	 
 	//Almacenar el log al insertar
 	$crud->callback_after_insert(array($this, 'log_insertar'));

	//Almacena el log despues de editar
	$crud->callback_after_update(array($this, 'log_editar'));
 	
 	
 	//$crud->field_type('DescPunzon', 'invisible'); 	 	 	
    
    //Deshabilito la funcion ver (Da conflictos con los id de las maquinas)
    $crud->unset_read();
    /* Generamos la tabla */
    //$crud->unset_jquery();
    //$crud->unset_jquery_ui();
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('cargardatosot/admincargardatosot', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }


//Funcion para validar que los campos sean de tipo decimal o flotante (con separador de .)
function decimal_numeric($valor)
{
 //if ($str <isdecimal&nummeric>) //Use your logic to check here
 if($valor != '')
		 {
		 	if ((is_numeric($valor))||(is_float($valor))) //Use your logic to check here
		        {
		            return TRUE;
		        }
		        else
		        {
		        	$this->form_validation->set_message('decimal_numeric', '%s debe contener un valor entero o decimal (con .)');
		            return FALSE;
		            
		        }
		  }	
	
}


/*function obtiene_idprod_edit($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_datos_ot->obtiene_datos1_edit($post_array, $primary_key);	
 	 	 	
 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return $consulta;	
}*/


/*function obtiene_datos_prod_edit($post_array, $primary_key)
{
 $consulta1 = $this->Model_gestion_datos_ot->obtiene_id_prod_edit($post_array, $primary_key);	
 $consulta2 = $this->Model_gestion_datos_ot->obtiene_cod_desc_prod_edit($post_array, $primary_key);	
 $consulta3 = array_combine($consulta1, $consulta2);
 	 	 	 	
	 if($consulta3 == FALSE)
	  			 return FALSE;	  			 
	  		else return $consulta3;	
}*/


//Funcion para calcular el rendimiento 
function calcula_rendimiento($value, $row)
{
 //Formula de REndimiento 
 //Verificar las formulas segun las subetapas
 $calculo = ((($row->Cant_Real + $row->Muestra_ControlCalidad + $row->Muestra_ValidacionEstabilidad) * 100)/$row->Cant_Teorica);
 //return $calculo." - ".round($calculo);
 return round($calculo)." %";		
}


//Funcion para Evaluar el rendimiento 
/*function evalua_rendimiento($value, $row)
{
 $calculo = (($row->Cant_Real * 100)/$row->Cant_Teorica);
 //Determino un valor por defecto pero en realidad tengo que comparar con los Estandares
 $val_estandar = 96;
 $frase_supera = 'SI SUPERA';
 $frase_no_supera = 'NO SUPERA';
 if($calculo < $val_estandar) 
 		 return "<FONT COLOR='red'><strong>".$frase_no_supera."</strong></FONT>";	
 	else return "<FONT COLOR='blue'>".$frase_supera."</FONT>";		
}*/


//Funcion para almacenar el log al insertar
function log_insertar($post_array, $primary_key) 
	{	
	 $consulta = $this->Model_gestion_datos_ot->graba_log_datos_add($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
		
	} 

//Funcion para almacenar el log al editar
function log_editar($post_array, $primary_key) 
	{	
	 $consulta = $this->Model_gestion_datos_ot->graba_log_datos_edit($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
		
	}


/*----------------------Funciones para agregar columnas-------------------------*/

//Obtengo el lote para la columna
function columna_lote($value, $row)
{
 $consulta = $this->Model_gestion_datos_ot->consulta_lote($row);
 
 //$calculo = ($row->Cant_Bueno * 100)/$row->Cant_Entregado;
 
 return $consulta;
}


//Obtengo el c�digo del producto para la columna
function columna_codprodws($value, $row)
{
 $consulta = $this->Model_gestion_datos_ot->consulta_codprodws($row);
 
 return $consulta;
}


//Obtengo la descripci�n del producto para la columna
function columna_descprodws($value, $row)
{
 $consulta = $this->Model_gestion_datos_ot->consulta_descprodws($row);
 
 return $consulta;
}


//Obtengo el tiempo de preparaci�n para la columna
function columna_tpo_preparacion($value, $row)
{
 $consulta = $this->Model_gestion_datos_ot->consulta_tpo_preparacion($row);
 
 return $consulta;
}


//Obtengo el tiempo de ejecuci�n para la columna
function columna_tpo_ejecucion($value, $row)
{
 $consulta = $this->Model_gestion_datos_ot->consulta_tpo_ejecucion($row);
 
 return $consulta;
}


//Obtengo las muestras para CC para la columna
function columna_muestra_cc($value, $row)
{
 $consulta = $this->Model_gestion_datos_ot->consulta_muestra_cc($row);
 
 return $consulta;
}



function Quitar_AP($primary_key)
	{
	 $resPHP = "<script type='text/javascript'>
	             var res = confirm('Esta seguro que quiere cambiar el estado?');
	             document.write(res);
	             console.log(res);
	             
	             if(res == true)
	             		{
	             		 
	             		 console.log('Se imprime True');
	             		 window.location.href = 'http://127.0.0.1/produccion/index.php/cargardatosot/admincargardatosot';
	             		 }
	             	else {
	             		  
	             		  console.log('Se imprime False'); 
	             		  window.location.href = 'http://127.0.0.1/produccion/index.php/principal/inicio';
	             		 }
	             
	             </script>";
	  
	  
	  /*if ($resPHP == true)
	  			 echo "Se impreime True";
	  		else echo "Se impreime False";	
	  echo $resPHP;  */         
	 //document.write('Res = ' + res);
	 //$this->load->helper('url');
	 /*if($resPHP == true)
				 { //echo "<script languaje=javascript>alert('El Registro a sido Modificado'); </script>";
				  //echo "<script languaje=javascript> window.location ='http://www.google.com.ar'; </script>";
				  echo ("Se imprime True");
				  echo "<script languaje=javascript> window.location.href = 'http://127.0.0.1/produccion/index.php/cargardatosot/admincargardatosot'; </script>";
				 	/*$Mje = "<script type='text/javascript'>
						   alert('Holaaa');
							
							</script>";*/
						      //echo $Mje;       
			/*    } 
			else {
				 echo ("Se imprime False");
				  echo "<script languaje=javascript> window.location.href = 'http://127.0.0.1/produccion/index.php/principal/inicio'; </script>";
				}	*/		  
				 
				 //redirect(base_url('site_url(/cargardatosot/admincargardatosot'));
	 			
	 			//echo "TRUE";	 			
	 			/*{
				 $consulta = $this->Model_gestion_datos_ot->quitar_estado_ap($primary_key);
	 			 if($consulta == TRUE)
	 			 			 {$Mje = "<script type='text/javascript'>
						             window.location.replace = 'http://www.google.com.ar';
						             
						             </script>";
						      //echo $Mje;       
						      }       
						      
	 			 			 //redirect('cargardatosot/admincargardatosot');
				  	 	else echo "Error en la consulta";
	 			 	
				}*/
	 			
	 	
	 			//echo "FALSE";
	 			//redirect('cargardatosot/admincargardatosot');
	 			//echo "FALSE";
	 			/*$consulta = $this->Model_gestion_datos_ot->quitar_estado_ap($primary_key);	
				 	 	 	
				  if($consulta == FALSE)
				  			 return FALSE;	  			 
				   		else return TRUE;*/
				   			 
	 /* Codigo para probar
	 original
	 function confirmation(a) {
    var currString = "<?php echo $currString ?>";
    var answer = confirm("Are you sure you want to delete this item?");
    if (answer){
        alert("The item has been deleted")
        window.location = "list.php?s=" + currString + "&=delete=true&id=" + a;
    }
    else{
        alert("The item has not been deleted");
	}
	*/
	/*$asd="<script type='text/javascript'>
	 var clave_primaria = '<?php echo $primary_key ?>';
    var answer = confirm('Esta seguro que quiere cambiar el estado?');
    if (answer){
        alert('El estado fue cambiado')
        window.location = 'list.php?s=' + currString + '&=delete=true&id=' + a;
        window.location ='cambiarestado/Quitar_AP_2?clave=' + clave_primaria;
    }
    else{
        alert('El estado no fue cambiado');
	 
	 </script>";*/
	 
	 /*
	 //echo "prompt(Hola);";
	 //$resPHP = "<script> document.write(res) </script>";
	 //echo "resPHP = ".$resPHP;
	 //echo "console.log(res);";
	 //echo "$('.eliminar').click(function(event){";
    /* echo "event.preventDefault();";
     echo "confirm('�Realmente desea eliminar este post?');";
        //echo "window.location = this.href;";
	 //echo "});";*/
	 
	 //echo "</script>"; 
	 
	/* if($resPHP == true)	 
				 {
				  /*echo "<script type='text/javascript'>";
				  echo "alert(res)";
				  echo "alert(".$resPHP.")";
				  echo "</script>";
				  $consulta = $this->Model_gestion_datos_ot->quitar_estado_ap($primary_key);	
				 	 	 	
				  if($consulta == FALSE)
				  			 return FALSE;	  			 
				   		else //return TRUE;	
				  			redirect('cargardatosot/admincargardatosot');
				 }
			else redirect('cargardatosot/admincargardatosot');*/	 
	}

}
?>
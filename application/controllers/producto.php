<?
/* Heredamos de la clase CI_Controller */
class Producto extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
    
    $this->load->model('Model_gestion_producto');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('producto/adminproducto');
  }
 
  
  function adminproducto()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if($this->session->userdata('Nivel') == 0)
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('prodelaboracion');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Producto');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    /*Relacion con la Tabla Maquina*/
    //$crud->set_relation('IdMaquinaDefecto','maquina','DescMaquina');
    
    /*Relacion con la Tabla IdClaseProd de Producto*/    
    $crud->set_relation('IdClaseProd','ClaseProd','DescClaseProd');
    
    
    /*Relacion con la Tabla Tipo de Producto*/    
    $crud->set_relation('IdTipoProd','tipoprod','DescTipoProd');
    
    /*Relacion con la Tabla Paso de Producto*/    
    $crud->set_relation('IdPasoProd','pasoprod','DescPasoProd');
    
    
    /*Relacion con la Tabla VoMM de Producto*/    
    $crud->set_relation('IdVoMM','VoMM','DescVoMM');
        
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    $crud->display_as('IdMaquinaDefecto','Maquina por Defecto');  
    $crud->display_as('IdTipoProd','Tipo de Producto');  
    $crud->display_as('IdPasoProd','Paso del Producto');  
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'CodProdElab',
      'DescProdElab',
      'IdClaseProd',
      'IdVoMM',
      'IdPasoProd',
      'IdTipoProd',
      'EstadoProdElab'
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'CodProdElab',
      'DescProdElab',
      'IdClaseProd',
      'IdVoMM',
      'IdPasoProd',
      'IdTipoProd',
      'EstadoProdElab'
    );
 
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('CodProdElab','DescProdElab','IdClaseProd','IdVoMM','IdPasoProd','IdTipoProd');
    
    /*Cambiar el Label de los campos*/
    $crud->display_as('CodProdElab','C&oacute;digo de Producto');
    $crud->display_as('DescProdElab','Descripci&oacute;n de Producto');
    $crud->display_as('IdClaseProd','Clase');
    //$crud->display_as('IdMaquinaDefecto','Maquina por Defecto');
    $crud->display_as('IdVoMM','VoMM');
    $crud->display_as('EstadoProdElab','Estado');
    
    $crud->display_as('prodelaboracion','Producto'); 
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('IdProdElab','CodProdElab','DescProdElab','IdClaseProd','IdVoMM','IdPasoProd','IdTipoProd');
 	
 	$i=0;			// Variable de session que me indica el nivel de usuario
 					// El 0 representa el nivel mas alto
 	if ($i == 1)
 			 {
			  $crud->field_type('EstadoProdElab','invisible');
			  $crud->set_lang_string('list_delete','Eliminar');
			  $crud->set_lang_string('delete_success_message','El Producto fue eliminado exitosamente.');
			  	
			 } 			 
		else {
			  $crud->field_type('EstadoProdElab','enum',array('AC','DI'));
			  $crud->set_lang_string('list_delete','Cambiar Estado');
			  $crud->set_lang_string('delete_success_message','El estado del producto se cambio exitosamente');
			  		
			 }	
		
		
 	/*Determina los valores que va a tomar el Estado de Producto*/
 	 //$crud->field_type('EstadoProdElab','enum',array('AC','DI'));
  	
  	/*****Oculto la opcion de borrar para que no se puedan eliminar productos*****/
  	/*De esta forma solo se permite cambiar el estado del producto: AC (activo) o DI (Discontinuado)*/
  	//$crud->unset_delete();
  	
  	$crud->set_rules('CodProdElab','C&oacute;digo de Producto','trim|required|min_length[2]|max_length[10]|regex_match[/^[0-9]+$/]');
  	$crud->set_rules('DescProdElab','Descripci&oacute;n de Producto','trim|required');
  	
  	/*Llama a la funcion para hacer la validacion de los campos del producto antes de hacer un insert*/
  	$crud->callback_before_insert(array($this,'valida_campos_producto_add'));
  	
  	/*Llama a la funcion para hacer la validacion de los campos antes de hacer el editar*/ 	 	
  	$crud->callback_before_update(array($this,'valida_campos_producto_edit'));
  	
  	/*Llamada a la funcion para almacenar en el log cuando se inserta*/
  	$crud->callback_after_insert(array($this, 'graba_log_add'));		//Funcion que se llama despues de insertar
	
	/*Llamada a la funcion para almacenar en el log cuando se edita*/
  	$crud->callback_after_update(array($this, 'graba_log_edit'));		//Funcion que se llama despues de editar
	
	if ($i == 1)
			  		$crud->callback_delete(array($this,'elimina_producto1'));				//Llama a la funcion Eliminar, que en realidad hace un cambio de estado
			else	$crud->callback_delete(array($this,'elimina_producto2'));
	
	
	/*Segun el estado en el que se encuentre, modifica los mensajes que me va a mostrar*/
	$crud->set_lang_string('alert_delete','�Esta seguro que quiere cambiar el estado al Producto?');
	$state = $crud->getState();
	if($state == 'add')
				$crud->set_lang_string('insert_error','Error al Insertar, verifique si el C&oacute;digo y/o la Descripci&oacute;n no se encuentren cargados');
		else if($state == 'edit')
				        {
						$crud->set_lang_string('update_error','Error al Editar, verifique si el C&oacute;digo y/o la Descripci&oacute;n no se encuentren cargados');
						/*Lo pongo en invisible pero lo utilizo para realizar la busqueda al momento de editar los campos*/
						$crud->field_type('IdProdElab','hidden');	
						}
				

	$crud->unset_print();
	$crud->unset_export();					
				        
	/*Hacer un switch case para evaluar los estados*/
	/*Pagina de referencia*/
	/* http://www.grocerycrud.com/forums/topic/2525-callback-addedit-field-changes-the-display-on-the-read-method/ */	
	
		
 
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('producto/adminproducto', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  } 

  /*Llama al modelo para realizar la validacion de los campos del producto antes de insertar*/
  function valida_campos_producto_add($post_array) 
   { 
	 $msj = $this->Model_gestion_producto->consulta_campos_producto_add($post_array);
	  
	  /*Si desde el modelo me retorna false, retornamos false para no hacer de nuevo la carga del mismo producto*/	  
	  if($msj == FALSE)
	  			 	return FALSE;	  			 
	  		else { /*Antes de asignar los valores les elimina los espacios en blanco delante y al final de la cadena*/ 
             	   $post_array['CodProdElab'] = trim($post_array['CodProdElab']);
             	   $post_array['DescProdElab'] = trim($post_array['DescProdElab']);
             	   return $post_array;         
             	  }	  	
	}
  
  
  /*Llama al modelo para realizar la validacion de los campos del producto despues de editarlos*/
  function valida_campos_producto_edit($post_array) 
   { 
	 $msj = $this->Model_gestion_producto->consulta_campos_producto_edit($post_array);
	  
	  /*Si desde el modelo me retorna false, retornamos false para no hacer de nuevo la carga del mismo producto*/	  
	  if($msj == FALSE)
	  			 	return FALSE;	  			 
	  		else { /*Antes de asignar los valores les elimina los espacios en blanco delante y al final de la cadena*/ 
             	   $post_array['CodProdElab'] = trim($post_array['CodProdElab']);
             	   $post_array['DescProdElab'] = trim($post_array['DescProdElab']);
             	   return $post_array;         
             	  }	  	
	}


   /*Funcion que llama al modelo para insertar los registros del log cuando hago un insertar*/
   function graba_log_add($post_array, $primary_key)
   {   	
   	$consulta = $this->Model_gestion_producto->graba_log_prod_add($post_array, $primary_key);		if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
   }
      	 
   /*Funcion que llama al modelo para insertar los registros del log cuando hago un editar*/
   function graba_log_edit($post_array, $primary_key)
   {   	
   	$consulta = $this->Model_gestion_producto->graba_log_prod_edit($post_array, $primary_key);		if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
   }   	 

	/*Funcion que llama al modelo para eliminar un prodcuto (Cambia el estado a DI)*/
	function elimina_producto1($primary_key)
	{
	 $consulta = $this->Model_gestion_producto->cambia_estado_di($primary_key);	
	 	 	 	
	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;	
	}

	/*Funcion que llama al modelo para cambiar el estado de un prodcuto*/
	function elimina_producto2($primary_key)
	{
	 /*Obtengo el estado de un producto para determinar el cambio de estado que hago*/
	 $cons_estado = $this->Model_gestion_producto->obtiene_estado_prod($primary_key);	
	 if($cons_estado == 'AC')
	 		 $consulta = $this->Model_gestion_producto->cambia_estado_di($primary_key);	
	 	else if($cons_estado == 'DI')
	 			$consulta = $this->Model_gestion_producto->cambia_estado_ac($primary_key);		
	 	 	
	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;	
	}

}
?>
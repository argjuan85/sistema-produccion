<?
/* Heredamos de la clase CI_Controller */
class Listar_productos_ws extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    //-----------------------------------------------------
	//Set los valores del php.ini (C:\Windows\php.ini)
	//-----------------------------------------------------
		  
		ini_set("memory_limit","512M"); 		//Se aumento el limite de memoria de 8M a 512M porque nos deba un error
		ini_set("max_execution_time","0");		//Se cambio de 30 a 0, de esta forma el tiempo d ejecucion en segundos no tiene limite
		ini_set("max_input_time","-1");			//Se cambio de 60 a -1, de esta forma el tiempo de entrada en segundos no tiene limite 
	/*------------------------------------------------------------------------------------------*/
    
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('listar_productos_ws/adminlistar_productos_ws');
  }
 
  
  function adminlistar_productos_ws()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		/*$crud->where ('EstadoProdWS', 'IC');
		$crud->where ('EstadoProdWS', 'ic');
		$crud->set_table ('productosws');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('productosws');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Productos WS');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
      
        
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
        
        
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    $crud->display_as('IdProductoWS','Identificador de Producto WS');  
    $crud->display_as('CodProdWS','C&oacute;digo de Producto');  
    $crud->display_as('DescProdWS','Descripci&oacute;n del Producto');  
    $crud->display_as('EstadoProdWS','Estado del Producto');  
    $crud->display_as('CodUniMed','C&oacute;digo Unidad de Medida'); 
    $crud->display_as('DescUniMed','Descripci&oacute; de la Unidad de Medida');  
    
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    /*$crud->required_fields(
      'CodProdElab',
      'DescProdElab',
      'IdVoMM'
    );*/
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'IdProductoWS',
      'CodProdWS',
      'DescProdWS',
      'EstadoProdWS',
      'CodUniMed',
      'DescUniMed'
    );
 
 	$crud->unset_add();				//Deshabilita la funcion add (A�adir)
 	$crud->unset_edit();			//Deshabilita la funcion edit (Editar)
 	$crud->unset_delete();			//Deshabilita la funcion delete (Eliminar)
 
 	$crud->unset_export();			//Ocultamos el boton Exportar
 	$crud->unset_print();			//Ocultamos el boton Imprimir
 
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('listar_productos_ws/adminlistar_productos_ws', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }
}
?>
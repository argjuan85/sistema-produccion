<?/* Heredamos de la clase CI_Controller */
class Pdf_datos_conciliacion extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    //$this->load->model('Grocery_crud_model');
    $this->load->model('Model_gestion_conciliacion');
    /* Cargamos la base de datos */
    $this->load->database();
	// $this->load->library('/MPDF/mpdf.php');
    /* Cargamos la libreria*/
   // $this->load->library('grocery_crud');
 
    /* Añadimos el helper al controlador */
    //$this->load->helper('url');
  //$this->load->library('usuariolib');
  	ini_set('memory_limit', '-1');
    ini_set("pcre.backtrack_limit","1000000");
    ini_set("max_execution_time","0");
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('generar_pdf/generar_pdf_conciliacion');
  }
 
  
  public function generar_pdf_conciliacion()
  {
   /*Prueba obteniendo los valores de los productos*/
  //$consulta = $this->Model_gestion_mostrar_datos_ot->obtiene_datos();	
  
  //$ot = "<script> document.write(valor) </script>";
  //$ot = ($_POST['valor']);
  
  //Recibo el dato que le envío desde el boton
  //$IdOT = $_GET['IdOT'];
  
  //Llamo al modelo para obtener los valores de las conciliaciones
  //$cons_datos_conciliacion = $this->Model_gestion_conciliacion->obtiene_datos_conciliacion($IdOT);
  
  
  /*-------------------------------Armar el PDF------------------------------*/
  
  
   //ARREGLAR LAS LEYENDAS DEL PDF
   
   //ARREGLAR EN LA CONSULTA QUE NO PERMITA INGRESAR VALORES DUPLICADOS (EN LOTE %LIKE%)
  
  
  /*-------------------------------------------------------------------------*/
  
  $html2 = $_GET['texto_html'];
  $NumOT = $_GET['NumOT'];
  //var_dump($html);
  //die;
  //$ot = $_REQUEST["data[1].IdOT"]; 
     
   /*Llama al modelo para traer los datos del log*/
   //$consulta = $this->Model_gestion_log->consulta_campos_log();
   
  // if($consulta->num_rows()>0)
   
   //$IdOT = $this->session->userdata('IdOT');
    //Llamo al modelo para obtener los valores de la ot
  	$cons_datos_ot = $this->Model_gestion_conciliacion->obtiene_datos_ot_NumOt($NumOT);
    
    //De esta forma doy vuelta la fecha que traigo desde la base de datos para mostrarla en el pdf
    $dia = substr($cons_datos_ot->FechaVto, -2);
	$mes = substr($cons_datos_ot->FechaVto, -5, 2);
	$anio = substr($cons_datos_ot->FechaVto, -10, 4);
   	$fecha_vto_conv = $dia .'-'. $mes .'-'. $anio;
    
    //$fecha_conv = $cons_datos_ot->FechaVto;
          	
  	$html1 = "<html><div class='container-fluid'>
  	
  	<table width='100%'>
  		<tr>
  			<td width='10%'>PRODUCTO: </td>
  			<td width='40%'>".$cons_datos_ot->DescProdWS."</td>
  			<td width='50%'>REALIZADO: </td>
  		</tr>
  		<tr>
  			<td width='10%'>CÓDIGO: </td>
  			<td width='40%'>".$cons_datos_ot->CodProdWS."</td>
  			<td width='50%'>CONTROLADO: </td>
  		</tr>
  		<tr>
  			<td width='10%'>LOTE: </td>
  			<td width='40%'>".$cons_datos_ot->Lote."</td>
  			<td width='50%'>AUTORIZADO: </td>
  		</tr>
  		<tr>
  			<td width='10%'>FECHA: </td>
  			<td width='40%'>".$fecha_vto_conv."</td>
  			<td width='50%'></td>
  		</tr>
  	  </table>
   </div>
  	 
  	
  	<br>
  	
  	";
  	
  	$html1 .= "</div>
  	</html>
  	";
  	
   
   /*Desde acá se empieza a generar el PDF*/
   
   //	$this->load->library('/MPDF/mpdf.php');
    //require_once("../MPDF/mpdf.php");
	//require_once('/MPDF/mpdf.php');
   /* ini_set('memory_limit', '-1');
    ini_set("pcre.backtrack_limit","1000000");
    ini_set('max_execution_time',0);*/
    
    //-----------------------------------------------------
	//Set los valores del php.ini (C:\Windows\php.ini)
	//-----------------------------------------------------
		  
	//	ini_set('memory_limit', '-1');
      //  ini_set("pcre.backtrack_limit","1000000");
        //ini_set("max_execution_time","0"); 
        
    /*  ini_set('memory_limit','32M'); 		//Se aumento el limite de memoria de 8M a 512M porque nos deba un error
		ini_set("max_execution_time","0");		//Se cambio de 30 a 0, de esta forma el tiempo d ejecucion en segundos no tiene limite
		ini_set("max_input_time","-1");*/
	/*------------------------------------------------------------------------------------------*/
    //echo "About to create pdf";
  // $mpdf->debug = true;
    //require('/MPDF/mpdf.php');
  /*  $mpdf=new mPDF(); 		//Pagina Vertical
   
    $mpdf=new mPDF('UTF-8','A4-L');			//Pagina Horizontal
     
    //$mpdf->SetTitle("Lista de Log");
    
    $mpdf->SetHTMLHeader("<div><h2 style='margin-left:240px;'>Lista en PDF</h2></div>");  
   
    $mpdf->WriteHTML("<p>Hello World</p>", 2);
	
	$texto1 = "<table class='table'> <tr>
		      							<td colspan='2' ><strong>Prueba del texto 1<br></strong></td>
		      							<td colspan='2' ><strong>Prueba del texto otro<br></strong></td>
		      						</tr> 
		      </table>";
		      
	$texto2 = "<table class='table'> <tr>
		      							<td colspan='2' ><strong>Prueba del texto 2<br></strong></td>
		      						</tr> 
		      </table>";	      
	
	$html = " <table class='table1'>
	<thead>
		<tr>
			<th></th>
			<th scope='col' abbr='Starter'>Smart Starter</th>
			<th scope='col' abbr='Medium'>Smart Medium</th>
			<th scope='col' abbr='Business'>Smart Business</th>
			<th scope='col' abbr='Deluxe'>Smart Deluxe</th>
			<th scope='col' abbr='Deluxe'>asde</th>
			<th scope='col' abbr='Deluxe'>sdf</th>
			<th scope='col' abbr='Deluxe'>Smart Deluxe asddsadsadsa</th>
			<th scope='col' abbr='Deluxe'>Smart Deluxe1</th>
			<th scope='col' abbr='Deluxe'>Smart Deluxe2</th>
			<th scope='col' abbr='Deluxe'>Smart Deluxe3</th>
			<th scope='col' abbr='Deluxe'>Smart Deluxe4</th>
			<th scope='col' abbr='Deluxe'>Smart Deluxe5</th>
			
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th scope='row' style='background-color:#ddd;'>Price per month</th>
			<td style='background-color:#ddd;'>$ 2.90</td>
			<td style='background-color:#ddd;'>$ 5.90</td>
			<td style='background-color:#ddd;'>$ 9.90</td>
			<td style='background-color:#ddd;'>$ 14.90</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<th scope='row'>Storage Space</th>
			<td>512 MB</td>
			<td>1 GB</td>
			<td>2 GB</td>
			<td>4 GB</td>
		</tr>
		<tr>
			<th scope='row'>Bandwidth</th>
			<td>50 GB</td>
			<td>100 GB</td>
			<td>150 GB</td>
			<td>Unlimited</td>
		</tr>
		<tr>
			<th scope='row'>MySQL Databases</th>
			<td>Unlimited prueba prueba prueba prueba prueba</td>
			<td>Unlimited</td>
			<td>Unlimited</td>
			<td>Unlimited</td>
		</tr>
		<tr>
			<th scope='row'>Setup</th>
			<td>19.90 $</td>
			<td>12.90 $</td>
			<td>free</td>
			<td>free</td>
		</tr>
		<tr>
			<th scope='row'>PHP 5</th>
			<td><span class='check'></span></td>
			<td><span class='check'></span></td>
			<td><span class='check'></span></td>
			<td><span class='check'></span></td>
		</tr>
		<tr>
			<th scope='row'>Ruby on Rails</th>
			<td><span class='check'></span></td>
			<td><span class='check'></span></td>
			<td><span class='check'></span></td>
			<td><span class='check'></span></td>
		</tr>
	</tbody>
</table> ";
	
	/*$textos_juntos = $texto1."<br>".$texto2;
	
	$mpdf->WriteHTML($textos_juntos,2);*/
/*	$mpdf->WriteHTML($html,2);
	//$mpdf->Output('C:/instaladores/invoice.pdf', 'I');
	$mpdf->Output('Genera_pdf.pdf', 'I');
	//$mpdf->Output();
    */

  $this->load->library('/MPDF/mpdf.php');
    //require_once("../MPDF/mpdf.php");
	//require_once('/MPDF/mpdf.php');
   /* ini_set('memory_limit', '-1');
    ini_set("pcre.backtrack_limit","1000000");
    ini_set('max_execution_time',0);*/
    
    //-----------------------------------------------------
	//Set los valores del php.ini (C:\Windows\php.ini)
	//-----------------------------------------------------
		  
		/*ini_set('memory_limit', '-1');
        ini_set("pcre.backtrack_limit","1000000");
        ini_set("max_execution_time","0"); */
        
    /*  ini_set('memory_limit','32M'); 		//Se aumento el limite de memoria de 8M a 512M porque nos deba un error
		ini_set("max_execution_time","0");		//Se cambio de 30 a 0, de esta forma el tiempo d ejecucion en segundos no tiene limite
		ini_set("max_input_time","-1");*/
	/*------------------------------------------------------------------------------------------*/
    //echo "About to create pdf";
    $mpdf->debug = true;
    //require('/MPDF/mpdf.php');
   
    //$mpdf=new mPDF();
    //$mpdf=new mPDF('','legal','','',15,15,40,20,5,5,'P');
    //margin-left, margin-right, margin-top, margin-bottom, margin-header, margin-footer, orientacion 
    $mpdf=new mPDF('UTF-8','A4-L','','',10,10,10,10,5,5);
    //$mpdf=new mPDF('UTF-8','A4-L');
    
    $mpdf->SetTitle("Datos de Conciliación");
    /*Cargo una variable con el contenido del Header*/
    //$Header = "<div><h1 style='margin-left:300px;'>Registro de Conciliación de</h1>";
    //$Header .= "<h1 style='margin-left:250px;'>Materiales y Productos Terminados</h1></div>";
    
    //Tengo que asignar espacios para porder armar el texto
    $Header = "<div><h2>FOGL-MAR-0208/07 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    REGISTRO DE CONCILIACIÓN</h2>";
    $Header .= "<h2 style='margin-left:410px;'>DE MATERIALES</h2></div>";
    $mpdf->SetHTMLHeader($Header);  
   
    //$mpdf->WriteHTML("<p>Hello World</p>", 2);
	
	/*---------------Configuracion realizadas-------------------*/
	$MPDF->allow_output_buffering = true;
	$mpdf->useSubstitutions=false;
	$mpdf->cacheTables = true;
	
	//$mpdf->SetWatermarkImage('img/Captura(Logo Raffo).png',1,'',array(25,3));
	$mpdf->SetWatermarkImage('img/MonteVerde.png',1,'',array(250,3));
	$mpdf->showWatermarkImage = true;
	
	$mpdf->simpleTables=true;
	$mpdf->packTableData=true;
	
	/*---------------------------------------------------------*/
	$mpdf->AddPage();
	
	$Fecha_Actual = date("d/m/Y");
	$Hora_Actual = date("H:i:s");
	//var_dump(date("d/m/Y H:i:s"));
	//die;
	$usuario= $this->session->userdata('Usuario');;
	
	$html = $html1;
	
	$html = "<html><div class='container-fluid'>
    <!--<legend>Conciliación</legend>-->
    <br>
    <br>
    <br>
    <table width='25%' border='1'><tr><td>Fecha de Vigencia: 09/FEB/2015</td></tr></table>
    <!--<p border-style='1'>Fecha de Vigencia: 09/FEB/2015</p>-->
    <div align=\"right\"><span><i>Emitido el ".$Fecha_Actual." a las ".$Hora_Actual."</i></span></div>
    <div align=\"right\"><span><i>Usuario: ".$usuario."</i></span></div>
    <br>";
    
    //Agrego el html con los datos de la ot
    $html .= $html1;
    
    $html .= "<!--<table class=\"table table-striped\" style=\"border-collapse: collapse;\">-->
    <table border='1' cellpadding='0' cellspacing='0'>
        <thead>
            <tr style='background-color:#D3D3D3;'>
                <!--<th width='10' rowspan='2'>N°</th>-->
                <!--<th rowspan='2'>Num. OT</th>-->
                <th rowspan='2'>CÓDIGO</th>
                <!--Se agregan espacios en el encabezado de la columna para que muestre la descripción a lo largo, ya que toma el ancho de la columna como el texto del encabezado-->
                <th style='width:10cm' rowspan='2'>MATERIAL</th>
                <!--<th rowspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATERIAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>-->
                <!--<th rowspan='2'>Lote</th>-->
                <th rowspan='2'>CANTIDAD ENTREGADA</th>
                <th rowspan='2'>PEDIDO ADICIONAL</th>
                <th colspan='2'>BUENO</th>
                <th colspan='2'>ROTO</th>
                <th colspan='2'>DESTRUIDO</th>
                <th colspan='2'>FALTANTE</th>
                <th colspan='2'>DEVUELTO</th>
                <th colspan='2'>ERROR</th>
            </tr>
            
            <tr style='background-color:#D3D3D3;'>
            	<th>Cantidad</th>
            	<th>%</th>
            	<th>Cantidad</th>
            	<th>%</th>
            	<th>Cantidad</th>
            	<th>%</th>
            	<th>Cantidad</th>
            	<th>%</th>
            	<th>Cantidad</th>
            	<th>%</th>
            	<th>Cantidad</th>
            	<th>%</th>
            	
            </tr>    
                
                
                <!--<tr style='background-color:#D3D3D3;'>
                 <th>Num. OT</th>
                <th>Cod. Producto</th>
                <th>Desc. Producto</th>
                <th>Lote</th>
                <th>Cant. Entregado</th>
                <th>Cant. Adicional</th>
                <th>Cant. Bueno</th>
                <th>Cant. Roto</th>
                <th>Cant. Destruido</th>
                <th>Cant. Faltante</th>
                <th>Cant. Devuelto</th>
                <th>Cant. Error</th>-->
                
                                
                <!--<th width='10'>N°</th>
                <th>NumOT</th>
                <th>CodProdElab</th>
                <th>DescProdElab</th>
                <th>Lote</th>
                <th>FechaCarga</th>
                <th>FechaVto</th>
                <th>SubEtapa</th>
                <th>DescVaP</th>
                <th>Cant_Teorica</th>
                <th>Cant_Real</th>
                <th>Cant_Muest_Productiva</th>
                <th>Cant_Muest_No_Productiva</th>
                <th>Tiempo_Preparacion</th>
                <th>Tiempo_Ejecucion</th>
                <th>Observacion_DatosOT</th>
                <th>DescMaquina</th>
                <th>DescPlanta</th>
                <th>DescPunzon</th>
                <th>Peso_Promedio</th>
                <th>Muestra_ControlCalidad</th>
                <th>Muestra_ValidacionEstabilidad</th>-->
            </tr>
        
            
        
        </thead>
        <tbody>";
        $html .= $html2;
        
       /* if($consulta!=FALSE)
        //if(count($consulta)>0)
	    	{			
		    	$i=0;
		    	foreach ($consulta as $cons)
		    	{
		    	 $i++;
		    	 if($i%2==0)		//Uso este código para colorear las filas pares
		    	  			 $html .= "<tr style='background-color:#D3D3D3;'>";
		    	  			 //$html .= "<tr style='background-color:#ddd;'>";
		    	  		else $html .= "<tr>";	
		    	 
				 $html .= "
				 				<td>".$i."</td>
		                        <td>".$cons->IdProdElab."</td>
		                        <td>".$cons->CodProdElab."</td>
		                        <td>".$cons->DescProdElab."</td>
		                        <td>".$cons->DescMaquina."</td>
		                        <td>".$cons->EstadoProdElab." : ".$ot."</td>
		                    </tr>";
				 
					
				}
			}	
	 	else $html .= "<tr><td colspan='10'><center>No existen datos</center></td></tr>"; */

      $html .=" 
       </tbody>
    </table>
    
</div>
<br>
<br>
<!--<div><h3>SOPS-MAR-0431 / SOPS-MAR-0722</h3></div>-->

</html>";
	
	
	$mpdf->debug = true;
	
	//$stylesheet = file_get_contents('css/bootstrap.css');
	/*Cargo una variable con el contenido del Footer*/
	//$Footer = "<div align=\"center\"><span>Página {PAGENO} de {nbpg}</span></div>";
	$Footer = "<div><span><strong>SOPS-MAR-0431 / SOPS-MAR-0722<strong></span><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Página {PAGENO} de {nbpg}</span></div>";
	//$Footer = "<div align=\"center\"><span>Página {PAGENO}</span></div>";
	$mpdf->SetHTMLFooter($Footer);
	
	$mpdf->WriteHTML($stylesheet, 1);
	$mpdf->WriteHTML($html,2);
	//$mpdf->WriteHTML($textos_juntos,2);
	//$mpdf->Output('C:/instaladores/invoice.pdf', 'I');
	$mpdf->Output('Datos Conciliacion ('.$Fecha_Actual.' a las '.$Hora_Actual.').pdf', 'I');
	//$mpdf->Output();
    
  
  }
}?>
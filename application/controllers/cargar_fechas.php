<?
/* Heredamos de la clase CI_Controller */
class Cargar_fechas extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
    
    $this->load->model('Model_gestion_ot');
    
    /*Cargar libreria donde se encuentra la validacion de la fecha*/
    //$this->load->library('usuariolib');
     
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('cargar_fechas/admincargar_fechas');
  }
 
  
  function admincargar_fechas()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('ot');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Fechas');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    
    //$crud->set_relation('IdProdWS','productosws','{CodProdWS} - {DescProdWS}');
    //$crud->set_relation('IdVaP','vap','DescVaP');

    
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    /*$crud->display_as('IdMaquinaDefecto','Maquina por Defecto');  
    $crud->display_as('IdTipoProd','Tipo de Producto');  
    $crud->display_as('IdPasoProd','Paso del Producto');  */
    
    $crud->display_as('NumOT','Nro de OT'); 
    $crud->display_as('Fecha_Visado','Fecha de Visado');
    $crud->display_as('Fecha_Liberado','Fecha de Liberado');
    $crud->display_as('Usuario_Visado','Usuario que Visa');
    $crud->display_as('Usuario_Liberado','Usuario que Libera');
    
    //$crud->display_as('IdVaP','VaP');
    //$crud->display_as('IdProdWS','C&oacute;digo - Producto');
        
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    /*$crud->required_fields(
      'IdOT',
      'IdSector',
      'Observacion_Entrega'
    );*/
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'NumOT',
      'Fecha_Visado',
      'Fecha_Liberado',
      'Usuario_Visado',
      'Usuario_Liberado',
      'Caja'      
    );
 
 	//$crud->callback_before_insert(array($this,'valida_caja'));
 
 	//$crud->set_rules('Fecha_Visado','Fecha de Visado','callback_valida_fecha');
 	//$crud->set_rules('Caja','caja','callback_valida_caja');
 	 	
 	//$crud->set_rules('Fecha_Liberado','Fecha de Liberado','callback_valida_fecha');
 
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	//$crud->add_fields('IdOT','IdSector','Observacion_Entrega');
    
   //$crud->callback_field('IdSector',array($this,'prueba'));
   
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('NumOT','Fecha_Visado','Fecha_Liberado','Usuario_Visado','Usuario_Liberado','Caja');
    $state_code = $crud->getState();
   	if($state_code == 'edit') 
	   		{
	        $crud->field_type('NumOT', 'readonly');
	        $crud->field_type('Usuario_Visado', 'hidden');
	        $crud->field_type('Usuario_Liberado', 'hidden');
	        //$crud->change_field_type('NumOT', 'disabled');
	  		}
	
	//Validamos para que el campo Caja sea solamente entero 
	$crud->set_rules('Caja','Caja','trim|integer');
	
	
	//Validamos que las fechas cargadas no sean anteriores a la actual
	$crud->set_rules('Fecha_Visado','Fecha de Visado','callback_valida_fecha'); 
	$crud->set_rules('Fecha_Liberado','Fecha de Liberado','callback_valida_fecha'); 
	
	//$crud->set_rules('Tiempo_Ejecucion','Tiempo de Ejecucion','trim|callback_decimal_numeric');
	
	  	
    //Llama a la funcion para verificar si cambiaron las fechas y almacenar el usuario que realiz� las modificaciones
    $crud->callback_before_update(array($this,'carga_datos_edit'));
    
    
    //Almacena el log despues de editar las fechas
	$crud->callback_after_update(array($this, 'log_editar_fechas'));
    
     
    //Deshabilito operaciones        
    $crud->unset_add();
    $crud->unset_delete();
    $crud->unset_print();
    $crud->unset_export();		
				       
   
 
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('cargar_fechas/admincargar_fechas', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }
 
 
 
 //
 function carga_datos_edit($post_array, $primary_key) 
	{	
		//Consulto las fechas que tengo cargadas en la base de datos
		$fecha_visado = $this->Model_gestion_ot->obtiene_fecha_visado($primary_key);
	 	$fecha_liberado = $this->Model_gestion_ot->obtiene_fecha_liberado($primary_key);
		
		//Preguntamos si la fecha de visado es distinto de null para cargar el usuario 
	 	if($post_array['Fecha_Visado'] != $fecha_visado)
	 			{
				 //Obtenemos el usuario que realiza el visado de la session 
			 	 $usuario_visado = $this->session->userdata('Usuario');
			 	 $post_array['Usuario_Visado'] = $usuario_visado;	
				}
		
		//Preguntamos si la fecha de liberado es distinto de null para cargar el usuario 
		if($post_array['Fecha_Liberado'] != $fecha_liberado)
	 			{
				 //Obtenemos el usuario que realiza el visado de la session 
			 	 $usuario_liberado = $this->session->userdata('Usuario');
			 	 $post_array['Usuario_Liberado'] = $usuario_liberado;	
				}
	 	
	 return $post_array;
	}


//Funcion para almacenar el log al editar
function log_editar_fechas($post_array, $primary_key) 
	{	
	 $consulta = $this->Model_gestion_ot->graba_log_ot_editar_fechas($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
		
	}
 
 
 
 //Funcion para validar que la fecha cargada sea posterior a la fecha actual
function valida_fecha($fecha_ingresada)
{
 $fecha_actual = date("d-m-Y");
 if($fecha_ingresada != '')
		 {
		 	if ($fecha_ingresada >= $fecha_actual) //Use your logic to check here
		        {
		            return TRUE;
		        }
		        else
		        {
		        	//$this->form_validation->set_message('decimal_numeric', '%s debe contener un valor entero o decimal (con .)');
		        	$this->form_validation->set_message('valida_fecha', 'La %s no debe ser anterior a la fecha actual');
		            return FALSE;
		            
		        }
		  }	
	
}
 
  
  
}
?>
<?
/* Heredamos de la clase CI_Controller */
class Usuario extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
 	/*Llamamos al modelo para hacer las consultas a la base de datos*/
 	$this->load->model('Model_gestion_usuario');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
    
    //Libreria para trabajar con password
    $this->load->library('usuariolib');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('usuario/adminusuario');
  }
 
  
  function adminusuario()
  {
  	//Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
  	
  	//Valida el nivel del usuario
  	if($this->session->userdata('Nivel') == 0)
 		{//Inicio del if de nivel de usuario
    
    try{
		
	    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('usuario');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Usuario');
    
    
    /*Relacion con la Tabla sectorusuario*/
    $crud->set_relation('IdSector','sectorusuario','DescSector');
        
      
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    $crud->display_as('IdSector','Sector');
    $crud->display_as('EstadoUsuario','Estado');
            
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'IdUsuario',
      'Usuario',
      //'Password',
      'Nivel',
      'IdSector'
      //'EstadoUsuario'
      );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'Usuario',
      'Nivel',
      'IdSector',
      'EstadoUsuario'
    );
 
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('Usuario','Nivel','IdSector');
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('Usuario','Nivel','IdSector');
 	
 	//Seteamos los valores del select de Nivel para que me permita seleccionar solamente los valores 
 	//$crud->field_type('Nivel','enum',array('0','1','2','3'));
 	
 	//Armamos el select para el nivel de usuario
 	$crud->field_type('Nivel','dropdown',array('0' => '0 - Administrador del Sistema', '1' => '1 - Administrativo','2' => '2 - Administrativo OT' , '3' => '3 - Consulta'));
 	
 	//De esta forma hago que la variable sea de tipo password
 	//$crud->field_type('Password', 'password');
 	
 	//Cambio el texto de los botones de Cambiar Password
 	//$crud->set_lang_string('form_save_and_go_back','Cancelar');
    //$crud->set_lang_string('form_save','Guardar');
    $crud->set_lang_string('form_insert_loading','');
 	
 	/*Llama a la funcion para hacer la validacion de los campos de la subetapa antes de hacer un insert*/
  	//$crud->callback_before_insert(array($this,'valida_campos_subetapa_add'));
  	
 	/*Llama a la funcion para hacer la validacion de los campos antes de hacer el editar*/ 	 	
  	//$crud->callback_before_update(array($this,'valida_campos_subetapa_edit'));
 	
 	//Llama a las funciones para trabajar con Password
 	/*$crud->callback_before_insert(array($this,'encrypt_password_callback'));
	$crud->callback_before_update(array($this,'encrypt_password_callback'));
	$crud->callback_edit_field('Password',array($this,'decrypt_password_callback'));*/
	
 	//$crud->callback_before_insert(array($this,'convertir_pass'));
 	
 	//Llama a la funcion para almacenar el log despues de insertar
 	$crud->callback_after_insert(array($this, 'log_usuario_add'));
	
	//Llama a la funcion para almacenar el log despues de editar
	$crud->callback_after_update(array($this, 'log_usuario_edit'));
 	
 	//Cambio el texto del boton Eliminar y los mensajes que se muestran al cambiar de estado un registro
 	$crud->set_lang_string('list_delete','Cambiar Estado');
	$crud->set_lang_string('alert_delete','Esta seguro que quiere cambiar el estado al Usuario?');
	$crud->set_lang_string('delete_success_message','El estado del usuario se cambio exitosamente');
	
 	
 	//Cambiamos el estado del usuario
 	$crud->callback_delete(array($this,'cambia_estado_usuario'));
 	
 	/*Llamada a la funcion para almacenar en el log cuando se inserta*/
  	//$crud->callback_after_insert(array($this, 'graba_log_subetapa_add'));		//Funcion que se llama despues de insertar
 
 	/*Llamada a la funcion para almacenar en el log cuando se edita*/
  	//$crud->callback_after_update(array($this, 'graba_log_edit'));		//Funcion que se llama despues de editar
 	
 	/*Ocultamos los botomes Imprimir y Exportar*/
 	$crud->unset_print();
 	$crud->unset_export();
 	//$crud->unset_delete();
 	$crud->unset_read();		//Ocultamos la opcion ver para que no no muestre el campo del password
 	
 	
 	
 	/*$state = $crud->getState();
	if($state == 'add')
				$crud->set_lang_string('insert_error','Error al Insertar, verifique la Descripci&oacute;n');
		else if($state == 'edit')
				        {
						$crud->set_lang_string('update_error','Error al Editar, verifique la Descripci&oacute;n');
						/*Lo pongo en invisible pero lo utilizo para realizar la busqueda al momento de editar los campos*/
					/*	$crud->field_type('IdSubetapa','hidden');	
						}
 */
 
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('usuario/adminusuario', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  	}//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  	
  	}//Fin del if de validaci�n de usuario 
  else redirect('');
  	
  }
  
  
 /* 
//Funcion para encriptar el Password  
function encrypt_password_callback($post_array, $primary_key = null)
{
$this->load->library('encrypt');
 
$key = 'super-secret-key';
$post_array['Password'] = $this->encrypt->encode($post_array['Password'], $key);
return $post_array;
}

//Funcion para desencriptar el Password 
function decrypt_password_callback($value)
{
$this->load->library('encrypt');
 
$key = 'super-secret-key';
$decrypted_password = $this->encrypt->decode($value, $key);
return "<input type='Password' name='Password' value='$decrypted_password' />";
}
*/  



/*function convertir_pass($post_array)
{
 //Llamo a la funcion que tengo en la libreria para encriptar el password antes de guardarlo
 $pass_conv = $this->usuariolib->codificar_pw($post_array);
 
 $post_array['Password'] = $pass_conv;
 
 return $post_array;
 	
}*/


/*
//Funciones para trabajar con password
function hmac($key, $data, $hash = 'md5', $blocksize = 64) 
{
 if (strlen($key)>$blocksize) {
    	$key = pack('H*', $hash($key));
	}
 $key  = str_pad($key, $blocksize, chr(0));
 $ipad = str_repeat(chr(0x36), $blocksize);
 $opad = str_repeat(chr(0x5c), $blocksize);
 return $hash(($key^$opad) . pack('H*', $hash(($key^$ipad) . $data)));
}


function codificar_pw($post_array) 
{
 mt_srand(microtime()*1000000);
 $semilla = substr('00' . dechex(mt_rand()), -3) .
 substr('00' . dechex(mt_rand()), -3) .
 substr('0' . dechex(mt_rand()), -2);
 $post_array['Password'] = $this->hmac($semilla, $post_array['Password'], 'md5', 64).$semilla;
 return $post_array;
}  
  
  */

/*Funcion que llama al modelo para cambiar el estado de un usuario*/
//Si esta Habilitado lo pasa a Deshabilitado y a la inversa
function cambia_estado_usuario($primary_key)
{
 /*Obtengo el estado de un usaurio para determinar que cambio de estado hago*/
 $cons_estado = $this->Model_gestion_usuario->obtiene_estado_usuario($primary_key);	
 if($cons_estado == 'Habilitado')
 		 $consulta = $this->Model_gestion_usuario->cambia_est_usuario_deshab($primary_key);	
 	else if($cons_estado == 'Deshabilitado')
 			$consulta = $this->Model_gestion_usuario->cambia_est_usuario_hab($primary_key);			 	
 if($consulta == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;	
}

  
  
//Llama a la funcion para almacenar el log cuando inserto
function log_usuario_add($post_array, $primary_key) 
{
 $consulta = $this->Model_gestion_usuario->graba_log_usuario_add($post_array, $primary_key);		
   	if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;

}   
  
  
//Llama a la funcion para almacenar el log cuando edito
function log_usuario_edit($post_array, $primary_key) 
{
 $consulta = $this->Model_gestion_usuario->graba_log_usuario_edit($post_array, $primary_key);		
   	if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;

}  
  
  
}
?>
<?
/* Heredamos de la clase CI_Controller */
class Punzon extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
          
    /* Cargamos la base de datos */
    $this->load->database();
    
    $this->load->model('Model_gestion_punzon');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
    
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('punzon/adminpunzon');
  }
 
  
  function adminpunzon()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    	
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
   $crud = new grocery_CRUD();
     
     //Muestro en la lista solamente los Punzones que estan con estado Habilitado
     $crud->where('EstadoPunzon','Habilitado');
      
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('punzon');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Punz&oacute;n');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
          
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    
    $crud->display_as('DescPunzon','Descripci&oacute;n de Punz&oacute;n');
    $crud->display_as('Fecha_Ingreso','Fecha de Ingreso');
    $crud->display_as('Fecha_Egreso','Fecha de Egreso');
    $crud->display_as('EstadoPunzon','Estado');    
       
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'DescPunzon',
      'EstadoPunzon'      
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    /*$crud->columns(
      'IdPunzon',
      'DescPunzon',
      'Fecha_Ingreso',
      'Fecha_Egreso',
      'EstadoPunzon'
    );
    $crud->where('EstadoPunzon', 'Habilitado');*/
    
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('DescPunzon','Fecha_Ingreso');
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('IdPunzon','DescPunzon','Fecha_Ingreso');
 
 	/*Determina los valores que va a tomar el Estado de Producto*/
 	//$crud->field_type('EstadoPunzon','enum',array('Habilitado','Deshabilitado'));
 	
 	$crud->set_rules('EstadoPunzon','Estado de Punz&oacute;n','trim|required|min_length[2]');
 	
 	$crud->callback_before_insert(array($this,'valida_campos_punzon'));
 	 	
 	
 	$i=0;			// Variable de session que me indica el nivel de usuario
 					// El 0 representa el nivel mas alto
 	if ($i == 1)
 			 {
			  $crud->field_type('EstadoPunzon','invisible');
			  //$crud->set_lang_string('list_delete','Eliminar');
			  $crud->unset_delete();
			  //$crud->set_lang_string('delete_success_message','El Producto fue eliminado exitosamente.');
			  /*Si el usuario es de nivel inferior no me muestra solo los punzones habilitados y no me muestra el estado*/
			  $crud->columns(
						      'DescPunzon',
						      'Fecha_Ingreso',
						      'Fecha_Egreso'
						    );
			   $crud->where('EstadoPunzon', 'Habilitado');
			  	
			 } 			 
		else {
			  $crud->field_type('EstadoPunzon','enum',array('Habilitado','Deshabilitado'));
			  $crud->set_lang_string('list_delete','Dar de Baja');
			  $crud->set_lang_string('delete_success_message','El Punz&oacute;n se dio de baja exitosamente');
			  
			  /*Si el usuario es de nivel superior me muestra todos los punzones*/
			  $crud->columns(
						      'DescPunzon',
						      'Fecha_Ingreso',
						      'Fecha_Egreso',
						      'EstadoPunzon'
						    );
			 
			 }
 	
 	/*Llama a la funcion para hacer la validacion de los campos del punzon antes de hacer un insert*/
 	$crud->callback_before_insert(array($this,'valida_campos_punzon_add'));
 	
 	/*Llama a la funcion para hacer la validacion de los campos antes de hacer el editar*/ 	 	
  	$crud->callback_before_update(array($this,'valida_campos_punzon_edit'));
 	
 	
 	$crud->callback_delete(array($this,'elimina_punzon'));				//Llama a la funcion que cambia el estado del punzon y almacena la fecha actual en la fecha de egreso
 	
 	/*Llamada a la funcion para almacenar en el log cuando se inserta*/
  	$crud->callback_after_insert(array($this, 'graba_log_add'));		//Funcion que se llama despues de insertar
 	
 	/*Llamada a la funcion para almacenar en el log cuando se edita*/
  	$crud->callback_after_update(array($this, 'graba_log_edit'));		//Funcion que se llama despues de editar
 
 	//Cambio el mensaje cuando cambio de estado un punzon (elimino)
 	$crud->set_lang_string('alert_delete','Esta seguro que quiere Deshabilitar el Punzon?');
 
 	$state = $crud->getState();
	if($state == 'add')
				$crud->set_lang_string('insert_error','Error al Insertar, verifique que el Punz&oacute;n no se encuentre cargado');
		else if($state == 'edit')
				        {
						$crud->set_lang_string('update_error','Error al Editar, verifique que el Punz&oacute;n no se encuentre cargado');
						/*Lo pongo en invisible pero lo utilizo para realizar la busqueda al momento de editar los campos*/
						$crud->field_type('IdPunzon','hidden');	
						}
 
 
 	$crud->unset_print();
	$crud->unset_export();
 
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('punzon/adminpunzon', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }
  
  
  /*Llama al modelo para realizar la validacion de los campos del punzon antes de insertar*/
  function valida_campos_punzon_add($post_array) 
   { 
	 $msj = $this->Model_gestion_punzon->consulta_campos_punzon_add($post_array);
	  
	  /*Si desde el modelo me retorna false, retornamos false para no hacer de nuevo la carga del mismo punzon*/	  
	  if($msj == FALSE)
	  			 	return FALSE;	  			 
	  		else { /*Antes de asignar los valores les elimina los espacios en blanco delante y al final de la cadena*/ 
             	   $post_array['DescPunzon'] = trim($post_array['DescPunzon']);
             	   return $post_array;         
             	  }	  	
	}
  
  
 /*Llama al modelo para realizar la validacion de los campos del punzon despues de editarlos*/
  function valida_campos_punzon_edit($post_array, $primary_key) 
   { 
	 $msj = $this->Model_gestion_punzon->consulta_campos_punzon_edit($post_array, $primary_key);
	  
	  /*Si desde el modelo me retorna false, retornamos false para no hacer de nuevo la carga del mismo producto*/	  
	  if($msj == FALSE)
	  			 	return FALSE;	  			 
	  		else { /*Antes de asignar los valores les elimina los espacios en blanco delante y al final de la cadena*/ 
             	   $post_array['DescPunzon'] = trim($post_array['DescPunzon']);
             	   return $post_array;         
             	  }	  	
	}
  
 
 /*Funcion que llama al modelo para cambiar el estado de un prodcuto*/
	function elimina_punzon($primary_key)
	{
	 /*Asigno a una variable la fecha actual*/
	 //$fecha = time();
	 //$fecha_conv = date("Y-m-d", $fecha);
	 $fecha = date('Y-m-d');
	 $consulta = $this->Model_gestion_punzon->cambia_estado_punzon($primary_key, $fecha); 	
	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;	
	}
 
 
  /*Funcion que llama al modelo para insertar los registros del log cuando hago un insertar*/
   function graba_log_add($post_array, $primary_key)
   {   	
   	$consulta = $this->Model_gestion_punzon->graba_log_punzon_add($post_array, $primary_key);		if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
   }
 
 
   /*Funcion que llama al modelo para insertar los registros del log cuando hago un editar*/
   function graba_log_edit($post_array, $primary_key)
   {   	
   	$consulta = $this->Model_gestion_punzon->graba_log_punzon_edit($post_array, $primary_key);		if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
   }  
    
}
?>
<?
/* Heredamos de la clase CI_Controller */
class Consulta_totales extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
          
    /* Cargamos la base de datos */
    $this->load->database();
    
    $this->load->model('Model_gestion_consulta_totales');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
    
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  
    ini_set('memory_limit', '-1');
    ini_set("pcre.backtrack_limit","1000000");
    ini_set("max_execution_time","0");
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('consulta_totales/adminconsulta_totales');
  }
 
  
  function adminconsulta_totales()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2)||($this->session->userdata('Nivel') == 3))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
   $crud = new grocery_CRUD();
      
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('datosot');
 
    /* Le asignamos un nombre */
    //$crud->set_subject('Consulta Totales');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
          
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    //$crud->set_relation('IdProdElab','prodelaboracion','{CodProdElab} - {DescProdElab}');
    $crud->set_relation('IdMaquina','maquina','DescMaquina');
      
    $crud->display_as('IdProd','Producto');
    $crud->display_as('Fecha_Desde','Desde Fecha de Carga');
    $crud->display_as('Fecha_Hasta','Hasta Fecha de Carga');
    $crud->display_as('IdMaquina','Maquina');
    
    //Variables tipo fecha
    $crud->field_type('Fecha_Desde', 'date');
    $crud->field_type('Fecha_Hasta', 'date');
       
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /*Cambios en el lenguaje para cambiar la intarface de la pantalla*/
 	$crud->set_lang_string('form_add','Seleccione los filtros de busqueda');
 	$crud->set_lang_string('list_record','');
    
    /* 	Se cambio el Boton Guardar por Generar PDF
 		Se cambio el Boton Guardar y volver a la lista por Cancelar
 	*/
 	$crud->set_lang_string('form_save','Generar PDF');
 	$crud->set_lang_string('form_save_and_go_back','Cancelar');
 	//$crud->set_lang_string('form_save','Cancelar');
 	$crud->set_lang_string('form_insert_loading','');
 	//$crud->set_lang_string('insert_success_message','Listado Generado Exitosamente. <div style="display:none">');
 	$crud->set_lang_string('insert_success_message','<div style="display:none">');
    
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    /*$crud->required_fields(
      'DescPunzon',
      'EstadoPunzon'      
    );*/
 
    /* Aqui le indicamos que campos deseamos mostrar */
    /*$crud->columns(
      'IdPunzon',
      'DescPunzon',
      'Fecha_Ingreso',
      'Fecha_Egreso',
      'EstadoPunzon'
    );
    $crud->where('EstadoPunzon', 'Habilitado');*/
    
    //Obtengo los datos del los producto para mostrarlos en el select
    //Obtengo la consulta SQL en un arreglo
     $array1_prod = $this->Model_gestion_consulta_totales->obtiene_IdProd();
     //Obtengo la consulta SQL en un arreglo
     $array2_prod = $this->Model_gestion_consulta_totales->obtiene_datos_prod();
	 //Combion los dos arreglos para armar el select
	 $array3_prod = array_combine($array1_prod, $array2_prod);
	 $crud->field_type('IdProd','dropdown',$array3_prod);
    
    
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('IdProd','Fecha_Desde','Fecha_Hasta','Lote','IdMaquina'); 	
 
 
 	//Llama a la funcion para que no haga el insert por defecto
 	$crud->callback_insert(array($this,'no_insert'));
 
 	//$crud->unset_add();
	$crud->unset_edit();
 	$crud->unset_print();
	$crud->unset_export();
 
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('consulta_totales/adminconsulta_totales', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }


//Funcion para que no haga el insert por defecto
function no_insert($post_array)
{	
 return TRUE;
}	 


function Buscar_datos_totales()
{
 $IdProd = $this->input->post('ajax_data[IdProd]');
 $Fecha_Desde = $this->input->post('ajax_data[Fecha_Desde]');
 $Fecha_Hasta = $this->input->post('ajax_data[Fecha_Hasta]');
 $Lote = $this->input->post('ajax_data[Lote]');
 $IdMaquina = $this->input->post('ajax_data[IdMaquina]');
 
 //Almaceno los valores por los que busco en variable de session para poder mostrarlos en el pdf
 $consulta_datos = $this->Model_gestion_consulta_totales->trae_info_pdf($IdProd, $IdMaquina);
 
 $datos_totales=  array(
		'IdProd' => $consulta_datos['DescProd'],		
		'Fecha_Desde' => $Fecha_Desde,
		'Fecha_Hasta' => $Fecha_Hasta,
		'Lote' =>  $Lote,
		'DescMaquina' => $consulta_datos['DescMaquina']
		
		); 
	$this->session->set_userdata($datos_totales);
  
  if(($IdProd == '')&&($Fecha_Desde == '')&&($Fecha_Hasta == '')&&($Lote == '')&&($IdMaquina == ''))
  	 		{						
  	 		$consulta = "Campos Vacios";
  	 		echo json_encode($consulta);	
  	 		 
  	 		}
  	  else {
			  //echo $textoBusqueda;
			  $consulta = $this->Model_gestion_consulta_totales->Buscar_totales_pdf($IdProd, $Fecha_Desde, $Fecha_Hasta, $Lote, $IdMaquina); 
			   
			   if($consulta != FALSE)
			    		 //return json_encode($consulta);
			    		 echo json_encode($consulta);    		 
			    	else {
			    		 	$consulta = "Consulta Vacia";
			    		 	echo json_encode($consulta);    		 
			    		 }
		    }	
  	
}
  
  
}
?>
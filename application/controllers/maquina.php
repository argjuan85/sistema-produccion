<?
/* Heredamos de la clase CI_Controller */
class Maquina extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
    
    $this->load->model('Model_gestion_maquina');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  	//$this->form_validation->set_message('is_unique', 'Ya se encuentra cargado el registro');
  	
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('maquina/adminmaquina');
  }

  
  function adminmaquina()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if($this->session->userdata('Nivel') == 0)
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('maquina');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Maquina');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    /*Relacion con la Tabla Etapa*/
    $crud->set_relation('IdEtapa','Etapa','DescEtapa');
    
    /*Relacion con la Tabla Planta*/
    $crud->set_relation('IdPlanta','Planta','DescPlanta');
     
    /*Para el campo Planta permite elegir uno entre dos valores*/
    //$crud->field_type('Planta','enum',array('PPFS','PPFG'));    
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
              
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');         
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'DescMaquina',
      'IdEtapa',
      'IdPlanta',
      'EstadoMaquina'
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'DescMaquina',
      'CodMaq',
      'IdEtapa',
      'IdPlanta',
      'EstadoMaquina'
    );
 
  	  
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('DescMaquina','CodMaq','IdEtapa','IdPlanta');
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('IdMaquina','DescMaquina','CodMaq','IdEtapa','IdPlanta');
    
    
    $i=0;			// Variable de session que me indica el nivel de usuario
 					// El 0 representa el nivel mas alto
 	if ($i == 1)
 			 {
			  $crud->field_type('EstadoMaquina','invisible');
			  $crud->set_lang_string('list_delete','Eliminar');
			  $crud->set_lang_string('delete_success_message','La Maquina fue eliminada exitosamente.');
			  	
			 } 			 
		else {
			  $crud->field_type('EstadoMaquina','enum',array('Habilitada','Deshabilitada'));
			  $crud->set_lang_string('list_delete','Cambiar Estado');
			  $crud->set_lang_string('delete_success_message','El estado de la Maquina se cambio exitosamente'); 		
			 }
    
    
    /*Detarmina los valores que va a tomar el Estado de la Maquina*/
 	//$crud->field_type('EstadoMaquina','enum',array('Habilitada','Deshabilitada'));
 
 	//$crud->set_rules('DescMaquina','Descripcion','trim');
 	$crud->set_rules('DescMaquina','Descripci&oacute;n de Maquina','trim|required|min_length[3]');
 	
 	/*Llama a la funcion para hacer la validacion de los campos antes de  */
 	$crud->callback_before_insert(array($this,'valida_campos_maquina_add')); 
 	
 	/*Llama a la funcion para hacer la validacion de los campos antes de hacer el editar*/ 	 	
  	$crud->callback_before_update(array($this,'valida_campos_maquina_edit')); 
 
 	$crud->display_as('DescMaquina','Descripci&oacute;n de Maquina');
 	$crud->display_as('CodMaq','C&oacute;digo de Maquina');
    $crud->display_as('IdEtapa','Etapa'); 
    $crud->display_as('IdPlanta','Planta');
    $crud->display_as('EstadoMaquina','Estado');
 
 	/*Llamada a funciones para almacenar en el log*/
  	$crud->callback_after_insert(array($this, 'graba_log_add'));		//Funcion que se llama despues de insertar
 
 	/*Llamada a funciones para almacenar en el log*/
  	$crud->callback_after_update(array($this, 'graba_log_edit'));		//Funcion que se llama despues de insertar
 
	 if ($i == 1)
		  		$crud->callback_delete(array($this,'elimina_maquina1'));				//Llama a la funcion Eliminar, que en realidad hace un cambio de estado
		else	$crud->callback_delete(array($this,'elimina_maquina2'));
 
 
 	/*Segun el estado en el que se encuentre, modifica los mensajes que me va a mostrar*/
	$crud->set_lang_string('alert_delete','�Esta seguro que quiere cambiar el estado de la Maquina?');
	$state = $crud->getState();
	if($state == 'add')
				$crud->set_lang_string('insert_error','Error al Insertar, verifique si la Descripci&oacute;n de la Maquina ya se encuentra cargada');
		else if($state == 'edit')
				        {
						$crud->set_lang_string('update_error','Error al Editar, verifique si la Descripci&oacute;n de la Maquina ya se encuentra cargada');
						/*Lo pongo en invisible pero lo utilizo para realizar la busqueda al momento de editar los campos*/
						$crud->field_type('IdMaquina','hidden');	
						}
				
 	
 	$crud->unset_print();
	$crud->unset_export();
 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('maquina/adminmaquina', $output);
   $this->load->view('footer');
   
   //$result = $crud->callback_before_insert(array($this,'consulta_maquina'));
    
 	        
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }
  
  
  /*Llama al modelo para realizar la validacion de los campos de al maquina antes de insertarlos*/
  function valida_campos_maquina_add($post_array) 
   { 
	 $msj = $this->Model_gestion_maquina->consulta_campos_maquina_add($post_array);
	  
	  /*Si desde el modelo me retorna false, retornamos false para no hacer de nuevo la carga de la misma maquina (es decir con la misma descripci�n)*/	  
	  if($msj == FALSE)
	  			 return FALSE;	  			 
	  		else {
	  			  //Si el codigo de la maquina esta cargado convierto las letras a mayusculas
	  			  if($post_array['CodMaq'] != '')
	  			  			$post_array['CodMaq'] = strtoupper($post_array['CodMaq']);
	  			  
	  			  $post_array['DescMaquina'] = trim($post_array['DescMaquina']);
             	  return $post_array;
	  			 }	
	}
 
 
 /*Llama al modelo para realizar la validacion de los campos de la maquina despues de editados*/
  function valida_campos_maquina_edit($post_array) 
   { 
	 $msj = $this->Model_gestion_maquina->consulta_campos_maquina_edit($post_array);
	  
	  /*Si desde el modelo me retorna false, retornamos false para no hacer de nuevo la carga del mismo producto*/	  
	  if($msj == FALSE)
	  			 	return FALSE;	  			 
	  		else { /*Antes de asignar los valores les elimina los espacios en blanco delante y al final de la cadena*/ 
             	   //Si el codigo de la maquina esta cargado convierto las letras a mayusculas
	  			  if($post_array['CodMaq'] != '')
	  			  			$post_array['CodMaq'] = strtoupper($post_array['CodMaq']);
             	   
             	   $post_array['DescMaquina'] = trim($post_array['DescMaquina']);
             	   return $post_array;         
             	  }	  	
	}
 
 
   /*Funcion que llama al modelo para insertar los registros del log cuando hago un insertar*/
   function graba_log_add($post_array, $primary_key)
   {   	
   	$consulta = $this->Model_gestion_maquina->graba_log_maq_add($post_array, $primary_key);		
   	if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
   }
 
 
 	/*Funcion que llama al modelo para insertar los registros del log cuando hago un editar*/
   function graba_log_edit($post_array, $primary_key)
   {   	
   	$consulta = $this->Model_gestion_maquina->graba_log_maq_edit($post_array, $primary_key);		
   	if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
   } 
   
 
 /*Funcion que llama al modelo para eliminar una maquina (Cambia el estado a Deshabilitada)*/
	function elimina_maquina1($primary_key)
	{
	 $consulta = $this->Model_gestion_maquina->cambia_estado_deshab($primary_key);	
	 	 	 	
	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;	
	}
  
  
  /*Funcion que llama al modelo para cambiar el estado de una maquina*/
	function elimina_maquina2($primary_key)
	{
	 /*Obtengo el estado de una maquina para determinar el cambio de estado que hago*/
	 $cons_estado = $this->Model_gestion_maquina->obtiene_estado_maq($primary_key);	
	 if($cons_estado == 'Habilitada')
	 		 $consulta = $this->Model_gestion_maquina->cambia_estado_deshab($primary_key);	
	 	else if($cons_estado == 'Deshabilitada')
	 			$consulta = $this->Model_gestion_maquina->cambia_estado_hab($primary_key);		
	 	 	
	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;	
	}
  
  
  
}
?>
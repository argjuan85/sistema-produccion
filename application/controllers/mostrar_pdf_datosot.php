<?/* Heredamos de la clase CI_Controller */

class Mostrar_pdf_datosot extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
  
 	$this->load->model('Model_gestion_mostrar_datos_ot');
 	 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
     
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  
  	ini_set('memory_limit', '-1');
    ini_set("pcre.backtrack_limit","1000000");
    ini_set("max_execution_time","0");
  
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('mostrar_pdf_datosot/admin_mostrar_pdf_datosot');
  }
 
  
  function admin_mostrar_pdf_datosot()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2)||($this->session->userdata('Nivel') == 3))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    //$crud = new grocery_CRUD();
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    
    $crud->set_table('datosot');
    //$crud->set_table('datosot','maquina','subetapa','etapa');
     
    /* Le asignamos un nombre */ 
    //$crud->set_subject('Buscar Datos de OT'); 		//Este nombre es el que va a aparecer en el formulario, en el boton a�adir, editar, etc
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/     
     
    /*Relacion con la Tabla OT*/
    $crud->set_relation('IdOT','ot','NumOT');
    
    $crud->set_relation('IdSubEtapa','subetapa','DescSubEtapa');
    
    
    /*Relacion con la Tabla Producto Elaboracion*/
    
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC', 'IdProdElab' => 27));
    //$crud->field_type('IdProdElab','dropdown',array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));
    //$crud->order_by('CodProdElab','desc');
    
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}');
    
    /*Se muestran todas las maquinas para poder emitir un listado con las maquinas con estado DI*/
    
    //Armamos el select de productows
    //Obtengo la consulta SQL en un arreglo
    $array1_prodws = $this->Model_gestion_mostrar_datos_ot->obtiene_idprodws();
    //Obtengo la consulta SQL en un arreglo
    $array2_prodws = $this->Model_gestion_mostrar_datos_ot->obtiene_desc_prodws();
    //Combino los dos arreglos para armar el select
	$array3_prodws = array_combine($array1_prodws, $array2_prodws);
	$crud->field_type('IdProdElab','dropdown',$array3_prodws);
    
    
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}');
    
    //$crud->where('EstadoProdElab','AC');
    //$crud->or_where('IdProdElab',1);
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC'));
    //$crud->or_where('IdProdElab',1);
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('IdProdElab' => 27));    
    
    /*Hace la relaci�n para mostrar la maquina perteneciente a una la subetapa seleccionada*/
    //$crud->set_relation('IdEtapa','Etapa');
    
    /*Se muestran todas las maquians para mostrar el listado con las maquinas con estado DI*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina');
    
    $state = $crud->getState();
	$state_info = $crud->getStateInfo();
	
	/*if($state == 'list')
	{
	 $array_result = $this->Model_gestion_mostrar_datos_ot->obtiene_datos_maq_list();    
     $crud->field_type('IdMaquina','multiselect',$array_result);	
	}*/
    
    //Obtengo la consulta SQL en un arreglo
    //$array1 = $this->Model_gestion_mostrar_datos_ot->obt_val1_maquina();
    //Obtengo la consulta SQL en un arreglo
    //$array2 = $this->Model_gestion_mostrar_datos_ot->obt_val2_maquina();
        
    //Funcion para combinar dos arreglos (Crea un nuevo array, usando una matriz para las claves y otra para sus valores) Con el primer arreglo arma las claves y con el segundo arma los valores
    //$array3 = array_combine($array1, $array2);
    
	//$datos = array('campo1' => 'valor1', 'campo2' => 'valor2', 'campo3' => 'valor3');
    //$crud->field_type('IdMaquina','dropdown',array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));
    //$crud->field_type('IdMaquina','dropdown',$consulta2);
    //$crud->field_type('IdMaquina','dropdown',$array3);
    
    
	/*$crud->where('datosot.IdMaquina','maquina.IdMaquina');
	$crud->where('datosot.IdSubEtapa','subetapa.IdSubEtapa');
    $crud->where('maquina.IdEtapa','etapa.IdEtapa');
    $crud->where('subetapa.IdEtapa','etapa.IdEtapa');*/
          
       
    /*Se muestran todos los punzones para emitir listado con los punzones en estado DI*/
    $crud->set_relation('IdPunzon','Punzon','DescPunzon');   
    
    $crud->field_type('Parcial','enum',array('Si','No'));
    
          
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    
    /*Relacion con la Tabla Punzon*/
    //$crud->set_relation('IdPunzon','Punzon','DescPunzon');
    
    /*Relacion con la Tabla Maquina*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
      
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
   /* $crud->required_fields(
      'IdOT',
      'IdProdElab',
      'IdSubEtapa',
      'Cant_Teorica',
      'Cant_Real',
      'IdMaquina',
      'Parcial',
      'HParcial',
      'Retrabajo'
    );*/
 
    /* Aqui le indicamos que campos deseamos mostrar */
  $crud->columns(
      'IdOT',
      'IdProdws',
      'Lote',
      'IdSubEtapa',
      //'IdMaquina',
      'IdPunzon'      
    );
    
    /*Ocultamos los campos y botones que no queremos que el usuario vea o utilice*/
 	//$crud->unset_read();			//Deshabilita la funcion view
 	//$crud->unset_edit();			//Deshabilita la funcion edit
 	//$crud->unset_delete();			//Deshabilita la funcion delete
 	//$crud->unset_export();			//Deshabilita la funcion export
 	//$crud->unset_print();			//Deshabilita la funcion print
    $crud->unset_operations(); 		//Deshabilita las operaciones
    //$crud->unset_columns('IdOT','NumOT','Lote','FechaVto','IdVaP','IdProdWS');
     
 	/*Cambiar el Label de los campos*/
    $crud->display_as('IdOT','Nro de OT');
    $crud->display_as('IdProdws','Producto');
    $crud->display_as('IdSubEtapa','SubEtapa');
    //$crud->display_as('IdMaquina','Maquina');
    $crud->display_as('IdPunzon','Punz&oacute;n');
    
    
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	//$crud->add_fields('IdOT','IdProdElab','IdSubEtapa','IdMaquina','IdPunzon','Parcial');
      
       
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    /*$crud->edit_fields('IdOT','IdProdElab','IdSubEtapa','Cant_Teorica','Cant_Real','Cant_Muest_Productiva','Cant_Muest_No_Productiva','Tiempo_Preparacion','Tiempo_Ejecucion','Observacion_DatosOT','IdMaquina','IdPunzon','Parcial','HParcial','Retrabajo','Lote_Anterior','Observacion_Reproceso','Cantidad_Rotulos','RotuloCP','Peso_Promedio','Muestra_ControlCalidad','Muestra_ValidacionEstabilidad');*/
 	
 	$crud->callback_column('IdProdws',array($this,'columna_producto'));
 	$crud->callback_column('Lote',array($this,'columna_lote'));
 		
 	/*Cambios en el lenguaje para cambiar la intarface de la pantalla*/
 	//$crud->set_lang_string('form_add','Seleccione los filtros de busqueda');
 	//$crud->set_lang_string('list_record','');
 	
 	/* 	Se cambio el Boton Guardar por Generar PDF
 		Se cambio el Boton Guardar y volver a la lista por Cancelar
 	*/
 	//$crud->set_lang_string('form_save','Generar PDF');
 	//$crud->set_lang_string('form_save_and_go_back','Cancelar');
 	//$crud->set_lang_string('form_save','Cancelar');
 	//$crud->set_lang_string('form_insert_loading','');
 	//$crud->set_lang_string('insert_success_message','Listado Generado Exitosamente');
 	//$crud->unset_jquery();
 	//$crud->unset_jquery_ui();
 	
 	
 	
 	 //window.location = "../../mensaje/comentario";
 	//window.open("'.site_url(strtolower('pdf').'/'.strtolower('generar_pdf')).'");
 	/*Campos y botones que se ocultan*/
 	//$crud->unset_columns('IdOT','IdProdElab','IdSubEtapa','IdMaquina','IdPunzon','Parcial');
 	//$crud->unset_back_to_list();
 	
 	/*Funcion que reemplaza al insert, la utilizo para generar la lista*/
 	
 	//$crud->callback_insert(array($this,'genera_listado'));
 	//$crud->callback_insert(array($this,'genera_listado'));
 	
 	
 	//window.open("index.php/pdf/generar_pdf");
 	
 	//$crud->callback_after_insert(array($this, 'genera'));
 	
 	/*Llamo a una funcion para que despues de insertar me limpie los campos menos el campor del numero de orden*/
 	//$crud->callback_after_insert(array($this, 'limpiar_campos'));
 	
 	//$crud->field_type('DescPunzon', 'invisible'); 	 	 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('mostrar_pdf_datosot/admin_mostrar_pdf_datosot', $output);
   $this->load->view('footer');
   
   	/*$state = $crud->getState();
	if($state == 'ajax_list')
		{
			redirect('/pdf/generar_pdf','refresh');
		}
	   */
	  
	 	
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  	
  	}//Fin del if de validaci�n de usuario 
  else redirect('');
  	
  }


//Funcion que obtiene informaci�n para armar el pdf de datos de ot 
function Buscar_info()
{
   		
 //var_dump($this->input->post());
 //$IdProdElab = $this->input->post('IdProdElab');	
 $NumOT = $this->input->post('ajax_data[NumOT]');
 $CodDescProd = $this->input->post('ajax_data[CodDescProd]');
 $Lote = $this->input->post('ajax_data[Lote]');
 $DescSubEtapa = $this->input->post('ajax_data[DescSubEtapa]');
 //$DescMaquina = $this->input->post('ajax_data[DescMaquina]');
 $DescPunzon = $this->input->post('ajax_data[DescPunzon]');
 //$Parcial = $this->input->post('ajax_data[Parcial]');
 
  //echo $textoBusqueda;
   //$consulta = $this->Model_gestion_mostrar_datos_ot->Buscar_datos($IdOT, $IdProdElab, $IdSubEtapa,$IdMaquina, $IdPunzon, $Parcial); 
   //$consulta = $this->Model_gestion_mostrar_datos_ot->Buscar_datos_like($IdProdElab);
   $consulta = $this->Model_gestion_mostrar_datos_ot->Buscar_datos_ot_like($NumOT, $CodDescProd, $Lote, $DescSubEtapa, $DescPunzon);
   
   if($consulta != FALSE)
    		 //return json_encode($consulta);
    		 echo json_encode($consulta);    		 
    	else return FALSE;
	 	       
   
  	
}

//Carga en la columna con el C�digo y la Descricpi�n del producto
public function columna_producto($value, $row)
{
 //$row->IdOT
 $consulta = $this->Model_gestion_mostrar_datos_ot->Buscar_datos_producto($row->IdOT);
 
 return $consulta[0]->CodProdWS." - ".$consulta[0]->DescProdWS;
}

//Carga en la columna el lote
public function columna_lote($value, $row)
{
 //$row->IdOT
 $consulta = $this->Model_gestion_mostrar_datos_ot->Buscar_lote($row->IdOT);
 
 return $consulta[0]->Lote;
}




}
?>
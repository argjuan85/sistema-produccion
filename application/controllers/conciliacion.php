<?
/* Heredamos de la clase CI_Controller */
class Conciliacion extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
  	$this->load->library('session');
  	
 	$this->load->model('Model_gestion_conciliacion');
 	$this->load->model('Model_gestion_datos_ot');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
     
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('conciliacion/adminconciliacion');
  }
 
 

  
  function adminconciliacion()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
  	
  	$state = $crud->getState();
	 
	if($state == 'list')
		{
		$IdOT = $_GET['IdOT'];
		$crud->where('conciliacion.NumOT',$IdOT);
		
		//Almaceno en una variable de session el valor que envio para poder trabajarlo en todas las funciones
		$this->session->set_userdata('IdOT',$IdOT);
		
		}
  	
  	
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('conciliacion');
 
    /* Le asignamos un nombre */ 
    $crud->set_subject('Conciliaci&oacute;n'); 		//Este nombre es el que va a aparecer en el formulario, en el boton a�adir, editar, etc
    
    /*Relaciones*/
    $crud->set_relation('NumOT','ot','NumOT');
   
    //$crud->set_relation('CodProd','prodelaboracion','{CodProdElab} - {DescProdElab}',array('CodProd' => 'CodProdElab'));
    $crud->set_relation('CodProdWS','productosws','{CodProdWS} - {DescProdWS}');
    //$crud->set_relation('NumOT','ot','NumOT');
    
    $crud->display_as('CodProd','C&oacute;digo - Producto');
    
    //Setear el lenguaje 
    //El boton Guardar y volver a la lista se cambio por Cancelar
    //El boton Guardar se cambio por Guardar y volver a la lista
    $crud->set_lang_string('form_save_and_go_back','Cancelar');
    $crud->set_lang_string('form_save','Guardar y volver a la lista');
    
    //El boton Actualizar y volver a la lista se cambio por Cancelar
    //El boton Actualizar Cambios se cambio por Actualizar y volver a la lista
    $crud->set_lang_string('form_update_and_go_back','Cancelar');
    $crud->set_lang_string('form_update_changes','Actualizar y volver a la lista');
    
    
    //Personalizar Mensajes de Error 
    $crud->set_lang_string('update_error','Ocurrio un error al actualizar. El registro ya esta cargado');
    
    /*Relacion con la Tabla Maquina*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
      
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
		    		    
		    //Se agreg� y se le sac� el ; en el date.timezone = America/Argentina/San_Juan en el php.ini para no tener que estar modificando en cada controlador (Aun as� la hora no queda exacta)
		    //Originalmente estaba ;date.timezone =
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'NumOT',
      'CodProdWS',
      'Lote'    
    );
 
 
 	//$crud->callback_insert(array($this,'redirect'));
 	//$crud->callback_before_insert(array($this,'redirect'));
 	
 	
    /* Aqui le indicamos que campos deseamos mostrar */
  	$crud->columns('NumOT','CodProdWS','Lote','Cant_Entregado','Cant_Adicional','Cant_Bueno','%Bueno','Cant_Roto','%Roto','Cant_Destruido','%Dest','Cant_Faltante','%Falt','Cant_Devuelto','%Dev','Cant_Error','%Error'      
    );
    
    /*******Reglas de validacion*****/
    /*Regla para validar que se ingrese el numero de OT con 2 letras (mayusculas o minusculas, antes de insertar se convierten a mayusculas) y 6 n�meros*/
 	//$crud->set_rules('NumOT','Nro de OT','rtrim|regex_match[/^[a-zA-Z]{2}[0-9]{6}$/]');
 	//Valida que el numero de OT no contenga espacios en blanco antes de la cadena, en el caso de tener espacios en blanco me avisa y no deja insertar 
    
    /*Ocultamos los campos y botones que no queremos que el usuario vea o utilice*/
 	/*$crud->unset_read();			//Deshabilita la funcion view
 	$crud->unset_edit();			//Deshabilita la funcion edit
 	$crud->unset_delete();			//Deshabilita la funcion delete
 	$crud->unset_export();			//Deshabilita la funcion export
 	$crud->unset_print();			//Deshabilita la funcion print
    */
    
    /*Llamada a la funcion que pone los valores de la ot en mayusculas antes de insertarlos en la base de datos*/ 
    //$crud->callback_before_insert(array($this,'Conv_mayusculas'));
   
   // $crud->unset_columns('IdOT','NumOT','Lote','FechaVto','IdVaP','IdProdWS');
     
 	/*Cambiar el Label de los campos*/
    /*$crud->display_as('IdOT','Ident. de OT');
    $crud->display_as('NumOT','Nro de OT');
    $crud->display_as('FechaVto','Fecha de Vencimiento');
    $crud->display_as('IdVaP','VaP');
    $crud->display_as('IdProdWS','Ident. del Producto WS');*/
    
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('NumOT','CodProdWS','Lote','Cant_Entregado','Cant_Adicional','Cant_Bueno','Cant_Roto','Cant_Destruido','Cant_Faltante','Cant_Devuelto','Cant_Error');            
             
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('NumOT','CodProdWS','Lote','Cant_Entregado','Cant_Adicional','Cant_Bueno','Cant_Roto','Cant_Destruido','Cant_Faltante','Cant_Devuelto','Cant_Error');
 	
 	//Llama a la funcion para armar el campo de la ot cuando hago un add
 	$crud->callback_add_field('NumOT',array($this,'armar_NumOT_add'));
 	
 	//Llama a la funcion para armar el campo de la ot cuando hago un edit
 	$crud->callback_edit_field('NumOT',array($this,'armar_NumOT_edit'));
 	
 	//Llama a la funcion antes de insertar para convertir el valor de la ot en el id de la ot y que inserte este en la base de datos 	
 	$crud->callback_before_insert(array($this,'conv_nro_ot_add'));
 	
 	//Llama a la funcion antes de actualizar para convertir el valor de la ot en el id de la ot y que no cambie ese valor en la base de datos 	
 	$crud->callback_before_update(array($this,'conv_nro_ot_edit'));
 	
 	//Hago invisible la Cantidad Error para que pueda hacer el calculo pero no lo muestro
 	$crud->field_type('Cant_Error','invisible');
 	
 	
 	/*Llamadas as las Funciones para calcular los porcentajes*/
 	$crud->callback_column('%Bueno',array($this,'porc_bue'));
 	$crud->callback_column('%Roto',array($this,'porc_roto'));
 	$crud->callback_column('%Dest',array($this,'porc_dest'));
 	$crud->callback_column('%Falt',array($this,'porc_falt'));
 	$crud->callback_column('%Dev',array($this,'porc_dev'));
 	$crud->callback_column('%Error',array($this,'porc_error'));
 	//Llamo a una funcion para verificar que la suma de todos los porcentajes me de 100%
 	//$crud->callback_column('%Total',array($this,'porc_total'));
 	
 	 	
 	//De esta forma redirecciona cuando me muestra el mensaje que se inserto correctamente (ADD)
 	//Tambien le concateno la parte final de la url con el id que estaba enviando
 	//Le saque el mensaje del comienzo para que no me muestre nada
 	$crud->set_lang_string('insert_success_message',
		 'Los datos se Insertaron correctamente.
		 <script type="text/javascript">
		  window.location = "'.site_url(strtolower('conciliacion').'/'.strtolower('adminconciliacion')).'?IdOT='.$this->session->userdata('IdOT').'";
		 </script>
		 <div style="display:none">
		 '
   		);
 	
 	
 	//De esta forma redirecciona cuando me muestra el mensaje que se actualizo correctamente (EDIT)
 	//Tambien le concateno la parte final de la url con el id que estaba enviando
 	//Le saque el mensaje del comienzo para que no me muestre nada
 	$crud->set_lang_string('update_success_message',
		 'Los datos se Actualizaron correctamente.
		 <script type="text/javascript">
		  window.location = "'.site_url(strtolower('conciliacion').'/'.strtolower('adminconciliacion')).'?IdOT='.$this->session->userdata('IdOT').'";
		 </script>
		 <div style="display:none">
		 '
   		);
 	
 	//Validaciones de los campos
	/*--------------------------------------------------------------------------------------*/
 	$crud->set_rules('Lote','Lote','trim|min_length[3]');
 	$crud->set_rules('Cant_Entregado','Cant Entregado','trim|integer');
 	$crud->set_rules('Cant_Adicional','Cant Adicional','trim|integer');
 	$crud->set_rules('Cant_Bueno','Cant Bueno','trim|integer');
 	$crud->set_rules('Cant_Roto','Cant Roto','trim|integer');
 	$crud->set_rules('Cant_Destruido','Cant Destruido','trim|integer');
 	$crud->set_rules('Cant_Faltante','Cant Faltante','trim|integer');
 	$crud->set_rules('Cant_Devuelto','Cant Devuelto','trim|integer');
 	
 	/*--------------------------------------------------------------------------------------*/
 	
 	
 	/*Almacenamos el Log*/
 	//Almacenar el log al insertar conciliacion
 	$crud->callback_after_insert(array($this, 'log_insertar_conciliacion'));

	//Almacena el log despues de editar conciliacion
	$crud->callback_after_update(array($this, 'log_editar_conciliacion'));
 	
 	//Almacena el log al eliminar una conciliacion, antes de eliminar para obtener los valores
 	$crud->callback_before_delete(array($this,'log_eliminar_conciliacion'));

 	
 	//Para traer los datos de la ot y mostrarlos en pantalla (El valor que envio es la id de ot)
 	$datos_ot = $this->Model_gestion_conciliacion->obtiene_datos_ot_id($this->session->userdata('IdOT'));
 	//Cargamos los valores obtenidos en la consulta en la variable $data
 	
 	//De esta forma doy vuelta la fecha que traigo desde la base de datos para mostrarla la pantalla de las conciliaciones
    $dia = substr($datos_ot->FechaVto, -2);
	$mes = substr($datos_ot->FechaVto, -5, 2);
	$anio = substr($datos_ot->FechaVto, -10, 4);
   	$datos_ot->FechaVto = $dia .'-'. $mes .'-'. $anio;
 	
 	$data['valor'] = $datos_ot; 
 	
 	//Ocultar botones
 	$crud->unset_read();		//Oculto el boton ver
 	$crud->unset_export();		//Ocultar el boton exportar
 	$crud->unset_print();		//Ocultar el boton imprimir
 	 	
 
 	//$crud->field_type('DescPunzon', 'invisible'); 	 	 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
  
   $this->load->view('menu');
   $this->load->view('conciliacion_mostrar_datos/adminconciliacion_mostrar_datos', $data);
   //Consulto si estoy en el listado para mostrar los botones de exportar a PDF y Cancelar
   //Si estoy en add o edit no muestro los botones
   if($state == 'list')
   			{
			 $this->load->view('conciliacion_mostrar_botones/adminconciliacion_mostrar_botones');	
			}
   $this->load->view('conciliacion/adminconciliacion', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }  




//Funcion que llamo para armar el campo NumOT y hacerlo solo lectura, adem�s que no me de inconvenientes al querer cargar los registros en la base de datos 
function armar_NumOT_add()
{ 
 //Trabajo con el valor que tengo en la variable de session y hago una consulta para obtener el nro de ot de la base de datos
 $value = $this->session->userdata('IdOT');
 $result = $this->Model_gestion_datos_ot->obtiene_nro_ot($value);
 return '<input type="text" value="'.$result.'" name="NumOT" readonly>';	
 
}

//Funcion que llamo para armar el campo NumOT y hacerlo solo lectura, adem�s que no me de inconvenientes al querer cargar los registros en la base de datos 
function armar_NumOT_edit($value, $primary_key)
{
 $result = $this->Model_gestion_datos_ot->obtiene_nro_ot($value);
 //$variable = 'AP'; 	
 return '<input type="text" value="'.$result.'" name="NumOT" readonly>';	
}


//Funcion que convierte el nro de ot que tengo en el campo por el id de la ot, para luego insertar este en la base de datos
function conv_nro_ot_add($post_array)
{
 $post_array['NumOT'] = $this->session->userdata('IdOT');
 
 //Hago la validaci�n para saber si el IdOT, IdProducto y el Lote que se quieren insertar ya estan cargados
 $result = $this->Model_gestion_conciliacion->consulta_valores_add($post_array['NumOT'], $post_array['CodProdWS'], $post_array['Lote']);
 //Si la consulta retorna TRUE, quiere decir que el registro que se quiere insertar ya esta cargado
 if($result == TRUE)  
 		 return FALSE;
 	else {
 		  //De esta forma hago el calculo de la Cantidad Error
 		  $post_array['Cant_Error'] = ($post_array['Cant_Entregado'] + $post_array['Cant_Adicional'] - $post_array['Cant_Bueno'] - $post_array['Cant_Roto'] - $post_array['Cant_Destruido'] - $post_array['Cant_Faltante'] - $post_array['Cant_Devuelto']) * (-1);
 		  
 		  return $post_array;
 		 }
}


//Funcion que convierte el nro de ot que tengo en el campo por el id de la ot, para luego insertar este en la base de datos
function conv_nro_ot_edit($post_array, $primary_key)
{
 $post_array['NumOT'] = $this->session->userdata('IdOT');
 $result = $this->Model_gestion_conciliacion->consulta_valores_edit($post_array['NumOT'], $post_array['CodProdWS'], $post_array['Lote'], $primary_key);
 //Si la consulta retorna TRUE, quiere decir que el registro con los valores que se quiere actualizar ya esta cargado
 if($result == TRUE)
 			return FALSE;
 	else {
 		  //De esta forma hago el calculo de la Cantidad Error
 		  $post_array['Cant_Error'] = ($post_array['Cant_Entregado'] + $post_array['Cant_Adicional'] - $post_array['Cant_Bueno'] - $post_array['Cant_Roto'] - $post_array['Cant_Destruido'] - $post_array['Cant_Faltante'] - $post_array['Cant_Devuelto']) * (-1);
 
 		 return $post_array;
 		  			
 		 }		
 
 
}

/*-----------------------Funciones para armar las columnas de procentajes-----------------*/

function porc_bue($value, $row)
{
 $calculo = ($row->Cant_Bueno * 100)/$row->Cant_Entregado;
 //return $calculo;	
 //Redondea el valor del calculo y lo muestra con dos digitos decimales
 return round($calculo, 2);
}

function porc_roto($value, $row)
{
 $calculo = ($row->Cant_Roto * 100)/$row->Cant_Entregado;
 //return $calculo;	
 //Redondea el valor del calculo y lo muestra con dos digitos decimales
 return round($calculo, 2);
}

function porc_dest($value, $row)
{
 $calculo = ($row->Cant_Destruido * 100)/$row->Cant_Entregado;
 //return $calculo;	
 //Redondea el valor del calculo y lo muestra con dos digitos decimales
 return round($calculo, 2);
}

function porc_falt($value, $row)
{
 $calculo = ($row->Cant_Faltante * 100)/$row->Cant_Entregado;
 //return $calculo;	
 //Redondea el valor del calculo y lo muestra con dos digitos decimales
 return round($calculo, 2);
}

function porc_dev($value, $row)
{
 $calculo = ($row->Cant_Devuelto * 100)/$row->Cant_Entregado;
 //return $calculo;	
 //Redondea el valor del calculo y lo muestra con dos digitos decimales
 return round($calculo, 2);
}

function porc_error($value, $row)
{
 $calculo = ($row->Cant_Error * 100)/$row->Cant_Entregado;
 //return $calculo;	
 //Redondea el valor del calculo y lo muestra con dos digitos decimales
 return round($calculo, 2);
}

//Esta funcion la utilizo para verificar que la suma de todos los porcentajes me de 100%
function porc_total($value, $row)
{
 $calculo = (($row->Cant_Bueno * 100)/$row->Cant_Entregado) + (($row->Cant_Roto * 100)/$row->Cant_Entregado) + (($row->Cant_Destruido * 100)/$row->Cant_Entregado) + (($row->Cant_Faltante * 100)/$row->Cant_Entregado) + (($row->Cant_Devuelto * 100)/$row->Cant_Entregado) + ((($row->Cant_Error * 100)/$row->Cant_Entregado)*(-1));
 //return $calculo;	
 //Redondea el valor del calculo y lo muestra con dos digitos decimales
 return round($calculo, 2);
}


/*---------------------------------------------------------------------------------*/


//Esta funcion almacena el log cuando se inserta una conciliacion
function log_insertar_conciliacion($post_array,$primary_key)
{
 $consulta = $this->Model_gestion_conciliacion->graba_log_conc_add($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
}



//Esta funcion almacena el log cuando se edita una conciliacion
function log_editar_conciliacion($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_conciliacion->graba_log_conc_edit($post_array, $primary_key);		
 if($consulta == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
}



//Esta funcion almacena el log cuando se elimina una conciliacion
function log_eliminar_conciliacion($primary_key)
{
 $consulta = $this->Model_gestion_conciliacion->graba_log_conc_delete($primary_key);	
   	
 if($consulta == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
  		
}






/*-------------------------------Fin de las Funciones----------------------------*/


}
?>
<?
/* Heredamos de la clase CI_Controller */
class Listado_Entrega_sector extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
 	$this->load->model('Model_gestion_entrega_sector');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('listado_entrega_sector/adminlistado_entrega_sector');
  }
 
  
  function adminlistado_entrega_sector()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2)||($this->session->userdata('Nivel') == 3))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('entregasector');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Listado Entrega/Sector');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    /*Relacion con la Tabla Entrega Sector*/
    $crud->set_relation('IdOT','ot','NumOT');
    
    /*Relacion con la Tabla Sector*/    
    $crud->set_relation('IdSector','sector','DescSector');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    /*$crud->display_as('IdMaquinaDefecto','Maquina por Defecto');  
    $crud->display_as('IdTipoProd','Tipo de Producto');  
    $crud->display_as('IdPasoProd','Paso del Producto');  */
    
    $crud->display_as('IdOT','Nro de OT'); 
    $crud->display_as('IdSector','Sector');
    $crud->display_as('FechaEntregaSector','Fecha de Entrega al Sector');
    $crud->display_as('Vers_Protocolo','Versi&oacute;n de Protocolo');
    $crud->display_as('Observacion_Entrega','Observaci&oacute;n de la Entrega');  
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    /*$crud->required_fields(
      'IdOT',
      'IdSector',
      'Observacion_Entrega'
    );*/
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'IdOT',
      'IdSector',
      'FechaEntregaSector',
      'Vers_Protocolo',
      'Observacion_Entrega'
      
    );
 
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	//$crud->add_fields('IdOT','IdSector','Observacion_Entrega');
    
   //$crud->callback_field('IdSector',array($this,'prueba'));
   
    /*Cambiar el Label de los campos*/
   /* $crud->display_as('CodProdElab','C&oacute;digo de Producto');
    $crud->display_as('DescProdElab','Descripci&oacute;n de Producto');
    $crud->display_as('IdVoMM','VoMM');*/
    
   // $crud->display_as('prodelaboracion','Producto'); 
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    //$crud->edit_fields('IdOT','IdSector','Observacion_Entrega');
  
  	$crud->unset_operations();
  	
  	/*$crud->unset_add();
  	$crud->unset_edit();
  	$crud->unset_read();
 	$crud->unset_delete();
 	$crud->unset_print();
 	$crud->unset_export();*/
 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('listado_entrega_sector/adminlistado_entrega_sector', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }



//Funcion que obtiene informaci�n para armar el pdf de datos de entrega al sector 
function Buscar_info_entrega_sector()
{
 //var_dump($this->input->post());
 //$IdProdElab = $this->input->post('IdProdElab');	
 $NumOT = $this->input->post('ajax_data[NumOT]');
 $Sector = $this->input->post('ajax_data[Sector]');
 $FechaEntregaSector = $this->input->post('ajax_data[FechaEntregaSector]');
 $Vers_Protocolo = $this->input->post('ajax_data[Vers_Protocolo]');
 $Observacion_Entrega = $this->input->post('ajax_data[Observacion_Entrega]');
 //$Parcial = $this->input->post('ajax_data[Parcial]');
 
 $consulta = $this->Model_gestion_entrega_sector->Buscar_datos_entrega_like($NumOT, $Sector, $FechaEntregaSector, $Vers_Protocolo, $Observacion_Entrega);
   
   if($consulta != FALSE)
    		 //return json_encode($consulta);
    		 echo json_encode($consulta);    		 
    	else return FALSE;
	 	       
   
  	
}

  
}
?>
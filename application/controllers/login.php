<?
/* Heredamos de la clase CI_Controller */
class Login extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 	
 	$this->load->model('Model_gestion_usuario');
 	
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
    $this->load->library('usuariolib');
    $this->load->library('session');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    redirect('inicio');
  }
 
  
  function inicio()
  {
   
 
    /* Creamos el objeto */
    //$crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    //$crud->set_theme('datatables');
 
    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
   // $crud->set_table('personas');
 
    /* Le asignamos un nombre */
    //$crud->set_subject('personas');
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
   // $crud->set_relation('IdDepto','Departamento','DescDepartamento');
    
    /* Asignamos el idioma espa�ol */
    //$crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    //date_default_timezone_set('America/Argentina/San_Juan');
    
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    /*$crud->required_fields(
      'Idpersona',
      'Nombre',
      'Apellido',
      'IdDepto'
    );*/
 
    /* Aqui le indicamos que campos deseamos mostrar */
    /*$crud->columns(
      'Idpersona',
      'Nombre',
      'Apellido',
      'Cantidad',
      'Fecha_Nac',
      'IdDepto'
    );*/
 
    /* Generamos la tabla */
    //$output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menulog');
   $this->load->view('login');
   //$this->load->view('footer');
   
 
    
  }
 
 
 /*-------------------------Funcion para validar en AD------------------------------*/
 //Funcion para validar el usuario en Active Directory
 //ESTA FUNCION AUTENTICA USUARIOS
public function adtest2($usuario_LDAP, $contrasena_LDAP)
{
                                   
  //desactivamos los erroes por seguridad
  //error_reporting(0);
  error_reporting(E_ALL); //activar los errores (en modo depuraci�n)


//cargo parametros con la info para conectar al AD
  $servidor_LDAP = "raffo.local";
  $servidor_dominio = "RAFFO";
  $ldap_dn = "DC=raffo,DC=local";
  

  //echo "<h3>Validar en servidor LDAP desde PHP</h3>";
  //echo "Conectando con servidor LDAP desde PHP...";
  
  $conectado_LDAP = ldap_connect($servidor_LDAP);//conexion 
  ldap_set_option($conectado_LDAP, LDAP_OPT_PROTOCOL_VERSION, 3);
  ldap_set_option($conectado_LDAP, LDAP_OPT_REFERRALS, 0);

  if ($conectado_LDAP) 
			  {
			    //echo "<br>Conectado correctamente al servidor LDAP " . $servidor_LDAP;

			                   //echo "<br><br>Comprobando usuario y contrase�a en Servidor LDAP";
			    //agrego @ para evitar el reporte de error en usuarios invalidos, esto se valida mas adelanto
			    @$autenticado_LDAP = ldap_bind($conectado_LDAP, $usuario_LDAP . "@" . $servidor_dominio, $contrasena_LDAP);
			    if ($autenticado_LDAP)
			    			{
			                    // echo "<br>Autenticaci�n en servidor LDAP desde Apache y PHP correcta.";
			                    return TRUE;
			                    
			                 } 
			           else  {
			                 /*echo "<br><br>No se ha podido autenticar con el servidor LDAP: ". 
			                     ", verifique el usuario y la contrase�a introducidos";*/
			                	//echo 'Authorization failed';
			                	return FALSE;
			                 }
			  ldap_unbind($conectado_LDAP);
			 }
  
	  else 
		  	{
		    /*echo "<br><br>No se ha podido realizar la conexi�n con el servidor LDAP: " .
		        $servidor_LDAP;*/
		        return FALSE;
		  	}
  
//return ;
                
}
/*-----------------Fin de la funcion para validar en AD----------------------*/ 
 
 
  
 /*-------------Funcion que realiza la validacion de usuario en AD-------------*/
 //Funcion para verificar el acceso del usuario, validando usuario y pass en AD 
 //Funcion que reemplaza a la funcion para validar usuario en base de datos local
 function control()
 {
  
  //validacion de codeigniter  set_rules(nombre_var, label para alerta,regla)
	$this->form_validation->set_rules('usuario', 'Usuario', 'required');
	$this->form_validation->set_rules('pass', '	Contrase&ntilde;a', 'required');
	if($this->form_validation->run() == FALSE)
		{
			redirect('');
		}
	else
	{
		$usuario = $this->input->post('usuario');
		$pass = $this->input->post('pass');
		//Llama a la funcion que valida el usuario en AD
		$valida_usuario_AD = $this->adtest2($usuario, $pass);
		
		if($valida_usuario_AD == TRUE)
				{
				 //Obtiene los datos del usuario
				 $login = $this->Model_gestion_usuario->obtener_login($usuario);
				 
				 if($login != FALSE)
						{
						 //Obtiene la descripcion del sector del usuario
						 //$desc_sector = $this->Model_gestion_usuario->obtener_desc_sector($login->IdSector);
						 //Verifico que el usuario esta Habilitado en la base de datos local
						 if(($login->EstadoUsuario) == 'Habilitado')
									{
									 $dataSession =  array(
											'IdUsuario' => $login->IdUsuario,		
											'Usuario' => $login->Usuario,
											'Nivel' => $login->Nivel,				//Nivel de privilegios del usuario
											'Sector' => $login->IdSector,				//Sector al que pertenece el usuario
											'ip_pc' => $this->input->ip_address(),	//IP de la pc 
											'tiempo' => time()						//Fecha y Hora  
											
											); 
										$this->session->set_userdata($dataSession);
										
										//var_dump($this->session->userdata('Sector'));
										//die;										
										//session_start();
										redirect('principal/inicio');
									 }
								else redirect('');
							}
						else redirect('');		 	
				}
			else redirect('');		
			
	}
  
  	
 }
 /*----------Fin de la funcion que verifica usuarios en AD------------*/
 
 
 
 
 /*------------Utilizar esta funcion para validar los usuarios de manera local----------*/
 //Funcion para validar usuario y pass con datos cargados en la base de datos localmente
 //Funcion para usar reemplazando a la funcion que valida en AD
 /*function control()
 {
  
  //validacion de codeigniter  set_rules(nombre_var, label para alerta,regla)
	$this->form_validation->set_rules('usuario', 'Usuario', 'required');
	$this->form_validation->set_rules('pass', '	Contrase&ntilde;a', 'required');
	if($this->form_validation->run() == FALSE)
		{
			redirect('');
		}
	else
	{
		$usuario = $this->input->post('usuario');
		$pass = $this->input->post('pass');
				
		$login = $this->Model_gestion_usuario->obtener_login($usuario);
		//var_dump($login);die;
		if($login != FALSE)
				{
					//if($this->usuariolib->verificar_pw($pass,$login->password)&&$login->estado=='habilitado'){//si es correcto
					// (VALIDA PASSWORD) C�digo para validar el pass del usuario en la base de datos
					if( ($pass == $login->Password) && ($login->EstadoUsuario) == 'Habilitado' )	
							{//Guardar variables de sesion
								$dataSession=  array(
									'IdUsuario' => $login->IdUsuario,
									'Usuario' => $login->Usuario,
									'Nivel' => $login->Nivel,		//Nivel de privilegios del usuario
									'Sector' => $login->IdSector,
									'ip_pc' => $this->input->ip_address(),
									'tiempo' => time()
									
									); 
								$this->session->set_userdata($dataSession);
								redirect('principal/inicio');
								//$this->principal();
							}
					else redirect('');
					
					// (NO VALIDA PASSWORD) C�digo sin validar el pass del usuario en la base de datos
					//Comentar este c�digo
					$dataSession=  array(
									'IdUsuario' => $login->IdUsuario,
									'Usuario' => $login->Usuario,
									'Nivel' => $login->Nivel,		//Nivel de privilegios del usuario
									'Sector' => $login->IdSector,
									'ip_pc' => $this->input->ip_address(),
									'tiempo' => time()
									
									); 
								$this->session->set_userdata($dataSession);
								redirect('principal/inicio');
					
				}
			else redirect('');
			
	}
  
  	
 } */
 /*------------Fin de la funcion para hacer la validacion de usuarios de manera local---------------*/
 
 
 
 
 //Funcion para salir del sistema
 function salir()
{

	//$array_items = array('IdUsuario' => '', 'Usuario' => '','Nivel' => '', 'IdSector' => '','ip_pc' => '', 'tiempo' => '');
	//$this->session->unset_userdata($array_items);
	
	$this->session->sess_destroy();
	//delete_cookie('ci_session');
	foreach($_COOKIE as $key=>$value) {
	  setcookie($key,"",1);
	  }
	unset($this->session->userdata);
	redirect('');

}
 
 
  
  
}
?>
<?
/* Heredamos de la clase CI_Controller */
class Entrega_sector extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
 	$this->load->model('Model_gestion_entrega_sector');
 	//$this->load->model('Model_gestion_producto');
 	 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('entrega_sector/adminentrega_sector');
  }
 
  
  function adminentrega_sector()
  {
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('ot','entregasector');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Entrega/Sector');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    /*Relacion con la Tabla Entrega Sector*/
    $crud->set_relation('IdOT','ot','NumOT');
    
    /*Relacion con la Tabla VaP*/
    $crud->set_relation('IdVaP','vap','DescVaP');
    
    
    /*Relacion con la Tabla Sector*/    
    //$crud->set_relation('IdSector','sector','DescSector');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    /*$crud->display_as('IdMaquinaDefecto','Maquina por Defecto');  
    $crud->display_as('IdTipoProd','Tipo de Producto');  
    $crud->display_as('IdPasoProd','Paso del Producto');  */
    
    $crud->display_as('IdOT','Nro de OT'); 
    $crud->display_as('FechaVto','Fecha de Vencimiento');
    $crud->display_as('IdVaP','VaP');
    $crud->display_as('IdProdWS','C&oacute;digo - Producto');
    
    $crud->display_as('IdSector','Sector');
    $crud->display_as('FechaEntregaSector','Fecha de Entrega al Sector');
    $crud->display_as('Vers_Protocolo','Versi&oacute;n de Protocolo');
    $crud->display_as('Observacion_Entrega','Observaci&oacute;n de la Entrega');  
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
     
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'IdOT',
      'Lote',
      'FechaVto',
      'IdVaP',
      'IdProdWS',
      'IdSector',
      'FechaEntregaSector',
      'Vers_Protocolo',
      'Observacion_Entrega'
      /*'IdOT',
      'Lote',
      'IdProdWS',
      'IdSector',
      'FechaEntregaSector',
      'Vers_Protocolo',
      'Observacion_Entrega'*/
      
    );
 
 	      
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      //'IdOT',
      //'NumOT',
      'Lote',
      'FechaVto',
      'IdVaP',
      'IdProdWS',
      'IdSector'
      //'IdSector',
      //'Observacion_Entrega'
    );
 
 	
 	$state = $crud->getState();
	$state_info = $crud->getStateInfo();
 	if($state == 'add')
				$crud->set_relation('IdProdWS','Productosws','{CodProdWS} - {DescProdWS}',array('EstadoProdWS' => 'AC'));
			
		else {
				if($state == 'edit')
					{
						$primary_key = $state_info->primary_key;
			 			
			 			//Obtengo el id del producto
						$idprodws = $this->Model_gestion_entrega_sector->obtiene_IdProdws($primary_key);
						//Obtengo la consulta SQL en un arreglo
					    $array1 = $this->Model_gestion_entrega_sector->obtiene_IdProductosWS($idprodws);
					    //Obtengo la consulta SQL en un arreglo
					    $array2 = $this->Model_gestion_entrega_sector->obtiene_DescProductosWS($idprodws);
						//Combion los dos arreglos para armar el select
						$array3 = array_combine($array1, $array2);
						$crud->field_type('IdProdWS','dropdown',$array3);
			 			
			 			
			 			//$crud->set_relation('IdProdWS','Productosws','{CodProdWS} - {DescProdWS}',array('EstadoProdWS' => 'AC'));
			 		}
			 	else  $crud->set_relation('IdProdWS','Productosws','{CodProdWS} - {DescProdWS}');	
			}	 	
 	
 	
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('IdOT','Lote','FechaVto','IdVaP','IdProdWS','IdSector','Vers_Protocolo','Observacion_Entrega');
    
   	//$crud->callback_field('IdSector',array($this,'prueba'));
   
    //Armamos el select de sector
    //Obtengo la consulta SQL en un arreglo
    $array1_sector = $this->Model_gestion_entrega_sector->obtiene_idsector();
    //Obtengo la consulta SQL en un arreglo
    $array2_sector = $this->Model_gestion_entrega_sector->obtiene_desc_sector();
    //Combion los dos arreglos para armar el select
	$array3_sector = array_combine($array1_sector, $array2_sector);
	$crud->field_type('IdSector','dropdown',$array3_sector);
    
    
    
    /*Cambiar el Label de los campos*/
   /* $crud->display_as('CodProdElab','C&oacute;digo de Producto');
    $crud->display_as('DescProdElab','Descripci&oacute;n de Producto');
    $crud->display_as('IdVoMM','VoMM');*/
    
    
   // $crud->display_as('prodelaboracion','Producto'); 
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('IdOT','Lote','FechaVto','IdVaP','IdProdWS','IdSector','Vers_Protocolo','Observacion_Entrega');
  	
  	//$crud->field_type('FechaEntregaSector', 'readonly');
  	
  	//Reglas de validaci�n para los campos
  	$crud->set_rules('Lote','Lote','trim|required|min_length[3]|alpha_numeric');
  	
  	//Llama a la funcion que reemplaza el insertar por defecto
  	//Inserta tambien el log
  	$crud->callback_insert(array($this,'insertar_tablas'));
  	
  	//Llama a la funcion que reemplaza el update por defecto
  	//Inserta tambien el log
  	$crud->callback_update(array($this,'editar_tablas'));
  	
  	 	
 	/*Almacenamos el Log*/
 	//Almacenar el log al insertar Entrega a los Sectores
 	//$crud->callback_after_insert(array($this, 'log_insertar_ot_entrega'));

	//Almacena el log despues de editar conciliacion
	$crud->callback_after_update(array($this, 'log_editar_ot_entrega'));
 	
 	//Almacena el log al eliminar una conciliacion, antes de eliminar para obtener los valores
 	//$crud->callback_before_delete(array($this,'log_eliminar_estado_sector'));
  	
  	//Completa las columnas
  	//$crud->callback_column('IdSector',array($this,'columna_sector'));
  	
  	
  	
  	$crud->unset_read();
 	$crud->unset_delete();
 	$crud->unset_print();
    //$crud->unset_export();
 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('entrega_sector/adminentrega_sector', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }
 
 
//Esta funcion almacena el log cuando se inserta una entrega a un sector
function log_insertar_ot_entrega($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_entrega_sector->graba_log_ot_entrega_add($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
}


//Esta funcion almacena el log cuando se edita una entrega a un sector
function log_editar_entrega_sector($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_entrega_sector->graba_log_entrega_sector_edit($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
}


//Esta funcion almacena el log cuando se edita una conciliacion
function log_editar_conciliacion($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_entrega_sector->graba_log_entrega_sector_edit($post_array, $primary_key);		
 if($consulta == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
} 
 

//Funcion que reemplaza el insertar por defecto
//Inserta la tabla ot y la tabla entrega_sector
function insertar_tablas($post_array) {
//$this->load->library('encrypt');
//$key = 'super-secret-key';
//$post_array['password'] = $this->encrypt->encode($post_array['password'], $key);
$consulta = $this->Model_gestion_entrega_sector->inserta_registros($post_array);		
 if($consulta == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
}    


//Funcion que reemplaza el update por defecto
// la tabla ot y la tabla entrega_sector
function editar_tablas($post_array, $primary_key) {

$consulta = $this->Model_gestion_entrega_sector->editar_registros($post_array, $primary_key);		
 if($consulta == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
} 


/*-------------------------------------------------------------------*/
//Obtengo el lote para la columna
function columna_sector($value, $row)
{
 $consulta = $this->Model_gestion_entrega_sector->consulta_sector($row);
 
 //$calculo = ($row->Cant_Bueno * 100)/$row->Cant_Entregado;
 
 return $consulta;
}

/*----------------------------------------------------------------------*/
  
  
}
?>
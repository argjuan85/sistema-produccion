<?
/* Heredamos de la clase CI_Controller */
class Subetapa extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
 	/*Llamamos al modelo para hacer las consultas a la base de datos*/
 	$this->load->model('Model_gestion_subetapa');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('subetapa/adminsubetapa');
  }
 
  
  function adminsubetapa()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('subetapa');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Subetapa');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    /*Relacion con la Tabla Etapa*/
    $crud->set_relation('IdEtapa','Etapa','DescEtapa');
        
    /*Para el campo Orden permite elegir uno entre varios valores*/
    $crud->field_type('Orden','enum',array('1','2','3','4','5','6','7','8','9','10'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    
    $crud->display_as('DescSubEtapa','Descripci&oacute;n de Subetapa');
    $crud->display_as('IdEtapa','Etapa');  
        
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'DescSubEtapa',
      'IdEtapa',
      'Orden'
      );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'DescSubEtapa',
      'IdEtapa',
      'Orden',
      'Objetivo',
      'EstadoSub'
    );
 
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('DescSubEtapa','IdEtapa','Orden','Objetivo');
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('IdSubetapa','DescSubEtapa','IdEtapa','Orden','Objetivo','EstadoSub');
 	
 	/*Llama a la funcion para hacer la validacion de los campos de la subetapa antes de hacer un insert*/
  	$crud->callback_before_insert(array($this,'valida_campos_subetapa_add'));
  	
 	/*Llama a la funcion para hacer la validacion de los campos antes de hacer el editar*/ 	 	
  	$crud->callback_before_update(array($this,'valida_campos_subetapa_edit'));
 	
 	
 	/*Llamada a la funcion para almacenar en el log cuando se inserta*/
  	$crud->callback_after_insert(array($this, 'graba_log_subetapa_add'));		//Funcion que se llama despues de insertar
 
 	/*Llamada a la funcion para almacenar en el log cuando se edita*/
  	$crud->callback_after_update(array($this, 'graba_log_subetapa_edit'));		//Funcion que se llama despues de editar
 	
 	//Ocultamos el boton Eliminar
 	$crud->unset_delete();
 	
 	/*Ocultamos los botomes Imprimir y Exportar*/
 	$crud->unset_print();
 	$crud->unset_export();
 	
 	$state = $crud->getState();
	if($state == 'add')
				$crud->set_lang_string('insert_error','Error al Insertar, verifique la Descripci&oacute;n');
		else if($state == 'edit')
				        {
						$crud->set_lang_string('update_error','Error al Editar, verifique la Descripci&oacute;n');
						/*Lo pongo en invisible pero lo utilizo para realizar la busqueda al momento de editar los campos*/
						$crud->field_type('IdSubetapa','hidden');	
						}
 
 
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('subetapa/adminsubetapa', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }
  
  
  
  /*Llama al modelo para realizar la validacion de los campos de la subetapa antes de insertar*/ 
  function valida_campos_subetapa_add($post_array) 
   { 
	 $msj = $this->Model_gestion_subetapa->campos_subetapa_add($post_array);
	  
	  /*Si desde el modelo me retorna false, retornamos false para no hacer de nuevo la carga de la misma subetapa*/	  
	  if($msj == FALSE)
	  			 	return FALSE;	  			 
	  		else { /*Antes de asignar los valores les elimina los espacios en blanco delante y al final de la cadena*/ 
             	   $post_array['DescSubEtapa'] = trim($post_array['DescSubEtapa']);
             	   $post_array['EstadoSub'] = 'Habilitada';
             	   return $post_array;         
             	   //return TRUE;
             	  }	  	
	}
  
  
  /*Llama al modelo para realizar la validacion de los campos de la Subetapa despues de editarlos*/
  function valida_campos_subetapa_edit($post_array, $primary_key) 
   { 
	 $msj = $this->Model_gestion_subetapa->campos_subetapa_edit($post_array, $primary_key);
	  
	  /*Si desde el modelo me retorna false, retornamos false para no hacer de nuevo la carga de la misma subetapa*/	  
	  if($msj == FALSE)
	  			 	return FALSE;	  			 
	  		else { /*Antes de asignar los valores les elimina los espacios en blanco delante y al final de la cadena*/ 
             	   $post_array['DescSubEtapa'] = trim($post_array['DescSubEtapa']);
             	   return $post_array;         
             	  }	  	
	}
  
  
/*Funcion que llama al modelo para insertar los registros del log cuando hago un insertar*/
   function graba_log_subetapa_add($post_array, $primary_key)
   {   	
   	$consulta = $this->Model_gestion_subetapa->graba_log_add($post_array, $primary_key);		
   	if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
   }  
  
  
  /*Funcion que llama al modelo para insertar los registros del log cuando hago un editar*/
   function graba_log_subetapa_edit($post_array, $primary_key)
   {   	
   	$consulta = $this->Model_gestion_subetapa->graba_log_edit($post_array, $primary_key);		if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
   } 
  
  
  
}
?>
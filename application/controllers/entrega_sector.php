<?
/* Heredamos de la clase CI_Controller */
class Entrega_sector extends CI_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
 	$this->load->model('Model_gestion_entrega_sector');
 		 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('entrega_sector/adminentrega_sector');
  }
 
  
  function adminentrega_sector()
  {
  	//Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
	
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('entregasector');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Entrega/Sector');
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/
    
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    /*Relacion con la Tabla Entrega Sector*/
    //$crud->set_relation('IdOT','ot','NumOT');
    
    /*Relacion con la Tabla VaP*/
    //$crud->set_relation('IdVaP','vap','DescVaP');
    
    
    $crud->set_relation('IdOT','ot','NumOT');
    $crud->set_relation('IdSector','sector','DescSector');
    
    /*Relacion con la Tabla Sector*/    
    //$crud->set_relation('IdSector','sector','DescSector');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    /*$crud->display_as('IdMaquinaDefecto','Maquina por Defecto');  
    $crud->display_as('IdTipoProd','Tipo de Producto');  
    $crud->display_as('IdPasoProd','Paso del Producto');  */
    
    
    $crud->display_as('IdOT','Nro de OT'); 
    //$crud->display_as('NumOT','Nro de OT'); 
    $crud->display_as('FechaVto','Fecha de Vencimiento');
    //$crud->display_as('IdVaP','VaP');
    $crud->display_as('IdProdWS','C&oacute;digo - Producto');
    
    $crud->display_as('IdSector','Sector');
    $crud->display_as('FechaEntregaSector','Fecha de Entrega al Sector');
    $crud->display_as('Vers_Protocolo','Versi&oacute;n de Protocolo');
    $crud->display_as('Observacion_Entrega','Observaci&oacute;n de la Entrega'); 
    $crud->display_as('Usuario_Carga','Usuario Carga');  
    $crud->display_as('Usuario_Edita','Usuario Edita');   
    
    $crud->unset_texteditor('Observacion_Entrega','full_text');
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
     
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'IdOT',
      'Lote',
      'FechaVto',
      'Cod_Desc_Prod',
      'IdSector',
      'FechaEntregaSector',
      'Vers_Protocolo',
      'Observacion_Entrega',
      'Usuario_Carga',
      'Usuario_Edita'
      
    );
 	
 	$crud->fields('NumOT-Lote-CodyDescProd','IdSector','Vers_Protocolo','Observacion_Entrega');
 	      
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    /*$crud->required_fields(
      //'IdOT',
      //'NumOT',
      //'Lote',
      //'NumOT-Lote-CodyDescProd',
      'IdSector',
      ///'FechaEntregaSector',
      'Observacion_Entrega'
    );*/
 	
 	
 	//$crud->set_rules('NumOT-Lote-CodyDescProd','NumOT-Lote-CodyDescProd','required');
    
    //$crud->edit_fields('IdSector','Vers_Protocolo','Observacion_Entrega');
    
    $state = $crud->getState();
	$state_info = $crud->getStateInfo();
 	//Se puso de esta forma para que tome los campos del required_fields
 	if($state == 'add'|| $state=='insert_validation')
				{
				 
				 /* Aqui le decimos a grocery que estos campos son obligatorios */
				 $crud->required_fields(
				      //'IdOT',
				      //'NumOT',
				      //'Lote',
				      'NumOT-Lote-CodyDescProd',
				      'IdSector',
				      ///'FechaEntregaSector',
				      'Observacion_Entrega'
				    );
				 //$crud->set_rules('NumOT-Lote-CodyDescProd','NumOT-Lote-CodyDescProd','required');
				 //Obtengo la consulta SQL en un arreglo
			     $array1_datos_ot = $this->Model_gestion_entrega_sector->obtiene_IdOT();
			     //Obtengo la consulta SQL en un arreglo
			     $array2_datos_ot = $this->Model_gestion_entrega_sector->obtiene_datos_ot();
				 //Combion los dos arreglos para armar el select
				 $array3_datos_ot = array_combine($array1_datos_ot, $array2_datos_ot);
				 $crud->field_type('NumOT-Lote-CodyDescProd','dropdown',$array3_datos_ot);	
				
				/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 				//$crud->add_fields('NumOT-Lote-CodyDescProd','IdSector','Vers_Protocolo','Observacion_Entrega');
 				
				}
		//Se puso de esta forma para que tome los campos del required_fields
		else if($state == 'edit'|| $state=='update_validation')
				  	 	{
				  	 	/* Aqui le decimos a grocery que estos campos son obligatorios */
						$crud->required_fields(
						      //'IdOT',
						      //'NumOT',
						      //'Lote',
						      //'NumOT-Lote-CodyDescProd',
						      'IdSector',
						      ///'FechaEntregaSector',
						      'Observacion_Entrega'
						    );
				  	 	
				  	 	//$crud->edit_fields('IdSector','Vers_Protocolo','Observacion_Entrega');
				  	 	$crud->edit_fields('Datos_OT','NumOT-Lote-CodyDescProd','IdSector','Vers_Protocolo','Observacion_Entrega');
				  	 	//Armo el campo para mostrar la informaci�n de la ot
				  	 	$crud->display_as('Datos_OT','Datos OT');
				  	 	$crud->callback_edit_field('Datos_OT',array($this,'armar_datos_ot'));
				  	 	
				  	 	//Armo el select por si quiero cambiar el valor del campo de los datos de la ot
				  	 	//Obtengo la consulta SQL en un arreglo
					    $array1_datos_ot = $this->Model_gestion_entrega_sector->obtiene_IdOT();
					    //Obtengo la consulta SQL en un arreglo
					    $array2_datos_ot = $this->Model_gestion_entrega_sector->obtiene_datos_ot();
						//Combion los dos arreglos para armar el select
						$array3_datos_ot = array_combine($array1_datos_ot, $array2_datos_ot);
						$crud->field_type('NumOT-Lote-CodyDescProd','dropdown',$array3_datos_ot);
				  	 	
				  	 	}
				 else  $crud->field_type('NumOT-Lote-CodyDescProd','hidden');
				 
    
   	//$crud->callback_field('IdSector',array($this,'prueba'));
   
    //Armamos el select de sector
    //Obtengo la consulta SQL en un arreglo
    /*$array1_sector = $this->Model_gestion_entrega_sector->obtiene_idsector();
    //Obtengo la consulta SQL en un arreglo
    $array2_sector = $this->Model_gestion_entrega_sector->obtiene_desc_sector();
    //Combion los dos arreglos para armar el select
	$array3_sector = array_combine($array1_sector, $array2_sector);
	$crud->field_type('IdSector','dropdown',$array3_sector);*/
    
    
    
    /*Cambiar el Label de los campos*/
   /* $crud->display_as('CodProdElab','C&oacute;digo de Producto');
    $crud->display_as('DescProdElab','Descripci&oacute;n de Producto');
    $crud->display_as('IdVoMM','VoMM');*/
    
    
   // $crud->display_as('prodelaboracion','Producto'); 
    
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    //$crud->edit_fields('IdOT','Lote','FechaVto','IdVaP','IdProdWS','IdSector','Vers_Protocolo','Observacion_Entrega');
  	
  	//$crud->field_type('FechaEntregaSector', 'readonly');
  	
  	//Reglas de validaci�n para los campos
  	//$crud->set_rules('Lote','Lote','trim|required|min_length[3]|alpha_numeric');
  	
  	//Llamamos a la funcion antes de insertar 
  	//Validamos que se hayan ingresado todos los campos
  	//$crud->callback_before_insert(array($this,'valida_campos'));
  	  	
  	//Llama a la funcion que reemplaza el insertar por defecto
  	$crud->callback_insert(array($this,'insertar_entrega_sector'));
  	
  	//Llama a la funcion para insertar el log
  	//$crud->callback_after_insert(array($this,'log_insert_entrega'));
  	
  	//Reemplaza a la funcion update por defecto 
  	$crud->callback_update(array($this,'editar_entrega_sector'));
  	
  	//Llama a la funcion para insertar el log
  	//$crud->callback_after_update(array($this,'log_editar_entrega'));
  	
  	//Llama a la funcion que reemplaza el update por defecto
  	//Inserta tambien el log
  	//$crud->callback_update(array($this,'editar_tablas'));
  	
  	//Cambia el nombre al boton para exportar en excel
    $crud->set_lang_string('list_export','Exportar Excel');
  	 	
 	/*Almacenamos el Log*/
 	//Almacenar el log al insertar Entrega a los Sectores
 	//$crud->callback_after_insert(array($this, 'log_insertar_ot_entrega'));
	
	//Almacena el log despues de editar conciliacion
	//$crud->callback_after_update(array($this, 'log_editar_ot_entrega'));
 	
 	//Almacena el log al eliminar una conciliacion, antes de eliminar para obtener los valores
 	//$crud->callback_before_delete(array($this,'log_eliminar_estado_sector'));
  	
  	//Completa las columnas
  	$crud->callback_column('NumOT',array($this,'columna_NumOT'));
  	$crud->callback_column('Lote',array($this,'columna_Lote'));
  	$crud->callback_column('FechaVto',array($this,'columna_FechaVto'));
  	$crud->callback_column('Cod_Desc_Prod',array($this,'columna_Datos_Prod'));
  	
  	
  	//$crud->unset_read();
 	$crud->unset_delete();
 	$crud->unset_print();
    //$crud->unset_export();
 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('entrega_sector/adminentrega_sector', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }
 
//Funcion que reemplaza al insertar por defecto
function insertar_entrega_sector($post_array)
{
 $consulta = $this->Model_gestion_entrega_sector->insertar_add($post_array);		
 
 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
}

//No se utiliza porque tambien inserto el log cuando inserto el registro
//Funcion que inserta el log
/*function log_insert_entrega($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_entrega_sector->insertar_log_add($post_array, $primary_key);		
 
 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
}*/


//Funcion que reemplaza al editar por defecto
function editar_entrega_sector($post_array, $primary_key)
{
 //var_dump($post_array);
 //die;
 $consulta = $this->Model_gestion_entrega_sector->editar_entrega($post_array, $primary_key);		
 
 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
}


//Funcion que inserta el log editar
function log_editar_entrega($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_entrega_sector->log_editar_entrega($post_array, $primary_key);		
 
 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
}
 
//Esta funcion almacena el log cuando se inserta una entrega a un sector
/*function log_insertar_ot_entrega($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_entrega_sector->graba_log_ot_entrega_add($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
}*/


//Esta funcion almacena el log cuando se edita una entrega a un sector
/*function log_editar_entrega_sector($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_entrega_sector->graba_log_entrega_sector_edit($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
}*/


//Esta funcion almacena el log cuando se edita una conciliacion
/*function log_editar_conciliacion($post_array, $primary_key)
{
 $consulta = $this->Model_gestion_entrega_sector->graba_log_entrega_sector_edit($post_array, $primary_key);		
 if($consulta == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
} */
 

//Funcion que reemplaza el insertar por defecto
//Inserta la tabla ot y la tabla entrega_sector
/*function insertar_tablas($post_array) {
//$this->load->library('encrypt');
//$key = 'super-secret-key';
//$post_array['password'] = $this->encrypt->encode($post_array['password'], $key);
$consulta = $this->Model_gestion_entrega_sector->inserta_registros($post_array);		
 if($consulta == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
}    */


//Funcion que reemplaza el update por defecto
// la tabla ot y la tabla entrega_sector
/*function editar_tablas($post_array, $primary_key) {

$consulta = $this->Model_gestion_entrega_sector->editar_registros($post_array, $primary_key);		
 if($consulta == FALSE)
  			 return FALSE;	  			 
  		else return TRUE;
} 
*/

/*-------------------------------------------------------------------*/
//Obtengo el Numero de ot para la columna
function columna_NumOT($value, $row)
{
 $consulta = $this->Model_gestion_entrega_sector->consulta_NumOT($row);
 
 //$calculo = ($row->Cant_Bueno * 100)/$row->Cant_Entregado;
 
 return $consulta;
}

//Obtengo el Lote para la columna
function columna_Lote($value, $row)
{
 $consulta = $this->Model_gestion_entrega_sector->consulta_Lote($row);
 
 //$calculo = ($row->Cant_Bueno * 100)/$row->Cant_Entregado;
 
 return $consulta;
}


//Obtengo el Lote para la columna
function columna_FechaVto($value, $row)
{
 $consulta = $this->Model_gestion_entrega_sector->consulta_FechaVto($row);
 
 $dia = substr($consulta, -2);
 $mes = substr($consulta, -5, 2);
 $anio = substr($consulta, -10, 4);
 $fecha_vto_conv = $dia.'-'.$mes.'-'.$anio;
 
 //$calculo = ($row->Cant_Bueno * 100)/$row->Cant_Entregado;
 
 //return $consulta;
 return $fecha_vto_conv;
}

//Obtengo los datos del producto para la columna
function columna_Datos_Prod($value, $row)
{
 $consulta = $this->Model_gestion_entrega_sector->consulta_Datos_Prod($row);
 
 /*$dia = substr($consulta->FechaVto, -2);
 $mes = substr($consulta->FechaVto, -5, 2);
 $anio = substr($consulta->FechaVto, -10, 4);
 $fecha_vto_conv = $dia .'-'. $mes .'-'. $anio;*/
 
 //$calculo = ($row->Cant_Bueno * 100)/$row->Cant_Entregado;
 
 return $consulta;
}


/*----------------------------------------------------------------------*/

//Armamos el campo con los datos de la OT solo lectura
function armar_datos_ot($value, $primary_key)
{
 $consulta = $this->Model_gestion_entrega_sector->consulta_Datos_OT($value ,$primary_key);
 //$variable = 'AP'; 	
 
 return '<input type="text" value="'.$consulta.'" name="Datos_OT" style="width:1000px;" readonly>';	
}
        
  
  
}
?>
<?
/* Heredamos de la clase CI_Controller */
class CargarOT extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
 	//$this->load->model('Model_gestion_punxmaq');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
     
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('cargarot/admincargarot');
  }
 
  
  function admincargarot()
  {
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
  	
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('ot');
 
    /* Le asignamos un nombre */ 
    $crud->set_subject('Carga OT'); 		//Este nombre es el que va a aparecer en el formulario, en el boton a�adir, editar, etc
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/     
     
    /*Relacion con la Tabla VaP*/
    $crud->set_relation('IdVaP','vap','DescVaP');
    
    /*Relacion con la Tabla Producto WS - Filtra los productos que estan en estado AC*/
    $crud->set_relation('IdProdWS','Productosws','{CodProdWS} - {DescProdWS}',array('EstadoProdWS' => 'AC'));
        
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    
    /*Relacion con la Tabla Punzon*/
    //$crud->set_relation('IdPunzon','Punzon','DescPunzon');
    
    /*Relacion con la Tabla Maquina*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
      
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'NumOT',
      'Lote',
      'FechaVto',
      'IdVaP',
      'IdProdWS'    
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
  	$crud->columns(
      'IdOT',
      'NumOT',
      'Lote',
      'FechaVto',
      'IdVaP',
      'IdProdWS'
    );
    
    /*******Reglas de validacion*****/
    /*Regla para validar que se ingrese el numero de OT con 2 letras (mayusculas o minusculas, antes de insertar se convierten a mayusculas) y 6 n�meros*/
 	$crud->set_rules('NumOT','Nro de OT','rtrim|regex_match[/^[a-zA-Z]{2}[0-9]{6}$/]');
 	//Valida que el numero de OT no contenga espacios en blanco antes de la cadena, en el caso de tener espacios en blanco me avisa y no deja insertar 
    
    /*Ocultamos los campos y botones que no queremos que el usuario vea o utilice*/
 	$crud->unset_read();			//Deshabilita la funcion view
 	$crud->unset_edit();			//Deshabilita la funcion edit
 	$crud->unset_delete();			//Deshabilita la funcion delete
 	$crud->unset_export();			//Deshabilita la funcion export
 	$crud->unset_print();			//Deshabilita la funcion print
    
    /*Llamada a la funcion que pone los valores de la ot en mayusculas antes de insertarlos en la base de datos*/ 
    $crud->callback_before_insert(array($this,'Conv_mayusculas'));
   
   // $crud->unset_columns('IdOT','NumOT','Lote','FechaVto','IdVaP','IdProdWS');
     
 	/*Cambiar el Label de los campos*/
    $crud->display_as('IdOT','Ident. de OT');
    $crud->display_as('NumOT','Nro de OT');
    $crud->display_as('FechaVto','Fecha de Vencimiento');
    $crud->display_as('IdVaP','VaP');
    $crud->display_as('IdProdWS','Ident. del Producto WS');
    
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('NumOT','Lote','FechaVto','IdVaP','IdProdWS');            
             
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    //$crud->edit_fields('DescPunzon','Fecha_Ingreso','Fecha_Egreso');
 	
 	//$crud->field_type('DescPunzon', 'invisible'); 	 	 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('cargarot/admincargarot', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }

	function Conv_mayusculas($post_array) 
	{	
		$NumOT_Mayusc = strtoupper($post_array['NumOT']); 		//Convierte la cadena en may�sculas antes de almacenarla
			
		//$elim_espacios = trim($NumOT_Mayusc); 					//Elimina los espacios en blanco de al comienzo y al final de la cadena
		$post_array['NumOT'] = $NumOT_Mayusc;
	 
		return $post_array;
	}   

}
?>
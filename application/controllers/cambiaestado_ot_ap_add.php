<?
/* Heredamos de la clase CI_Controller */
class Cambiaestado_ot_ap_add extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
  
 	$this->load->model('Model_gestion_datos_ot');
 	 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
     
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('cambiaestado_ot_ap_add/admincambiaestado_ot_ap_add');
  }
 
  
  function admincambiaestado_ot_ap_add()
  {
  	//Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2))
 		{//Inicio del if de nivel de usuario
  	
    try{
 
    /* Creamos el objeto */
    //$crud = new grocery_CRUD();
    $crud = new grocery_CRUD();
 	
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
   
    $crud->set_table('datosot');
    //$crud->set_table('datosot','maquina','subetapa','etapa');
     
    /* Le asignamos un nombre */ 
    $crud->set_subject('Cambiar Estado de OT'); 		//Este nombre es el que va a aparecer en el formulario, en el boton a�adir, editar, etc
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/     
     
    /*Relacion con la Tabla OT*/
    //$crud->set_relation('IdOT','ot','NumOT');
    
       
     /*Relacion con la Tabla SubEtapa*/
    //$crud->set_relation('IdSubEtapa','subetapa','DescSubEtapa');
    
    /*Relacion con la Tabla Producto Elaboracion*/
    
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC', 'IdProdElab' => 27));
    //$crud->field_type('IdProdElab','dropdown',array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));
    //$crud->order_by('CodProdElab','desc');
    
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}');
    //$crud->order_by('CodProdElab','desc');
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC'));
    //$crud->where('EstadoProdElab','AC');
    //$crud->or_where('IdProdElab',1);
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC'));
    //$crud->or_where('IdProdElab',1);
    //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('IdProdElab' => 27));
    
    /*Saca los botones para editar el campo de texto*/ 
    //$crud->unset_texteditor('Observacion_DatosOT','full_text');
    
    /*Obtiene el estado en el que se encuentra la aplicacion*/
    /*$state = $crud->getState();
    if($state == 'add')
    //Para agregar un registro muestro solamente los que estan en AC o habilitados
		    {
			 $crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC'));	
			 
			 $crud->set_relation('IdMaquina','Maquina','DescMaquina',array('EstadoMaquina' => 'Habilitada'));
			 $crud->set_relation('IdPunzon','Punzon','DescPunzon',array('EstadoPunzon' => 'Habilitado'));   
			
			}
    	else //if($state == 'edit')
    		 //Para editar o las demas acciones no evalua los estados para poder observar todos los registros	
				{
				 //$CP = $this->Model_gestion_datos_ot->obtiene_idprod();
				 //$CP = $crud->callback_before_update(array($this,'obtiene_idprod_edit')); 
				 
				 //$CP = array('coche' => 'Rojo','moto' => 'verde','avion' => 'amarillo');
				 //$crud->field_type('IdProdElab','dropdown',$CP);
				 $crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}');
				 $crud->set_relation('IdMaquina','Maquina','DescMaquina');
			     $crud->set_relation('IdPunzon','Punzon','DescPunzon');   
				 //$CP = $post_array['IdProdElab'];
				 //$crud->field_type('IdProdElab','dropdown',$CP);
				 //$crud->field_type('IdProdElab','integer',$CP);
				 //$crud->where('ProdElaboracion.IdProdElab', $CP);
				 
				 //$crud->where('prodelaboracion.EstadoProdElab', 'AC');
				 //$crud->where('IdProdElab', $mois);
				 //$crud->order_by('ProdElaboracion.IdProdElab', 'desc');
				 //$crud->or_where('ProdElaboracion.IdProdElab',1);
				 //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('IdProdElab' => '27', 'EstadoProdElab' => 'AC'));
				 			 
				 //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC'));
				 //$crud->where('ProdElaboracion.IdProdElab', $CP);
				 //$crud->or_where('ProdElaboracion.EstadoProdElab', 'AC');
				 
				 //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab} - {EstadoProdElab}',array('IdProdElab' => $CP));
				 //$crud->where('EstadoProdElab', 'AC');
	    		 //$crud->or_where('IdProdElab', $CP);
	             //$crud->field_type('IdProdElab','dropdown',array(1 => 'active', 2 => 'private',3 => 'spam' , 4 => 'deleted'));
	             //$crud->field_type('IdProdElab','dropdown',$CP);
	             //$crud->where('IdProdElab',$CP);
	             //$crud->field_type('IdProdElab','dropdown',$CP);
	             //$crud->set_relation('IdProdElab','ProdElaboracion','{CodProdElab} - {DescProdElab}',array('EstadoProdElab' => 'AC'));
	              
	             //$crud->where('IdProdElab',$CP);
	             //$crud->set_relation('IdProdElab','ProdElaboracion','CodProdElab', '(SELECT CodProdElab FROM ProdElaboracion WHERE EstadoProdElab = AC)');
	             	
				}*/
    
    
    /*Hace la relaci�n para mostrar la maquina perteneciente a una la subetapa seleccionada*/
    //$crud->set_relation('IdEtapa','Etapa');
    
    /*Relacion con la Tabla Maquina, permite seleccionar solo las maquinas Habilitadas*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina',array('EstadoMaquina' => 'Habilitada'));
	/*$crud->where('datosot.IdMaquina','maquina.IdMaquina');
	$crud->where('datosot.IdSubEtapa','subetapa.IdSubEtapa');
    $crud->where('maquina.IdEtapa','etapa.IdEtapa');
    $crud->where('subetapa.IdEtapa','etapa.IdEtapa');*/
          
       
    /*Relacion con la Tabla Punzon, permite seleccionar solo los punzones habilitados*/
    //$crud->set_relation('IdPunzon','Punzon','DescPunzon',array('EstadoPunzon' => 'Habilitado'));   
    
    //$crud->field_type('Parcial','enum',array('Si','No'));
    
    //$crud->field_type('HParcial','enum',array('Si','No'));
    
    //$crud->field_type('Retrabajo','enum',array('Si','No'));
          
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    
    /*Relacion con la Tabla Punzon*/
    //$crud->set_relation('IdPunzon','Punzon','DescPunzon');
    
    /*Relacion con la Tabla Maquina*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
      
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
 
    /* Aqui le indicamos que campos deseamos mostrar */
  /*$crud->columns(
      'IdDatosOT', 
      'IdOT',
      'IdProdElab',
      'IdSubEtapa',
      'Cant_Teorica',
      'Cant_Real',
      'IdMaquina',
      'IdPunzon'      
    );*/
    
    /*Ocultamos los campos y botones que no queremos que el usuario vea o utilice*/
 	//$crud->unset_read();			//Deshabilita la funcion view
 	//$crud->unset_edit();			//Deshabilita la funcion edit
 	//$crud->unset_delete();			//Deshabilita la funcion delete
 	//$crud->unset_export();			//Deshabilita la funcion export
 	//$crud->unset_print();			//Deshabilita la funcion print
     
    //$crud->unset_columns('IdOT','NumOT','Lote','FechaVto','IdVaP','IdProdWS');
     
    //$crud->field_type('AP','dropdown',array('AP' => 'AP', 'NULL' => 'Vaciar')); 
     
 	$state = $crud->getState();
	$state_info = $crud->getStateInfo();
	if ($state == 'edit')
				 {
					/* Aqui le decimos a grocery que estos campos son obligatorios */
				    $crud->required_fields(
				      //'AP',
				      'Codigo',
				      'Version'
					   
				    );
				
				 	/*Cambiar el Label de los campos*/
				    $crud->display_as('AP','Estado AP');
				    $crud->display_as('Codigo','C&oacute;digo');
				    $crud->display_as('Version','Versi&oacute;n');
				    
				    
				    $crud->set_lang_string('form_update_changes','Actualizar Cambios');
				 	$crud->set_lang_string('form_update_and_go_back','Cancelar'); 
				 	
				 	
							 	
			 	 }
			

 	
 	/*$crud->set_lang_string('update_success_message',
		 'Los datos fueron exitosamente almacenados. Espere mientras se redirecciona.
		 <script type="text/javascript">
		  window.location = "'.site_url(strtolower('principal').'/'.strtolower('inicio')).'";
		 </script>
		 <div style="display:none">
		 '
   ); */ 
    
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	/*$crud->add_fields('IdOT','IdProdElab','IdSubEtapa','Cant_Teorica','Cant_Real','Cant_Muest_Productiva','Cant_Muest_No_Productiva','Tiempo_Preparacion','Tiempo_Ejecucion','Observacion_DatosOT','IdMaquina','IdPunzon','Parcial','HParcial','Retrabajo','Lote_Anterior','Observacion_Reproceso','Cantidad_Rotulos','RotuloCP','Peso_Promedio','Muestra_ControlCalidad','Muestra_ValidacionEstabilidad');*/
    
     
     $crud->callback_before_update(array($this,'Conv_codigo_mayusculas')); 
     
     /*Llamada a funciones para almacenar en el log*/
  	$crud->callback_after_update(array($this, 'graba_log_ap_edit_add'));		//Funcion que se llama despues de hacer un update (guarda en el log cuando se pone el estado ap)
       
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('AP','Codigo','Version');
    
    $crud->callback_edit_field('AP',array($this,'armar_ap'));
 	
 	$crud->set_rules('Codigo','C&oacute;digo','required|min_length[13]|max_length[15]');
 	$crud->callback_edit_field('Codigo',array($this,'armar_codigo'));
 	//$crud->set_rules('NumOT','Nro de OT','rtrim|regex_match[/^[a-zA-Z]{2}[0-9]{6}$/]');
 	
 	$crud->set_rules('Version','Versi&oacute;n','required|max_length[2]');
 	$crud->callback_edit_field('Version',array($this,'armar_version'));
 	
 	//De esta forma no me muestra el mensaje para volver a la lista despues de actualizar los datos
 	$crud->set_lang_string('update_success_message','Sus datos han sido actualizados correctamente.
 	<div style="display:none">
 	');
     
     
    
 	/*Llamo a una funcion para que despues de insertar me limpie los campos menos el campor del numero de orden*/
 	//$crud->callback_after_insert(array($this, 'limpiar_campos'));
 	
 	//$crud->callback_after_update(array($this, 'Despues_insert'));
 	
 	//$crud->field_type('DescPunzon', 'invisible'); 	 	 	
    /* Generamos la tabla */
    //$crud->unset_jquery();
    //$crud->unset_jquery_ui();
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('cambiaestado_ot_ap_add/admincambiaestado_ot_ap_add', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  
  }



function armar_ap($value, $primary_key)
{
 //$variable = 'AP'; 	
 return '<input type="text" value="AP" name="AP" style="width:50px;" readonly>';	
}

function armar_codigo($value, $primary_key)
{
 return '<input type="text" maxlength="15" value="'.$value.'" name="Codigo" style="width:150px;" placeholder="Ej: XX-XXX">';
 /*return '<input type="text" maxlength="2" value="40" id="Parte1" style="width:40px;"> - <input type="text" maxlength="3" value="MAR" id="Parte2" style="width:60px;"> - <input type="text" maxlength="10" value="0265410688" id="Parte3" style="width:100px;">';*/	
}

function armar_version($value, $primary_key)
{
 return '<input type="text" maxlength="2" value="'.$value.'" name="Version" style="width:50px;" placeholder="Ej: 01">';	
}


function Conv_codigo_mayusculas($post_array) 
{	
	$Codigo_Mayusc = strtoupper($post_array['Codigo']); 		//Convierte la cadena en may�sculas antes de almacenarla
		
	//$elim_espacios = trim($NumOT_Mayusc); 					//Elimina los espacios en blanco de al comienzo y al final de la cadena
	$post_array['Codigo'] = $Codigo_Mayusc;
 
	return $post_array;
}



/*Funcion que llama al modelo para insertar los registros del log cuando hago un update (cargo los datos para el estado ap)*/
   function graba_log_ap_edit_add($post_array, $primary_key)
   {   	
   	$consulta = $this->Model_gestion_datos_ot->graba_log_ot_ap_add($post_array, $primary_key);		
   	if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
   }



/*function Quitar_AP($primary_key)
	{
	 $resPHP = "<script type='text/javascript'>
	             var res = confirm('Esta seguro que quiere cambiar el estado?');
	             document.write(res);
	             
	             if(res == true) 
	                      {
	                      	console.log(res);
	                      	window.location.href = 'http://127.0.0.1/produccion/index.php/cargardatosot/admincargardatosot';
	                      }
	                 else  {
	                 	    console.log(res);
	                 	    window.location.href = 'http://127.0.0.1/produccion/index.php/principal/inicio';   
	                 	    }
	             </script>";
	  echo $resPHP;         
	 
	  if($resPHP == true)
	  			  {
	  			  	echo $resPHP; 
	  			  	$consulta = $this->Model_gestion_datos_ot->quitar_estado_ap($primary_key);	
				 	 	 	
					  /*if($consulta == FALSE)
					  			 return FALSE;	  			 
					   		else return TRUE;*/
					   		/*
				   }
			else echo $resPHP; 	   
	}*/

}
?>
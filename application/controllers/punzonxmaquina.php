<?
/* Heredamos de la clase CI_Controller */
class Punzonxmaquina extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
 	$this->load->model('Model_gestion_punxmaq');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('punzonxmaquina/adminpunzonxmaquina');
  }
 
  
  function adminpunzonxmaquina()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if($this->session->userdata('Nivel') == 0)
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('punxmaq');
 
    /* Le asignamos un nombre */ 
    $crud->set_subject('Punz&oacute;n por Maquina'); 		//Este nombre es el que va a aparecer en el formulario, en el boton a�adir, editar, etc
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/     
        
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    
    
    $state = $crud->getState();
	$state_info = $crud->getStateInfo();
	
	if($state == 'add')
			 {	
			  /*Relacion con la Tabla Punzon*/
			  $crud->set_relation('IdPunzon','Punzon','DescPunzon',array('EstadoPunzon' => 'Habilitado'));
			  /*Relacion con la Tabla Maquina*/
			  $crud->set_relation('IdMaquina','Maquina','DescMaquina',array('EstadoMaquina' => 'Habilitada'));	
			 }
		else if($state == 'edit')
					 {
					  
					  //Obtengo el id del registro que estoy editando
					  $primary_key = $state_info->primary_key;
					  $idpun = $this->Model_gestion_punxmaq->obtiene_idpun($primary_key);
					  
					  $array_result_pun = $this->Model_gestion_punxmaq->obtiene_datos_pun_edit($idpun);
					  $crud->field_type('IdPunzon','dropdown',$array_result_pun);
					  
					  
					  $idmaq = $this->Model_gestion_punxmaq->obtiene_idmaq($primary_key);
					  
					  $array_result_maq = $this->Model_gestion_punxmaq->obtiene_datos_maq_edit($idmaq);
					  $crud->field_type('IdMaquina','dropdown',$array_result_maq);
					  
					  
					  
					 }
				else {
					  /*Relacion con la Tabla Punzon*/
    				  $crud->set_relation('IdPunzon','Punzon','DescPunzon');
    				  /*Relacion con la Tabla Maquina*/
    				  $crud->set_relation('IdMaquina','Maquina','DescMaquina');
					 }	
    
    /*Relacion con la Tabla Punzon*/
    //$crud->set_relation('IdPunzon','Punzon','DescPunzon');
    
    /*Relacion con la Tabla Maquina*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    $crud->display_as('IdPunzon','Punz&oacute;n');  
    $crud->display_as('IdMaquina','Maquina');  
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'IdPunzon',
      'IdMaquina'     
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'IdPunzon',
      'IdMaquina'
    );
 
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('IdPunzon','IdMaquina');
    
    /*Cambiar el Label de los campos*/
    //$crud->display_as('CodProdElab','C&oacute;digo de Producto');
       
    /*Definimos que campos son los que voy a mostrar para la edicion en al edit*/
    $crud->edit_fields('IdPunzon','IdMaquina');
 
 	/*Ocultamos los campos y botones que no queremos que el usuario vea o utilice*/
 	$crud->unset_read();			//Deshabilita la funcion view
 	//$crud->unset_edit();			//Deshabilita la funcion edit
 	$crud->unset_delete();			//Deshabilita la funcion delete
 	$crud->unset_export();			//Deshabilita la funcion export
 	$crud->unset_print();			//Deshabilita la funcion print	
 		
 	//$crud->set_lang_string('form_error_message','No se pudo insertar!!! Ya se encuentran cargados.');
 	
 	/*Llama a la funcion antes de realizar el insert para verificar que no se inserten muchas veces la misma convinaci�n de punz�n/maquina*/
 	//$crud->callback_insert(array($this,'verifica_punxmaq'));
 	$crud->callback_before_insert(array($this,'verifica_punxmaq')); 	 		
 		
 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('punzonxmaquina/adminpunzonxmaquina', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }


/*Funcion que llama al model para verificar la carga de la relaci�n punz�n/maquina*/
 function verifica_punxmaq($post_array)
	{ 
	 $msj = $this->Model_gestion_punxmaq->consulta_punxmaq($post_array);
	  
	  /*Si desde el modelo me retorna false, retornamos false para no hacer de nuevo la carga de la misma relaci�n*/	  
	  if($msj == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;	  	
	}

}
?>
<? 

/* Heredamos de la clase CI_Controller */
class Principal extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
    //$this->load->library('session');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    redirect('inicio');
  }
 
  
  function inicio()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    	//Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2)||($this->session->userdata('Nivel') == 3))
 		{//Inicio del if de nivel de usuario
 
    /* Creamos el objeto */
    //$crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    //$crud->set_theme('datatables');
 
    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
   // $crud->set_table('personas');
 
    /* Le asignamos un nombre */
    //$crud->set_subject('personas');
    
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
   // $crud->set_relation('IdDepto','Departamento','DescDepartamento');
    
    /* Asignamos el idioma espa�ol */
    //$crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    //date_default_timezone_set('America/Argentina/San_Juan');
    
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    /*$crud->required_fields(
      'Idpersona',
      'Nombre',
      'Apellido',
      'IdDepto'
    );*/
 
    /* Aqui le indicamos que campos deseamos mostrar */
    /*$crud->columns(
      'Idpersona',
      'Nombre',
      'Apellido',
      'Cantidad',
      'Fecha_Nac',
      'IdDepto'
    );*/
 
    /* Generamos la tabla */
    //$output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('principal');
   $this->load->view('footer');
   
   }//Fin del if de nivel de usuario
  	else redirect('');
	
	}//Fin del if de validaci�n de usuario 
  else redirect('');
    
  }
}
?>
<?
/* Heredamos de la clase CI_Controller */
class Cargar_editarOT extends CI_Controller {
 
  function __construct()
  {
     parent::__construct();
 
    $this->load->model('Grocery_crud_model');
    /* Cargamos la base de datos */
    $this->load->database();
    
    $this->load->model('Model_gestion_producto');
 	$this->load->model('Model_gestion_ot');
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* A�adimos el helper al controlador */
    $this->load->helper('url');
  
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    //redirect('personas/administracion');
    redirect('cargar_editarot/admincargar_editarot');
  }
 
  
  function admincargar_editarot()
  {
    //Validamos si el usuario ingreso al sistema correctamente (Logueandose)  
    $Usuario = $this->session->userdata('Usuario');
    if(($Usuario!='')&&($Usuario!=null))
    {
    
    //Valida el nivel del usuario
  	if(($this->session->userdata('Nivel') == 0)||($this->session->userdata('Nivel') == 1)||($this->session->userdata('Nivel') == 2))
 		{//Inicio del if de nivel de usuario
    
    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionamos el tema */
    $crud->set_theme('datatables');
 
	/***Codigo para mostrar en la tabla las filas que cumplan con alguna condicion***/
		 /*$crud->where ('Cantidad', 35);
		$crud->set_table ('personas');
		$crud->order_by ('Idpersona');*/
	/*********************************************/
		
 	/* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('ot');
 
    /* Le asignamos un nombre */ 
    $crud->set_subject('OT'); 		//Este nombre es el que va a aparecer en el formulario, en el boton a�adir, editar, etc
    
    /*Relacion con la Tabla VaP*/
    $crud->set_relation('IdVaP','vap','DescVaP');
    
    //Me muestra todos los productos esten en cualquiera de los estados 
    //$crud->set_relation('IdProdWS','Productosws','{CodProdWS} - {DescProdWS}');
    
    
    $state = $crud->getState();				//Obtiene el estado en el que se encuentra la aplicaci�n 
	$state_info = $crud->getStateInfo();	//Para obtener el id del registro con el que estoy trabajando
	if($state == 'add')
				{
				 /*Relacion con la Tabla Producto WS - Filtra los productos que estan en estado AC*/
   				 $crud->set_relation('IdProdWS','Productosws','{CodProdWS} - {DescProdWS}',array('EstadoProdWS' => 'AC'));
   				 	
				}
   		else  {
	   		   if($state == 'edit')
					{
					 $primary_key = $state_info->primary_key;	//obtiene el id del registro que estoy editando
					 //Obtengo el id del producto
					 $idprod = $this->Model_gestion_producto->obtiene_IdProd($primary_key);
					 //Obtengo la consulta SQL en un arreglo
				     $array1 = $this->Model_gestion_producto->obtiene_IdProductos($idprod);
				     //Obtengo la consulta SQL en un arreglo
				     $array2 = $this->Model_gestion_producto->obtiene_DescProductos($idprod);
					 //Combion los dos arreglos para armar el select
					 $array3 = array_combine($array1, $array2);
					 $crud->field_type('IdProdWS','dropdown',$array3);
					
					}
				else{
					 $crud->set_relation('IdProdWS','Productosws','{CodProdWS} - {DescProdWS}');	
					}
			 }
    
    
    /**Codigo para cargar solo los valores indicados en el arreglo**/
    //$crud->field_type('Cantidad','enum',array(30,40));		//Permite seleccionar un solo valor
    //$crud->field_type('Cantidad','set',array(30,40)); //Permite seleccionar los dos valores 
    //$crud->field_type('Cantidad','dropdown', array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));			//Permite seleccionar una opcion y almacena el valor 
    /***********************************/     
        
    /*Hace la relacion por id de departamento y me trae el nombre del departamento*/
    /*IdDepto 			Nombre del campo en la tabla con la clave foranea
      Departamento 		Nombre de la tabla donde esta el valor que quiero mostrar 
      DescDepartamento	Nombre del campo donde esta el valor que quiero reemplazar*/
    //$crud->set_relation('IdDepto','Departamento','DescDepartamento');
    
    /*Relacion con la Tabla Punzon*/
    //$crud->set_relation('IdPunzon','Punzon','DescPunzon');
    
    /*Relacion con la Tabla Maquina*/
    //$crud->set_relation('IdMaquina','Maquina','DescMaquina');
                
    /*Para el campo VoMM permite elegir uno entre dos valores*/
    //$crud->field_type('VoMM','enum',array('Venta','Muestra M&eacute;dica'));
    
    /*Para el campo Estado de Producto permite elegir uno entre varios valores*/
    //$crud->field_type('EstadoProdElab','enum',array('AC','DI','IC','AL','AR'));
    
    /* Asignamos el idioma espa�ol */
    $crud->set_language('spanish');
    
    /*Le cambio el nombre a la columna IdDepto por Nombre de Departamento*/
    //$crud->display_as('IdDepto','Nombre de Departamento');  
    
    //$crud->display_as('IdOT','Ident. de OT');
    $crud->display_as('NumOT','Nro de OT');
    $crud->display_as('FechaVto','Fecha de Vencimiento');
    $crud->display_as('IdVaP','VaP');
    $crud->display_as('IdProdWS','Ident. del Producto WS');
     
    
		    /*Se agrago este codigo para cambiar la configuracion de la fecha ya que daba un error cuando se mostraba la grilla*/
		    date_default_timezone_set('America/Argentina/San_Juan');
          
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      //'NumOT',
      'Lote',
      'FechaVto',
      'IdVaP',
      'IdProdWS'   
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      //'IdOT',
      'NumOT',
      'Lote',
      'FechaVto',
      'IdVaP',
      'IdProdWS'
      
    );
 
 	/*******Reglas de validacion*****/
    /*Regla para validar que se ingrese el numero de OT con 2 letras (mayusculas o minusculas, antes de 	actualizar se convierten a mayusculas) y 6 n�meros*/
 	$crud->set_rules('NumOT','Nro de OT','rtrim|regex_match[/^[a-zA-Z]{2}[0-9]{6}$/]');
 	
 	//Regla para validar el campo Lote
 	$crud->set_rules('Lote','Lote','trim|required|min_length[3]');
 	
 	/*Definimos que campos son los que voy a mostrar para la carga en al add*/
 	$crud->add_fields('NumOT','Lote','FechaVto','IdVaP','IdProdWS');         
    /*Definimos que campos son los que voy a mostrar para la carga en al edit*/
    $crud->edit_fields('NumOT','Lote','FechaVto','IdVaP','IdProdWS');
    
    $crud->set_lang_string('update_error','Ocurrio un error al actualizar. Verifique que la OT no se encuentre cargada.');
    
    //Ocultamos los campos Usuario_Visado y Usiario_Liberado para que no sean editable y los cargamos con el usuario de la session al momento de hacer el insert o el edit
    //$crud->field_type('Usuario_Visado','hidden');
    //$crud->field_type('Usuario_Liberado','hidden');
     
 	/*Ocultamos los campos y botones que no queremos que el usuario vea o utilice*/
 	//$crud->unset_add();				//Deshabilitar la funcion add
 	//$crud->unset_read();			//Deshabilita la funcion view
 	//$crud->unset_edit();			//Deshabilita la funcion edit
 	$crud->unset_delete();			//Deshabilita la funcion delete
 	$crud->unset_export();			//Deshabilita la funcion export
 	$crud->unset_print();			//Deshabilita la funcion print	
 	 
 	 /*Llamada a la funcion que pone los valores de la ot en mayusculas antes de insertarlos en la base de datos*/ 
    $crud->callback_before_insert(array($this,'Conv_mayusculas_add'));
 	 
 	 
 	 /*Llamada a la funcion que pone el valor de la ot en mayusculas antes de actualizar en la base de datos por las dudas que se modifiquen*/ 
    $crud->callback_before_update(array($this,'Conv_mayusculas_edit'));	
 	
 	
 	//Almacenar el log al insertar
 	$crud->callback_after_insert(array($this, 'log_insertar'));

	//Almacena el log despues de editar
	$crud->callback_after_update(array($this, 'log_editar'));
 	
 	
 	
 	//$crud->field_type('DescPunzon', 'invisible'); 	 	 	
    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
   $this->load->view('menu');
   $this->load->view('cargar_editarot/admincargar_editarot', $output);
   $this->load->view('footer');
   
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  
  }//Fin del if de nivel de usuario
  	else redirect('principal/inicio');
  
  }//Fin del if de validaci�n de usuario 
  else redirect('');
  
  }

	//Funcion que convierte en mayuscula el numero de OT y adem�s verifica que ese numero de OT no este cargada
	function Conv_mayusculas_add($post_array) 
	{	
		if ($post_array['NumOT'] != '')
				{	 
				 $Num_Mayusc = strtoupper($post_array['NumOT']);;
				 $post_array['NumOT'] = $Num_Mayusc;
				 
				 $consulta = $this->Model_gestion_ot->consulta_ot_cargada_add($post_array);
				 
				 if($consulta != FALSE)
						 return $post_array;
					else return FALSE; 			
				}

	}   
	
	
	//Funcion que convierte en mayusculas el Numero de la OT y verifica que la OT que se quiere cargar no se encuentre cargada
	function Conv_mayusculas_edit($post_array, $primary_key) 
	{	
		if ($post_array['NumOT'] != '')
		{
		 $Num_Mayusc = strtoupper($post_array['NumOT']);;
		 $post_array['NumOT'] = $Num_Mayusc;
	 	
	 	 $consulta = $this->Model_gestion_ot->consulta_ot_cargada_edit($post_array, $primary_key);
				 
				 if($consulta != FALSE)
						 return $post_array;
					else return FALSE; 	
	 	
	 	}
	}



//Funcion para almacenar el log al insertar
function log_insertar($post_array, $primary_key) 
	{	
	 $consulta = $this->Model_gestion_ot->graba_log_ot_add($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
		
	}  


//Funcion para almacenar el log al editar
function log_editar($post_array, $primary_key) 
	{	
	 $consulta = $this->Model_gestion_ot->graba_log_ot_edit($post_array, $primary_key);		
   	 if($consulta == FALSE)
	  			 return FALSE;	  			 
	  		else return TRUE;
		
	}


}
?>
<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_datos_ot2 extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion para obtener el código del producto
/*function obtiene_idprod($post_array, $primary_key)
{
 $IdProd = $post_array['IdProdElab'];
 $this->db->select('IdProdElab');
 $this->db->where('IdProdElab',$IdProd);
 //$this->db->from('prodelaboracion');
 $query = $this->db->get('prodelaboracion');
 $dato = $query->row()->IdProdElab; 
 
 if($query->num_rows()>0)
                  return FALSE;
             else return $dato;
 
}*/

/*function obtiene_idprod($post_array, $primary_key)
{
 $IdProd = $post_array['IdProdElab'];
 //$where = '(IdProdElab=$IdProd or EstadoProdElab = "AC")';
 
 $this->db->select('CodProdElab');
 $this->db->where('IdProdElab',$IdProd);
 $this->db->or_where('EstadoProdElab','AC');
 //$this->db->where($where);
 $query = $this->db->get('prodelaboracion');
 
	$records = array();
	foreach ($query->result() as $row)
		{
        $records[] = $row;
		}
	/*while ($row = mysql_fetch_assoc($query))
	{
	  $records[] = $row['IdProdElab'];
	}*/
 
 /*return $records;
 
}*/

//Funcion que obtiene el id del producto para luego armar el arreglo para el dropdown
function obtiene_idprod($primary_key)
{
 //$this->db->select('CodProdElab');
 $this->db->select('IdProdElab');
 $this->db->from('datosot');
 $this->db->where('IdDatosOT',$primary_key);

 $query = $this->db->get();
 
 return $query->row()->IdProdElab;
}


//Funcion que obtiene el arreglo resultante para armar el dropdown
function obtiene_datos_prod_edit($idprod)
{
 //$IdProd = $post_array['IdProdElab'];
 //$this->db->select('CodProdElab');
 $this->db->select('IdProdElab, CodProdElab, DescProdElab');
 $this->db->from('prodelaboracion');
 $this->db->where('IdProdElab',$idprod);
 $this->db->or_where('EstadoProdElab','AC');
 $this->db->order_by('IdProdElab', 'asc');
 //$query = $this->db->get('prodelaboracion');
 $query = $this->db->get();
 
 $array1_prod = array();
 $array2_prod = array();
 $array3_prod = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_prod, $row->IdProdElab);
  array_push($array2_prod, $row->CodProdElab." - ".$row->DescProdElab);
 }
 
 $array3_prod = array_combine($array1_prod, $array2_prod);
 return $array3_prod;
 
}



//Se utilizó para hacer una prueba
//Funcion que obtiene el Número de una OT para filtrar el crud
/*function obtiene_NumOT($IdOT)
{
 //$this->db->select('CodProdElab');
 $this->db->select('NumOT');
 $this->db->from('ot');
 $this->db->where('IdOT',$IdOT);

 $query = $this->db->get();
 
 return $query->row()->NumOT;
}*/






/*Fin de las funciones del modelo*/








//Funcion para consultar si una Descripción de Maquina ya estan cargadas
function consulta_campos_maquina_add($post_array){
	  	  
	  $DescripcionMaq = trim($post_array['DescMaquina']);
	  $this->db->where('DescMaquina',$DescripcionMaq);
	  $this->db->from('maquina');
	  $query = $this->db->get();
	  $dato_estado_maq = $query5->row()->EstadoMaquina;
	  
	  /*Si la descripcion de la maquina se encuentran cargada retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}
	

/*Funcion para consultar si una Descripción de Maquina ya estan cargadas*/
function consulta_campos_maquina_edit($post_array){
	  
	  $IdMaquina = $post_array['IdMaquina'];	  
	  $DescripcionMaq = trim($post_array['DescMaquina']);
	  $this->db->where('IdMaquina !=',$IdMaquina);
	  $this->db->where('DescMaquina',$DescripcionMaq);
	  $this->db->from('maquina');
	  $query = $this->db->get();
	  
	  /*Si la descripcion de la maquina se encuentran cargada retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}


/*Funcion que me devuelve los valores de las relaciones para completar el log*/
function graba_log_maq_add($post_array, $primary_key)
{
	$this->db->select('DescEtapa');
	$this->db->where('IdEtapa',$post_array['IdEtapa']);
	$query1 = $this->db->get('etapa');
	$dato_desc_etapa = $query1->row()->DescEtapa;
 		
	$this->db->select('DescPlanta');
	$this->db->where('IdPlanta',$post_array['IdPlanta']);
	$query2 = $this->db->get('planta');
	$dato_desc_planta = $query2->row()->DescPlanta;
	
	$this->db->select('EstadoMaquina');
	$this->db->where('IdMaquina',$primary_key);
	$query5 = $this->db->get('maquina');
	$dato_estado_maq = $query5->row()->EstadoMaquina;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: maquina",
   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$post_array['DescMaquina'].",  Etapa: ".$dato_desc_etapa.",  Planta: ".$dato_desc_planta.",  Est.Maq: ".$dato_estado_maq
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_maq_edit($post_array, $primary_key)
{
	$this->db->select('DescEtapa');
	$this->db->where('IdEtapa',$post_array['IdEtapa']);
	$query1 = $this->db->get('etapa');
	$dato_desc_etapa = $query1->row()->DescEtapa;
 		
	$this->db->select('DescPlanta');
	$this->db->where('IdPlanta',$post_array['IdPlanta']);
	$query2 = $this->db->get('planta');
	$dato_desc_planta = $query2->row()->DescPlanta;
	
	$this->db->select('EstadoMaquina');
	$this->db->where('IdMaquina',$primary_key);
	$query5 = $this->db->get('maquina');
	$dato_estado_maq = $query5->row()->EstadoMaquina;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Se edito un registo",
   	"Tabla" => "Tabla: maquina",
   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$post_array['DescMaquina'].",  Etapa: ".$dato_desc_etapa.",  Planta: ".$dato_desc_planta.",  Est.Maq: ".$dato_estado_maq
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}


/*Funcion que elimina una maquina, cambia el estado de la maquina a Habilitada*/
function cambia_estado_deshab($primary_key)
{
 $this->db->where('IdMaquina', $primary_key);
 $this->db->update('maquina',array('EstadoMaquina' => 'Deshabilitada'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('DescMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_DescMaquina = $query1->row()->DescMaquina;
 
 $this->db->select('EstadoMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_EstadoMaquina = $query1->row()->EstadoMaquina;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Elimino una Maquina",
			   	"Tabla" => "Tabla: maquina",
			   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$dato_DescMaquina.", Estado: ".$dato_EstadoMaquina
			 
				);
 
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}


/*Funcion que elimina una maquina, cambia el estado de la maquina a Habilitada*/
function cambia_estado_hab($primary_key)
{
 $this->db->where('IdMaquina', $primary_key);
 $this->db->update('maquina',array('EstadoMaquina' => 'Habilitada'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('DescMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_DescMaquina = $query1->row()->DescMaquina;
 
 $this->db->select('EstadoMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_EstadoMaquina = $query1->row()->EstadoMaquina;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Habilito una Maquina",
			   	"Tabla" => "Tabla: maquina",
			   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$dato_DescMaquina.", Estado: ".$dato_EstadoMaquina
			 
				);
 
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}


/*Funcion que obtiene el Estado de una Maquina*/
function obtiene_estado_maq($primary_key)
{
 $this->db->select('EstadoMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query5 = $this->db->get('maquina');
 $dato_estado = $query5->row()->EstadoMaquina;
 return $dato_estado;	
}

function obtiene_nro_ot($primary_key)
{
 $this->db->select('NumOT');
 $this->db->where('IdOT',$primary_key);
 $query = $this->db->get('ot');
 $dato_NumOT = $query->row()->NumOT;
 return $dato_NumOT;
 	
}

function borrar_ap($primary_key)
{
 $this->db->where('IdDatosOT', $primary_key);
 
 $this->db->update('datosot', array('AP' => null, 'Codigo' => null, 'Version' => null));
 
 //Primero obtenfo la id de la ot con la clave primaria enviada
	$this->db->select('IdOT');
	$this->db->from('datosot');
	$this->db->where('IdDatosOT',$primary_key);
	$query = $this->db->get();
	$dato_IdOT = $query->row()->IdOT;
	
	//Despues obtengo el numero de la ot con la id obtenida antes
	$this->db->select('NumOT');
	$this->db->from('ot');
	$this->db->where('IdOT',$dato_IdOT);
	$query2 = $this->db->get();
	$dato_NumOT = $query2->row()->NumOT;
	
	$logs_edit_ap_del = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Se borro el estado AP una OT",
   	"Tabla" => "Tabla: Datos OT",
   	"Valores" => "Id DatosOT: ".$primary_key.",  Nro OT: ".$dato_NumOT
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_edit_ap_del);
 
 
 return TRUE;
 	
}



/*Funcion que obtiene los valores para completar el log al cargar los datos de ap*/
function graba_log_ot_ap_add($post_array, $primary_key)
{	
	//Primero obtenfo la id de la ot con la clave primaria enviada
	$this->db->select('IdOT');
	$this->db->from('datosot');
	$this->db->where('IdDatosOT',$primary_key);
	$query = $this->db->get();
	$dato_IdOT = $query->row()->IdOT;
	
	//Despues obtengo el numero de la ot con la id obtenida antes
	$this->db->select('NumOT');
	$this->db->from('ot');
	$this->db->where('IdOT',$dato_IdOT);
	$query = $this->db->get();
	$dato_NumOT = $query->row()->NumOT;
	
	$logs_edit_ap_add = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Se puso en estado AP una OT",
   	"Tabla" => "Tabla: Datos OT",
   	"Valores" => "Id DatosOT: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Estado: AP ,  C&oacute;digo: ".$post_array['Codigo'].",  Versi&oacute;n: ".$post_array['Version']
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_edit_ap_add);
	 
	return TRUE;
}


/*Funcion que obtiene los valores para completar el log al borrar los datos de ap*/
function graba_log_ot_ap_del($post_array, $primary_key)
{	
	//Primero obtenfo la id de la ot con la clave primaria enviada
	$this->db->select('IdOT');
	$this->db->from('datosot');
	$this->db->where('IdDatosOT',$primary_key);
	$query = $this->db->get();
	$dato_IdOT = $query->row()->IdOT;
	
	//Despues obtengo el numero de la ot con la id obtenida antes
	$this->db->select('NumOT');
	$this->db->from('ot');
	$this->db->where('IdOT',$dato_IdOT);
	$query2 = $this->db->get();
	$dato_NumOT = $query2->row()->NumOT;
	
	$logs_edit_ap_del = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Se borro el estado AP una OT",
   	"Tabla" => "Tabla: Datos OT",
   	"Valores" => "Id DatosOT: ".$primary_key.",  Nro OT: ".$dato_NumOT
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_edit_ap_del);
	 
	return TRUE;
}


//Funcion que hace la consulta para armar el select de maquina
function Buscamaquina($Subetapa_buscar)
{
 $this->db->select('maquina.IdMaquina, maquina.DescMaquina');
 $this->db->from('datosot');
 $this->db->join('subetapa', 'datosot.IdSubetapa = subetapa.IdSubetapa');
 $this->db->join('etapa', 'subetapa.IdEtapa = etapa.IdEtapa');
 $this->db->join('maquina', 'etapa.IdEtapa = maquina.IdEtapa');
 $this->db->where('datosot.IdSubEtapa',$Subetapa_buscar);
 $this->db->where('maquina.EstadoMaquina','Habilitada');
 //De esta forma hago que no se me repitan los valores de la consulta
 $this->db->distinct();
 $this->db->order_by("maquina.DescMaquina", "asc");
 $query = $this->db->get();
 
 if($query->num_rows()>0)
            return $query->result();
       else return FALSE;	
}


//Funcion que hace la consulta para armar el select de punzon
function Buscapunzon($IdMaquina_buscar)
{
 $this->db->select('punzon.IdPunzon, punzon.DescPunzon');
 $this->db->from('punzon');
 $this->db->join('punxmaq', 'punxmaq.IdPunzon = punzon.IdPunzon');
 $this->db->join('maquina', 'maquina.IdMaquina = punxmaq.IdMaquina');
 $this->db->where('maquina.IdMaquina',$IdMaquina_buscar);
 $this->db->where('punzon.EstadoPunzon','Habilitado');
 $this->db->order_by("punzon.DescPunzon", "asc");
 $query = $this->db->get();
 
 if($query->num_rows()>0)
            return $query->result();
       else return FALSE;	
}







/*------------------------FIN DE LAS FUNCIONES----------------------------*/	
	



}
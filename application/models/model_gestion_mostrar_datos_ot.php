<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_mostrar_datos_ot extends CI_Model {

	function __construct() {
		parent::__construct();
	}


/*function obtiene_datos_ot($post_array)
{
 $IdOT = $post_array['IdOT'];
 //$this->db->select('IdDatosOT');
 $this->db->where('IdOT',$IdOT);
 $this->db->from('datosot');
 $query = $this->db->get();
 $query->result();
 
 return $query->result();
 
}*/



function obtiene_datos()
{
 //$IdProd = $post_array['IdProdElab'];
 $this->db->select('IdProdElab, CodProdElab, DescProdElab, DescMaquina, EstadoProdElab');
 $this->db->from('prodelaboracion');
 //$this->db->where('IdProdElab', $IdProd);
 $this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->result();
             else return FALSE;		
}

//Obtiene el id de los productos de ws
function obtiene_idprodws()
{
 //$IdProd = $post_array['IdProdElab'];
 $this->db->select('IdProductoWS');
 $this->db->from('productosws');
 //$this->db->where('IdProdElab', $IdProd);
 $this->db->order_by("IdProductoWS","asc");
 $query = $this->db->get();

 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->IdProductoWS);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 return $array;
	
}

//Obtiene el codigo y la descripcion de los productos ws
function obtiene_desc_prodws()
{
 //$IdProd = $post_array['IdProdElab'];
 $this->db->select('IdProductoWS, CodProdWS, DescProdWS');
 $this->db->from('productosws');
 //$this->db->where('IdProdElab', $IdProd);
 $this->db->order_by("IdProductoWS","asc");
 $query = $this->db->get();
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->CodProdWS." - ".$row->DescProdWS);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 return $array;		
}


function Buscar_datos($IdOT, $IdProdElab, $IdSubEtapa, $IdMaquina, $IdPunzon, $Parcial)
{
 //Voy consultando si las variables enviadas estan vacias para realiza diferentes consultas
 
 $this->db->select('ot.NumOT, prodelaboracion.CodProdElab, prodelaboracion.DescProdElab, ot.Lote, Fecha_Carga, ot.FechaVto, vap.DescVaP, datosot.Cant_Teorica, datosot.Cant_Real, datosot.Cant_Muest_Productiva, datosot.Cant_Muest_No_Productiva, datosot.Tiempo_Preparacion, datosot.Tiempo_Ejecucion, datosot.Observacion_DatosOT, datosot.IdMaquina, punzon.DescPunzon, datosot.Peso_Promedio, datosot.Muestra_ControlCalidad, datosot.Muestra_ValidacionEstabilidad');

$this->db->from('datosot');
						 
$this->db->join('ot','ot.IdOT = datosot.IdOT');
$this->db->join('prodelaboracion','prodelaboracion.IdProdElab = datosot.IdProdElab');
$this->db->join('vap','vap.IdVaP = ot.IdVaP');
//$this->db->join('maquina','maquina.IdMaquina = datosot.IdMaquina');
//$this->db->join('planta','planta.IdPlanta = maquina.IdPlanta');
$this->db->join('punzon','punzon.IdPunzon = datosot.IdPunzon');
 
 

/*-----------Las variables que se nombran abajo son las que se evaluan-----------*/			 
//$NumOT, $CodDescProdElab, $DescSubEtapa, $DescMaquina, $DescPunzon

if($NumOT == '')
		 {
		  if($DescSubEtapa == '')
		  			{
				     if($DescMaquina == '')
								{
								 if($DescPunzon == '')
										   {
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										   $this->db->like('punzon.DescPunzon',$DescPunzon);
										   if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}
										  }
								}
						   else {//$DescMaquina != Vacio
								  $this->db->like('DescMaquina',$DescMaquina);
								  if($DescPunzon == '')
										   {
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										    $this->db->like('punzon.DescPunzon',$DescPunzon);
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}
										   }
								 }
				    }	   
			  else	{//$DescSubEtapa != Vacio
			  	     $this->db->like('subetapa.DescSubEtapa',$DescSubEtapa);
			  	     if($DescMaquina == '')
								{
								 if($DescPunzon == '')
										   {
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										   $this->db->like('punzon.DescPunzon',$DescPunzon);
										   if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}
										  }
								}
						   else {//$DescMaquina != Vacio
								  $this->db->like('DescMaquina',$DescMaquina);
								  if($DescPunzon == '')
										   {
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										    $this->db->like('punzon.DescPunzon',$DescPunzon);
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}
										   }
								 }
			  	     
			  	      
			        }
			  }
	     else {//$NumOT != Vacio
	           $this->db->like('ot.NumOT',$NumOT);
	           if($DescSubEtapa == '')
		  			{
				     if($DescMaquina == '')
								{
								 if($DescPunzon == '')
										   {
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										   $this->db->like('punzon.DescPunzon',$DescPunzon);
										   if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}
										  }
								}
						   else {//$DescMaquina != Vacio
								  $this->db->like('DescMaquina',$DescMaquina);
								  if($DescPunzon == '')
										   {
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										    $this->db->like('punzon.DescPunzon',$DescPunzon);
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}
										   }
								 }
				    }	   
			  else	{//$DescSubEtapa != Vacio
			  	     $this->db->like('subetapa.DescSubEtapa',$DescSubEtapa);
			  	     if($DescMaquina == '')
								{
								 if($DescPunzon == '')
										   {
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										   $this->db->like('punzon.DescPunzon',$DescPunzon);
										   if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}
										  }
								}
						   else {//$DescMaquina != Vacio
								  $this->db->like('DescMaquina',$DescMaquina);
								  if($DescPunzon == '')
										   {
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										    $this->db->like('punzon.DescPunzon',$DescPunzon);
										    if($CodDescProdElab != '')
										    			{//$CodDescProdElab != '', sino no hace nada
														 $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					  									 $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
														}
										   }
								 }
			  	     
			  	      
			        }
		 	
		      }				

/*-------------------------------------------------------------------------------*/

 
 
 
$query = $this->db->get();

if($query->num_rows()>0)
                  return $query->result();
             else return FALSE;

}




//Funcion que obtiene el arreglo resultante para armar el Multiselect field para al add
function obtiene_datos_maq_list()
{
 //$IdProd = $post_array['IdProdElab'];
 //$this->db->select('CodProdElab');
 $this->db->select('IdMaquina, DescMaquina, CodMaq');
 $this->db->from('maquina');
 //$this->db->where('IdProdElab',$idprod);
 //$this->db->where('EstadoMaquina','Habilitada');
 $this->db->order_by('IdMaquina', 'asc');
 //$query = $this->db->get('prodelaboracion');
 $query = $this->db->get();
 
 $array1_maq = array();
 $array2_maq = array();
 $array3_maq = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_maq, $row->IdMaquina);
  array_push($array2_maq, $row->DescMaquina." -- ".$row->CodMaq);
 }
 
 $array3_maq = array_combine($array1_maq, $array2_maq);
 return $array3_maq;
 
}


/*Funcion que se utiliza para obtener los datos de la ot para el archivo pdf teniendo en cuenta los filtros de la plantilla*/
function Buscar_datos_ot_like($NumOT, $CodDescProd, $Lote, $DescSubEtapa, $DescPunzon)
{
 
//Armo el arreglo para recorrer componente por componente
 $array_datos = array('NumOT' => $NumOT, 'CodDescProd' => $CodDescProd, 'Lote' => $Lote, 'DescSubEtapa' => $DescSubEtapa, 'DescPunzon' => $DescPunzon);

/*$this->db->select('IdMaquina, DescMaquina, CodMaq');
$this->db->from('maquina');
$this->db->like('DescMaquina',$DescMaquina);
$this->db->or_like('CodMaq',$DescMaquina);
$query_maq = $this->db->get();*/


$this->db->select('ot.NumOT, productosws.CodProdWS, productosws.DescProdWS, ot.Lote, Fecha_Carga, ot.FechaVto, subetapa.DescSubEtapa, vap.DescVaP, datosot.Cant_Teorica, datosot.Cant_Real, datosot.Cant_Muest_Productiva, datosot.Cant_Muest_No_Productiva, datosot.Tiempo_Preparacion, datosot.Tiempo_Ejecucion, datosot.Observacion_DatosOT, punzon.DescPunzon, datosot.Peso_Promedio, datosot.Muestra_ControlCalidad, datosot.Muestra_ValidacionEstabilidad');

$this->db->from('datosot');

//Se realizo con left outer join porque sino no mostraba todos los resultados
$this->db->join('ot','ot.IdOT = datosot.IdOT', 'left outer');
$this->db->join('productosws','productosws.IdProductoWS = ot.IdProdWS', 'left outer');
$this->db->join('vap','vap.IdVaP = ot.IdVaP', 'left outer');


//$this->db->join('maquina','maquina.IdMaquina = datosot.IdMaquina');


$this->db->join('subetapa','subetapa.IdSubetapa = datosot.IdSubEtapa', 'left outer');
//$this->db->join('planta','planta.IdPlanta = maquina.IdPlanta');
$this->db->join('punzon','punzon.IdPunzon = datosot.IdPunzon', 'left outer');




foreach($array_datos as $clave => $dato) 
 {
        switch($clave) 
        {
         case 'NumOT':
    	    		 if($dato != '')
			   				$this->db->like('ot.NumOT',$NumOT);			
			   		 break; 	  	
	    
		case 'DescSubEtapa':
		    	    	 if($dato != '')
					   			$this->db->like('subetapa.DescSubEtapa',$DescSubEtapa);			
					   	 break;
					  	
		case 'DescPunzon':
	    	    	 if($dato != '')
				   			$this->db->like('punzon.DescPunzon',$DescPunzon);			
				   	 break;
				   	 
		case 'Lote':
	    	    	 if($dato != '')
				   			$this->db->like('ot.Lote',$Lote);			
				   	 break;		   	  
				  				  	
		/*case 'DescMaquina':
		    	    	 if($dato != '')
					   		foreach($query_maq->result() as $row)
 					   		{
					   		 $this->db->where("maquina.DescMaquina",$DescMaquina);
					   		}
					   	  break;*/ 
					   	  
		case 'CodDescProd':
		    	    	 if($dato != '')
					   			{
					   			 $this->db->like("productosws.CodProdWS",$CodDescProd);									 	 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
					   			}
					   	 break;			   	  		  	 	
        }
 }



/*-----------Las variables que se nombran abajo son las que se evaluan-----------*/			 
//$NumOT, $CodDescProdElab, $DescSubEtapa, $DescMaquina, $DescPunzon
//Pregunto al final por $CodDescProdElab porque daba problemas con el or_like
/*
if($NumOT == '')
		 {
		  if($DescSubEtapa == '')
		  			{
				     if($DescMaquina == '')
								{
								 if($DescPunzon == '')
										   {
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										   $this->db->like('punzon.DescPunzon',$DescPunzon);
										   if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}
										  }
								}
						   else {//$DescMaquina != Vacio
								  $this->db->like('DescMaquina',$DescMaquina);
								  if($DescPunzon == '')
										   {
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										    $this->db->like('punzon.DescPunzon',$DescPunzon);
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}
										   }
								 }
				    }	   
			  else	{//$DescSubEtapa != Vacio
			  	     $this->db->like('subetapa.DescSubEtapa',$DescSubEtapa);
			  	     if($DescMaquina == '')
								{
								 if($DescPunzon == '')
										   {
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										   $this->db->like('punzon.DescPunzon',$DescPunzon);
										   if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}
										  }
								}
						   else {//$DescMaquina != Vacio
								  $this->db->like('DescMaquina',$DescMaquina);
								  if($DescPunzon == '')
										   {
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										    $this->db->like('punzon.DescPunzon',$DescPunzon);
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}
										   }
								 }
			  	     
			  	      
			        }
			  }
	     else {//$NumOT != Vacio
	           $this->db->like('ot.NumOT',$NumOT);
	           if($DescSubEtapa == '')
		  			{
				     if($DescMaquina == '')
								{
								 if($DescPunzon == '')
										   {
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										   $this->db->like('punzon.DescPunzon',$DescPunzon);
										   if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}
										  }
								}
						   else {//$DescMaquina != Vacio
								  $this->db->like('DescMaquina',$DescMaquina);
								  if($DescPunzon == '')
										   {
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										    $this->db->like('punzon.DescPunzon',$DescPunzon);
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}
										   }
								 }
				    }	   
			  else	{//$DescSubEtapa != Vacio
			  	     $this->db->like('subetapa.DescSubEtapa',$DescSubEtapa);
			  	     if($DescMaquina == '')
								{
								 if($DescPunzon == '')
										   {
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										   $this->db->like('punzon.DescPunzon',$DescPunzon);
										   if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}
										  }
								}
						   else {//$DescMaquina != Vacio
								  $this->db->like('DescMaquina',$DescMaquina);
								  if($DescPunzon == '')
										   {
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}	
										   }
									 else  {//$DescPunzon != Vacio
										    $this->db->like('punzon.DescPunzon',$DescPunzon);
										    if($CodDescProd != '')
										    			{//$CodDescProd != '', sino no hace nada
														 $this->db->like("productosws.CodProdWS",$CodDescProd);
					  									 $this->db->or_like("productosws.DescProdWS",$CodDescProd);	
														}
										   }
								 }
			  	     
			  	      
			        }
		 	
		      }				

*/
/*-------------------------------------------------------------------------------*/



/*
if($NumOT == '')
		 {
		  if($CodDescProdElab != '')
		  			 {//$CodDescProdElab != Vacio
					  //De esta forma armo para hacer el OR del like de producto
					   $array_prod = array('prodelaboracion.CodProdElab' => $CodDescProdElab, 'prodelaboracion.DescProdElab' => $CodDescProdElab);
					   $this->db->or_like($array_prod);	
					 }
				
		 }			
	else {
		  $this->db->like('ot.NumOT',$NumOT);
		  if($CodDescProdElab != '')
		  			 {//$CodDescProdElab != Vacio
					  //De esta forma armo para hacer el OR del like de producto
					   $this->db->like("prodelaboracion.CodProdElab",$CodDescProdElab);
					   $this->db->or_like("prodelaboracion.DescProdElab",$CodDescProdElab);	
					 }
		 }
*/








//$this->db->like("CodProdElab",$IdProdElab);
//$this->db->or_like("DescProdElab",$IdProdElab);
//$query = $this->db->like("IdProdElab",$IdProdElab);
//$query = $this->db->get("entradas");

//$query = $this->db->like("body","sentencia");
//$query = $this->db->get("entradas");

						 
//$this->db->join('ot','ot.IdOT = datosot.IdOT');
//$this->db->join('prodelaboracion','prodelaboracion.IdProdElab = datosot.IdProdElab');
/*$this->db->join('vap','vap.IdVaP = ot.IdVaP');
$this->db->join('maquina','maquina.IdMaquina = datosot.IdMaquina');
$this->db->join('planta','planta.IdPlanta = maquina.IdPlanta');
$this->db->join('punzon','punzon.IdPunzon = datosot.IdPunzon');*/
 	
					 
$query = $this->db->get();


if($query->num_rows()>0)
                  return $query->result();
             else return FALSE;

}



//Funcion para para hace la consulta de los valores para enviarlos al pdf
function Buscar_datos_pdf($IdOT, $IdProdWS, $Lote, $IdSubEtapa, $Fecha_Desde, $Fecha_Hasta, $IdMaquina, $IdPunzon) 
{
 //Armo el arreglo para recorrer componente por componente
 $array_datos = array('IdOT' => $IdOT, 'IdProdWS' => $IdProdWS, 'Lote' => $Lote, 'IdSubEtapa' => $IdSubEtapa, 'Fecha_Desde' => $Fecha_Desde, 'Fecha_Hasta' => $Fecha_Hasta, 'IdMaquina' => $IdMaquina, 'IdPunzon' => $IdPunzon);
 

$this->db->select('ot.NumOT, productosws.CodProdWS, productosws.DescProdWS, ot.Lote, datosot.Fecha_Carga, ot.FechaVto, subetapa.DescSubEtapa, vap.DescVaP, datosot.Cant_Teorica, datosot.Cant_Real, datosot.Tiempo_Preparacion, datosot.Tiempo_Ejecucion, datosot.Observacion_DatosOT, datosot.IdMaquina, punzon.DescPunzon, planta.DescPlanta, datosot.Cantidad_Rotulos, datosot.Peso_Promedio, datosot.Muestra_ControlCalidad, datosot.Muestra_ValidacionEstabilidad, datosot.AP, datosot.Codigo, datosot.Version');

$this->db->from('datosot');

$this->db->join('ot','ot.IdOT = datosot.IdOT', 'left outer');
$this->db->join('productosws','productosws.IdProductoWS = ot.IdProdWS', 'left outer');
$this->db->join('vap','vap.IdVaP = ot.IdVaP', 'left outer');
$this->db->join('maquina','maquina.IdMaquina = datosot.IdMaquina');
$this->db->join('subetapa','subetapa.IdSubetapa = datosot.IdSubEtapa', 'left outer');
$this->db->join('planta','planta.IdPlanta = maquina.IdPlanta');
$this->db->join('punzon','punzon.IdPunzon = datosot.IdPunzon', 'left outer');


foreach($array_datos as $clave => $dato) 
 {
        switch($clave) 
        {
         case 'IdOT':
    	    		 if($dato != '')
			   				$this->db->where('ot.IdOT',$IdOT);			
			   		 break; 
			  	
	    case 'IdProdWS':
		    	    	 if($dato != '')
					   			$this->db->where("productosws.IdProductoWS",$IdProdWS);			
					   	 break;
					   	 
		case 'Lote':
		    	    	 if($dato != '')
					   			$this->db->where("ot.Lote",$Lote);			
					   	 break;
					   	  
		
		case 'IdSubEtapa':
		    	    	 if($dato != '')
					   			$this->db->where('subetapa.IdSubEtapa',$IdSubEtapa);			
					   	 break;
					  	
	    case 'Fecha_Desde':
		    	    	 if($dato != '')
		    	    	 		{
					   			 //De esta forma obtengo los valores separados de la fecha
					   			 $dia = substr($Fecha_Desde, -10, 2);
					   			 $mes = substr($Fecha_Desde, -7, 2);
								 $anio = substr($Fecha_Desde, -4);
							   	 $Fecha_Desde_conv = $anio .'-'. $mes .'-'. $dia.' 00:00:00';
					   			 
					   			 $this->db->where('datosot.Fecha_Carga >=',$Fecha_Desde_conv);
					   			 //$this->db->where('datosot.Fecha_Carga >=',$Fecha_Desde);			
					   			} 
					   	 break;
					
		case 'Fecha_Hasta':
		    	    	 if($dato != '')
					   			{					   		  	 
								 //De esta forma obtengo los valores separados de la fecha
					   			 $dia = substr($Fecha_Hasta, -10, 2);
					   			 $mes = substr($Fecha_Hasta, -7, 2);
								 $anio = substr($Fecha_Hasta, -4);
							   	 $Fecha_Hasta_conv = $anio .'-'. $mes .'-'. $dia.' 23:59:59';
					   			 
					   			 $this->db->where('datosot.Fecha_Carga <=',$Fecha_Hasta_conv);			
					   			}
					   	 break;
					  	
		case 'IdPunzon':
	    	    	 if($dato != '')
				   			$this->db->where('punzon.IdPunzon',$IdPunzon);			
				   	 break; 
				  				  	
		case 'IdMaquina':
		    	    	 if($dato != '')
					   		{$this->db->like("datosot.IdMaquina",",".$IdMaquina);
					 		 $this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
					 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
					 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
					   		}
					   	  break; 		  	 	
        }
 }

$query = $this->db->get();

/*
while (current($array_datos)) {
    
    switch (key($array_datos)) 
    {
    case 'IdOT':{
    	    	 if($array_datos != '')
			   			$this->db->where('ot.IdOT',$IdOT);			
			   	 break; 
			  	}
    case 'IdProdElab':{
	    	    	 if($array_datos != '')
				   			$this->db->where("prodelaboracion.IdProdElab",$IdProdElab);			
				   	 break; 
				  	}
    case 'IdSubEtapa':{
	    	    	 if($array_datos != '')
				   			$this->db->where('subetapa.IdSubEtapa',$IdSubEtapa);			
				   	 break; 
				  	}
	case 'IdPunzon':{
    	    	 if($array_datos != '')
			   			$this->db->where('punzon.IdPunzon',$IdPunzon);			
			   	 break; 
			  	}			  	
	case 'IdMaquina':{
	    	    	 if($array_datos != '')
				   		{$this->db->like("datosot.IdMaquina",",".$IdMaquina);
				 		 $this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
				 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
				 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
				   		}
				   	  break; 
				  	 }
	
		}//Fin del switch
    
    next($array_datos);
}*/

/*
for($i=0;$i<count($array_datos);$i++)
 {
  	
  switch (key($array_datos[$i])) {
    case 'IdOT':{
    	    	 if($array_datos[$i] != '')
			   			$this->db->where('ot.IdOT',$IdOT);			
			   	 break; 
			  	}
   case 'IdProdElab':{
	    	    	 if($array_datos[$i] != '')
				   			$this->db->where("prodelaboracion.IdProdElab",$IdProdElab);			
				   	 break; 
				  	}
    case 'IdSubEtapa':{
	    	    	 if($array_datos[$i] != '')
				   			$this->db->where('subetapa.IdSubEtapa',$IdSubEtapa);			
				   	 break; 
				  	}
	case 'IdPunzon':{
    	    	 if($array_datos[$i] != '')
			   			$this->db->where('punzon.IdPunzon',$IdPunzon);			
			   	 break; 
			  	}			  	
	case 'IdMaquina':{
	    	    	 if($array_datos[$i] != '')
				   		{$this->db->or_like("datosot.IdMaquina",",".$IdMaquina);
				 		 $this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
				 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
				 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
				   		}
				   	  break; 
				  	 }
	
		}//Fin del switch	
		
 }//Fin del for*/

/* 
 
 $consulta = "SELECT ot.NumOT, prodelaboracion.CodProdElab, prodelaboracion.DescProdElab, ot.Lote, Fecha_Carga, ot.FechaVto, subetapa.DescSubEtapa, vap.DescVaP, datosot.Cant_Teorica, datosot.Cant_Real, datosot.Cant_Muest_Productiva, datosot.Cant_Muest_No_Productiva, datosot.Tiempo_Preparacion, datosot.Tiempo_Ejecucion, datosot.Observacion_DatosOT, datosot.IdMaquina, punzon.DescPunzon, datosot.Peso_Promedio, datosot.Muestra_ControlCalidad, datosot.Muestra_ValidacionEstabilidad ";
 
 $consulta .= "FROM datosot ";
 
 $consulta .= "INNER JOIN ot ON ot.IdOT = datosot.IdOT ";
 $consulta .= "INNER JOIN prodelaboracion ON prodelaboracion.IdProdElab = datosot.IdProdElab ";
 $consulta .= "INNER JOIN vap ON vap.IdVaP = ot.IdVaP ";
 $consulta .= "INNER JOIN subetapa ON subetapa.IdSubetapa = datosot.IdSubEtapa ";
 $consulta .= "INNER JOIN punzon ON punzon.IdPunzon = datosot.IdPunzon ";
 
 for($i=0;$i<count($array_datos);$i++)
 {
  	
  switch (key($array_datos[$i])) {
    case 'IdOT':{
    	    	 if($array_datos[$i] != '')
			   			$consulta .= " WHERE ot.IdOT = ".$array_datos[$i];			
			   	 break; 
			  	}
   case 'IdProdElab':{
	    	    	 if($array_datos[$i] != '')
				   			$consulta .= " WHERE prodelaboracion.IdProdElab = ".$array_datos[$i];			
				   	 break; 
				  	}
    case 'IdSubEtapa':{
	    	    	 if($array_datos[$i] != '')
				   			$consulta .= " WHERE subetapa.IdSubEtapa = ".$array_datos[$i];			
				   	 break; 
				  	}
	case 'IdPunzon':{
    	    	 if($array_datos[$i] != '')
			   			$consulta .= " WHERE punzon.IdPunzon = ".$array_datos[$i];			
			   	 break; 
			  	}			  	
	case 'IdMaquina':{
	    	    	 if($array_datos[$i] != '')
				   		{$consulta .= " WHERE datosot.IdMaquina LIKE %,".$array_datos[$i];
				   		 $consulta .= " OR datosot.IdMaquina LIKE %,".$array_datos[$i].",%";
				   		 $consulta .= " OR datosot.IdMaquina LIKE ".$array_datos[$i].",%";
				   		 $consulta .= " OR datosot.IdMaquina LIKE ".$array_datos[$i];
				   		}
				   	  break; 
				  	 }
	
		}//Fin del switch	
		
 }//Fin del for
 
 $query = mysql_query ($consulta);*/
 
 
 
 /*
 $this->db->select('ot.NumOT, prodelaboracion.CodProdElab, prodelaboracion.DescProdElab, ot.Lote, Fecha_Carga, ot.FechaVto, subetapa.DescSubEtapa, vap.DescVaP, datosot.Cant_Teorica, datosot.Cant_Real, datosot.Cant_Muest_Productiva, datosot.Cant_Muest_No_Productiva, datosot.Tiempo_Preparacion, datosot.Tiempo_Ejecucion, datosot.Observacion_DatosOT, datosot.IdMaquina, punzon.DescPunzon, datosot.Peso_Promedio, datosot.Muestra_ControlCalidad, datosot.Muestra_ValidacionEstabilidad');

$this->db->from('datosot');

$this->db->join('ot','ot.IdOT = datosot.IdOT');
$this->db->join('prodelaboracion','prodelaboracion.IdProdElab = datosot.IdProdElab');
$this->db->join('vap','vap.IdVaP = ot.IdVaP');
//$this->db->join('maquina','maquina.IdMaquina = datosot.IdMaquina');
$this->db->join('subetapa','subetapa.IdSubetapa = datosot.IdSubEtapa');
//$this->db->join('planta','planta.IdPlanta = maquina.IdPlanta');
$this->db->join('punzon','punzon.IdPunzon = datosot.IdPunzon');



/*-----------Las variables que se nombran abajo son las que se evaluan-----------*/			 
//$NumOT, $CodDescProdElab, $DescSubEtapa, $DescPunzon
/*
if($IdOT == '')
		 {
		  if($IdSubEtapa == '')
		  			{
					 if($IdPunzon == '')
							   {
							    if($IdProdElab == '')
							    			{
											 if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}
											}
							    			
									else    {//$IdProdElab != ''
										    $this->db->where("prodelaboracion.IdProdElab",$IdProdElab);
										    if($IdMaquina != '')
											 		{
											 		 $this->db->or_like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}	
									 		}
											
							   }
						 else  {//$IdPunzon != Vacio
							   $this->db->where('punzon.IdPunzon',$IdPunzon);
							    if($IdProdElab == '')
							    			{
											 if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}
											}
							    			
									else    {//$IdProdElab != ''
										    $this->db->where("prodelaboracion.IdProdElab",$IdProdElab);
										    if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		// $this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}	
									 		}
									
							   }
					 }
				else {//$IdSubEtapa != Vacio
					  $this->db->where('subetapa.IdSubEtapa',$IdSubEtapa);
					  if($IdPunzon == '')
							   {
							    if($IdProdElab == '')
							    			{
											 if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}
											}
							    			
									else    {//$IdProdElab != ''
										    $this->db->where("prodelaboracion.IdProdElab",$IdProdElab);
										    if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}	
									 		}
											
							   }
						 else  {//$IdPunzon != Vacio
							   $this->db->where('punzon.IdPunzon',$IdPunzon);
							    if($IdProdElab == '')
							    			{
											 if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}
											}
							    			
									else    {//$IdProdElab != ''
										    $this->db->where("prodelaboracion.IdProdElab",$IdProdElab);
										    if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}	
									 		}
									
							   }
			 	  	 }		 						   
			}			   
	else {//$IdOT != Vacio
	      $this->db->where('ot.IdOT',$IdOT);
		  if($IdSubEtapa == '')
		  			{
					 if($IdPunzon == '')
							   {
							    if($IdProdElab == '')
							    			{
											 if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}
											}
							    			
									else    {//$IdProdElab != ''
										    $this->db->where("prodelaboracion.IdProdElab",$IdProdElab);
										    if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		// $this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}	
									 		}
											
							   }
						 else  {//$IdPunzon != Vacio
							   $this->db->where('punzon.IdPunzon',$IdPunzon);
							    if($IdProdElab == '')
							    			{
											 if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		// $this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		// $this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}
											}
							    			
									else    {//$IdProdElab != ''
										    $this->db->where("prodelaboracion.IdProdElab",$IdProdElab);
										    if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}	
									 		}
									
							   }
					 }
				else {//$IdSubEtapa != Vacio
					  $this->db->where('subetapa.IdSubEtapa',$IdSubEtapa);
					  if($IdPunzon == '')
							   {
							    if($IdProdElab == '')
							    			{
											 if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		// $this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}
											}
							    			
									else    {//$IdProdElab != ''
										    $this->db->where("prodelaboracion.IdProdElab",$IdProdElab);
										    if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}	
									 		}
											
							   }
						 else  {//$IdPunzon != Vacio
							   $this->db->where('punzon.IdPunzon',$IdPunzon);
							    if($IdProdElab == '')
							    			{
											 if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		 //$this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}
											}
							    			
									else    {//$IdProdElab != ''
										    $this->db->where("prodelaboracion.IdProdElab",$IdProdElab);
										    if($IdMaquina != '')
											 		{
											 		 $this->db->like("datosot.IdMaquina",",".$IdMaquina);
											 		 //$this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");
											 		// $this->db->or_like("datosot.IdMaquina",$IdMaquina.",");
											 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);
											 		}	
									 		}
									
							   }
			 	  	 }
		
		 }					   
						   			

/*-------------------------------------------------------------------------------*/
/*
$query = $this->db->get();
*/


/*if($query->num_rows()>0)
			 return $query->result();
		else return FALSE;	*/

/*if($query->num_rows()>0)
		{
		 while($row=mysql_fetch_array($query))
			 {
			 	
			 $val_id_maq = $row['IdMaquina'];
			 
			 $this->db->select('DescMaquina');
			 $this->db->from('maquina');
			 $this->db->where('maquina.IdMaquina',$val_id_maq);
			 $query_maq = $this->db->get();
			 
			 $query->result()->DescMaquina = $query_maq->result()->DescMaquina;
			 }
		 return $query->result();
		 	
		}
	else return FALSE;*/

/*
if($query->num_rows()>0)
		 {
		 foreach ($query->result() as $row)
			{
			 $val_id_maq = $row->IdMaquina;
			     $array_maq = array();
				 $cadena = '';
				 $i = 0;
				 $j = 0;
				 while($i<strlen($val_id_maq))
					 {
					  if($val_id_maq[$i] != ',')
						  		{
						  		 $cadena .= $val_id_maq[$i]; 	
						  		}
					  	   else {
						   	     $array_maq[$j] = $cadena; 
						   	     $j++;
						   	     $cadena = '';
						        }
					  
					  $i++;
					  //$array_maq[$j] = $cadena; 		
					 }//fin del while
				 if($i>=strlen($val_id_maq)) 
					 	$array_maq[$j] = $cadena;	
			     
			 $cadena_maq = '';
			 $k=0;
			 while(($k<count($array_maq))&&($array_maq[$k] != $IdMaquina))
		 	  {
			   $k++;		 	  
			  }
			  if($k>=count($array_maq))
			  		$cadena = 'VACIA';
			}
		 }
	else return FALSE;	*/


if($query->num_rows()>0)
		{ 	
		foreach ($query->result() as $row)
			{
			     //$row->IdMaquina = 'Prueba';
			     //$val_id_maq = $row->IdMaquina;	
			     //$variable = array('valor1', 'valor2', 'valor3');	     
			     //$row->IdMaquina = $val_id_maq[0];
			     //$row->IdMaquina = count($variable);
			     
			     $val_id_maq = $row->IdMaquina;
			     $array_maq = array();
				 $cadena = '';
				 $i = 0;
				 $j = 0;
				 while($i<strlen($val_id_maq))
					 {
					  if($val_id_maq[$i] != ',')
						  		{
						  		 $cadena .= $val_id_maq[$i]; 	
						  		}
					  	   else {
						   	     $array_maq[$j] = $cadena; 
						   	     $j++;
						   	     $cadena = '';
						        }
					  
					  $i++;
					  //$array_maq[$j] = $cadena; 		
					 }//fin del while
				 if($i>=strlen($val_id_maq)) 
					 	$array_maq[$j] = $cadena;
					 	
				$cadena_maq = '';
			    for($k=0;$k<count($array_maq);$k++)
					  {
					   //$cadena_maq .= $array_maq[$k].", ";   				   
					   $this->db->select('DescMaquina');
					   $this->db->from('maquina');
					   $this->db->where('IdMaquina',$array_maq[$k]);
					   $query_maq = $this->db->get();
					   
					   $cadena_maq .= $query_maq->row()->DescMaquina.", "; 
					  	 
					  }//Fin del for
				  		 
			 	//Asigno las maquinas concatenadas al campo de la consulta
			 	$row->IdMaquina = $cadena_maq;
	   		}
   		return $query->result();
   		}
   	else return FALSE;
   	
     		
  
/*-------------------------------------------*/			
	

}






function obtiene_valores1()
{
 //$IdProd = $post_array['IdProdElab'];
 $this->db->select('CodProdElab');
 $this->db->from('prodelaboracion');
 $this->db->order_by("CodProdElab","asc");
 //$this->db->where('IdProdElab', $IdProd);
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 //$query = $this->db->get();
 $array = array();
 $i=0;
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->CodProdElab);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 //var_dump($array);
 
 /*var_dump($query->result());
 echo "<br>";
 echo "<br>";
 var_dump($query->row_array());
 echo "<br>";
 echo "<br>";
 var_dump($query->result_array());*/
// die;
return $array;
 /*if($query->num_rows()>0)
                  //return json_encode($query->result());
                  //return $query->result_array();
                  
                  return $query->row_array();
             else return FALSE;		*/
}



function obtiene_valores2()
{
 //$IdProd = $post_array['IdProdElab'];
 $this->db->select('CodProdElab, DescProdElab');
 $this->db->from('prodelaboracion');
 $this->db->order_by("CodProdElab","asc");
 //$this->db->where('IdProdElab', $IdProd);
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 $array = array();
 $i=0;
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->CodProdElab." - ".$row->DescProdElab);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 return $array;
 
 /*if($query->num_rows()>0)
                  return $query->result_array();
             else return FALSE;	*/	
}


function obt_val1_maquina()
{
 //$IdProd = $post_array['IdProdElab'];
 $this->db->select('IdMaquina');
 $this->db->from('maquina');
 $this->db->order_by("IdMaquina","asc");
 //$this->db->where('IdProdElab', $IdProd);
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 //$query = $this->db->get();
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->IdMaquina);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 //var_dump($array);
 
 /*var_dump($query->result());
 echo "<br>";
 echo "<br>";
 var_dump($query->row_array());
 echo "<br>";
 echo "<br>";
 var_dump($query->result_array());*/
// die;
return $array;
 /*if($query->num_rows()>0)
                  //return json_encode($query->result());
                  //return $query->result_array();
                  
                  return $query->row_array();
             else return FALSE;		*/
}



function obt_val2_maquina()
{
 //$IdProd = $post_array['IdProdElab'];
 $this->db->select('IdMaquina, DescMaquina');
 $this->db->from('maquina');
 $this->db->order_by("IdMaquina","asc");
 //$this->db->where('IdProdElab', $IdProd);
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->DescMaquina);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 return $array;
 
 /*if($query->num_rows()>0)
                  return $query->result_array();
             else return FALSE;	*/	
}



/*function obtiene_datos()
{
 
 $this->db->select('IdProdElab, CodProdElab, DescProdElab, DescMaquina, EstadoProdElab');
 $this->db->from('prodelaboracion');
 $this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->result();
             else return FALSE;		
}*/


//Funcion para obtener el código del producto
/*function obtiene_idprod($post_array, $primary_key)
{
 $IdProd = $post_array['IdProdElab'];
 $this->db->select('IdProdElab');
 $this->db->where('IdProdElab',$IdProd);
 //$this->db->from('prodelaboracion');
 $query = $this->db->get('prodelaboracion');
 $dato = $query->row()->IdProdElab; 
 
 if($query->num_rows()>0)
                  return FALSE;
             else return $dato;
 
}*/

/*function obtiene_idprod($post_array, $primary_key)
{
 $IdProd = $post_array['IdProdElab'];
 //$where = '(IdProdElab=$IdProd or EstadoProdElab = "AC")';
 
 $this->db->select('CodProdElab');
 $this->db->where('IdProdElab',$IdProd);
 $this->db->or_where('EstadoProdElab','AC');
 //$this->db->where($where);
 $query = $this->db->get('prodelaboracion');
 
	$records = array();
	foreach ($query->result() as $row)
		{
        $records[] = $row;
		}
	/*while ($row = mysql_fetch_assoc($query))
	{
	  $records[] = $row['IdProdElab'];
	}*/
 
 /*return $records;
 
}*/

function obtiene_idprod($post_array, $primary_key)
{
 $IdProd = $post_array['IdProdElab'];
 $IdProd = (string) $IdProd;
 return $IdProd;
 
}

//Obtiene los datos del productows
function Buscar_datos_producto($IdOT)
{
 $this->db->select('productosws.CodProdWS, productosws.DescProdWS');
 $this->db->from('ot');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 $this->db->where('ot.IdOT',$IdOT);
 //$this->db->from('prodelaboracion');
 $query = $this->db->get();
  
 return $query->result();
 
}

//Obtiene el lote de una ot
function Buscar_lote($IdOT)
{
 $this->db->select('Lote');
 $this->db->from('ot');
 $this->db->join('datosot','ot.IdOT = datosot.IdOT');
 $this->db->where('ot.IdOT',$IdOT);
 //$this->db->from('prodelaboracion');
 $query = $this->db->get();
  
 return $query->result();
 
}



/*-----------------------Fin de las funciones del modelo----------------------------------*/


}
<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_usuario extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion para obtener el password de un usuario 
function obtiene_password($Id_Usuario)
	{
	    	
	  $this->db->select('Password');	  
	  $this->db->from('usuario');
	  $this->db->where('IdUsuario',$Id_Usuario);
	  $query = $this->db->get();
	  
	  if($query->num_rows()>0)
                  return FALSE;
             else return $query->row()->Password;        
	

	}



//Funcion para hacer el update del password
function update_password($pass_nuevo)
{
$primary_key = 2; //Esto simula el id del usuario que lo obtengo de la session
  
$data = array(
       'Password' => $pass_nuevo
    );
 
$this->db->where('IdUsuario', $primary_key);
$this->db->update('usuario', $data);
	  
return TRUE;        

}


//Funcion para obtener los datos del usuario, si este esta registrado en la base
function obtener_login($usuario)
{
	$this->db->where('Usuario', $usuario);
	$query = $this->db->get('usuario');
	//$str = $this->db->last_query();
	//var_dump($query->row());die;
	//echo $str;
	if($query->num_rows()>0)
		{
			return $query->row();//devuelvo la fila
		}
		else return FALSE;
	
	/*if($this->db->_error_message()!=''){
		die($this->db->_error_message());//mata proceso y muestra mensaje de error
	}
	else{
		if($query->num_rows()!=0)
		{
			return $query->row();//devuelvo una fila
		}
		else return "error";
	}*/
}



//Funcion para obtener la descripcion del sector al que pertenece el usuario
function obtener_desc_sector($IdSector)
{
	$this->db->select('DescSector');
	$this->db->where('IdSectorUsuario', $IdSector);
	$this->db->from('sectorusuario');
	$query = $this->db->get();
	//$str = $this->db->last_query();
	//var_dump($query->row());die;
	//echo $str;
	if($query->num_rows()>0)
		{
			return $query->row()->DescSector;//devuelvo la descripcion del sector
		}
		else return FALSE;
}




/*Funcion que almacena el log de usaurio cuando inserto uno nuevo*/
function graba_log_usuario_add($post_array, $primary_key)
{	
	/*Obtiene los valores para cargar el log*/
	$this->db->select('DescSector');
	$this->db->from('usuario');
	$this->db->join('sectorusuario','usuario.IdSector = sectorusuario.IdSectorUsuario');
	$this->db->where('IdUsuario',$primary_key);
	$query1 = $this->db->get();
	$dato_DescSector = $query1->row()->DescSector;	
			
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: usuario",
   	"Valores" => "Id.Usuario: ".$primary_key.", Usuario: ".$post_array['Usuario'].", Nivel: ".$post_array['Nivel'].", Sector: ".$dato_DescSector.", Estado: Habilitado"
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



/*Funcion que almacena el log de usaurio cuando edito un usuario*/
function graba_log_usuario_edit($post_array, $primary_key)
{	
	/*Obtiene los valores para cargar el log*/
	$this->db->select('DescSector');
	$this->db->from('usuario');
	$this->db->join('sectorusuario','usuario.IdSector = sectorusuario.IdSectorUsuario');
	$this->db->where('IdUsuario',$primary_key);
	$query1 = $this->db->get();
	$dato_DescSector = $query1->row()->DescSector;		
			
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Edito un registo",
   	"Tabla" => "Tabla: usuario",
   	"Valores" => "Id.Usuario: ".$primary_key.", Usuario: ".$post_array['Usuario'].", Nivel: ".$post_array['Nivel'].", Sector: ".$dato_DescSector.", Estado: Habilitado"
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



/*Funcion que obtiene el Estado de un Usuario*/
function obtiene_estado_usuario($primary_key)
{
 $this->db->select('EstadoUsuario');
 $this->db->from('usuario');
 $this->db->where('IdUsuario',$primary_key);
 $query = $this->db->get();
 $dato_estado = $query->row()->EstadoUsuario;
 return $dato_estado;	
}


/*Funcion que elimina el usuario, cambia el estado del usuario a Deshabilitado*/
//Además almacena el log de cambio de estado (Deshabilitado) al usuario 
function cambia_est_usuario_deshab($primary_key)
{
 $this->db->where('IdUsuario', $primary_key);
 $this->db->update('usuario',array('EstadoUsuario' => 'Deshabilitado'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('Usuario, Nivel, DescSector, EstadoUsuario');
 $this->db->from('usuario');
 $this->db->join('sectorusuario','usuario.IdSector = sectorusuario.IdSectorUsuario');
 $this->db->where('IdUsuario',$primary_key);
 $query = $this->db->get();
 $dato_Usuario = $query->row()->Usuario;

 $dato_Nivel = $query->row()->Nivel;
 $dato_DescSector = $query->row()->DescSector;
 $dato_EstadoUsuario = $query->row()->EstadoUsuario;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Deshabilito un usuario",
			   	"Tabla" => "Tabla: usuario",
			   	"Valores" => "Id.Usuario: ".$primary_key.",  Usuario: ".$dato_Usuario.",  Desc. Sector: ".$dato_DescSector.", Estado: ".$dato_EstadoUsuario
			 
				);
 
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}


/*Funcion que elimina el usuario, cambia el estado del usuario a Habilitado*/
//Además almacena el log de cambio de estado (Habilitado) al usuario 
function cambia_est_usuario_hab($primary_key)
{
 $this->db->where('IdUsuario', $primary_key);
 $this->db->update('usuario',array('EstadoUsuario' => 'Habilitado'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('Usuario, Nivel, DescSector, EstadoUsuario');
 $this->db->from('usuario');
 $this->db->join('sectorusuario','usuario.IdSector = sectorusuario.IdSectorUsuario');
 $this->db->where('IdUsuario',$primary_key);
 $query = $this->db->get();
 $dato_Usuario = $query->row()->Usuario;

 $dato_Nivel = $query->row()->Nivel;
 $dato_DescSector = $query->row()->DescSector;
 $dato_EstadoUsuario = $query->row()->EstadoUsuario;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Habilito un usuario",
			   	"Tabla" => "Tabla: usuario",
			   	"Valores" => "Id.Usuario: ".$primary_key.",  Usuario: ".$dato_Usuario.",  Desc. Sector: ".$dato_DescSector.", Estado: ".$dato_EstadoUsuario
			 
				);
 
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}

	
	
/*--------------------------FIN DE LAS FUNCIONES-------------------------------------------*/	

}
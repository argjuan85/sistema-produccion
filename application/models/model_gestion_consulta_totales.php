<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_consulta_totales extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion que obtiene los valores para armar el pdf 
function trae_info_pdf($IdProd, $IdMaquina)
{
 if($IdProd != '')
		 { 
		 //Obtengo el producto
		 $this->db->select('CodProdWS, DescProdWS');
		 $this->db->from('productosws');
		 $this->db->where('IdProductoWS',$IdProd);
		 $query = $this->db->get();
		 $dato_IdProd = $query->row()->CodProdWS." - ".$query->row()->DescProdWS;
		 }
	else $dato_IdProd = '';	 
 
 if($IdMaquina != '')
 		{
		 //Obtengo la maquina
		 $this->db->select('DescMaquina');
		 $this->db->from('maquina');
		 $this->db->where('IdMaquina',$IdMaquina);
		 $query1 = $this->db->get();
		 $dato_DescMaquina = $query1->row()->DescMaquina;
		}
	else $dato_DescMaquina = ''; 
 
 $valores =  array(
		'DescProd' =>$dato_IdProd,		
		'DescMaquina' => $dato_DescMaquina
		
		);
  
 return $valores;	
 	
}




//Funcion para para hace la consulta de los valores para enviarlos al pdf
function Buscar_totales_pdf($IdProd, $Fecha_Desde, $Fecha_Hasta, $Lote, $IdMaquina) 
{
 //Armo el arreglo para recorrer componente por componente
 $array_datos = array('IdProd' => $IdProd, 'Fecha_Desde' => $Fecha_Desde, 'Fecha_Hasta' => $Fecha_Hasta, 'Lote' => $Lote, 'IdMaquina' => $IdMaquina);
 
$this->db->select('productosws.IdProductoWS , productosws.CodProdWS, productosws.DescProdWS, ot.Lote, datosot.Fecha_Carga, datosot.IdSubetapa, datosot.Cant_Teorica, datosot.Cant_Real, datosot.IdMaquina');

$this->db->from('datosot');

$this->db->join('ot','ot.IdOT = datosot.IdOT');
$this->db->join('productosws','productosws.IdProductoWS  = ot.IdProdWS');
//$this->db->join('maquina','maquina.IdMaquina = datosot.IdMaquina');


foreach($array_datos as $clave => $dato) 
 {
        switch($clave) 
        {
	    case 'IdProd':
		    	    	 if($dato != '')
					   			$this->db->where("productosws.IdProductoWS",$IdProd);			
					   	 break; 
		
		case 'Fecha_Desde':
		    	    	 if($dato != '')
		    	    	 		{
					   			 //De esta forma obtengo los valores separados de la fecha
					   			 $dia = substr($Fecha_Desde, -10, 2);
					   			 $mes = substr($Fecha_Desde, -7, 2);
								 $anio = substr($Fecha_Desde, -4);
							   	 $Fecha_Desde_conv = $anio .'-'. $mes .'-'. $dia.' 00:00:00';
					   			 
					   			 $this->db->where('datosot.Fecha_Carga >=',$Fecha_Desde_conv);
					   			 //$this->db->where('datosot.Fecha_Carga >=',$Fecha_Desde);			
					   			} 
					   	 break;
					
		case 'Fecha_Hasta':
		    	    	 if($dato != '')
					   			{					   		  	 
								 //De esta forma obtengo los valores separados de la fecha
					   			 $dia = substr($Fecha_Hasta, -10, 2);
					   			 $mes = substr($Fecha_Hasta, -7, 2);
								 $anio = substr($Fecha_Hasta, -4);
							   	 $Fecha_Hasta_conv = $anio .'-'. $mes .'-'. $dia.' 23:59:59';
					   			 
					   			 $this->db->where('datosot.Fecha_Carga <=',$Fecha_Hasta_conv);			
					   			}
					   	 break;
		
		case 'Lote':
		    	    	 if($dato != '')
					   			$this->db->where('ot.Lote',$Lote);			
					   	 break;
				  				  	
		case 'IdMaquina':
		    	    	 if($dato != '')
					   		{$this->db->like("datosot.IdMaquina",",".$IdMaquina);			//Ej: ,9
					 		 $this->db->or_like("datosot.IdMaquina",",".$IdMaquina.",");	//Ej: ,9,
					 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina.",");		//Ej: 9,
					 		 $this->db->or_like("datosot.IdMaquina",$IdMaquina);			//Ej: 9
					   		}
					   	  break; 		  	 	
        }
 }

$query = $this->db->get();

	
//Obtenermos los nombre de las maquinas
if($query->num_rows()>0)
		{ 	
		foreach ($query->result() as $row)
			{		     
			     $val_id_maq = $row->IdMaquina;
			     $array_maq = array();
				 $cadena = '';
				 $i = 0;
				 $j = 0;
				 while($i<strlen($val_id_maq))
					 {
					  if($val_id_maq[$i] != ',')
						  		{
						  		 $cadena .= $val_id_maq[$i]; 	
						  		}
					  	   else {
						   	     $array_maq[$j] = $cadena; 
						   	     $j++;
						   	     $cadena = '';
						        }
					  
					  $i++;
					  
					 }//fin del while
				 if($i>=strlen($val_id_maq)) 
					 	$array_maq[$j] = $cadena;
					 	
				$cadena_maq = '';
			    for($k=0;$k<count($array_maq);$k++)
					  {
					   
					   $this->db->select('DescMaquina');
					   $this->db->from('maquina');
					   $this->db->where('IdMaquina',$array_maq[$k]);
					   $query_maq = $this->db->get();
					   
					   $cadena_maq .= $query_maq->row()->DescMaquina.", "; 
					  	 
					  }//Fin del for
				  		 
			 	//Asigno las maquinas concatenadas al campo de la consulta
			 	$row->IdMaquina = $cadena_maq;
	   		}
   		return $query->result();
   		}
   	else return FALSE;
 }
  
 
 
/*-----------fUNCIONES PARA ARMAR EL SELECT DE PRODUCTOS---------*/
//Obtengo el id del producto para armar el select
function obtiene_IdProd()	
{
 //Funcion que obtiene los id de los productos que estan en estado AC junto con el id del producto que estoy editando, sea cual sea su estado  
 $this->db->select('IdProductoWS');
 $this->db->from('productosws');
 //$this->db->where('IdProductoWS', $idprodws);
 //$this->db->or_where('EstadoProdWS', 'AC');
 //$this->db->or_where('EstadoProdWS', 'ac');
 $this->db->order_by("IdProductoWS","asc");
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->IdProductoWS);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }

return $array;
 
}

//Obtengo los datos de los productos para armar el select
function obtiene_datos_prod()
{
 //Funcion que obtiene los Codigos y la Descripcion de los productos que estan en estado AC junto con el Código y la descripcion del producto que estoy editando, sea cual sea su estado
 $this->db->select('IdProductoWS, CodProdWS, DescProdWS');
 $this->db->from('productosws');
 
 $this->db->order_by("IdProductoWS","asc");
 //$this->db->where('IdProdElab', $IdProd);
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->CodProdWS." - ".$row->DescProdWS);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 return $array;
 
 /*if($query->num_rows()>0)
                  return $query->result_array();
             else return FALSE;	*/	
}

//---------------------------------------------------------------------------------//
 
 
 
 
 
 
  
/*-----------------------FIN DE LAS FUNCIONES--------------------*/		






}
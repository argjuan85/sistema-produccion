<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_punzon_archivo extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion para buscar la descripcion de un punzon para armar el campo
function obtiene_descpun($IdPun)
{
 $this->db->select('DescPunzon');
 $this->db->where('IdPunzon',$IdPun);
 $query = $this->db->get('punzon');
 $DescPunzon = $query->row()->DescPunzon;
 return $DescPunzon;
 	
}


//Funcion que consulta si ese archivo ya se encuentra cargado para ese punzon
function consulta_archivo_add($IdPun, $cadena)
	{
	  $this->db->select('IdPun, Archivo');	  
	  $this->db->from('punzon_archivo');
	  	   
	  $this->db->like('punzon_archivo.Archivo',$cadena);
	  $this->db->where('IdPun',$IdPun);

	  $query = $this->db->get();
	  
	  
	  /*Si la consulta devuelve valores, es decir que ya estan cargados y retorna TRUE, sino retorna FALSE*/
       if($query->num_rows()>0)
                  return TRUE;
             else return FALSE;         
	}



//Funcion que consulta si ese registro que estamosd editando ya tiene ese archivo cargado para ese punzón
function consulta_archivo_edit($primary_key, $IdPun, $cadena)
	{
	  $this->db->select('IdPun_Arch, IdPun, Archivo');	  
	  $this->db->from('punzon_archivo');
	  	   
	  $this->db->like('punzon_archivo.Archivo',$cadena);
	  $this->db->where('IdPun',$IdPun);
	  $this->db->where('IdPun_Arch !=',$primary_key);
	  
	  $query = $this->db->get();
	  
	  /*Si la consulta devuelve valores, es decir que ya estan cargados y retorna TRUE, sino retorna FALSE*/
       if($query->num_rows()>0)
                  return TRUE;
             else return FALSE;         
	}






//Funcion para consultar si una Descripción de un punzon ya estan cargada
function consulta_campos_punzon_add($post_array)
	{
	  	  
	  $DescripcionPunzon = trim($post_array['DescPunzon']);
	  $this->db->where('DescPunzon',$DescripcionPunzon);
	  $this->db->from('punzon');
	  $query = $this->db->get();
	  
	  /*Si la descripcion del punzon se encuentran cargado retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}


//Funcion para consultar si una Descripción de un punzon ya estan cargada
function consulta_campos_punzon_edit($post_array)
	{
	  	  
	  $IdPunzon = $post_array['IdPunzon'];
	  $DescripcionPunzon = trim($post_array['DescPunzon']);
	  $this->db->where('IdPunzon !=',$IdPunzon);
	  $this->db->where('DescPunzon',$DescripcionPunzon);
	  $this->db->from('punzon');
	  $query = $this->db->get();
	  
	  /*Si la descripcion del punzon se encuentran cargado retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}


//Funcion para obtener la Descripcion del Archivo por si lo elimino
function busca_desc_arch($primary_key)
{
 $this->db->select('Archivo');
 $this->db->from('punzon_archivo');
 $this->db->where('IdPun_Arch',$primary_key);
 $query = $this->db->get();
 
 if($query->num_rows()>0)
          return $query->row()->Archivo;
     else return FALSE;	
	
}


//Funcion para almacenar el log al eliminar un archivo dentro del editar
function log_elim_arch($IdArchPun, $Archivo)
{
 $logs_elim_arch = array(
	   	"UsuarioSO" => $this->session->userdata('Usuario'),
	   	"UsuarioSistema" => $this->session->userdata('Usuario'),
	   	"PC" => $this->session->userdata('ip_pc'), 	
	   	"Nivel" => $this->session->userdata('Nivel'),
	   	"Accion" => "Elimino un Archivo",
	   	"Tabla" => "Tabla: punzon_archivo",
	   	"Valores" => "Id. Pun_Arch: ".$IdArchPun.", Archivo: ".$Archivo
	 
		);
	
	$this->db->insert('Log_Produccion',$logs_elim_arch);
	 
	return TRUE;	
	
}



/*Funcion para completar el log cuando inserto un Archivo a un punzon*/
function log_pun_arch_add($post_array, $primary_key)
{	

  $logs_insert_pun_arch = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un Archivo a un Punzon",
   	"Tabla" => "Tabla: punzon_archivo",
   	"Valores" => "Id. Punzon-Archivo: ".$primary_key.", Punzon: ".$post_array['IdPun'].", Archivo: ".$post_array['Archivo']
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert_pun_arch);
	 
	return TRUE;


}



/*Funcion para completar el log cuando inserto un Archivo a un punzon*/
function log_pun_arch_edit($post_array, $primary_key)
{	

  $logs_insert_pun_arch = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Edito un Archivo a un Punzon",
   	"Tabla" => "Tabla: punzon_archivo",
   	"Valores" => "Id. Punzon-Archivo: ".$primary_key.", Punzon: ".$post_array['IdPun'].", Archivo: ".$post_array['Archivo']
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert_pun_arch);
	 
	return TRUE;


}




/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_punzon_add($post_array, $primary_key)
{	
	/*Carga el estado (Habilitado) de un punzon recien ingresado*/
	$this->db->where('IdPunzon', $primary_key);
 	$this->db->update('punzon',array('EstadoPunzon' => 'Habilitado'));
	
	/*Obtiene los valores para cargar el log*/
	$this->db->select('Fecha_Ingreso');
	$this->db->where('IdPunzon',$primary_key);
	$query3 = $this->db->get('punzon');
	$dato_fecha_ingreso = $query3->row()->Fecha_Ingreso;
	$dato_fecha_ingreso = date('d-m-Y');
	
	$this->db->select('EstadoPunzon');
	$this->db->where('IdPunzon',$primary_key);
	$query5 = $this->db->get('punzon');
	$dato_estado = $query5->row()->EstadoPunzon;
		
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: punzon",
   	"Valores" => "Id.Punzon: ".$primary_key.", Descripcion: ".$post_array['DescPunzon'].", Fecha_Ingreso: ".$dato_fecha_ingreso.", Est.Punzon: ".$dato_estado
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}


/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_punzon_edit($post_array, $primary_key)
{	
	
	/*Obtiene los valores para cargar el log*/
	$this->db->select('Fecha_Ingreso');
	$this->db->where('IdPunzon',$primary_key);
	$query3 = $this->db->get('punzon');
	$dato_fecha_ingreso = $query3->row()->Fecha_Ingreso;
	$dato_fecha_ingreso = date('d-m-Y');
	
	$this->db->select('EstadoPunzon');
	$this->db->where('IdPunzon',$primary_key);
	$query5 = $this->db->get('punzon');
	$dato_estado = $query5->row()->EstadoPunzon;
		
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego edito un registo",
   	"Tabla" => "Tabla: punzon",
   	"Valores" => "Id.Punzon: ".$primary_key.", Descripcion: ".$post_array['DescPunzon'].", Fecha_Ingreso: ".$dato_fecha_ingreso.", Est.Punzon: ".$dato_estado
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;

}


/*Funcion que elimina el prodcuto, cambia el estado del producto a AC*/
function cambia_estado_punzon($primary_key, $fecha)
{
 /*Elimina el punzon, es decir, le cambia el estado a Deshabilitado*/
 $this->db->where('IdPunzon', $primary_key);
 $update_array = array(
			   	"Fecha_Egreso" => $fecha,
			   	"EstadoPunzon" => "Deshabilitado"
			 
				);
 $this->db->update('punzon',$update_array);
 
 /*Hace la recuperacion de los valores para almacenar el log cuando elimino*/
 $this->db->select('DescPunzon');
 $this->db->where('IdPunzon',$primary_key);
 $query1 = $this->db->get('punzon');
 $dato_descripcion = $query1->row()->DescPunzon;
 
 $this->db->select('Fecha_Ingreso');
 $this->db->where('IdPunzon',$primary_key);
 $query2 = $this->db->get('punzon');
 $dato_fecha_ingreso = $query2->row()->Fecha_Ingreso;
 $dato_fecha_ingreso = date('d-m-Y');
 
 $this->db->select('Fecha_Egreso');
 $this->db->where('IdPunzon',$primary_key);
 $query3 = $this->db->get('punzon');
 $dato_fecha_egreso = $query3->row()->Fecha_Egreso;
 $dato_fecha_egreso = date('d-m-Y');
 
 $this->db->select('EstadoPunzon');
 $this->db->where('IdPunzon',$primary_key);
 $query4 = $this->db->get('punzon');
 $dato_estado = $query4->row()->EstadoPunzon;
 
 $logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Elimino un registo",
   	"Tabla" => "Tabla: punzon",
   	"Valores" => "Id.Punzon: ".$primary_key.", Descripcion: ".$dato_descripcion.", Fecha_Ingreso: ".$dato_fecha_ingreso.", Fecha_Egreso: ".$dato_fecha_egreso.", Est.Punzon: ".$dato_estado
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}





function graba_datos($IdPun, $DescArch)
{	
			
	$insert = array(
   	"IdPun" => $IdPun,
   	"Archivo" => $DescArch
   	
 
	);
	
	$this->db->insert('punzon_archivo',$insert);
	
	//Código para insertar en el log
	//De esta forma obtenemos el ultimo id insertado
	$id = $this->db->insert_id();
	
	//Obtenemos la fecha del ultimo id insertado
	$this->db->select('Fecha_Hora_Reg');
	$this->db->from('punzon_archivo');
	$this->db->where('IdPun_Arch',$id);
	$query = $this->db->get();
	$fecha_reg = $query->row()->Fecha_Hora_Reg;
	
	//Ordeno la fecha para almacenarla en el log
	$dia = substr($fecha_reg, -11, 2);
	$mes = substr($fecha_reg, -14, 2);
	$anio = substr($fecha_reg, -19, 4);
   	
   	$hora = substr($fecha_reg, -8, 2); 
   	$min = substr($fecha_reg, -5, 2);
   	$seg = substr($fecha_reg, -2);
   	$fecha_reg_conv = $dia .'-'. $mes .'-'. $anio.' '.$hora.':'.$min.':'.$seg;
	
	
	//De esta forma obtengo los valores separados de la fecha
	 /*$dia = substr($Fecha_Desde, -10, 2);
	 $mes = substr($Fecha_Desde, -7, 2);
	 $anio = substr($Fecha_Desde, -4);
   	 $Fecha_Desde_conv = $anio .'-'. $mes .'-'. $dia.' 00:00:00';*/

	
	$logs_insert = array(
	   	"UsuarioSO" => $this->session->userdata('Usuario'),
	   	"UsuarioSistema" => $this->session->userdata('Usuario'),
	   	"PC" => $this->session->userdata('ip_pc'), 	
	   	"Nivel" => $this->session->userdata('Nivel'),
	   	"Accion" => "Inserto un registo",
	   	"Tabla" => "Tabla: punzon_archivo",
	   	"Valores" => "Id: ".$id.", IdPun: ".$IdPun.", Archivo: ".$DescArch.", Fecha Registro: ".$fecha_reg_conv
	 
		);
	
	$this->db->insert('Log_Produccion',$logs_insert);	 
	
	return TRUE;
}



//Funcion que obtiene los datos del registro antes de borrarlo
function obtiene_datos_registro($primary_key)
{	
	
	$this->db->select('IdPun, Archivo, Fecha_Hora_Reg');
	$this->db->from('punzon_archivo');
	$this->db->where('IdPun_Arch',$primary_key);
	$query = $this->db->get();
	
	return $query->result();
	
	
}

//Funcion que Guarda el log cuando elimino un Punzon-Archivo
function Log_Delete_Punz_Arch($primary_key)
{	
	//Obtengo la descripcion del punzon para cargarla en el log
	$this->db->select('DescPunzon');
	$this->db->from('punzon');
	$this->db->where('IdPunzon',$this->session->flashdata('IdPun'));
	$query = $this->db->get();
	$Desc_Pun = $query->row()->DescPunzon;

	$logs_insert = array(
	   	"UsuarioSO" => $this->session->userdata('Usuario'),
	   	"UsuarioSistema" => $this->session->userdata('Usuario'),
	   	"PC" => $this->session->userdata('ip_pc'), 	
	   	"Nivel" => $this->session->userdata('Nivel'),
	   	"Accion" => "Elimino un registo",
	   	"Tabla" => "Tabla: punzon_archivo",
	   	"Valores" => "Id_Reg: ".$primary_key.", Punzon: ".$Desc_Pun.", Archivo: ".$this->session->flashdata('Archivo').", Fecha: ".$this->session->flashdata('Fecha_Hora_Reg')
	 
		);
	
	$this->db->insert('Log_Produccion',$logs_insert);	 
	//C:\AppServ\www\produccion\assets\uploads\files
	//Elimino el archivo de la carpeta
	//produccion\assets\uploads\files
	//$ruta = '\assets\uploads\files';
	//$ruta = 'produccion\assets\uploads\files'.$this->session->flashdata('Archivo');
	
	//C:\AppServ\www\produccion\assets\uploads\files
	
	//Almaceno la ruta de los archivos
	$ruta = 'C:/AppServ/www/produccion/assets/uploads/files';
	//$ruta = getcwd();
	
	//Capturo el nombre del archivo 
	$archivo = (string) $this->session->flashdata('Archivo');
	
	//Elimino el archivo fisico de la carpeta
	//unlink('c:/Prueba.php');
	unlink($ruta."/".$archivo);
	
	return TRUE;
	
	
}	
	
/*--------------------------FIN DE LAS FUNCIONES-------------------------------------------*/	


}
<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_subetapa extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion para consultar si una Descripción de una Subetapa ya estan cargadas
function campos_subetapa_add($post_array)
{
	$DescSubEtapa = trim($post_array['DescSubEtapa']);
	$this->db->where('DescSubEtapa',$DescSubEtapa);
	$this->db->from('subetapa');
	$query = $this->db->get();
	
	if($query->num_rows()>0)
             return FALSE;
        else return TRUE;
	
}


//Funcion para consultar si una Descripción de una Subetapa ya estan cargada
function campos_subetapa_edit($post_array, $primary_key)
	{	  
	  //$IdSubetapa = $post_array['IdSubetapa'];
	  $DescSubetapa = trim($post_array['DescSubEtapa']);
	  $this->db->where('IdSubetapa !=',$primary_key);
	  $this->db->where('DescSubEtapa',$DescSubetapa);
	  $this->db->from('subetapa');
	  $query = $this->db->get();
	  
	  /*Si la descripcion del punzon se encuentran cargado retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}


/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_add($post_array, $primary_key)
{	
	/*Obtiene los valores para cargar el log*/
	/*$this->db->select('DescSubEtapa');
	$this->db->where('IdSubetapa',$primary_key);
	$query = $this->db->get('subetapa');
	$dato_DescSubEtapa = $query->row()->DescSubEtapa;*/
	
	//$IdEtapa = $post_array['IdEtapa'];
	$this->db->select('DescEtapa');
	$this->db->where('IdEtapa',$post_array['IdEtapa']);
	$query1 = $this->db->get('etapa');
	$dato_DescEtapa = $query1->row()->DescEtapa;
		
	/*$this->db->select('Orden');
	$this->db->where('IdSubetapa',$primary_key);
	$query2 = $this->db->get('subetapa');
	$dato_Orden = $query2->row()->Orden;*/
	
	/*$this->db->select('Objetivo');
	$this->db->where('IdSubetapa',$primary_key);
	$query3 = $this->db->get('subetapa');
	$dato_Objetivo = $query3->row()->Objetivo;*/
	
	$this->db->select('EstadoSub');
	$this->db->where('IdSubetapa',$primary_key);
	$query4 = $this->db->get('subetapa');
	$dato_EstadoSub = $query4->row()->EstadoSub;
		
	$logs_insert = array(
					   	"UsuarioSO" => $this->session->userdata('Usuario'),
					   	"UsuarioSistema" => $this->session->userdata('Usuario'),
					   	"PC" => $this->session->userdata('ip_pc'), 	
					   	"Nivel" => $this->session->userdata('Nivel'),
					   	"Accion" => "Agrego un registo",
					   	"Tabla" => "Tabla: subetapa",
					   	"Valores" => "Id.SubEtapa: ".$primary_key.", Descripcion: ".$post_array['DescSubEtapa'].", Etapa: ".$dato_DescEtapa.", Orden: ".$post_array['Orden'].", Objetivo: ".$post_array['Objetivo'].", Estado: ".$dato_EstadoSub
					 
						);
	
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}


/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_edit($post_array, $primary_key)
{	
	/*Obtiene los valores para cargar el log*/
	$this->db->select('DescEtapa');
	$this->db->where('IdEtapa',$post_array['IdEtapa']);
	$query1 = $this->db->get('etapa');
	$dato_DescEtapa = $query1->row()->DescEtapa;
		
	/*$this->db->select('Orden');
	$this->db->where('IdSubetapa',$primary_key);
	$query2 = $this->db->get('subetapa');
	$dato_Orden = $query2->row()->Orden;*/
	
	/*$this->db->select('Objetivo');
	$this->db->where('IdSubetapa',$primary_key);
	$query3 = $this->db->get('subetapa');
	$dato_Objetivo = $query3->row()->Objetivo;*/
	
	/*$this->db->select('EstadoSub');
	$this->db->where('IdSubetapa',$primary_key);
	$query4 = $this->db->get('subetapa');
	$dato_EstadoSub = $query4->row()->EstadoSub;*/
		
	$logs_insert = array(
					   	"UsuarioSO" => $this->session->userdata('Usuario'),
					   	"UsuarioSistema" => $this->session->userdata('Usuario'),
					   	"PC" => $this->session->userdata('ip_pc'), 	
					   	"Nivel" => $this->session->userdata('Nivel'),
					   	"Accion" => "Se Edito un registo",
					   	"Tabla" => "Tabla: subetapa",
					   	"Valores" => "Id.SubEtapa: ".$primary_key.", Descripcion: ".$post_array['DescSubEtapa'].", Etapa: ".$dato_DescEtapa.", Orden: ".$post_array['Orden'].", Objetivo: ".$post_array['Objetivo'].", EstadoSub: ".$post_array['EstadoSub']
					 
						);
	
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}




	
	
/*--------------------------FIN DE LAS FUNCIONES-------------------------------------------*/	

}
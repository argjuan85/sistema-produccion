<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_punxmaq extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion para consultar si un punzón y una maquina ya estan cargadas
function consulta_punxmaq($post_array){
	  	  
	  $this->db->where('IdPunzon',$post_array['IdPunzon']);
	  $this->db->where('IdMaquina',$post_array['IdMaquina']);
	  $this->db->from('punxmaq');
	  $query = $this->db->get();
	  
	  /*Si la relación punzón/maquina se encuentra, quiere decir que ya esta cargada y retorna false, sino retorna true*/
	  if($query->num_rows()>0)
			  	 return FALSE;
			else return TRUE;	 	    
   
	}
	


//Funcion que obtiene el id del punzon para luego armar el arreglo para el dropdown
function obtiene_idpun($primary_key)
{
 $this->db->select('IdPunzon');
 $this->db->from('punxmaq');
 $this->db->where('IdPunxMaq',$primary_key);

 $query = $this->db->get();
 
 return $query->row()->IdPunzon;
}


//Funcion que obtiene el id de la maquina para luego armar el arreglo para el dropdown
function obtiene_idmaq($primary_key)
{
 $this->db->select('IdMaquina');
 $this->db->from('punxmaq');
 $this->db->where('IdPunxMaq',$primary_key);

 $query = $this->db->get();
  
 return $query->row()->IdMaquina;
}



//Funcion que obtiene el arreglo resultante para armar el dropdown
function obtiene_datos_pun_edit($idpun)
{
 
 $this->db->select('IdPunzon, DescPunzon');
 $this->db->from('punzon');
 $this->db->where('IdPunzon',$idpun);
 $this->db->or_where('EstadoPunzon','Habilitado');
 $this->db->order_by('IdPunzon', 'asc');
 
 $query = $this->db->get();
 
 $array1_pun = array();
 $array2_pun = array();
 $array3_pun = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_pun, $row->IdPunzon);
  array_push($array2_pun, $row->DescPunzon);
 }
 
 $array3_pun = array_combine($array1_pun, $array2_pun);
 return $array3_pun;
 
}


//Funcion que obtiene el arreglo resultante para armar el dropdown
function obtiene_datos_maq_edit($idmaq)
{
 
 $this->db->select('IdMaquina, DescMaquina');
 $this->db->from('maquina');
 $this->db->where('IdMaquina',$idmaq);
 $this->db->or_where('EstadoMaquina','Habilitada');
 $this->db->order_by('IdMaquina', 'asc');
 
 $query = $this->db->get();
 
 $array1_maq = array();
 $array2_maq = array();
 $array3_maq = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_maq, $row->IdMaquina);
  array_push($array2_maq, $row->DescMaquina);
 }
 
 $array3_maq = array_combine($array1_maq, $array2_maq);
 return $array3_maq;
 
}


/*Fin de las funciones*/








	


	function consulta_chofer($dni_chofer){
		$this->db->where('nro_documento',$dni_chofer);
		$query = $this->db->get('chofer');

		return $query->num_rows();
	}




//Obtiene el id de un chofe cargado
	function obtener_idchofer($dni_chofer){
		$this->db->select('idchofer');
		$this->db->where('nro_documento',$dni_chofer);
		$query = $this->db->get('chofer');
		if($query->num_rows()>0)
			return $query->row();
		else return 'false'; 
	}

	function obtener_chofer_por_id($id){
		$this->db->where('idchofer',$id);
		$query = $this->db->get('chofer');
		if($query->num_rows()>0)
			return $query->row();
		else return 'no_existe'; 
	}


	function modificar_chofer($data,$id_chofer){
		$this->db->where('idchofer',$id_chofer);
		$this->db->update('chofer', $data); 
		if($this->db->_error_message() == '')
		{
			$id_usuario = $this->session->userdata('id_usuario');
			$ip_pc = $this->input->ip_address();
			$accion = 'Modificar un chofer';
			$tabla = 'chofer';

			$array_log_chofer = array(
				'idusuario' => $id_usuario,
				'iporigen' => $ip_pc,
				'accion' => $accion,
				'tabla_afectada' => $tabla,
				'id_tabla' => $id_chofer
				);

			$query2 = $this->db->insert('log',$array_log_chofer);

			if($this->db->_error_message() == '')
				return 'correcto';
			else {
				echo $query2;
				return $this->db->_error_message();
			}
		}
		else 
			return $this->db->_error_message();
	}

	function listado_chofer(){
		$query = $this->db->get('chofer');
		return $query->result();
	}

/*
	function consulta_repac($dni){
		//ENLAZAMOS a $db_repac LA BASE DE DATO DE REPAC
		$db_repac = $this->load->database('repac', TRUE);
		$db_repac->where('dni',$dni);
		$query = $db_repac->get('conductor');

		if($query->num_rows()>0)
		{
			$id_cond = $query->row()->id_conductor;
			$sql = "SELECT * FROM repac.conductor WHERE (EXISTS(SELECT * FROM repac.art_22 WHERE id_cond = '$id_cond') OR EXISTS(SELECT * FROM repac.deudores_alimentos WHERE id_conductor = '$id_cond' ) OR EXISTS(SELECT * FROM repac.homicidio_lesiones WHERE id_conductor = '$id_cond') OR EXISTS(SELECT * FROM repac.conductor c INNER JOIN repac.acta_infraccion a ON c.id_conductor = a.id_conductor WHERE a.id_conductor = '$id_cond')) AND (id_conductor = '$id_cond');";
			$query2 = $db_repac->query($sql);
			if($query2->num_rows()>0)
			{
				$estado = '';
				
				$sql="SELECT * FROM repac.conductor cond 
				INNER JOIN repac.acta_infraccion actas ON cond.id_conductor=actas.id_conductor 
				WHERE cond.id_conductor = '$id_cond' 
				ORDER BY actas.fecha;";
				$query3 = $db_repac->query($sql);
				if($query3->num_rows()>0)				
					$estado = 'A';
				else {
					$sql="SELECT * FROM repac.conductor cond 
					INNER JOIN repac.acta_infraccion actas ON cond.id_conductor=actas.id_conductor 
					INNER JOIN repac.inhabilitacion inha ON actas.id_acta=inha.id_acta 
					WHERE cond.id_conductor = '$id_cond' 
					ORDER BY actas.fecha;";
					$query4 = $db_repac->query($sql);	
					if($query4->num_rows()>0)	{
					
					if (($arre['fecha_comienzo_inhabilitacion'] != NULL) and ($arre['rehabilitado'] == NULL))
														{
															$estado = 'I';
															echo "<br> ESTAD ".$estado;
														}
														else 
															if (($arre['resolucion'] != NULL) and ($arre['fecha_comienzo_inhabilitacion'] == NULL) and ($arre['fecha_oca'] != NULL))
																$estado = 'I';
															else 
																$estado = 'A';
														}
					
						foreach($query4->result_array() as $arre)	
						{
							if (($arre['fecha_comienzo_inhabilitacion'] != NULL) and ($arre['rehabilitado'] == NULL))
							{
								$estado = 'I';
							}
							else 
								if (($arre['resolucion'] != NULL) and ($arre['fecha_comienzo_inhabilitacion'] == NULL) and ($arre['fecha_oca'] != NULL))
									$estado = 'I';
								else 
									$estado = 'A';
							}
						}
					}
				}	
			}
		else
			return "error";
		return $query4->row();
	}*/


	//Funcion para buscar habilitacion del chofer en repac
	/*function busca_repac($dni){
		$query=$this->db_repac->insert('chofer',$data);
		//var_dump($data);die;
		if($query->num_rows > 0)
		{
			return 'correcto';
		}
		else 
		{
			return $this->db->_error_message();
		}		
	}*/

}
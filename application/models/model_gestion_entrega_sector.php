<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_entrega_sector extends CI_Model {

	function __construct() {
		parent::__construct();
	}



/*Funcion que almacena el log al insertar entrega a un sector*/
function graba_log_ot_entrega_add($post_array, $primary_key)
{
	
	
	
	$this->db->select('ot.NumOT');
	$this->db->from('entregasector');
	$this->db->join('ot','entregasector.IdOT = ot.IdOT');
	$this->db->where('entregasector.IdEntregaSector',$primary_key);
	$query1 = $this->db->get();
	$dato_NumOT = $query1->row()->NumOT;

	
	$this->db->select('sector.DescSector');
	$this->db->from('entregasector');
	$this->db->join('sector','entregasector.IdSector = sector.IdSector');
	$this->db->where('entregasector.IdEntregaSector',$primary_key);
	$query2 = $this->db->get();
	$dato_DescSector = $query2->row()->DescSector;
	
	//Obtengo la Fecha y la Hora de la Base de Datos a que es de tipo timestamp
	$this->db->select('FechaEntregaSector');
	$this->db->from('entregasector');
	$this->db->where('IdEntregaSector',$primary_key);
	$query3 = $this->db->get();
	$dato_FechaEntregaSector = $query3->row()->FechaEntregaSector;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Inserto un registo",
   	"Tabla" => "Tabla: entregasector",
   	"Valores" => "Id.Entrega Sector: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Desc. Sector: ".$dato_DescSector.", Fecha de Entrega: ".$dato_FechaEntregaSector.", Versón de Protocolo: ".$post_array['Vers_Protocolo'].",  Observación de la Entrega: ".$post_array['Observacion_Entrega']
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



/*Funcion que almacena el log al insertar entrega a un sector*/
function graba_log_entrega_sector_edit($post_array, $primary_key)
{
	$this->db->select('ot.NumOT');
	$this->db->from('entregasector');
	$this->db->join('ot','entregasector.IdOT = ot.IdOT');
	$this->db->where('entregasector.IdEntregaSector',$primary_key);
	$query1 = $this->db->get();
	$dato_NumOT = $query1->row()->NumOT;

	
	$this->db->select('sector.DescSector');
	$this->db->from('entregasector');
	$this->db->join('sector','entregasector.IdSector = sector.IdSector');
	$this->db->where('entregasector.IdEntregaSector',$primary_key);
	$query2 = $this->db->get();
	$dato_DescSector = $query2->row()->DescSector;
	

	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Edito un registo",
   	"Tabla" => "Tabla: entregasector",
   	"Valores" => "Id.Entrega Sector: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Desc. Sector: ".$dato_DescSector.", Fecha de Entrega: ".$post_array['FechaEntregaSector'].", Versón de Protocolo: ".$post_array['Vers_Protocolo'].",  Observación de la Entrega: ".$post_array['Observacion_Entrega']
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



//Funcion para hace la consulta de los valores de las entregas para enviarlos al pdf
function Buscar_datos_entrega_like($NumOT, $Sector, $FechaEntregaSector, $Vers_Protocolo, $Observacion_Entrega) 
{
 //Armo el arreglo para recorrer componente por componente
 $array_datos = array('NumOT' => $NumOT, 'Sector' => $Sector, 'FechaEntregaSector' => $FechaEntregaSector, 'Vers_Protocolo' => $Vers_Protocolo, 'Observacion_Entrega' => $Observacion_Entrega);
 

$this->db->select('ot.NumOT, sector.DescSector, entregasector.FechaEntregaSector, entregasector.Vers_Protocolo, entregasector.Observacion_Entrega');

$this->db->from('entregasector');

$this->db->join('ot','ot.IdOT = entregasector.IdOT');
$this->db->join('sector','entregasector.IdSector = sector.IdSector');


foreach($array_datos as $clave => $dato) 
 {
        switch($clave) 
        {
         case 'NumOT':
    	    		 if($dato != '')
			   				$this->db->like('ot.NumOT',$NumOT);			
			   		 break; 
			  	
	    case 'Sector':
		    	    	 if($dato != '')
					   			$this->db->like("sector.DescSector",$Sector);			
					   	 break; 
		
		case 'FechaEntregaSector':
		    	    	 if($dato != '')
					   			$this->db->like('entregasector.FechaEntregaSector',$FechaEntregaSector);			
					   	 break;
					  	
	    case 'Vers_Protocolo':
		    	    	 if($dato != '')
		    	    	 		{  			 
					   			 $this->db->like('entregasector.Vers_Protocolo',$Vers_Protocolo);
					   			 
					   			} 
					   	 break;
					
		case 'Observacion_Entrega':
		    	    	 if($dato != '')
					   			{					   		  	 
								 $this->db->like('entregasector.Observacion_Entrega',$Observacion_Entrega);			
					   			}
					   	 break;
		 		  	 	
        }
 }

$query = $this->db->get();

if($query->num_rows()>0)
                  return $query->result();
             else return FALSE;


}




/*---------------Funciones para armar el select de sector----------------*/
//Funcion para obtener el id de los sectores
function obtiene_idsector()	
{
 //Funcion que obtiene los id de los productos que estan en estado AC junto con el id del producto que estoy editando, sea cual sea su estado  
 $this->db->select('IdSector');
 $this->db->from(' sector');
 $this->db->order_by("IdSector","asc");
 $query = $this->db->get();
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->IdSector);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
return $array;
}

//Funcion para obtener la descripción de los sectores
function obtiene_desc_sector()	
{
 //Funcion que obtiene los id de los productos que estan en estado AC junto con el id del producto que estoy editando, sea cual sea su estado  
 $this->db->select('IdSector, DescSector');
 $this->db->from(' sector');
 $this->db->order_by("IdSector","asc");
 $query = $this->db->get();
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->DescSector);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
return $array;
}

/*----------------------------------------------------------------------*/


//Funcion que inserta los registro de las dos tablas
function inserta_registros($post_array)	
{	 
 //Inserta en la tabla ot 
 //Convierto la fecha para poderla insertar en la Base de datos
 $dia = substr($post_array['FechaVto'], -10, 2);
 $mes = substr($post_array['FechaVto'], -7, 2);
 $anio = substr($post_array['FechaVto'], -4);
 $FechaVto_conv = $anio .'-'. $mes .'-'. $dia;
 
 $reg_ot_insert = array(
	   	"Lote" => $post_array['Lote'],
	   	"FechaVto" => $FechaVto_conv,
	   	"IdVaP" => $post_array['IdVaP'], 	
	   	"IdProdWS" => $post_array['IdProdWS'] 
		);
	 
	$this->db->insert('ot',$reg_ot_insert);
	
	$last_id_ot = $this->db->insert_id();
	
	//Obtenemos el numero de ot si es que se ingreso
	if($post_array['IdOT'] == '')
				{
				 $this->db->select('NumOT');
				 $this->db->from('ot');
				 $this->db->where('ot.IdOT',$last_id_ot);
				 $query = $this->db->get();
				 $dato_NumOT = $query->row()->NumOT;	
				}
			else $dato_NumOT = '';	

	//Obtenemos la descripcion de vap
	$this->db->select('vap.DescVaP');
	$this->db->from('ot');
	$this->db->join('vap','ot.IdVaP = vap.IdVaP');
	$this->db->where('ot.IdOT',$last_id_ot);
	$query1 = $this->db->get();
	$dato_DescVaP = $query1->row()->DescVaP;
	
	//Obtenemos el codigo del producto WS
	$this->db->select('productosws.CodProdWS');
	$this->db->from('ot');
	$this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
	$this->db->where('ot.IdOT',$last_id_ot);
	$query2 = $this->db->get();
	$dato_CodProdWS = $query2->row()->CodProdWS;
	
	//Obtenemos la descripcion del producto WS
	$this->db->select('productosws.DescProdWS');
	$this->db->from('ot');
	$this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
	$this->db->where('ot.IdOT',$last_id_ot);
	$query3 = $this->db->get();
	$dato_DescProdWS = $query3->row()->DescProdWS;
	
	
	//Inserta en la tabla log
	$logs_insert_ot = array(
		   	"UsuarioSO" => $this->session->userdata('Usuario'),
		   	"UsuarioSistema" => $this->session->userdata('Usuario'),
		   	"PC" => $this->session->userdata('ip_pc'), 	
		   	"Nivel" => $this->session->userdata('Nivel'),
		   	"Accion" => "Agrego un registo",
		   	"Tabla" => "Tabla: ot",
		   	"Valores" => "Id. OT: ".$last_id_ot.",  Nro OT: ".$dato_NumOT.", Lote: ".$post_array['Lote'].", Fecha de Vto: ".$FechaVto_conv.", VaP: ".$dato_DescVaP.",  Cod. Prod: ".$dato_CodProdWS.", Desc. Prod: ".$dato_DescProdWS
		 
			);
			 
	$this->db->insert('Log_Produccion',$logs_insert_ot);
	
	
	$reg_entrega_insert = array(
		   	"IdOT" => $last_id_ot,
		   	"IdSector" => $post_array['IdSector'],
		   	"Vers_Protocolo" => $post_array['Vers_Protocolo'],
		   	"Observacion_Entrega" => $post_array['Observacion_Entrega']
		   	 
			);
				 
	$this->db->insert('entregasector',$reg_entrega_insert);	
	
	$last_id_entrega = $this->db->insert_id();
	
	//Obtengo el sector
	$this->db->select('sector.DescSector');
	$this->db->from('entregasector');
	$this->db->join('sector','entregasector.IdSector = sector.IdSector');
	$this->db->where('entregasector.IdEntregaSector',$last_id_entrega);
	$query4 = $this->db->get();
	$dato_DescSector = $query4->row()->DescSector;
	
	//Obtengo la Fecha de entrega al sector
	$this->db->select('FechaEntregaSector');
	$this->db->from('entregasector');
	$this->db->where('IdEntregaSector',$last_id_entrega);
	$query5 = $this->db->get();
	$dato_FechaEntregaSector = $query5->row()->FechaEntregaSector;
	
	
	//Inserta en la tabla log
	$logs_insert_entrega = array(
		   	"UsuarioSO" => $this->session->userdata('Usuario'),
		   	"UsuarioSistema" => $this->session->userdata('Usuario'),
		   	"PC" => $this->session->userdata('ip_pc'), 	
		   	"Nivel" => $this->session->userdata('Nivel'),
		   	"Accion" => "Agrego un registo",
		   	"Tabla" => "Tabla: entregasector",
		   	"Valores" => "Id. Entrega: ".$last_id_entrega.",  Nro OT: ".$dato_NumOT.", Sector: ".$dato_DescSector.", Fecha de Entrega al Sector: ".$dato_FechaEntregaSector.", Ver. Protocolo: ".$post_array['Vers_Protocolo'].",  Observacion de Entrega: ".$post_array['Observacion_Entrega']
		 
	);
			 
	$this->db->insert('Log_Produccion',$logs_insert_entrega);
	
	return TRUE;
	
}



//----------------FUNCIONES PARA ARMAR EL SELECT PRODUCTO WS DE EDITAR OT------------------//
function obtiene_IdProdws($primary_key)
{
 //Obtiene el id del producto
 $this->db->select('IdProdWS');
 $this->db->from('ot');
 $this->db->where('IdOT', $primary_key);
 $query = $this->db->get();	
 //Obtengo el valor del id en un arreglo para poder individualizar el valor y retornarlo como string
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->IdProdWS);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 $valor = $array[0];
 /*var_dump($valor);
 echo "<br>";
 echo $valor;
 die;*/
 
 return $valor;
}


//Obtengo el id de la ot para armar el select
function obtiene_IdOT()	
{
 //Funcion que obtiene los id de los productos que estan en estado AC junto con el id del producto que estoy editando, sea cual sea su estado  
 $this->db->select('IdOT');
 $this->db->from('ot');
 //$this->db->where('IdProductoWS', $idprodws);
 //$this->db->or_where('EstadoProdWS', 'AC');
 //$this->db->or_where('EstadoProdWS', 'ac');
 $this->db->order_by("IdOT","asc");
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->IdOT);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }

return $array;
 
}

//Obtengo los datos de la ot y del producto para armar el select
function obtiene_datos_ot()
{
 //Funcion que obtiene los Codigos y la Descripcion de los productos que estan en estado AC junto con el Código y la descripcion del producto que estoy editando, sea cual sea su estado
 $this->db->select('ot.IdOT, ot.NumOT, ot.Lote, productosws.CodProdWS, productosws.DescProdWS');
 $this->db->from('ot');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 //$this->db->where('IdProductoWS', $idprodws);
 //$this->db->or_where('EstadoProdWS', 'AC');
 //$this->db->or_where('EstadoProdWS', 'ac');
 $this->db->order_by("ot.IdOT","asc");
 //$this->db->where('IdProdElab', $IdProd);
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->NumOT." - ".$row->Lote." - ".$row->CodProdWS." - ".$row->DescProdWS);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 return $array;
 
 /*if($query->num_rows()>0)
                  return $query->result_array();
             else return FALSE;	*/	
}

//---------------------------------------------------------------------------------//

//Obtengo el Numero de ot para la columna
function consulta_NumOT($row)
{
 $this->db->select('NumOT');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->where('ot.IdOT',$row->IdOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->NumOT;
             else return FALSE;	
}

//Obtengo el Lote para la columna
function consulta_Lote($row)
{
 $this->db->select('Lote');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->where('ot.IdOT',$row->IdOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->Lote;
             else return FALSE;	
}


//Obtengo la fecha de vencimiento para la columna
function consulta_FechaVto($row)
{
 $this->db->select('FechaVto');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->where('ot.IdOT',$row->IdOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->FechaVto;
             else return FALSE;	
}



//Obtengo los datos del producto para la columna
function consulta_Datos_Prod($row)
{
 $this->db->select('CodProdWS, DescProdWS');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 $this->db->where('ot.IdOT',$row->IdOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->CodProdWS." - ".$query->row()->DescProdWS;
             else return FALSE;	
}



//Inserta los datos en la tabla entrega_sector
//Además almaceno el Log
function insertar_add($post_array)
{
 	
 	
			 $insert = array(
			   	"IdOT" => $post_array['NumOT-Lote-CodyDescProd'],
			   	"IdSector" => $post_array['IdSector'],
			   	"FechaEntregaSector" => date('Y-m-d H:i:s'), 	
			   	"Vers_Protocolo" => $post_array['Vers_Protocolo'],
			   	"Observacion_Entrega" => $post_array['Observacion_Entrega'],
			   	"Usuario_Carga" => $this->session->userdata('Usuario')
			 
				);
			 
			  $query = $this->db->insert('entregasector',$insert);
			    
			  if($query != FALSE)
			  			{/*----------------------ALMACENO EL LOG AL INSERTAR---------------------------*/
			  			 //Obtengo el id del ultimo registro insertado
						 $ultimo_id = $this->db->insert_id();
						 
						 //Obtengo los datos para insertar el log
						 $this->db->select('NumOT');
						 $this->db->from('ot');
						 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
						 $query = $this->db->get();
						 $dato_NumOT = $query->row()->NumOT;
						 
						 $this->db->select('Lote');
						 $this->db->from('ot');
						 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
						 $query1 = $this->db->get();
						 $dato_Lote = $query1->row()->Lote;
						 
						 $this->db->select('FechaVto');
						 $this->db->from('ot');
						 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
						 $query2 = $this->db->get();
						 $dato_FechaVto = $query2->row()->FechaVto;
						 
						 $this->db->select('productosws.CodProdWS');
						 $this->db->from('ot');
						 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
						 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
						 $query3 = $this->db->get();
						 $dato_CodProdWS = $query3->row()->CodProdWS;
						 
						 $this->db->select('productosws.DescProdWS');
						 $this->db->from('ot');
						 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
						 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
						 $query4 = $this->db->get();
						 $dato_DescProdWS = $query4->row()->DescProdWS;
						 
						 
						 $this->db->select('sector.DescSector');
						 $this->db->from('entregasector');
						 $this->db->join('sector','entregasector.IdSector =  sector.IdSector');
						 $this->db->where('entregasector.IdEntregaSector',$ultimo_id);
						 $query5 = $this->db->get();
						 $dato_DescSector = $query5->row()->DescSector;
						 
						 $this->db->select('FechaEntregaSector');
						 $this->db->from('entregasector');
						 $this->db->where('IdEntregaSector',$ultimo_id);
						 $query6 = $this->db->get();
						 $dato_FechaEntregaSector = $query6->row()->FechaEntregaSector;
						 
						 /*$this->db->select('Usuario_Carga');
						 $this->db->from('entregasector');
						 $this->db->where('IdEntregaSector',$ultimo_id);
						 $query7 = $this->db->get();
						 $dato_Usuario_Carga = $query7->row()->Usuario_Carga;*/
						 
						 
						 $logs_insert = array(
								   	"UsuarioSO" => $this->session->userdata('Usuario'),
								   	"UsuarioSistema" => $this->session->userdata('Usuario'),
								   	"PC" => $this->session->userdata('ip_pc'), 	
								   	"Nivel" => $this->session->userdata('Nivel'),
								   	"Accion" => "Agrego un registo",
								   	"Tabla" => "Tabla: entregasector",
								   	//"Valores" => "Hola",
								   	"Valores" => "Id. EntregaSector: ".$ultimo_id.",  Nro OT: ".$dato_NumOT.", Lote: ".$dato_Lote.", FechaVto: ".$dato_FechaVto.", Cod. Prod: ".$dato_CodProdWS.", Desc. Prod: ".$dato_DescProdWS.", Sector: ".$dato_DescSector.", Fecha de Entrega a Sector: ".$dato_FechaEntregaSector.", Vers_Protocolo: ".$post_array['Vers_Protocolo'].", Observacion de Entrega: ".$post_array['Observacion_Entrega'].", Usuario Carga: ".$this->session->userdata('Usuario')
						 
										);
							 
							/*$logs_insert = array(
								   	"UsuarioSO" => "Prueba",
								   	"UsuarioSistema" => "Prueba2"
								   );	*/
							 
							$this->db->insert('Log_Produccion',$logs_insert);
							/*----------------------FIN DEL LOG---------------------------*/ 
							return TRUE;
									 
						 	
						}
					else return FALSE;	
  				
  
}


//Edita los datos en la tabla entrega_sector
//Además almaceno el Log
function editar_entrega($post_array, $primary_key)
{
 if($post_array['NumOT-Lote-CodyDescProd'] != 0)
 			{
			 $update = array(
		   	 "IdOT" => $post_array['NumOT-Lote-CodyDescProd'],
		   	 "IdSector" => $post_array['IdSector'],
		   	 //"FechaEntregaSector" => date(), 	
		   	 "Vers_Protocolo" => $post_array['Vers_Protocolo'],
		   	 "Observacion_Entrega" => $post_array['Observacion_Entrega'],
		   	 "Usuario_Edita" => $this->session->userdata('Usuario')
		 
			 );
			 	
			}
		else {
			  $update = array(
			  //"IdOT" => $post_array['NumOT-Lote-CodyDescProd'],
			  "IdSector" => $post_array['IdSector'],
			  //"FechaEntregaSector" => date(), 	
			  "Vers_Protocolo" => $post_array['Vers_Protocolo'],
			  "Observacion_Entrega" => $post_array['Observacion_Entrega'],
			  "Usuario_Edita" => $this->session->userdata('Usuario')
			 
			  );
			  
			 } 	
 
 
 
  $query = $this->db->update('entregasector',$update, array('IdEntregaSector' => $primary_key));
  
  if($query != FALSE)
  			{/*----------------------ALMACENO EL LOG AL EDITAR---------------------------*/
  			 //Obtengo los datos para insertar el log
			 
			 $this->db->select('ot.NumOT');
			 $this->db->from('entregasector');
			 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
			 $this->db->where('entregasector.IdEntregaSector',$primary_key);
			 $query = $this->db->get();
			 $dato_NumOT = $query->row()->NumOT;
			 
			 $this->db->select('ot.Lote');
			 $this->db->from('entregasector');
			 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
			 $this->db->where('entregasector.IdEntregaSector',$primary_key);
			 $query1 = $this->db->get();
			 $dato_Lote = $query1->row()->Lote;
			 
			 $this->db->select('ot.FechaVto');
			 $this->db->from('entregasector');
			 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
			 $this->db->where('entregasector.IdEntregaSector',$primary_key);
			 $query2 = $this->db->get();
			 $dato_FechaVto = $query2->row()->FechaVto;
			 
			 $this->db->select('productosws.CodProdWS');
			 $this->db->from('entregasector');
			 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
			 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
			 $this->db->where('entregasector.IdEntregaSector',$primary_key);
			 $query3 = $this->db->get();
			 $dato_CodProdWS = $query3->row()->CodProdWS;
			 
			 $this->db->select('productosws.DescProdWS');
			 $this->db->from('entregasector');
			 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
			 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
			 $this->db->where('entregasector.IdEntregaSector',$primary_key);
			 $query4 = $this->db->get();
			 $dato_DescProdWS = $query4->row()->DescProdWS;
			 
			 
			 $this->db->select('DescSector');
			 $this->db->from('entregasector');
			 $this->db->join('sector','entregasector.IdSector =  sector.IdSector');
			 $this->db->where('IdEntregaSector',$primary_key);
			 $query5 = $this->db->get();
			 $dato_DescSector = $query5->row()->DescSector;
			 
			 $this->db->select('FechaEntregaSector');
			 $this->db->from('entregasector');
			 $this->db->where('IdEntregaSector',$primary_key);
			 $query6 = $this->db->get();
			 $dato_FechaEntregaSector = $query6->row()->FechaEntregaSector;
			 
			 $this->db->select('Usuario_Carga');
			 $this->db->from('entregasector');
			 $this->db->where('IdEntregaSector',$primary_key);
			 $query7 = $this->db->get();
			 $dato_Usuario_Carga = $query7->row()->Usuario_Carga;
			 
			 /*$this->db->select('Usuario_Edita');
			 $this->db->from('entregasector');
			 $this->db->where('IdEntregaSector',$primary_key);
			 $query8 = $this->db->get();
			 $dato_Usuario_Edita = $query8->row()->Usuario_Edita;*/
			 
			 
			 $logs_insert = array(
					   	"UsuarioSO" => $this->session->userdata('Usuario'),
					   	"UsuarioSistema" => $this->session->userdata('Usuario'),
					   	"PC" => $this->session->userdata('ip_pc'), 	
					   	"Nivel" => $this->session->userdata('Nivel'),
					   	"Accion" => "Edito un registo",
					   	"Tabla" => "Tabla: entregasector",
					   	"Valores" => "Id. EntregaSector: ".$primary_key.", Nro OT: ".$dato_NumOT.", Lote: ".$dato_Lote.", FechaVto: ".$dato_FechaVto.", Cod. Prod: ".$dato_CodProdWS.", Desc. Prod: ".$dato_DescProdWS.", Sector: ".$dato_DescSector.", Fecha de Entrega a Sector: ".$dato_FechaEntregaSector.", Vers_Protocolo: ".$post_array['Vers_Protocolo'].", Observacion de Entrega: ".$post_array['Observacion_Entrega'].", Usuario Carga: ".$dato_Usuario_Carga.", Usuario Edita: ".$this->session->userdata('Usuario')
			 
				);
				 
				$this->db->insert('Log_Produccion',$logs_insert);
				/*----------------------FIN DEL LOG---------------------------*/ 
				return TRUE;
						 	
						}
			  		else return FALSE;
  
  /*if($query!=FALSE)
                  return TRUE;
             else return FALSE;	 */

}

//No se utiliza porque tambien inserto el log cuando inserto el registro
//Inserta el log de la tabla entrega_sector
/*function insertar_log_add($post_array, $primary_key)
{
 
 $this->db->select('NumOT');
 $this->db->from('ot');
 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
 $query = $this->db->get();
 $dato_NumOT = $query->row()->NumOT;
 
 $this->db->select('Lote');
 $this->db->from('ot');
 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
 $query1 = $this->db->get();
 $dato_Lote = $query1->row()->Lote;
 
 $this->db->select('FechaVto');
 $this->db->from('ot');
 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
 $query2 = $this->db->get();
 $dato_FechaVto = $query2->row()->FechaVto;
 
 $this->db->select('productosws.CodProdWS');
 $this->db->from('ot');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
 $query3 = $this->db->get();
 $dato_CodProdWS = $query3->row()->CodProdWS;
 
 $this->db->select('productosws.DescProdWS');
 $this->db->from('ot');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 $this->db->where('IdOT',$post_array['NumOT-Lote-CodyDescProd']);
 $query4 = $this->db->get();
 $dato_DescProdWS = $query4->row()->DescProdWS;
 
 
 $this->db->select('sector.DescSector');
 $this->db->from('entregasector');
 $this->db->join('sector','entregasector.IdSector =  sector.IdSector');
 $this->db->where('entregasector.IdEntregaSector',$primary_key);
 $query5 = $this->db->get();
 $dato_DescSector = $query5->row()->DescSector;
 
 $this->db->select('FechaEntregaSector');
 $this->db->from('entregasector');
 $this->db->where('IdEntregaSector',$primary_key);
 $query6 = $this->db->get();
 $dato_FechaEntregaSector = $query6->row()->FechaEntregaSector;
 
 $this->db->select('Usuario_Carga');
 $this->db->from('entregasector');
 $this->db->where('IdEntregaSector',$primary_key);
 $query7 = $this->db->get();
 $dato_Usuario_Carga = $query7->row()->Usuario_Carga;
 
 
 $logs_insert = array(
		   	"UsuarioSO" => $this->session->userdata('Usuario'),
		   	"UsuarioSistema" => $this->session->userdata('Usuario'),
		   	"PC" => $this->session->userdata('ip_pc'), 	
		   	"Nivel" => $this->session->userdata('Nivel'),
		   	"Accion" => "Agrego un registo",
		   	"Tabla" => "Tabla: entregasector",
		   	//"Valores" => "Hola",
		   	"Valores" => "Id. EntregaSector: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Lote: ".$dato_Lote.", FechaVto: ".$dato_FechaVto.", Cod. Prod: ".$dato_CodProdWS.", Desc. Prod: ".$dato_DescProdWS.", Sector: ".$dato_DescSector.", Fecha de Entrega a Sector: ".$dato_FechaEntregaSector.", Vers_Protocolo: ".$post_array['Vers_Protocolo'].", Observacion de Entrega: ".$post_array['Observacion_Entrega'].", Usuario Carga: ".$dato_Usuario_Carga
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;	 

}*/



//Inserta el log de la tabla entrega_sector al editar
function log_editar_entrega($post_array, $primary_key)
{
 
 $this->db->select('ot.NumOT');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->where('entregasector.IdEntregaSector',$primary_key);
 $query = $this->db->get();
 $dato_NumOT = $query->row()->NumOT;
 
 $this->db->select('ot.Lote');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->where('entregasector.IdEntregaSector',$primary_key);
 $query1 = $this->db->get();
 $dato_Lote = $query1->row()->Lote;
 
 $this->db->select('ot.FechaVto');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->where('entregasector.IdEntregaSector',$primary_key);
 $query2 = $this->db->get();
 $dato_FechaVto = $query2->row()->FechaVto;
 
 $this->db->select('productosws.CodProdWS');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 $this->db->where('entregasector.IdEntregaSector',$primary_key);
 $query3 = $this->db->get();
 $dato_CodProdWS = $query3->row()->CodProdWS;
 
 $this->db->select('productosws.DescProdWS');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 $this->db->where('entregasector.IdEntregaSector',$primary_key);
 $query4 = $this->db->get();
 $dato_DescProdWS = $query4->row()->DescProdWS;
 
 
 $this->db->select('DescSector');
 $this->db->from('entregasector');
 $this->db->join('sector','entregasector.IdSector =  sector.IdSector');
 $this->db->where('IdEntregaSector',$primary_key);
 $query5 = $this->db->get();
 $dato_DescSector = $query5->row()->DescSector;
 
 $this->db->select('FechaEntregaSector');
 $this->db->from('entregasector');
 $this->db->where('IdEntregaSector',$primary_key);
 $query6 = $this->db->get();
 $dato_FechaEntregaSector = $query6->row()->FechaEntregaSector;
 
 $this->db->select('Usuario_Carga');
 $this->db->from('entregasector');
 $this->db->where('IdEntregaSector',$primary_key);
 $query7 = $this->db->get();
 $dato_Usuario_Carga = $query7->row()->Usuario_Carga;
 
 $this->db->select('Usuario_Edita');
 $this->db->from('entregasector');
 $this->db->where('IdEntregaSector',$primary_key);
 $query8 = $this->db->get();
 $dato_Usuario_Edita = $query8->row()->Usuario_Edita;
 
 
 $logs_insert = array(
		   	"UsuarioSO" => $this->session->userdata('Usuario'),
		   	"UsuarioSistema" => $this->session->userdata('Usuario'),
		   	"PC" => $this->session->userdata('ip_pc'), 	
		   	"Nivel" => $this->session->userdata('Nivel'),
		   	"Accion" => "Edito un registo",
		   	"Tabla" => "Tabla: entregasector",
		   	"Valores" => "Id. EntregaSector: ".$primary_key.", Nro OT: ".$dato_NumOT.", Lote: ".$dato_Lote.", FechaVto: ".$dato_FechaVto.", Cod. Prod: ".$dato_CodProdWS.", Desc. Prod: ".$dato_DescProdWS.", Sector: ".$dato_DescSector.", Fecha de Entrega a Sector: ".$dato_FechaEntregaSector.", Vers_Protocolo: ".$post_array['Vers_Protocolo'].", Observacion de Entrega: ".$post_array['Observacion_Entrega'].", Usuario Carga: ".$dato_Usuario_Carga.", Usuario Edita: ".$dato_Usuario_Edita
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;	 

}


//Obtengo los datos de las ot para armar el campo del edit
function consulta_Datos_OT($value ,$primary_key)
{
 //Selecciono el id de la ot
 $this->db->select('IdOT');
 $this->db->from('entregasector');
 $this->db->where('entregasector.IdEntregaSector',$primary_key);
 $query1 = $this->db->get();
 $dato_IdOT = $query1->row()->IdOT;
 
 //Selecciono los datos de la ot y el id del producto
 $this->db->select('NumOT, Lote, IdProdWS');
 $this->db->from('ot');
 $this->db->where('ot.IdOT',$dato_IdOT);
 $query2 = $this->db->get();
 $dato_OT = $query2->row()->NumOT." - ".$query2->row()->Lote;
 $dato_IdProd = $query2->row()->IdProdWS;
 
 //Seleccionamos los datos del producto
 $this->db->select('CodProdWS, DescProdWS');
 $this->db->from('productosws');
 $this->db->where('productosws.IdProductoWS',$dato_IdProd);
 $query3 = $this->db->get();
 $dato_Prod = $query3->row()->CodProdWS." - ".$query3->row()->DescProdWS;
 
 //Concatenamos todos los valores obtenidos
 $Valores = $dato_OT." - ".$dato_Prod;
 
 return $Valores;
 
 /*$this->db->select('CodProdWS, DescProdWS');
 $this->db->from('entregasector');
 $this->db->join('ot','entregasector.IdOT = ot.IdOT');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 $this->db->where('ot.IdOT',$row->IdOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->CodProdWS." - ".$query->row()->DescProdWS;
             else return FALSE;*/	
}



function consulta($post_array)
{
 var_dump($post_array);
 die;
 	
}


/*------------------------FIN DE LAS FUNCIONES----------------------------*/	
	



}
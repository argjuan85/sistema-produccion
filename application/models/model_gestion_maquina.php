<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_maquina extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion para consultar si una Descripción de Maquina ya estan cargadas
function consulta_campos_maquina_add($post_array){
	  	  
	  $DescripcionMaq = trim($post_array['DescMaquina']);
	  $this->db->where('DescMaquina',$DescripcionMaq);
	  $this->db->from('maquina');
	  $query = $this->db->get();
	  
	  /*Si la descripcion de la maquina se encuentran cargada retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}
	

/*Funcion para consultar si una Descripción de Maquina ya estan cargadas*/
function consulta_campos_maquina_edit($post_array){
	  
	  $IdMaquina = $post_array['IdMaquina'];	  
	  $DescripcionMaq = trim($post_array['DescMaquina']);
	  $this->db->where('IdMaquina !=',$IdMaquina);
	  $this->db->where('DescMaquina',$DescripcionMaq);
	  $this->db->from('maquina');
	  $query = $this->db->get();
	  
	  /*Si la descripcion de la maquina se encuentran cargada retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}


/*Funcion que me devuelve los valores de las relaciones para completar el log*/
function graba_log_maq_add($post_array, $primary_key)
{
	$this->db->select('DescEtapa');
	$this->db->where('IdEtapa',$post_array['IdEtapa']);
	$query1 = $this->db->get('etapa');
	$dato_desc_etapa = $query1->row()->DescEtapa;
 		
	$this->db->select('DescPlanta');
	$this->db->where('IdPlanta',$post_array['IdPlanta']);
	$query2 = $this->db->get('planta');
	$dato_desc_planta = $query2->row()->DescPlanta;
	
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: maquina",
   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$post_array['DescMaquina'].", Cod. Maq: ".$post_array['CodMaq'].",  Etapa: ".$dato_desc_etapa.",  Planta: ".$dato_desc_planta.",  Est.Maq: Habilitada"
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_maq_edit($post_array, $primary_key)
{
	$this->db->select('DescEtapa');
	$this->db->where('IdEtapa',$post_array['IdEtapa']);
	$query1 = $this->db->get('etapa');
	$dato_desc_etapa = $query1->row()->DescEtapa;
 		
	$this->db->select('DescPlanta');
	$this->db->where('IdPlanta',$post_array['IdPlanta']);
	$query2 = $this->db->get('planta');
	$dato_desc_planta = $query2->row()->DescPlanta;
	
	$this->db->select('EstadoMaquina');
	$this->db->where('IdMaquina',$primary_key);
	$query3 = $this->db->get('maquina');
	$dato_estado_maq = $query3->row()->EstadoMaquina;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Se edito un registo",
   	"Tabla" => "Tabla: maquina",
   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$post_array['DescMaquina'].", Cod. maq: ".$post_array['CodMaq'].",  Etapa: ".$dato_desc_etapa.",  Planta: ".$dato_desc_planta.",  Est.Maq: ".$dato_estado_maq
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}


/*Funcion que elimina una maquina, cambia el estado de la maquina a Habilitada*/
function cambia_estado_deshab($primary_key)
{
 $this->db->where('IdMaquina', $primary_key);
 $this->db->update('maquina',array('EstadoMaquina' => 'Deshabilitada'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('DescMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_DescMaquina = $query1->row()->DescMaquina;
 
 $this->db->select('EstadoMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_EstadoMaquina = $query1->row()->EstadoMaquina;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Deshabilito una Maquina",
			   	"Tabla" => "Tabla: maquina",
			   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$dato_DescMaquina.", Estado: ".$dato_EstadoMaquina
			 
				);
 
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}


/*Funcion que elimina una maquina, cambia el estado de la maquina a Habilitada*/
function cambia_estado_hab($primary_key)
{
 $this->db->where('IdMaquina', $primary_key);
 $this->db->update('maquina',array('EstadoMaquina' => 'Habilitada'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('DescMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_DescMaquina = $query1->row()->DescMaquina;
 
 $this->db->select('EstadoMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_EstadoMaquina = $query1->row()->EstadoMaquina;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Habilito una Maquina",
			   	"Tabla" => "Tabla: maquina",
			   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$dato_DescMaquina.", Estado: ".$dato_EstadoMaquina
			 
				);
 
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}


/*Funcion que obtiene el Estado de una Maquina*/
function obtiene_estado_maq($primary_key)
{
 $this->db->select('EstadoMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query5 = $this->db->get('maquina');
 $dato_estado = $query5->row()->EstadoMaquina;
 return $dato_estado;	
}


/*------------------------FIN DE LAS FUNCIONES----------------------------*/	
	



}
<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_conciliacion extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion que utilizo cuando Inserto un registro para verificar si los valores ya estan cargados 
function consulta_valores_add($NumOT, $CodProd, $Lote)
	{
	  $this->db->select('NumOT, CodProdWS, Lote');	  
	  $this->db->from('conciliacion');
	  $this->db->where('NumOT',$NumOT);
	  $this->db->where('CodProdWS',$CodProd);
	  $this->db->where('Lote',$Lote);
	  $query = $this->db->get();
	  
	  
	  /*Si la consulta devuelve valores, es decir que ya estan cargados y retorna TRUE, sino retorna FALSE*/
       if($query->num_rows()>0)
                  return TRUE;
             else return FALSE;         
	}



//Funcion que utilizo cuando Edito un Registro para verificar si los valores ya estan cargados 
function consulta_valores_edit($NumOT, $CodProd, $Lote, $primary_key)
	{
	  $this->db->select('NumOT, CodProdWS, Lote');	  
	  $this->db->from('conciliacion');
	  $this->db->where('NumOT',$NumOT);
	  $this->db->where('CodProdWS',$CodProd);
	  $this->db->where('Lote',$Lote);
	  //De esta forma consulto para que en el edit me permita insertar sobre el mismo registro con el que estoy trabajando, para los casos en que los valors que estoy comparando no los cambio
	  $this->db->where('IdConciliacion !=',$primary_key);
	  $query = $this->db->get();
	  
	  
	  /*Si la consulta devuelve valores, es decir que ya estan cargados y retorna TRUE, sino retorna FALSE*/
       if($query->num_rows()>0)
                  return TRUE;
             else return FALSE;         
	}


//Funcion para obtener los datos relacionados a una ot (El valor que recibo es el id de ot)
//Con estos valores voy a completar los campos de solo lectura la conciliacion
function obtiene_datos_ot_id($IdOT)
{
  $this->db->select('NumOT, Lote, FechaVto, CodProdWS, DescProdWS');	  
  $this->db->from('ot');
  $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
  $this->db->where('ot.IdOT',$IdOT);
  $query = $this->db->get();
  
  //var_dump($this->db->last_query());
  //die;
  if($query->num_rows()>0)
              return $query->row();
         else return FALSE;         
}


//Funcion para obtener los datos relacionados a una ot (El valor que recibo es el Num de ot)
//Con estos valores voy a completar los campos del pdf de conciliacion
function obtiene_datos_ot_NumOt($NumOT)
{
  $this->db->select('NumOT, Lote, FechaVto, CodProdWS, DescProdWS');	  
  $this->db->from('ot');
  $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
  $this->db->where('ot.NumOT',$NumOT);
  $query = $this->db->get();
  
  //var_dump($this->db->last_query());
  //die;
  if($query->num_rows()>0)
              return $query->row();
         else return FALSE;         
}




//Funcion para obtener los datos relacionados a una conciliacion
function obtiene_datos_conciliacion($IdOT)
{	  
  $this->db->select('conciliacion.IdConciliacion, ot.NumOT, productosws.CodProdWS, productosws.DescProdWS, conciliacion.Lote, conciliacion.Cant_Entregado, conciliacion.Cant_Adicional, conciliacion.Cant_Bueno, conciliacion.Cant_Roto, conciliacion.Cant_Destruido, conciliacion.Cant_Faltante, conciliacion.Cant_Devuelto, conciliacion.Cant_Error');
  $this->db->from('conciliacion');
  //$this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
  $this->db->join('ot','conciliacion.NumOT = ot.IdOT');
  $this->db->join('productosws','conciliacion.CodProdWS = productosws.IdProductoWS');
  $this->db->where('conciliacion.NumOT',$IdOT);
  $this->db->order_by("conciliacion.IdConciliacion", "asc");
  $query = $this->db->get();
  
   if($query->num_rows()>0)
              return $query->result();
         else return FALSE;         
}



/*Funcion que almacena el log al insertar conciliacion*/
function graba_log_conc_add($post_array, $primary_key)
{
	$this->db->select('ot.NumOT');
	$this->db->from('conciliacion');
	$this->db->join('ot','conciliacion.NumOT = ot.IdOT');
	$this->db->where('conciliacion.IdConciliacion',$primary_key);
	$query1 = $this->db->get();
	$dato_NumOT = $query1->row()->NumOT;
	
	$this->db->select('productosws.CodProdWS');
	$this->db->from('conciliacion');
	$this->db->join('productosws','conciliacion.CodProdWS = productosws.IdProductoWS');
	$this->db->where('conciliacion.IdConciliacion',$primary_key);
	$query2 = $this->db->get();
	$dato_CodProdWS = $query2->row()->CodProdWS;
	
	$this->db->select('productosws.DescProdWS');
	$this->db->from('conciliacion');
	$this->db->join('productosws','conciliacion.CodProdWS = productosws.IdProductoWS');
	$this->db->where('conciliacion.IdConciliacion',$primary_key);
	$query3 = $this->db->get();
	$dato_DescProdWS = $query3->row()->DescProdWS;
	
	//Obtener el valor del campo error ya que es calculado
	$this->db->select('Cant_Error');
	$this->db->from('conciliacion');
	$this->db->where('IdConciliacion',$primary_key);
	$query4 = $this->db->get();
	$dato_Cant_Error = $query4->row()->Cant_Error;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: conciliacion",
   	"Valores" => "Id. Conc: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Cod.Prod: ".$dato_CodProdWS.", Desc.Prod: ".$dato_DescProdWS.", Lote: ".$post_array['Lote'].",  Cant.Entregado: ".$post_array['Cant_Entregado'].",  Cant.Adicional: ".$post_array['Cant_Adicional'].",  Cant.Bueno: ".$post_array['Cant_Bueno'].",  Cant.Roto: ".$post_array['Cant_Roto'].",  Cant.Destruido: ".$post_array['Cant_Destruido'].",  Cant.Faltante: ".$post_array['Cant_Faltante'].",  Cant.Devuelto: ".$post_array['Cant_Devuelto'].",  Cant.Error: ".$dato_Cant_Error
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}




/*Funcion que almacena el log al insertar conciliacion*/
function graba_log_conc_edit($post_array, $primary_key)
{
	$this->db->select('ot.NumOT');
	$this->db->from('conciliacion');
	$this->db->join('ot','conciliacion.NumOT = ot.IdOT');
	$this->db->where('conciliacion.IdConciliacion',$primary_key);
	$query1 = $this->db->get();
	$dato_NumOT = $query1->row()->NumOT;
	
	$this->db->select('productosws.CodProdWS');
	$this->db->from('conciliacion');
	$this->db->join('productosws','conciliacion.CodProdWS = productosws.IdProductoWS');
	$this->db->where('conciliacion.IdConciliacion',$primary_key);
	$query2 = $this->db->get();
	$dato_CodProdWS = $query2->row()->CodProdWS;
	
	$this->db->select('productosws.DescProdWS');
	$this->db->from('conciliacion');
	$this->db->join('productosws','conciliacion.CodProdWS = productosws.IdProductoWS');
	$this->db->where('conciliacion.IdConciliacion',$primary_key);
	$query3 = $this->db->get();
	$dato_DescProdWS = $query3->row()->DescProdWS;

	//Obtener el valor del campo error ya que es calculado
	$this->db->select('Cant_Error');
	$this->db->from('conciliacion');
	$this->db->where('IdConciliacion',$primary_key);
	$query4 = $this->db->get();
	$dato_Cant_Error = $query4->row()->Cant_Error;
	
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Edito un registo",
   	"Tabla" => "Tabla: conciliacion",
   	"Valores" => "Id. Conc: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Cod.Prod: ".$dato_CodProdWS.", Desc.Prod: ".$dato_DescProdWS.", Lote: ".$post_array['Lote'].",  Cant.Entregado: ".$post_array['Cant_Entregado'].",  Cant.Adicional: ".$post_array['Cant_Adicional'].",  Cant.Bueno: ".$post_array['Cant_Bueno'].",  Cant.Roto: ".$post_array['Cant_Roto'].",  Cant.Destruido: ".$post_array['Cant_Destruido'].",  Cant.Faltante: ".$post_array['Cant_Faltante'].",  Cant.Devuelto: ".$post_array['Cant_Devuelto'].",  Cant.Error: ".$dato_Cant_Error
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



/*Funcion que almacena el log al eliminar conciliacion*/
function graba_log_conc_delete($primary_key)
{	
	$this->db->select('IdConciliacion, NumOT, CodProdWS, Lote, Cant_Entregado, Cant_Adicional, Cant_Bueno, Cant_Roto, Cant_Destruido, Cant_Faltante, Cant_Devuelto, Cant_Error');
	$this->db->from('conciliacion');
	$this->db->where('IdConciliacion',$primary_key);
	$query = $this->db->get();

	$this->db->select('ot.NumOT');
	$this->db->from('conciliacion');
	$this->db->join('ot','conciliacion.NumOT = ot.IdOT');
	$this->db->where('conciliacion.IdConciliacion',$query->row()->NumOT);
	$query1 = $this->db->get();
	$dato_NumOT = $query1->row()->NumOT;	
	
	$this->db->select('productosws.CodProdWS');
	$this->db->from('conciliacion');
	$this->db->join('productosws','conciliacion.CodProdWS = productosws.IdProductoWS');
	$this->db->where('conciliacion.IdConciliacion',$query->row()->NumOT);
	$query2 = $this->db->get();
	$dato_CodProdWS = $query2->row()->CodProdWS;
	
	$this->db->select('productosws.DescProdWS');
	$this->db->from('conciliacion');
	$this->db->join('productosws','conciliacion.CodProdWS = productosws.IdProductoWS');
	$this->db->where('conciliacion.IdConciliacion',$query->row()->NumOT);
	$query3 = $this->db->get();
	$dato_DescProdWS = $query3->row()->DescProdWS;

	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Elimino un registo",
   	"Tabla" => "Tabla: conciliacion",
   	"Valores" => "Id. Conc: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Cod.Prod: ".$dato_CodProdWS.", Desc.Prod: ".$dato_DescProdWS.", Lote: ".$query->row()->Lote.",  Cant.Entregado: ".$query->row()->Cant_Entregado.",  Cant.Adicional: ".$query->row()->Cant_Adicional.",  Cant.Bueno: ".$query->row()->Cant_Bueno.",  Cant.Roto: ".$query->row()->Cant_Roto.",  Cant.Destruido: ".$query->row()->Cant_Destruido.",  Cant.Faltante: ".$query->row()->Cant_Faltante.",  Cant.Devuelto: ".$query->row()->Cant_Devuelto.",  Cant.Error: ".$query->row()->Cant_Error
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}





	
/*--------------------------FIN DE LAS FUNCIONES-------------------------------------------*/	

}
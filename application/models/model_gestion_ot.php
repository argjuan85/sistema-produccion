<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_ot extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion para obtener el código del producto
/*function obtiene_idprod($post_array, $primary_key)
{
 $IdProd = $post_array['IdProdElab'];
 $this->db->select('IdProdElab');
 $this->db->where('IdProdElab',$IdProd);
 //$this->db->from('prodelaboracion');
 $query = $this->db->get('prodelaboracion');
 $dato = $query->row()->IdProdElab; 
 
 if($query->num_rows()>0)
                  return FALSE;
             else return $dato;
 
}*/

/*function obtiene_idprod($post_array, $primary_key)
{
 $IdProd = $post_array['IdProdElab'];
 //$where = '(IdProdElab=$IdProd or EstadoProdElab = "AC")';
 
 $this->db->select('CodProdElab');
 $this->db->where('IdProdElab',$IdProd);
 $this->db->or_where('EstadoProdElab','AC');
 //$this->db->where($where);
 $query = $this->db->get('prodelaboracion');
 
	$records = array();
	foreach ($query->result() as $row)
		{
        $records[] = $row;
		}
	/*while ($row = mysql_fetch_assoc($query))
	{
	  $records[] = $row['IdProdElab'];
	}*/
 
 /*return $records;
 
}*/



//Funcion que obtiene la fecha de visado y saber si fue modificada
function obtiene_fecha_liberado($primary_key)
{
 //$this->db->select('CodProdElab');
 $this->db->select('Fecha_Liberado');
 $this->db->from('ot');
 $this->db->where('IdOT',$primary_key);

 $query = $this->db->get();
 
 return $query->row()->Fecha_Liberado;
}



//Funcion que obtiene la fecha de visado y saber si fue modificada
function obtiene_fecha_visado($primary_key)
{
 //$this->db->select('CodProdElab');
 $this->db->select('Fecha_Visado');
 $this->db->from('ot');
 $this->db->where('IdOT',$primary_key);

 $query = $this->db->get();
 
 return $query->row()->Fecha_Visado;
}


/*Funcion que almacena el log al insertar ot*/
function graba_log_ot_add($post_array, $primary_key)
{
	$this->db->select('DescVaP');
	$this->db->from('vap');
	$this->db->where('IdVaP',$post_array['IdVaP']);
	$query = $this->db->get();
	$dato_descVaP = $query->row()->DescVaP;
	
	$this->db->select('DescProdWS');
	$this->db->from('productosws');
	$this->db->where('IdProductoWS',$post_array['IdProdWS']);
	$query2 = $this->db->get();
	$dato_DescProdWS = $query2->row()->DescProdWS;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: ot",
   	"Valores" => "Id. OT: ".$primary_key.", Nro OT: ".$post_array['NumOT'].", Lote: ".$post_array['Lote'].", Fecha ed Vto: ".$post_array['FechaVto'].", VaP: ".$dato_descVaP.", Prod.WS: ".$dato_DescProdWS
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



/*Funcion que almacena el log al editar ot*/
function graba_log_ot_edit($post_array, $primary_key)
{
	$this->db->select('DescVaP');
	$this->db->from('vap');
	$this->db->where('IdVaP',$post_array['IdVaP']);
	$query = $this->db->get();
	$dato_descVaP = $query->row()->DescVaP;
	
	$this->db->select('DescProdWS');
	$this->db->from('productosws');
	$this->db->where('IdProductoWS',$post_array['IdProdWS']);
	$query2 = $this->db->get();
	$dato_DescProdWS = $query2->row()->DescProdWS;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Edito un registo",
   	"Tabla" => "Tabla: ot",
   	"Valores" => "Id. OT: ".$primary_key.", Nro OT: ".$post_array['NumOT'].", Lote: ".$post_array['Lote'].", Fecha ed Vto: ".$post_array['FechaVto'].", VaP: ".$dato_descVaP.", Prod.WS: ".$dato_DescProdWS
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}




/*Funcion que almacena el log al editar las fechas de visado y/o liberado de ot*/
function graba_log_ot_editar_fechas($post_array, $primary_key)
{
	$this->db->select('NumOT');
	$this->db->from('ot');
	$this->db->where('IdOT',$primary_key);
	$query = $this->db->get();
	$dato_NumOT = $query->row()->NumOT;
	
	$this->db->select('Usuario_Visado');
	$this->db->from('ot');
	$this->db->where('IdOT',$primary_key);
	$query2 = $this->db->get();
	$dato_Usuario_Visado = $query2->row()->Usuario_Visado;
	
	$this->db->select('Usuario_Liberado');
	$this->db->from('ot');
	$this->db->where('IdOT',$primary_key);
	$query3 = $this->db->get();
	$dato_Usuario_Liberado = $query3->row()->Usuario_Liberado;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Edito un registo",
   	"Tabla" => "Tabla: ot",
   	"Valores" => "Id. OT: ".$primary_key.", Nro OT: ".$dato_NumOT.", Fecha de Visado: ".$post_array['Fecha_Visado'].", Fecha de Liberado: ".$post_array['Fecha_Liberado'].", Usuario Visado: ".$dato_Usuario_Visado.", Usuario Liberado: ".$dato_Usuario_Liberado.", Caja: ".$post_array['Caja']
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



//Funcion que obtiene el id del producto para luego armar el arreglo para el dropdown
function obtiene_idprod($primary_key)
{
 //$this->db->select('CodProdElab');
 $this->db->select('IdProdElab');
 $this->db->from('datosot');
 $this->db->where('IdDatosOT',$primary_key);

 $query = $this->db->get();
 
 return $query->row()->IdProdElab;
}


//Funcion que obtiene el arreglo resultante para armar el dropdown
function obtiene_datos_prod_edit($idprod)
{
 //$IdProd = $post_array['IdProdElab'];
 //$this->db->select('CodProdElab');
 $this->db->select('IdProdElab, CodProdElab, DescProdElab');
 $this->db->from('prodelaboracion');
 $this->db->where('IdProdElab',$idprod);
 $this->db->or_where('EstadoProdElab','AC');
 $this->db->order_by('IdProdElab', 'asc');
 //$query = $this->db->get('prodelaboracion');
 $query = $this->db->get();
 
 $array1_prod = array();
 $array2_prod = array();
 $array3_prod = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_prod, $row->IdProdElab);
  array_push($array2_prod, $row->CodProdElab." - ".$row->DescProdElab);
 }
 
 $array3_prod = array_combine($array1_prod, $array2_prod);
 return $array3_prod;
 
}



//Funcion para consultar si una ot ya se encuentra cargada cuando inserto
function consulta_ot_cargada_add($post_array)
{
  //Verifico si la ot se encuentra cargada
  $this->db->where('NumOT',$post_array['NumOT']);
  $this->db->from('ot');
  $query = $this->db->get();
  
  /*Si el Numero de OT se encuentran cargada retorna false, sino retorna true*/
   if($query->num_rows()>0)		
              return FALSE;
         else return TRUE;
             	
}


//Funcion para consultar si una ot ya se encuentra cargada cuando edito
function consulta_ot_cargada_edit($post_array, $primary_key)
{
  //Verifico si la ot se encuentra cargada
  $this->db->where('NumOT',$post_array['NumOT']);
  $this->db->where('IdOT !=',$primary_key);
  $this->db->from('ot');
  $query = $this->db->get();
  
  /*Si el Numero de OT se encuentran cargada retorna false, sino retorna true*/
   if($query->num_rows()>0)		
              return FALSE;
         else return TRUE;
             	
}



/*------------------------FIN DE LAS FUNCIONES----------------------------*/	
	

}
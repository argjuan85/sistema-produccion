<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_conciliacion_buscar extends CI_Model {

	function __construct() {
		parent::__construct();
	}








//Funcion para consultar si una Descripción de un punzon ya estan cargada
function consulta_campos_punzon_add($post_array)
	{
	  	  
	  $DescripcionPunzon = trim($post_array['DescPunzon']);
	  $this->db->where('DescPunzon',$DescripcionPunzon);
	  $this->db->from('punzon');
	  $query = $this->db->get();
	  
	  /*Si la descripcion del punzon se encuentran cargado retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}


//Funcion para consultar si una Descripción de un punzon ya estan cargada
function consulta_campos_punzon_edit($post_array)
	{
	  	  
	  $IdPunzon = $post_array['IdPunzon'];
	  $DescripcionPunzon = trim($post_array['DescPunzon']);
	  $this->db->where('IdPunzon !=',$IdPunzon);
	  $this->db->where('DescPunzon',$DescripcionPunzon);
	  $this->db->from('punzon');
	  $query = $this->db->get();
	  
	  /*Si la descripcion del punzon se encuentran cargado retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}


/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_punzon_add($post_array, $primary_key)
{	
	/*Carga el estado (Habilitado) de un punzon recien ingresado*/
	$this->db->where('IdPunzon', $primary_key);
 	$this->db->update('punzon',array('EstadoPunzon' => 'Habilitado'));
	
	/*Obtiene los valores para cargar el log*/
	$this->db->select('Fecha_Ingreso');
	$this->db->where('IdPunzon',$primary_key);
	$query3 = $this->db->get('punzon');
	$dato_fecha_ingreso = $query3->row()->Fecha_Ingreso;
	$dato_fecha_ingreso = date('d-m-Y');
	
	$this->db->select('EstadoPunzon');
	$this->db->where('IdPunzon',$primary_key);
	$query5 = $this->db->get('punzon');
	$dato_estado = $query5->row()->EstadoPunzon;
		
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: punzon",
   	"Valores" => "Id.Punzon: ".$primary_key.", Descripcion: ".$post_array['DescPunzon'].", Fecha_Ingreso: ".$dato_fecha_ingreso.", Est.Punzon: ".$dato_estado
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}


/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_punzon_edit($post_array, $primary_key)
{	
	
	/*Obtiene los valores para cargar el log*/
	$this->db->select('Fecha_Ingreso');
	$this->db->where('IdPunzon',$primary_key);
	$query3 = $this->db->get('punzon');
	$dato_fecha_ingreso = $query3->row()->Fecha_Ingreso;
	$dato_fecha_ingreso = date('d-m-Y');
	
	$this->db->select('EstadoPunzon');
	$this->db->where('IdPunzon',$primary_key);
	$query5 = $this->db->get('punzon');
	$dato_estado = $query5->row()->EstadoPunzon;
		
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego edito un registo",
   	"Tabla" => "Tabla: punzon",
   	"Valores" => "Id.Punzon: ".$primary_key.", Descripcion: ".$post_array['DescPunzon'].", Fecha_Ingreso: ".$dato_fecha_ingreso.", Est.Punzon: ".$dato_estado
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;

}


/*Funcion que elimina el prodcuto, cambia el estado del producto a AC*/
function cambia_estado_punzon($primary_key, $fecha)
{
 /*Elimina el punzon, es decir, le cambia el estado a Deshabilitado*/
 $this->db->where('IdPunzon', $primary_key);
 $update_array = array(
			   	"Fecha_Egreso" => $fecha,
			   	"EstadoPunzon" => "Deshabilitado"
			 
				);
 $this->db->update('punzon',$update_array);
 
 /*Hace la recuperacion de los valores para almacenar el log cuando elimino*/
 $this->db->select('DescPunzon');
 $this->db->where('IdPunzon',$primary_key);
 $query1 = $this->db->get('punzon');
 $dato_descripcion = $query1->row()->DescPunzon;
 
 $this->db->select('Fecha_Ingreso');
 $this->db->where('IdPunzon',$primary_key);
 $query2 = $this->db->get('punzon');
 $dato_fecha_ingreso = $query2->row()->Fecha_Ingreso;
 $dato_fecha_ingreso = date('d-m-Y');
 
 $this->db->select('Fecha_Egreso');
 $this->db->where('IdPunzon',$primary_key);
 $query3 = $this->db->get('punzon');
 $dato_fecha_egreso = $query3->row()->Fecha_Egreso;
 $dato_fecha_egreso = date('d-m-Y');
 
 $this->db->select('EstadoPunzon');
 $this->db->where('IdPunzon',$primary_key);
 $query4 = $this->db->get('punzon');
 $dato_estado = $query4->row()->EstadoPunzon;
 
 $logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Elimino un registo",
   	"Tabla" => "Tabla: punzon",
   	"Valores" => "Id.Punzon: ".$primary_key.", Descripcion: ".$dato_descripcion.", Fecha_Ingreso: ".$dato_fecha_ingreso.", Fecha_Egreso: ".$dato_fecha_egreso.", Est.Punzon: ".$dato_estado
 
	);
	
	$this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}

	
	
/*--------------------------FIN DE LAS FUNCIONES-------------------------------------------*/	
	

}
<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_datos_ot extends CI_Model {

	function __construct() {
		parent::__construct();
	}


//Funcion para obtener el código del producto
/*function obtiene_idprod($post_array, $primary_key)
{
 $IdProd = $post_array['IdProdElab'];
 $this->db->select('IdProdElab');
 $this->db->where('IdProdElab',$IdProd);
 //$this->db->from('prodelaboracion');
 $query = $this->db->get('prodelaboracion');
 $dato = $query->row()->IdProdElab; 
 
 if($query->num_rows()>0)
                  return FALSE;
             else return $dato;
 
}*/

/*function obtiene_idprod($post_array, $primary_key)
{
 $IdProd = $post_array['IdProdElab'];
 //$where = '(IdProdElab=$IdProd or EstadoProdElab = "AC")';
 
 $this->db->select('CodProdElab');
 $this->db->where('IdProdElab',$IdProd);
 $this->db->or_where('EstadoProdElab','AC');
 //$this->db->where($where);
 $query = $this->db->get('prodelaboracion');
 
	$records = array();
	foreach ($query->result() as $row)
		{
        $records[] = $row;
		}
	/*while ($row = mysql_fetch_assoc($query))
	{
	  $records[] = $row['IdProdElab'];
	}*/
 
 /*return $records;
 
}*/

//Funcion que obtiene el id del producto para luego armar el arreglo para el dropdown
function obtiene_idprod($primary_key)
{
 //$this->db->select('CodProdElab');
 $this->db->select('IdProdElab');
 $this->db->from('datosot');
 $this->db->where('IdDatosOT',$primary_key);

 $query = $this->db->get();
 
 return $query->row()->IdProdElab;
}


//Funcion que obtiene el id de la maquina para luego armar el multi select
function obtiene_idmaq($primary_key)
{
 //$this->db->select('CodProdElab');
 $this->db->select('IdMaquina');
 $this->db->from('datosot');
 $this->db->where('IdDatosOT',$primary_key);

 $query = $this->db->get();
 //var_dump($query->row()->IdMaquina);
 //die;
 
 return $query->row()->IdMaquina;
}




//Funcion que obtiene el arreglo resultante para armar el select para el add de productos
function obtiene_datos_prod_add()
{
 //$IdProd = $post_array['IdProdElab'];
 //$this->db->select('CodProdElab');
 $this->db->select('prodelaboracion.IdProdElab, prodelaboracion.CodProdElab, prodelaboracion.DescProdElab, claseprod.DescClaseProd');
 $this->db->from('prodelaboracion');
 //$this->db->where('IdProdElab',$idprod);
 //$this->db->join('prodelaboracion','datosot.IdProdElab = prodelaboracion.IdProdElab');
 $this->db->join('claseprod','prodelaboracion.IdClaseProd = claseprod.IdClaseProd');
 $this->db->where('prodelaboracion.EstadoProdElab','AC');
 $this->db->order_by('prodelaboracion.IdProdElab', 'asc');
 //$query = $this->db->get('prodelaboracion');
 $query = $this->db->get();
 
 $array1_prod = array();
 $array2_prod = array();
 $array3_prod = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_prod, $row->IdProdElab);
  //Armo el arreglo concatenando el Código del producto, la descripcion y la clase
  array_push($array2_prod, $row->CodProdElab." - ".$row->DescProdElab." - ".$row->DescClaseProd);
 }
 
 $array3_prod = array_combine($array1_prod, $array2_prod);
 return $array3_prod;
 
}


//Funcion que obtiene el arreglo resultante para armar el dropdown
function obtiene_datos_prod_edit($idprod)
{
 //$IdProd = $post_array['IdProdElab'];
 //$this->db->select('CodProdElab');
 $this->db->select('prodelaboracion.IdProdElab, prodelaboracion.CodProdElab, prodelaboracion.DescProdElab, claseprod.DescClaseProd');
 $this->db->from('prodelaboracion');
 //$this->db->join('prodelaboracion','datosot.IdProdElab = prodelaboracion.IdProdElab');
 $this->db->join('claseprod','prodelaboracion.IdClaseProd = claseprod.IdClaseProd');
 $this->db->where('prodelaboracion.IdProdElab',$idprod);
 $this->db->or_where('prodelaboracion.EstadoProdElab','AC');
 $this->db->order_by('prodelaboracion.IdProdElab', 'asc');
 //$query = $this->db->get('prodelaboracion');
 $query = $this->db->get();
 
 $array1_prod = array();
 $array2_prod = array();
 $array3_prod = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_prod, $row->IdProdElab);
  array_push($array2_prod, $row->CodProdElab." - ".$row->DescProdElab." - ".$row->DescClaseProd);
 }
 
 $array3_prod = array_combine($array1_prod, $array2_prod);
 return $array3_prod;
 
}


//Funcion que obtiene el arreglo resultante para armar el Multiselect field para al add
function obtiene_datos_maq_add()
{
 //$IdProd = $post_array['IdProdElab'];
 //$this->db->select('CodProdElab');
 $this->db->select('IdMaquina, DescMaquina, CodMaq');
 $this->db->from('maquina');
 //$this->db->where('IdProdElab',$idprod);
 $this->db->where('EstadoMaquina','Habilitada');
 $this->db->order_by('IdMaquina', 'asc');
 //$query = $this->db->get('prodelaboracion');
 $query = $this->db->get();
 
 $array1_maq = array();
 $array2_maq = array();
 $array3_maq = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_maq, $row->IdMaquina);
  array_push($array2_maq, $row->DescMaquina." -- ".$row->CodMaq);
 }
 
 $array3_maq = array_combine($array1_maq, $array2_maq);
 return $array3_maq;
 
}


//Funcion que obtiene el arreglo resultante para armar el Multiselect field para al edit
function obtiene_datos_maq_edit($array_maq)
{
 //$IdProd = $post_array['IdProdElab'];
 //$this->db->select('CodProdElab');
 $this->db->select('IdMaquina, DescMaquina, CodMaq');
 $this->db->from('maquina');
 
 $longitud = count($array_maq);
 if($longitud > 1)
 		 {
 		  $this->db->where('IdMaquina',$array_maq[0]);
	      for($i=0; $i<$longitud; $i++)
	       {
		    $this->db->or_where('IdMaquina',$array_maq[$i]);		   	
 	       }
		 }
	else if($longitud == 1)
				$this->db->where('IdMaquina',$array_maq[0]);	
		 
		   
 /*$cant = strlen($idmaq);
 if($cant > 1)
	 	{
	 	 $this->db->where('IdMaquina',$idmaq[0]);
		 for($i=1;$i<=$cant;$i++)
			 {
			 $this->db->or_where('IdMaquina',$idmaq[$i]);	
			 }
		}
	else $this->db->where('IdMaquina',$idmaq);	*/
  	  
 //$this->db->where('IdProdElab',$idprod);
 //$this->db->where('IdMaquina',$idmaq);
 $this->db->or_where('EstadoMaquina','Habilitada');
 
 $this->db->order_by('IdMaquina', 'asc');
 //$query = $this->db->get('prodelaboracion');
 $query = $this->db->get();
 
 $array1_maq = array();
 $array2_maq = array();
 $array3_maq = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_maq, $row->IdMaquina);
  array_push($array2_maq, $row->DescMaquina." -- ".$row->CodMaq);
 }
 
 $array3_maq = array_combine($array1_maq, $array2_maq);
 return $array3_maq;
 
}




//Funcion que obtiene el arreglo resultante para armar el Multiselect field para al edit
function obtiene_datos_maq_gral()
{
 //$IdProd = $post_array['IdProdElab'];
 //$this->db->select('CodProdElab');
 $this->db->select('IdMaquina, DescMaquina, CodMaq');
 $this->db->from('maquina');
 $this->db->order_by('IdMaquina', 'asc');
 //$query = $this->db->get('prodelaboracion');
 $query = $this->db->get();
 
 $array1_maq = array();
 $array2_maq = array();
 $array3_maq = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_maq, $row->IdMaquina);
  array_push($array2_maq, $row->DescMaquina." -- ".$row->CodMaq);
 }
 
 $array3_maq = array_combine($array1_maq, $array2_maq);
 return $array3_maq;
 
}


//Funcion que obtiene el id de la subetapa para luego armar el select
function obtiene_idsub($primary_key)
{
 //$this->db->select('CodProdElab');
 $this->db->select('IdSubetapa');
 $this->db->from('datosot');
 $this->db->where('IdDatosOT',$primary_key);

 $query = $this->db->get();
 
 return $query->row()->IdSubetapa;
}



//Funcion que obtiene el arreglo resultante para armar el Multiselect field para al edit
function obtiene_datos_sub_edit($idsub)
{
 //$IdProd = $post_array['IdProdElab'];
 //$this->db->select('CodProdElab');
 $this->db->select('IdSubetapa, DescSubEtapa');
 $this->db->from('subetapa');
 $this->db->where('IdSubetapa',$idsub);
 $this->db->or_where('EstadoSub','Habilitada');
 $this->db->order_by('IdSubetapa', 'asc');
 //$query = $this->db->get('prodelaboracion');
 $query = $this->db->get();
 
 $array1_sub = array();
 $array2_sub = array();
 $array3_sub = array();
 
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array1_sub, $row->IdSubetapa);
  array_push($array2_sub, $row->DescSubEtapa);
 }
 
 $array3_sub = array_combine($array1_sub, $array2_sub);
 return $array3_sub;
 
}




/*Funcion que almacena el log al insertar datos de ot*/
function graba_log_datos_add($post_array, $primary_key)
{
	$this->db->select('NumOT');
	$this->db->from('ot');
	$this->db->where('IdOT',$post_array['IdOT']);
	$query = $this->db->get();
	$dato_NumOT = $query->row()->NumOT;
	
	$this->db->select('DescSubEtapa');
	$this->db->from(' subetapa');
	$this->db->where('IdSubetapa',$post_array['IdSubEtapa']);
	$query1 = $this->db->get();
	$dato_DescSubEtapa = $query1->row()->DescSubEtapa;
	
	
	if($post_array['IdMaquina'] != '')
			{
			 /*Código para obtener los id de las maquinas registradas*/
			 /*Esto se utiliza para armar el multiselect*/
			 $this->db->select('IdMaquina');
			 $this->db->from('datosot');
			 $this->db->where('IdDatosOT',$primary_key);

			 $query5 = $this->db->get();
					 
			 $idmaq = $query5->row()->IdMaquina;
			 
			 //$idmaq	= $post_array['IdMaquina']; 
			 $array_maq = array();
			 $cadena = '';
			 $i = 0;
			 $j = 0;
			 while($i<strlen($idmaq))
			 {
			  if($idmaq[$i] != ',')
				  		{
				  		 $cadena .= $idmaq[$i]; 	
				  		}
			  	   else {
				   	     $array_maq[$j] = $cadena; 
				   	     $j++;
				   	     $cadena = '';
				        }
			  
			  $i++;
			  $array_maq[$j] = $cadena; 		
			 }
			 
			/*----------------------------------------------------*/
			
			/*------Codigo para obtener las descripciones de las maquinas-------*/
			$this->db->select('DescMaquina');
		 	$this->db->from('maquina');
		 
		 	$longitud = count($array_maq);
		 	if($longitud > 1)
		 		 {
		 		  $this->db->where('IdMaquina',$array_maq[0]);
			      for($i=0; $i<$longitud; $i++)
			       {
				    $this->db->or_where('IdMaquina',$array_maq[$i]);		   	
		 	       }
				 }
			else if($longitud == 1)
						$this->db->where('IdMaquina',$array_maq[0]);	
		 	  
		 	$this->db->order_by('IdMaquina', 'asc');
		 	//$query = $this->db->get('prodelaboracion');
		 	$query2 = $this->db->get();
		  	
		  	$maquinas = '';		//Variable que almacena las descripciones de las maquinas
		 	foreach($query2->result() as $row)
		 	 {
		  	  $maquinas .=	$row->DescMaquina.", ";
		 	 }
		 
		 	}
	 else $maquinas = '';
	
	if($post_array['IdPunzon'] != '')
			{	
			 $this->db->select('DescPunzon');
			 $this->db->from('punzon');
			 $this->db->where('IdPunzon',$post_array['IdPunzon']);
			 $query3 = $this->db->get();
			 $dato_DescPunzon = $query3->row()->DescPunzon;
			} 
		else $dato_DescPunzon = '';
	
	
	
	 $this->db->select('Fecha_Carga');
	 $this->db->from('datosot');
	 $this->db->where('IdDatosOT',$primary_key);
	 $query4 = $this->db->get();
	 $dato_Fecha_Carga = $query4->row()->Fecha_Carga;
	
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: datosot",
   	"Valores" => "Id. DatosOT: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Fecha de Carga: ".$dato_Fecha_Carga.", SubEtapa: ".$dato_DescSubEtapa.", Cant. Teorica: ".$post_array['Cant_Teorica'].", Cant. Real: ".$post_array['Cant_Real'].", Tpo de Preparacion: ".$post_array['Tiempo_Preparacion'].", Tpo de Ejecucion: ".$post_array['Tiempo_Ejecucion'].", Observacion DatosOT: ".$post_array['Observacion_DatosOT'].", Maquina: ".$maquinas.", Punzon: ".$dato_DescPunzon.", Parcial: ".$post_array['Parcial'].", HParcial: ".$post_array['HParcial'].", Reproceso: ".$post_array['Reproceso'].", Lote Anterior: ".$post_array['Lote_Anterior'].", Cant. Real: ".$post_array['Cant_Teorica'].", Prod.WS: ".$post_array['Cant_Teorica'].", Observacion Reproceso: ".$post_array['Observacion_Reproceso'].", Cantidad Rotulos: ".$post_array['Cantidad_Rotulos'].", Peso_Promedio: ".$post_array['Peso_Promedio'].", Peso_Promedio: ".$post_array['Peso_Promedio'].", Muestra Control de Calidad: ".$post_array['Muestra_ControlCalidad'].", Muestra Validacion y Estabilidad: ".$post_array['Muestra_ValidacionEstabilidad']
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}




/*Funcion que almacena el log al insertar datos de ot*/
function graba_log_datos_edit($post_array, $primary_key)
{
	$this->db->select('NumOT');
	$this->db->from('ot');
	$this->db->where('IdOT',$post_array['IdOT']);
	$query = $this->db->get();
	$dato_NumOT = $query->row()->NumOT;
	
	$this->db->select('DescSubEtapa');
	$this->db->from(' subetapa');
	$this->db->where('IdSubetapa',$post_array['IdSubEtapa']);
	$query1 = $this->db->get();
	$dato_DescSubEtapa = $query1->row()->DescSubEtapa;
	
	
	if($post_array['IdMaquina'] != '')
			{
			 /*Código para obtener los id de las maquinas registradas*/
			 /*Esto se utiliza para armar el multiselect*/
			 $this->db->select('IdMaquina');
			 $this->db->from('datosot');
			 $this->db->where('IdDatosOT',$primary_key);

			 $query5 = $this->db->get();
					 
			 $idmaq = $query5->row()->IdMaquina;
			 
			 //$idmaq	= $post_array['IdMaquina']; 
			 $array_maq = array();
			 $cadena = '';
			 $i = 0;
			 $j = 0;
			 while($i<strlen($idmaq))
			 {
			  if($idmaq[$i] != ',')
				  		{
				  		 $cadena .= $idmaq[$i]; 	
				  		}
			  	   else {
				   	     $array_maq[$j] = $cadena; 
				   	     $j++;
				   	     $cadena = '';
				        }
			  
			  $i++;
			  $array_maq[$j] = $cadena; 		
			 }
			 
			/*----------------------------------------------------*/
			
			/*------Codigo para obtener las descripciones de las maquinas-------*/
			$this->db->select('DescMaquina');
		 	$this->db->from('maquina');
		 
		 	$longitud = count($array_maq);
		 	if($longitud > 1)
		 		 {
		 		  $this->db->where('IdMaquina',$array_maq[0]);
			      for($i=0; $i<$longitud; $i++)
			       {
				    $this->db->or_where('IdMaquina',$array_maq[$i]);		   	
		 	       }
				 }
			else if($longitud == 1)
						$this->db->where('IdMaquina',$array_maq[0]);	
		 	  
		 	$this->db->order_by('IdMaquina', 'asc');
		 	//$query = $this->db->get('prodelaboracion');
		 	$query2 = $this->db->get();
		  	
		  	$maquinas = '';		//Variable que almacena las descripciones de las maquinas
		 	foreach($query2->result() as $row)
		 	 {
		  	  $maquinas .=	$row->DescMaquina.", ";
		 	 }
		 
		 	}
	 else $maquinas = '';
	
	if($post_array['IdPunzon'] != '')
			{	
			 $this->db->select('DescPunzon');
			 $this->db->from('punzon');
			 $this->db->where('IdPunzon',$post_array['IdPunzon']);
			 $query3 = $this->db->get();
			 $dato_DescPunzon = $query3->row()->DescPunzon;
			} 
		else $dato_DescPunzon = '';
	
	
	
	 $this->db->select('Fecha_Carga');
	 $this->db->from('datosot');
	 $this->db->where('IdDatosOT',$primary_key);
	 $query4 = $this->db->get();
	 $dato_Fecha_Carga = $query4->row()->Fecha_Carga;
	
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Edito un registo",
   	"Tabla" => "Tabla: datosot",
   	"Valores" => "Id. DatosOT: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Fecha de Carga: ".$dato_Fecha_Carga.", SubEtapa: ".$dato_DescSubEtapa.", Cant. Teorica: ".$post_array['Cant_Teorica'].", Cant. Real: ".$post_array['Cant_Real'].", Tpo de Preparacion: ".$post_array['Tiempo_Preparacion'].", Tpo de Ejecucion: ".$post_array['Tiempo_Ejecucion'].", Observacion DatosOT: ".$post_array['Observacion_DatosOT'].", Maquina: ".$maquinas.", Punzon: ".$dato_DescPunzon.", Parcial: ".$post_array['Parcial'].", HParcial: ".$post_array['HParcial'].", Reproceso: ".$post_array['Reproceso'].", Lote Anterior: ".$post_array['Lote_Anterior'].", Cant. Real: ".$post_array['Cant_Teorica'].", Prod.WS: ".$post_array['Cant_Teorica'].", Observacion Reproceso: ".$post_array['Observacion_Reproceso'].", Cantidad Rotulos: ".$post_array['Cantidad_Rotulos'].", Peso_Promedio: ".$post_array['Peso_Promedio'].", Peso_Promedio: ".$post_array['Peso_Promedio'].", Muestra Control de Calidad: ".$post_array['Muestra_ControlCalidad'].", Muestra Validacion y Estabilidad: ".$post_array['Muestra_ValidacionEstabilidad']
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}






/*Fin de las funciones del modelo*/








//Funcion para consultar si una Descripción de Maquina ya estan cargadas
function consulta_campos_maquina_add($post_array){
	  	  
	  $DescripcionMaq = trim($post_array['DescMaquina']);
	  $this->db->where('DescMaquina',$DescripcionMaq);
	  $this->db->from('maquina');
	  $query = $this->db->get();
	  $dato_estado_maq = $query5->row()->EstadoMaquina;
	  
	  /*Si la descripcion de la maquina se encuentran cargada retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}
	

/*Funcion para consultar si una Descripción de Maquina ya estan cargadas*/
function consulta_campos_maquina_edit($post_array){
	  
	  $IdMaquina = $post_array['IdMaquina'];	  
	  $DescripcionMaq = trim($post_array['DescMaquina']);
	  $this->db->where('IdMaquina !=',$IdMaquina);
	  $this->db->where('DescMaquina',$DescripcionMaq);
	  $this->db->from('maquina');
	  $query = $this->db->get();
	  
	  /*Si la descripcion de la maquina se encuentran cargada retorna false, sino retorna true*/
       if($query->num_rows()>0)
                  return FALSE;
             else return TRUE;         
	}


/*Funcion que me devuelve los valores de las relaciones para completar el log*/
function graba_log_maq_add($post_array, $primary_key)
{
	$this->db->select('DescEtapa');
	$this->db->where('IdEtapa',$post_array['IdEtapa']);
	$query1 = $this->db->get('etapa');
	$dato_desc_etapa = $query1->row()->DescEtapa;
 		
	$this->db->select('DescPlanta');
	$this->db->where('IdPlanta',$post_array['IdPlanta']);
	$query2 = $this->db->get('planta');
	$dato_desc_planta = $query2->row()->DescPlanta;
	
	$this->db->select('EstadoMaquina');
	$this->db->where('IdMaquina',$primary_key);
	$query5 = $this->db->get('maquina');
	$dato_estado_maq = $query5->row()->EstadoMaquina;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: maquina",
   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$post_array['DescMaquina'].",  Etapa: ".$dato_desc_etapa.",  Planta: ".$dato_desc_planta.",  Est.Maq: ".$dato_estado_maq
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_maq_edit($post_array, $primary_key)
{
	$this->db->select('DescEtapa');
	$this->db->where('IdEtapa',$post_array['IdEtapa']);
	$query1 = $this->db->get('etapa');
	$dato_desc_etapa = $query1->row()->DescEtapa;
 		
	$this->db->select('DescPlanta');
	$this->db->where('IdPlanta',$post_array['IdPlanta']);
	$query2 = $this->db->get('planta');
	$dato_desc_planta = $query2->row()->DescPlanta;
	
	$this->db->select('EstadoMaquina');
	$this->db->where('IdMaquina',$primary_key);
	$query5 = $this->db->get('maquina');
	$dato_estado_maq = $query5->row()->EstadoMaquina;
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Se edito un registo",
   	"Tabla" => "Tabla: maquina",
   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$post_array['DescMaquina'].",  Etapa: ".$dato_desc_etapa.",  Planta: ".$dato_desc_planta.",  Est.Maq: ".$dato_estado_maq
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}


/*Funcion que elimina una maquina, cambia el estado de la maquina a Habilitada*/
function cambia_estado_deshab($primary_key)
{
 $this->db->where('IdMaquina', $primary_key);
 $this->db->update('maquina',array('EstadoMaquina' => 'Deshabilitada'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('DescMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_DescMaquina = $query1->row()->DescMaquina;
 
 $this->db->select('EstadoMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_EstadoMaquina = $query1->row()->EstadoMaquina;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Elimino una Maquina",
			   	"Tabla" => "Tabla: maquina",
			   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$dato_DescMaquina.", Estado: ".$dato_EstadoMaquina
			 
				);
 
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}


/*Funcion que elimina una maquina, cambia el estado de la maquina a Habilitada*/
function cambia_estado_hab($primary_key)
{
 $this->db->where('IdMaquina', $primary_key);
 $this->db->update('maquina',array('EstadoMaquina' => 'Habilitada'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('DescMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_DescMaquina = $query1->row()->DescMaquina;
 
 $this->db->select('EstadoMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query1 = $this->db->get('maquina');
 $dato_EstadoMaquina = $query1->row()->EstadoMaquina;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Habilito una Maquina",
			   	"Tabla" => "Tabla: maquina",
			   	"Valores" => "Id.Maq: ".$primary_key.",  Descripcion: ".$dato_DescMaquina.", Estado: ".$dato_EstadoMaquina
			 
				);
 
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}


/*Funcion que obtiene el Estado de una Maquina*/
function obtiene_estado_maq($primary_key)
{
 $this->db->select('EstadoMaquina');
 $this->db->where('IdMaquina',$primary_key);
 $query5 = $this->db->get('maquina');
 $dato_estado = $query5->row()->EstadoMaquina;
 return $dato_estado;	
}

function obtiene_nro_ot($primary_key)
{
 $this->db->select('NumOT');
 $this->db->where('IdOT',$primary_key);
 $query = $this->db->get('ot');
 $dato_NumOT = $query->row()->NumOT;
 return $dato_NumOT;
 	
}

function borrar_ap($primary_key)
{
 $this->db->where('IdDatosOT', $primary_key);
 
 $this->db->update('datosot', array('AP' => null, 'Codigo' => null, 'Version' => null));
 
 //Primero obtenfo la id de la ot con la clave primaria enviada
	$this->db->select('IdOT');
	$this->db->from('datosot');
	$this->db->where('IdDatosOT',$primary_key);
	$query = $this->db->get();
	$dato_IdOT = $query->row()->IdOT;
	
	//Despues obtengo el numero de la ot con la id obtenida antes
	$this->db->select('NumOT');
	$this->db->from('ot');
	$this->db->where('IdOT',$dato_IdOT);
	$query2 = $this->db->get();
	$dato_NumOT = $query2->row()->NumOT;
	
	$logs_edit_ap_del = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Se borro el estado AP una OT",
   	"Tabla" => "Tabla: Datos OT",
   	"Valores" => "Id DatosOT: ".$primary_key.",  Nro OT: ".$dato_NumOT
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_edit_ap_del);
 
 
 return TRUE;
 	
}



/*Funcion que obtiene los valores para completar el log al cargar los datos de ap*/
function graba_log_ot_ap_add($post_array, $primary_key)
{	
	//Primero obtenfo la id de la ot con la clave primaria enviada
	$this->db->select('IdOT');
	$this->db->from('datosot');
	$this->db->where('IdDatosOT',$primary_key);
	$query = $this->db->get();
	$dato_IdOT = $query->row()->IdOT;
	
	//Despues obtengo el numero de la ot con la id obtenida antes
	$this->db->select('NumOT');
	$this->db->from('ot');
	$this->db->where('IdOT',$dato_IdOT);
	$query = $this->db->get();
	$dato_NumOT = $query->row()->NumOT;
	
	$logs_edit_ap_add = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Se puso en estado AP una OT",
   	"Tabla" => "Tabla: Datos OT",
   	"Valores" => "Id DatosOT: ".$primary_key.",  Nro OT: ".$dato_NumOT.", Estado: AP ,  C&oacute;digo: ".$post_array['Codigo'].",  Versi&oacute;n: ".$post_array['Version']
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_edit_ap_add);
	 
	return TRUE;
}


/*Funcion que obtiene los valores para completar el log al borrar los datos de ap*/
function graba_log_ot_ap_del($post_array, $primary_key)
{	
	//Primero obtenfo la id de la ot con la clave primaria enviada
	$this->db->select('IdOT');
	$this->db->from('datosot');
	$this->db->where('IdDatosOT',$primary_key);
	$query = $this->db->get();
	$dato_IdOT = $query->row()->IdOT;
	
	//Despues obtengo el numero de la ot con la id obtenida antes
	$this->db->select('NumOT');
	$this->db->from('ot');
	$this->db->where('IdOT',$dato_IdOT);
	$query2 = $this->db->get();
	$dato_NumOT = $query2->row()->NumOT;
	
	$logs_edit_ap_del = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Se borro el estado AP una OT",
   	"Tabla" => "Tabla: Datos OT",
   	"Valores" => "Id DatosOT: ".$primary_key.",  Nro OT: ".$dato_NumOT
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_edit_ap_del);
	 
	return TRUE;
}


//Funcion que obtiene el lote para mostrarlo en la grilla
function consulta_lote($row)
{
 //$IdProd = $post_array['IdProdElab'];
 $this->db->select('Lote');
 $this->db->from('datosot');
 //$this->db->where('IdProdElab', $IdProd);
 $this->db->join('ot','datosot.IdOT = ot.IdOT');
 $this->db->where('ot.IdOT',$row->IdOT);
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->Lote;
             else return FALSE;	
}


//Funcion que obtiene el código del producto ws para mostrarlo en la grilla
function consulta_codprodws($row)
{
 $this->db->select('productosws.CodProdWS');
 $this->db->from('datosot');
 $this->db->join('ot','datosot.IdOT = ot.IdOT');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 $this->db->where('ot.IdOT',$row->IdOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->CodProdWS;
             else return FALSE;	
 
}



//Funcion que obtiene la descripción del producto ws para mostrarlo en la grilla
function consulta_descprodws($row)
{
 $this->db->select('productosws.DescProdWS');
 $this->db->from('datosot');
 $this->db->join('ot','datosot.IdOT = ot.IdOT');
 $this->db->join('productosws','ot.IdProdWS = productosws.IdProductoWS');
 $this->db->where('ot.IdOT',$row->IdOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->DescProdWS;
             else return FALSE;	
 
}



//Funcion que obtiene el tiempo de preparación para mostrarlo en la grilla
function consulta_tpo_preparacion($row)
{
 $this->db->select('Tiempo_Preparacion');
 $this->db->from('datosot');
 $this->db->where('datosot.IdDatosOT',$row->IdDatosOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->Tiempo_Preparacion;
             else return FALSE;	
 
}



//Funcion que obtiene el tiempo de ejecución para mostrarlo en la grilla
function consulta_tpo_ejecucion($row)
{
 $this->db->select('Tiempo_Ejecucion');
 $this->db->from('datosot');
 $this->db->where('datosot.IdDatosOT',$row->IdDatosOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->Tiempo_Ejecucion;
             else return FALSE;	
 
}



//Funcion que obtiene el tiempo de ejecución para mostrarlo en la grilla
function consulta_muestra_cc($row)
{
 $this->db->select('Muestra_ControlCalidad');
 $this->db->from('datosot');
 $this->db->where('datosot.IdDatosOT',$row->IdDatosOT);
 $query = $this->db->get();
 if($query->num_rows()>0)
                  return $query->row()->Muestra_ControlCalidad;
             else return FALSE;	
 
}



/*------------------------FIN DE LAS FUNCIONES----------------------------*/	
	



}
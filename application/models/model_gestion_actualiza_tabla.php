<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Model_gestion_actualiza_tabla extends CI_Model {

	function __construct() {
		parent::__construct();
	}



function carga_valores()
{
 	
}




//Funcion para consultar si uno Código de Producto y/o una Descripción de Producto ya estan cargadas
function consulta_campos_producto_add($post_array){
	  
	  /*Se valida el campo Código de producto*/	  
	  $Codigoprod = trim($post_array['CodProdElab']);
	  $this->db->where('CodProdElab',$Codigoprod);
	  $this->db->from('prodelaboracion');
	  $query1 = $this->db->get();
	  
	  /*Se valida el campo Descripción del Producto*/
	  $DescripcionProd = trim($post_array['DescProdElab']);
	  $this->db->where('DescProdElab',$DescripcionProd);
	  $this->db->from('prodelaboracion');
	  $query2 = $this->db->get();
	  
	  /*Si el Código de Producto o la Descripción del Producto se encuentran cargados retorna false, sino retorna true*/
       if($query1->num_rows()>0)		
                  return FALSE;
             else if($query2->num_rows()>0)
                          return FALSE;
                     else return TRUE;
                     	
	}


//Funcion para validar los campos Código de Producto y Descripción
function consulta_campos_producto_edit($post_array)
{
	  
	  $IdProducto = $post_array['IdProdElab'];
	  /*Se valida el campo Código de producto*/	  
	  //Saca el Id para no preguntar por el mismo por si no se modifica Código y Descripcion
	  //Sino no deja realizar la oparacion porque lo toma como que ya esta cargado
	  $Codigoprod = trim($post_array['CodProdElab']);   
	 	  
	  $this->db->where('IdProdElab !=',$IdProducto);
	  $this->db->where('CodProdElab',$Codigoprod);
	  $this->db->from('prodelaboracion');
	  $query1 = $this->db->get();
	  
	  /*Se valida el campo Descripción del Producto*/
	  $DescripcionProd = trim($post_array['DescProdElab']);
	  $this->db->where('IdProdElab !=',$IdProducto);
	  $this->db->where('DescProdElab',$DescripcionProd);
	  $this->db->from('prodelaboracion');
	  $query2 = $this->db->get();
	  
	  /*Si el Código de Producto o la Descripción del Producto se encuentran cargados retorna false, sino retorna true*/
       if($query1->num_rows()>0)		
                  return FALSE;
             else if($query2->num_rows()>0)
                          return FALSE;
                     else return TRUE;
                     	
}
	
	
/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_prod_add($post_array, $primary_key)
{
	$this->db->select('DescClaseProd');
	$this->db->where('IdClaseProd',$post_array['IdClaseProd']);
	$query = $this->db->get('claseprod');
	$dato_clase = $query->row()->DescClaseProd;
	
	$this->db->select('DescVoMM');
	$this->db->where('IdVoMM',$post_array['IdVoMM']);
	$query1 = $this->db->get('vomm');
	$dato_vomm = $query1->row()->DescVoMM;
 	
	/*$this->db->select('DescMaquina');
	$this->db->from('maquina');
	$this->db->where('IdMaquina',$post_array['IdMaquinaDefecto']);
	$query2 = $this->db->get();
	$dato_maq_def = $query2->row()->DescMaquina;*/
	
	//Verifico que valor enviado, porque en el caso de no seleccionar opcion me envia cero y no encuentra relacion con el id de la tabla  
	if($post_array['IdPasoProd'] == 0)
			    $dato_paso = '';
		else {
				$this->db->select('DescPasoProd');
				$this->db->where('IdPasoProd',$post_array['IdPasoProd']);
				$query3 = $this->db->get('pasoprod');
				$dato_paso = $query3->row()->DescPasoProd;
			 }	
	
	//Verifico que valor enviado, porque en el caso de no seleccionar opcion me envia cero y no encuentra relacion con el id de la tabla
	if($post_array['IdTipoProd'] == 0)
			    $dato_tipo = '';
		else {
				$this->db->select('DescTipoProd');
				$this->db->where('IdTipo',$post_array['IdTipoProd']);
				$query4 = $this->db->get('tipoprod');
				$dato_tipo = $query4->row()->DescTipoProd;
			}

	
	$this->db->select('EstadoProdElab');
	$this->db->where('IdProdElab',$primary_key);
	$query5 = $this->db->get('prodelaboracion');
	$dato_estado = $query5->row()->EstadoProdElab;
	
	
	$logs_insert = array(
   	"UsuarioSO" => $this->session->userdata('Usuario'),
   	"UsuarioSistema" => $this->session->userdata('Usuario'),
   	"PC" => $this->session->userdata('ip_pc'), 	
   	"Nivel" => $this->session->userdata('Nivel'),
   	"Accion" => "Agrego un registo",
   	"Tabla" => "Tabla: prodelaboracion",
   	"Valores" => "Id.Prod: ".$primary_key.",  Cod.Prod: ".$post_array['CodProdElab'].",  Descripcion: ".$post_array['DescProdElab'].", Clase: ".$post_array['IdClaseProd'].",VoMM: ".$dato_vomm.",  PasoProd: ".$dato_paso.",  TipoProd: ".$dato_tipo.",  Est.Prod: ".$dato_estado
 
	);
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}	
	

/*Funcion que obtiene los valores de las relaciones para completar el log*/
function graba_log_prod_edit($post_array, $primary_key)
{
	$this->db->select('DescClaseProd');
	$this->db->from('claseprod');
	$this->db->where('IdClaseProd',$post_array['IdClaseProd']);
	$query = $this->db->get();
	$dato_clase = $query->row()->DescClaseProd;
	
	$this->db->select('DescVoMM');
	$this->db->where('IdVoMM',$post_array['IdVoMM']);
	$query1 = $this->db->get('vomm');
	$dato_vomm = $query1->row()->DescVoMM;
 	
	/*$this->db->select('DescMaquina');
	$this->db->where('IdMaquina',$post_array['IdMaquinaDefecto']);
	$query2 = $this->db->get('maquina');
	$dato_maq_def = $query2->row()->DescMaquina;*/
	
	
	//Verifico que valor enviado, porque en el caso de no seleccionar opcion me envia cero y no encuentra relacion con el id de la tabla  
	if($post_array['IdPasoProd'] == 0)
			    $dato_paso = '';
		else {
				$this->db->select('DescPasoProd');
				$this->db->where('IdPasoProd',$post_array['IdPasoProd']);
				$query3 = $this->db->get('pasoprod');
				$dato_paso = $query3->row()->DescPasoProd;
			 }	

	
	//Verifico que valor enviado, porque en el caso de no seleccionar opcion me envia cero y no encuentra relacion con el id de la tabla
	if($post_array['IdTipoProd'] == 0)
			    $dato_tipo = '';
		else {
				$this->db->select('DescTipoProd');
				$this->db->where('IdTipo',$post_array['IdTipoProd']);
				$query4 = $this->db->get('tipoprod');
				$dato_tipo = $query4->row()->DescTipoProd;
			}

	
	$this->db->select('EstadoProdElab');
	$this->db->where('IdProdElab',$primary_key);
	$query5 = $this->db->get('prodelaboracion');
	$dato_estado = $query5->row()->EstadoProdElab;
	
	$logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se edito un registo",
			   	"Tabla" => "Tabla: prodelaboracion",
			   	"Valores" => "Id.Prod: ".$primary_key.",  Cod.Prod: ".$post_array['CodProdElab'].",  Descripcion: ".$post_array['DescProdElab'].", Clase: ".$post_array['IdClaseProd'].", VoMM: ".$dato_vomm.",  PasoProd: ".$dato_paso.",  TipoProd: ".$dato_tipo.",  Est.Prod: ".$dato_estado
			 
				);	
	 
	$this->db->insert('Log_Produccion',$logs_insert);
	 
	return TRUE;
}



/*Funcion que elimina el prodcuto, cambia el estado del producto a DI*/
function cambia_estado_di($primary_key)
{
 $this->db->where('IdProdElab', $primary_key);
 $this->db->update('prodelaboracion',array('EstadoProdElab' => 'DI'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('CodProdElab');
 $this->db->where('IdProdElab',$primary_key);
 $query = $this->db->get('prodelaboracion');
 $dato_CodProdElab = $query->row()->CodProdElab;
 
 $this->db->select('DescProdElab');
 $this->db->where('IdProdElab',$primary_key);
 $query1 = $this->db->get('prodelaboracion');
 $dato_DescProdElab = $query1->row()->DescProdElab;
 
 $this->db->select('EstadoProdElab');
 $this->db->where('IdProdElab',$primary_key);
 $query2 = $this->db->get('prodelaboracion');
 $dato_EstadoProdElab = $query2->row()->EstadoProdElab;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Elimino un producto",
			   	"Tabla" => "Tabla: prodelaboracion",
			   	"Valores" => "Id.Prod: ".$primary_key.",  Cod.Prod: ".$dato_CodProdElab.",  Descripcion: ".$dato_DescProdElab.", Estado: ".$dato_EstadoProdElab
			 
				);
 
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}



/*Funcion que elimina el prodcuto, cambia el estado del producto a AC*/
function cambia_estado_ac($primary_key)
{
 $this->db->where('IdProdElab', $primary_key);
 $this->db->update('prodelaboracion',array('EstadoProdElab' => 'AC'));
 
 /*Inicia la recuperación de las variables para almacenar el log*/
 $this->db->select('CodProdElab');
 $this->db->where('IdProdElab',$primary_key);
 $query1 = $this->db->get('prodelaboracion');
 $dato_CodProdElab = $query1->row()->CodProdElab;
 
 $this->db->select('DescProdElab');
 $this->db->where('IdProdElab',$primary_key);
 $query2 = $this->db->get('prodelaboracion');
 $dato_DescProdElab = $query2->row()->DescProdElab;
 
 $this->db->select('EstadoProdElab');
 $this->db->where('IdProdElab',$primary_key);
 $query3 = $this->db->get('prodelaboracion');
 $dato_EstadoProdElab = $query3->row()->EstadoProdElab;
 
 $logs_insert = array(
			   	"UsuarioSO" => $this->session->userdata('Usuario'),
			   	"UsuarioSistema" => $this->session->userdata('Usuario'),
			   	"PC" => $this->session->userdata('ip_pc'), 	
			   	"Nivel" => $this->session->userdata('Nivel'),
			   	"Accion" => "Se Activo un producto",
			   	"Tabla" => "Tabla: prodelaboracion",
			   	"Valores" => "Id.Prod: ".$primary_key.",  Cod.Prod: ".$dato_CodProdElab.",  Descripcion: ".$dato_DescProdElab.", Estado: ".$dato_EstadoProdElab
			 
				);
 $this->db->insert('Log_Produccion',$logs_insert);
 
 return TRUE;	
}


/*Funcion que obtiene el Estado de un Producto*/
function obtiene_estado_prod($primary_key)
{
 $this->db->select('EstadoProdElab');
 $this->db->where('IdProdElab',$primary_key);
 $query5 = $this->db->get('prodelaboracion');
 $dato_estado = $query5->row()->EstadoProdElab;
 return $dato_estado;	
}



//----------------FUNCIONES PARA ARMAR EL SELECT PRODUCTO WS DE EDITAR OT------------------//
function obtiene_IdProd($primary_key)
{
 //Obtiene el id del producto
 $this->db->select('IdProdWS');
 $this->db->from('ot');
 $this->db->where('IdOT', $primary_key);
 $query = $this->db->get();	
 //Obtengo el valor del id en un arreglo para poder individualizar el valor y retornarlo como string
 $array = array();
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->IdProdWS);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 $valor = $array[0];
 /*var_dump($valor);
 echo "<br>";
 echo $valor;
 die;*/
 
 return $valor;
}


function obtiene_IdProductos($idprod)	
{
 //Funcion que obtiene los id de los productos que estan en estado AC junto con el id del producto que estoy editando, sea cual sea su estado  
 $this->db->select('IdProductoWS');
 $this->db->from(' productosws');
 $this->db->where('IdProductoWS', $idprod);
 $this->db->or_where('EstadoProdWS', 'AC');
 $this->db->or_where('EstadoProdWS', 'ac');
 $this->db->order_by("IdProductoWS","asc");
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 $array = array();
 $i=0;
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->IdProductoWS);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 //var_dump($array);
 
 /*var_dump($query->result());
 echo "<br>";
 echo "<br>";
 var_dump($query->row_array());
 echo "<br>";
 echo "<br>";
 var_dump($query->result_array());*/
// die;
return $array;
 /*if($query->num_rows()>0)
                  //return json_encode($query->result());
                  //return $query->result_array();
                  
                  return $query->row_array();
             else return FALSE;		*/
}


function obtiene_DescProductos($idprod)
{
 //Funcion que obtiene los Codigos y la Descripcion de los productos que estan en estado AC junto con el Código y la descripcion del producto que estoy editando, sea cual sea su estado
 $this->db->select('IdProductoWS, CodProdWS, DescProdWS');
 $this->db->from('productosws');
  $this->db->where('IdProductoWS', $idprod);
 $this->db->or_where('EstadoProdWS', 'AC');
 $this->db->or_where('EstadoProdWS', 'ac');
 $this->db->order_by("IdProductoWS","asc");
 //$this->db->where('IdProdElab', $IdProd);
 //$this->db->join('maquina','prodelaboracion.IdMaquinaDefecto = maquina.IdMaquina');
 /*$this->db->select('CodProdElab');
 $this->db->where('IdProdElab',1);
 $this->db->from('prodelaboracion');*/
 $query = $this->db->get();
 $array = array();
 $i=0;
 foreach($query->result() as $row)
 {
  //Funcion para insertar valores al final de un arreglo
  array_push($array, $row->CodProdWS." - ".$row->DescProdWS);	
 //Funciona con las dos alternativas
 /*{
  $array[$i] = $row->CodProdElab;	
  $i++;
 }*/
 }
 return $array;
 
 /*if($query->num_rows()>0)
                  return $query->result_array();
             else return FALSE;	*/	
}

//---------------------------------------------------------------------------------//






	
/******************************FIN DE LAS FUNCIONES***********************/	


}
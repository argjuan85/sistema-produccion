$(function () {
    $("#producto").hide();
    var next = 0;
    $('#add').click(function () {
        verifica_codigo($('#codigo').val(), next).done(function (data) {
            if (($('#stock').val() == 'entrada')||($('#stock').val() == 'ventas')) {
                if ((data) && ($('#cantidad').val() > 0) && ($('#costo').val() > 0))//validacion de campos
                {
                    var copia = $('#producto');
                    copia.clone().attr('id', 'producto' + next).appendTo("#form_productos").show();
                    var descripcion = data.toString();
                    $("#producto" + next).find("#pdescripcion").val(descripcion);
                    $("#producto" + next).find("#pcodigo").val($('#codigo').val());
                    $("#producto" + next).find("#pcantidad").val($('#cantidad').val());
                    $("#producto" + next).find("#pcosto").val($('#costo').val());

                } else {
                    alert("Verifique que todos los datos sean correctos");
                }
            }
            else {
                if ((data) && ($('#cantidad').val() > 0))//validacion de campos
                {
                    var copia = $('#producto');
                    copia.clone().attr('id', 'producto' + next).appendTo("#form_productos").show();
                    var descripcion = data.toString();
                    $("#producto" + next).find("#pdescripcion").val(descripcion);
                    $("#producto" + next).find("#pcodigo").val($('#codigo').val());
                    $("#producto" + next).find("#pcantidad").val($('#cantidad').val());
                    //$("#costo").val("123");

                } else {
                    alert("Verifique que todos los datos sean correctos");
                }
            }
        });
        next++;
    });
    $(document).on("click", ".remove-me", function (e) {
        var idproducto = $(this).parent().parent().attr("id");
        //alert(idproducto);
        $('#' + idproducto).remove();
        //next--;
    });

    $('#guardarlista').click(function () {
        if (next != 0) {
            //alert("hola");
            $("#producto").remove();
            $("#form_productos").submit();
        }
        else {
            alert("Ingrese productos a la lista");
        }

    });

});

function verifica_codigo(codigo, next) {
    var parametros = {
        'codigo': codigo
    };

    if ($('#stock').val()=="ventas") {
        return $.ajax({
            data: parametros,
            url: 'registrar_venta/verificar_productos',
            type: 'post',
            datatype: 'html'
        });
    } else {
        return $.ajax({
            data: parametros,
            url: 'entrada/verificar_productos',
            type: 'post',
            datatype: 'html'
        });
    }
}